import 'package:get/get.dart';
import 'package:myhoangtuan/view_model/share_friend_vm/share_friend_controller.dart';

class ShareFriendBinding implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut<ShareFriendController>(() => ShareFriendController());
  }

}