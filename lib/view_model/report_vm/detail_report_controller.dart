import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:video_player/video_player.dart';

import '../../model/feedback/feedback_detail.dart';
import '../../model/feedback/feedback_dto.dart';
import '../../utils/session/enum_session.dart';

class DetailReportController extends GetxController {
  ApiManager? apiManager;
  List<TextEditingController> textEdtControllers = <TextEditingController>[];
  Content? content = Get.arguments;
  FeedbackDetail? feedbackDetail;
  RxList<Videos> videos = RxList<Videos>();
  RxList<Images> images = RxList<Images>();
  RxList<Records> records = RxList<Records>();
  RxList<Files> files = RxList<Files>();
  RxBool isLoading = RxBool(false);
  RxBool isShowReply = RxBool(true);
  Rx<String> reply = RxString('');
  RxList<VideoPlayerController> videoPlayers = RxList<VideoPlayerController>();
  DetailReportController(this.apiManager);

  @override
  void onInit() {
    // TODO: implement onInit
    for (int i = 0; i <= 5; i++) {
      textEdtControllers.add(TextEditingController());
    }
    getDetailFeedback();
    super.onInit();
  }

  getDetailFeedback() async {
    try {
      isLoading.value = true;
      final request = await apiManager?.detailFeedback(content?.id);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading.value = false;
       feedbackDetail = FeedbackDetail.fromJson(result.data);
       textEdtControllers.first.text = feedbackDetail?.organizationBranchName ??'';
       textEdtControllers[1].text = feedbackDetail?.departmentName ??'';
       textEdtControllers[2].text = feedbackDetail?.performerName ??'';
       textEdtControllers[3].text = feedbackDetail?.peopleInvolved ??'';
       textEdtControllers[4].text = "${mapTypeFeedBack[feedbackDetail?.type]}";
       textEdtControllers[5].text = feedbackDetail?.content ??'';
        reply.value = feedbackDetail?.reply ??'';
       feedbackDetail?.files?.forEach((element) {
         files.add(element);
       });
       feedbackDetail?.videos?.forEach((element) {
          videos.add(element);
       });
       feedbackDetail?.images?.forEach((element) {
         images.add(element);
       });
       feedbackDetail?.records?.forEach((element) {
         records.add(element);
       });
       if(reply.value.isEmpty && files.isEmpty){
         isShowReply.value = false;
       }
      }
    } catch (ex) {
      isLoading.value = false;
      errNotification(context: Get.context!);
    }
  }
}
