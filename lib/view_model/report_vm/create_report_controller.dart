import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get/state_manager.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/booking/person_in_charge.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import '../../model/base_api.dart';
import '../../model/booking/booking_branch_dto.dart';
import '../../model/booking/doctor_booking_dto.dart';
import '../../model/booking/history_booking_dto.dart';
import '../../model/service/get_services_model.dart';

class CreateReportController extends GetxController {
  ApiManager? apiManager;

  CreateReportController(this.apiManager);

  RxnString? errChiNhanh = RxnString();
  RxnString? errPhongban = RxnString();
  RxnString? errLoaiPhanAnh = RxnString();
  List<TextEditingController> textEdtControllers = <TextEditingController>[];
  Rx<bool>? isLoading = false.obs;
  RxList listBranch = RxList<Data>();
  RxList listRoom = RxList<DepartmentDto>();
  RxList listDoctor = RxList<PersonInCharge>();
  RxList<PersonInCharge> listTypeReport = RxList();
  RxList<File> imageFiles = RxList<File>();
  GetServicesModel dataRequestServices = GetServicesModel();
  RxList<File> listFileImage = RxList();
  RxList<MultipartFile> listFileImagePush = RxList();
  RxList<File> listFileVideo = RxList();
  RxList<MultipartFile> listFileVideoPush = RxList();
  RxList<File> listFileAudio = RxList();
  RxList<MultipartFile> listFileAudioPush = RxList();
  RxList<File> listFileAll = RxList();
  Rx<bool?> isShowAction = false.obs;
  ContentHis? argument = Get.arguments;
  bool? isShowPban = true;

  @override
  void onInit() {
    isShowPban = argument?.packageType == 2 ? false: true;
    for (int i = 0; i <= 5; i++) {
      textEdtControllers.add(TextEditingController());
    }
    if(argument != null){
      textEdtControllers.first.text = argument?.organizationBranchName.toString() ??'';
      if(argument?.beautyBookingDetails?.first.departmentId != null){
        textEdtControllers[1].text = argument!.beautyBookingDetails!.first.departmentName.toString();
      }
      else{
        textEdtControllers[1].text ='';
      }
      argument?.beautyBookingDetails?.first.performer != null ? textEdtControllers[2].text = argument!.beautyBookingDetails!.first.performer!.fullName.toString():textEdtControllers[2].text = 'Không có người phụ trách';
      dataRequestServices.organizationBranchId = argument?.organizationBranchId;
      dataRequestServices.departmentId = argument?.beautyBookingDetails?.first.departmentId;
      dataRequestServices.performerId = argument?.beautyBookingDetails?.first.performerId;
      dataRequestServices.beautyBookingId = argument?.id;

    }
    dataRequestServices.rate = 5;
    // TODO: implement onInit
    listTypeReport.addAll([
      PersonInCharge(
          type: mapEnumTypeReport[TypeReport.thaido],
          fullName: mapEnumTypeReportString[TypeReport.thaido]),
      PersonInCharge(
          type: mapEnumTypeReport[TypeReport.chatluong],
          fullName: mapEnumTypeReportString[TypeReport.chatluong]),
      PersonInCharge(
          type: mapEnumTypeReport[TypeReport.giaca],
          fullName: mapEnumTypeReportString[TypeReport.giaca]),
      PersonInCharge(
          type: mapEnumTypeReport[TypeReport.chinhsach],
          fullName: mapEnumTypeReportString[TypeReport.chinhsach]),
      PersonInCharge(
          type: mapEnumTypeReport[TypeReport.khac],
          fullName: mapEnumTypeReportString[TypeReport.khac]),
    ]);
    getBranchApi();
    super.onInit();
  }

  // TODO lấy danh sách chi nhánh
  getBranchApi() async {
    listBranch.clear();
    try {
      final request = await apiManager?.getBranch();
      final result = BaseApiResponse.fromJson(request);
      BookingBranchDto bookingBranchDto =
          BookingBranchDto.fromJson(result.data);
      bookingBranchDto.data?.forEach((element) {
        listBranch.add(element);
      });
    } catch (err) {
      if (kDebugMode) {
        print(err);
      }
    }
  }

  getRoom({BuildContext? context}) async {
    isLoading?.value = true;
    listRoom.clear();
    try {
      GetServicesModel dataRequest = GetServicesModel(
          organizationBranchId: dataRequestServices.organizationBranchId);
      final request = await apiManager?.getRoom(data: dataRequest.toJson());
      final result = BaseApiResponse.fromJsonList(request);
      if (result.code == successful || result.code == create) {
        result.data.forEach((element) {
          DepartmentDto departmentDto = DepartmentDto.fromJson(element);
          listRoom.add(departmentDto);
        });
        isLoading?.value = false;
      } else {
        isLoading?.value = false;
        BaseApiResponse err = BaseApiResponse.fromJson(result.data);
        errNotification(context: context!, message: err.message);
      }
    } catch (err) {
      isLoading?.value = false;
      errNotification(context: context!);
    }
  }

  getPersonal({BuildContext? context}) async {
    isLoading?.value = true;
    listDoctor.clear();
    try {
      GetServicesModel dataRequest = GetServicesModel(
          organizationBranchId: dataRequestServices.organizationBranchId,
          departmentId: dataRequestServices.departmentId);
      final request =
          await apiManager?.getPersonInCharge(data: dataRequest.toJson());
      final result = BaseApiResponse.fromJsonList(request);
      if (result.code == successful || result.code == create) {
        result.data.forEach((element) {
          PersonInCharge data = PersonInCharge.fromJson(element);
          listDoctor.add(data);
        });
        isLoading?.value = false;
      } else {
        isLoading?.value = false;
        BaseApiResponse err = BaseApiResponse.fromJson(result.data);
        errNotification(context: context!, message: err.message);
      }
    } catch (err) {
      isLoading?.value = false;
      errNotification(context: context!);
    }
  }

  validateBooking() {
    isShowAction.value = false;
    if (textEdtControllers.first.text.isEmpty) {
      return errChiNhanh?.value = ' Vui lòng chọn chi nhánh';
    }
    errChiNhanh?.value = null;
    if(isShowPban!){
     if (textEdtControllers[1].text.isEmpty) {
       return errPhongban?.value = ' Vui lòng chọn phòng ban';
     }
   }
    errPhongban?.value = null;
    if (textEdtControllers[4].text.isEmpty) {
      return errLoaiPhanAnh?.value = ' Vui lòng chọn loại phản ánh';
    }
    errLoaiPhanAnh?.value = null;
    isShowAction.value = true;
  }

  createReport() async {
    try {
      listFileImagePush.clear();
      listFileVideoPush.clear();
      listFileAudioPush.clear();
      isLoading?.value = true;
      FormData formData =  FormData();
      for (File element in listFileImage) {
        formData.files.add(MapEntry("images",MultipartFile.fromFileSync(element.path,
            filename: element.path.split("/").last)));
      }
      for (File element in listFileVideo) {
        formData.files.add(MapEntry("videos",MultipartFile.fromFileSync(element.path,
            filename: element.path.split("/").last)));
      }
      for (File element in listFileAudio) {
        formData.files.add(MapEntry("records",MultipartFile.fromFileSync(element.path,
            filename: element.path.split("/").last)));
      }
      formData.fields.addAll([
        MapEntry("content", textEdtControllers.last.text.trim()),
        argument != null ? MapEntry("beautyBookingId", argument!.id.toString()) : const MapEntry("", ""),
        MapEntry("organizationBranchId", dataRequestServices.organizationBranchId.toString()),
        argument?.packageType ==2 ?const MapEntry('', '') : MapEntry("departmentId", dataRequestServices.departmentId.toString()),
        MapEntry("peopleInvolved", textEdtControllers[3].text.trim().toString()),
        MapEntry("type", dataRequestServices.type.toString()),
        MapEntry("rate", dataRequestServices.rate.toString()),
      ]);
      if(dataRequestServices.performerId != null){
        formData.fields.add( MapEntry("performerId",dataRequestServices.performerId.toString()));
      }
      final request = await apiManager?.createReport(data: formData);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading?.value = false;
        Get.back(result: true);
        if (kDebugMode) {
          print('day thanh cong');
        }
      } else {
        BaseErr baseErr = BaseErr.fromJson(result.data);
        if (kDebugMode) {
          print('errr ${baseErr.message}');
        }
        isLoading?.value = false;
        errNotification(context: Get.context!, message: baseErr.message ?? baseErr.data);
      }
    } catch (ex) {
      errNotification(context: Get.context!);
      isLoading?.value = false;
    }
  }
}
