import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/report_vm/detail_report_controller.dart';

class DetailReportBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut(() => DetailReportController(Get.find<ApiManager>()),fenix: true);
  }

}