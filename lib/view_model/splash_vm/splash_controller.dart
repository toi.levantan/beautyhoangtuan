import 'dart:async';
import 'dart:convert';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/model/service/res_services_dto.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import '../../main.dart';
import '../../model/base_api.dart';
import '../../model/fcm_body.dart';
import '../../model/login/user_data.dart';
import '../../utils/session/helper_notification.dart';

class SplashController extends GetxController {
  ApiManager? apiManager;

  SplashController(this.apiManager);
  late StreamSubscription streamSubscription;
  late ConnectivityResult result;
  Rx<bool> isDeviceConnected = false.obs;
  @override
  void onInit() {
    FirebaseMessaging.instance.getToken().then((value) => {
          PreferenceUtils.setString(tokenFcm, value!),
        });
    // app ở trạng thái đã thoát
    _initConfig();
    onInitFcm();
    initialization();
    checkHasInternet();
    startStreaming();
    super.onInit();
  }
  Future<void> checkHasInternet() async {
    result = await Connectivity().checkConnectivity();
    if (result != ConnectivityResult.none) {
      isDeviceConnected = true.obs;
    } else {
      isDeviceConnected = false.obs;
      Get.back();
      showDialogBox();
    }
  }
  showDialogBox() {
    showDialog(
        context: Get.context!,
        barrierDismissible: false,
        builder: (_) {
          return CupertinoAlertDialog(
            title: const Text('Không có internet'),
            content: const Text("Vui lòng kiểm tra kết nối internet"),
            actions: [
              CupertinoButton.filled(onPressed: () {
                Navigator.pop(Get.context!);
                checkHasInternet();
              },borderRadius: BorderRadius.zero,child: const Text('Thử lại'),)
            ],
          );
        });
  }
  startStreaming() {
    streamSubscription = Connectivity().onConnectivityChanged.listen((event) {
      checkHasInternet();
    });
  }
  Future<void> onInitFcm() async {
    const InitializationSettings initializationSettings =
        InitializationSettings(
      android: AndroidInitializationSettings('@mipmap/ic_launcher'),
      iOS: DarwinInitializationSettings(
          requestBadgePermission: true,
          requestAlertPermission: true,
          requestSoundPermission: true),
    );

    FirebaseMessaging.onMessage.listen((RemoteMessage remoteMessage) async {
      RemoteNotification? notification = remoteMessage.notification;
      FcmBody? fcmBody = FcmBody.fromJson(remoteMessage.data);
      Map<String, dynamic> json = await jsonDecode(fcmBody.body ?? '');
      Body body = Body.fromJson(json);
      FlutterAppBadger.updateBadgeCount(body.totalNotSeen ?? 0);
      countNotifyHome.value = body.totalNotSeen ?? 0;
      countNotifyHomeEmployee.value = body.totalNotSeen ??0;
      flutterLocalNotificationsPlugin.initialize(initializationSettings,
          onDidReceiveNotificationResponse: onDidReceiveNotificationResponse);
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );

      if (kDebugMode) {
        print('Trạng thái app mở');
      }
      flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification?.title ?? '',
          body.content,
          payload: body.screenUrl,
          NotificationDetails(
              iOS: DarwinNotificationDetails(badgeNumber: body.totalNotSeen),
              android: AndroidNotificationDetails(
                channel.id,
                number: body.totalNotSeen,
                channel.name,
                channelDescription: channel.description,
                playSound: true,
                channelShowBadge: true,
                icon: 'mipmap/ic_launcher',
              )));
    });
    FirebaseMessaging.onMessageOpenedApp
        .listen((RemoteMessage remoteMessage) async {
      RemoteNotification? notification = remoteMessage.notification;
      FcmBody? fcmBody = FcmBody.fromJson(remoteMessage.data);
      Map<String, dynamic> json = await jsonDecode(fcmBody.body ?? '');
      Body body = Body.fromJson(json);
      FlutterAppBadger.updateBadgeCount(body.totalNotSeen ?? 0);
      countNotifyHome.value = body.totalNotSeen ?? 0;
      if (body.screenUrl == Routers.historyBook) {
        Get.toNamed(body.screenUrl ?? Routers.notification, arguments: 1);
        return;
      }
      if (body.screenUrl == Routers.detail_report) {
        Content content = Content(id: int.parse(body.id ??'-1'));
        Get.toNamed(body.screenUrl ?? Routers.notification, arguments: content);
        return;
      }
      if (body.screenUrl == Routers.news_detail) {
        Content content = Content(id: int.parse(body.id ?? '-1'));
        Get.toNamed(body.screenUrl ?? Routers.notification, arguments: content);
        return;
      }
       else{
        Get.toNamed(body.screenUrl!);
      }
      flutterLocalNotificationsPlugin.initialize(
        initializationSettings,
        /*  onDidReceiveBackgroundNotificationResponse: notificationTapBackground,
      onDidReceiveNotificationResponse: onDidReceiveNotificationResponse*/
      );
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(
            alert: true,
            badge: true,
            sound: true,
          );
      if (kDebugMode) {
        print("trạng thái app ẩn");
      }
      flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification?.title ?? '',
          body.content,
          payload: body.screenUrl,
          NotificationDetails(
              iOS: DarwinNotificationDetails(badgeNumber: body.totalNotSeen),
              android: AndroidNotificationDetails(
                channel.id,
                number: body.totalNotSeen,
                channel.name,
                channelDescription: channel.description,
                playSound: true,
                channelShowBadge: true,
                icon: 'mipmap/ic_launcher',
              )));
    });
  }

  void _initConfig() async {
    final remoteConfig = FirebaseRemoteConfig.instance;
    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(minutes: 1),
      minimumFetchInterval: const Duration(minutes: 5),
    ));
    await remoteConfig.fetchAndActivate();
    PreferenceUtils.setString(
        urlShareAndroid, remoteConfig.getString(urlRemoteAndroid));
    PreferenceUtils.setString(
        urlShareIOS, remoteConfig.getString(urlRemoteIOS));
  }

  void initialization() async {
    checkExpired();
    FlutterNativeSplash.remove();
  }
// validate Expired Token và check type user
  void checkExpired() async {
    try {
      final request = await apiManager?.checkTypeUser();
      final result = BaseApiResponse.fromJson(request);
      if (result.code == expiredToken) {
        Get.offAllNamed(Routers.home);
        FlutterAppBadger.removeBadge();
        // getUnSubNotify();
        PreferenceUtils.remove(keyAccessToken);
        // check xem tài khoản đã đăng nhập chưa đăng nhâp rồi thì thêm thông tin
        checkHasLogin();
      } else {
          UserData userData = UserData.fromJson(result.data);
          if(userData.position == null && userData.position == ''){
            userData.positionShow = departmentType(userData.departmentType ??'');
            PreferenceUtils.setString(position, departmentType(userData.departmentType ??'')!);
          }
          else{
            userData.positionShow = positionType(userData.position ??'');
            PreferenceUtils.setString(position, positionType(userData.position ??'')!);
          }
          PreferenceUtils.setString(userKey, userData.fullName ?? '');
          PreferenceUtils.setString(phoneData, userData.phoneNumber ?? '');
          PreferenceUtils.setString(avatar,userData.imageStore!.isNotEmpty ? userData.imageStore?.first.fileId : "");
          PreferenceUtils.setString(address, userData.fullAddress ?? 'Chưa xác định');
          PreferenceUtils.setString(idAccount, userData.id.toString());
          PreferenceUtils.setString(email, userData.email ?? 'Chưa xác định');
          PreferenceUtils.setString(userAccount, userData.username ?? '');
          PreferenceUtils.setString(department, userData.departmentDTO?.departmentName.toString() ?? '');
          PreferenceUtils.setString(myUserName, userData.username.toString());
          PreferenceUtils.setString(birthday, "${timeStampToString(timeStamp: userData.birthday,dateTimeFormat: dOEMap[DateFormatCustom.DD_MM_YYYY])}");
          var encodedString = jsonEncode(userData);
          PreferenceUtils.setString(userDataKey, encodedString);
          if(userData.userType == CREATED){
            //loginCheckExpired();
            await getSubNotify();
            await getUserCreate();
            Get.offAllNamed(Routers.home);
        }
          if(userData.userType == STAFF){
            await getSubNotifyEmployee();
            //loginCheckExpired();
            Get.offAllNamed(Routers.home_staff);
        }
      }
    } catch (ex) {
      Get.offAllNamed(Routers.home);
    }
  }
  getUserCreate() async{
    final request = await apiManager?.createUser();
    final response = BaseApiResponse.fromJson(request);
    if(response.code == successful){
      print('tao tai khoan chat thanh cong');
    }
  }
  getSubNotify() async {
    // check keyAccess Token
    try {
      final object = {
        "tokens": ["${PreferenceUtils.getString(tokenFcm)}"]
      };
      final request = await apiManager?.subscribeNotify(data: object);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        if (kDebugMode) {
          print('dang ky fcm thanh cong');
        }
      } else {}
    } catch (ex) {
      if (kDebugMode) {
        print(ex);
      }
    }
  }
  getSubNotifyEmployee() async {
    // check keyAccesstoken
    if (PreferenceUtils.getString(keyAccessToken) != null) {
      final object = {
        "tokens": ["${PreferenceUtils.getString(tokenFcm)}"]
      };
      final request = await apiManager?.subscribeNotifyEmployee(data: object);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        if(kDebugMode){
          print('Đã ký FCM nhaân viên');
        }
      } else {

      }
    }
  }
  getUnSubNotify() async {
    // check keyAccesstoken
    final object = {
      "tokens": ["${PreferenceUtils.getString(tokenFcm)}"]
    };
    final request = await apiManager?.unsubscribeNotifySplash(data: object);
    final result = BaseApiResponse.fromJson(request);
    if (result.code == successful || result.code == create) {}
  }
}
