import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../model/booking/performer.dart';

class ContactController extends GetxController {
  ApiManager? apiManager;

  ContactController(this.apiManager);

  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  Rx<bool?> isLoading = false.obs;
  TextEditingController textEditingController = TextEditingController();
  RxList<Performer> contacts = RxList<Performer>();
  RxList<Performer> contactsRoot = RxList<Performer>();

  @override
  void onInit() {
    // TODO: implement onInit
    getListContact();
    super.onInit();
  }

  getListContact() async {
    final request = await apiManager?.getPhoneBook();
    final response = BaseApiResponse.fromJsonList(request);
    response.data.forEach((element) {
      Performer performer = Performer.fromJson(element);
      contactsRoot.add(performer);
    });
    filterContact();
  }

  filterContact() {
    if (textEditingController.text.isEmpty) {
      contacts.clear();
      contacts.addAll(contactsRoot);
    } else {
      contacts.clear();
      contacts.addAll(contactsRoot.where((p0) => p0.fullName!.toLowerCase().contains(textEditingController.text.trim().toLowerCase()) ||
          p0.phoneNumber!.toLowerCase().contains(textEditingController.text.trim().toLowerCase())).toList());
    }
  }
}
