import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/routes/routers.dart';
import '../../utils/app/application.dart';
import '../../utils/app/share_pref.dart';
import '../../utils/path/const_key.dart';

class ChangePassController extends GetxController {
  ApiManager? apiManager;

  var isLoading = false.obs;
  List<TextEditingController> listEdtController = <TextEditingController>[
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
  ];

  RxnString validateOldPass = RxnString();
  Rx<bool> hidePass = true.obs;
  Rx<bool> hideOldPass = true.obs;
  Rx<bool> hidePassConfirm = true.obs;
  Rx<String?>? validatePassword = RxnString();
  Rx<String?>? validateConfirmPassword = RxnString();
  Rx<bool?>? isOk = false.obs;

  ChangePassController(this.apiManager);

  validateCheckOldPassword() {
    validateOldPass.value =
        Application.checkEmptyErr(param: listEdtController.first.text);
  }

  validateCheckPassword() {
    validatePassword?.value =
        Application.checkValidatePassword(pwd: listEdtController[1].text);
  }

  checkConfirmPass() {
    if (listEdtController[2].text != listEdtController[1].text) {
      return validateConfirmPassword?.value = 'Mật khẩu chưa khớp';
    }
    return validateConfirmPassword?.value = null;
  }

  validateButton() {
    if (listEdtController.first.text.isNotEmpty &&
        listEdtController[1].text.isNotEmpty &&
        listEdtController[2].text.isNotEmpty) {
      if (validateOldPass.value == null &&
          validatePassword?.value == null &&
          validateConfirmPassword?.value == null) {
        return isOk?.value = true;
      }
      return isOk?.value = false;
    }
    return isOk?.value = false;
  }

  changePassword() async {
    try {
      isLoading.value = true;
      // đã map to json rồi
      final body = {
        "oldPassword": listEdtController.first.text.trim(),
        "newPassword": listEdtController[1].text.trim(),
      };
      final request = await apiManager?.changePass(body);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading.value = false;
        successNotification(
            context: Get.context!, message: 'Mật khẩu thay đổi thành công');
        await Future.delayed(const Duration(seconds: 1));
        await getUnSubNotify();
        FlutterAppBadger.removeBadge();
        await PreferenceUtils.remove(keyAccessToken);
        Get.offAllNamed(Routers.intro_page);
      } else {
        isLoading.value = false;
        BaseErr baseErrLogin = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!, message: baseErrLogin.message);
      }
    } catch (ex) {
      isLoading.value = false;
      errNotification(context: Get.context!);
    }
  }

  getUnSubNotify() async {
    // check keyAccesstoken

    final object = {
      "tokens": ["${PreferenceUtils.getString(tokenFcm)}"]
    };
    final request = await apiManager?.unsubscribeNotify(data: object);
    final result = BaseApiResponse.fromJson(request);
    if (result.code == successful || result.code == create) {
      if (kDebugMode) {
        print('Đã huy ký FCM');
      }
    }
  }
}
