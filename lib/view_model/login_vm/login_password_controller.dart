import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../core/routes/routers.dart';
import '../../model/base_api.dart';
import '../../model/login/access_dto.dart';
import '../../model/login/user_data.dart';
import '../../utils/app/application.dart';
import '../../utils/app/share_pref.dart';
import '../../utils/app/size_app.dart';
import '../../utils/path/const_key.dart';
import '../../utils/session/enum_session.dart';
import '../../utils/widget/snack_bar.dart';

class LoginPwdController extends GetxController {
  ApiManager? apiManager;
  Rx<bool> hidePass = true.obs;
  Rx<String?>? errNumberPhone = RxnString();
  Rx<String?>? errPass = RxnString();
  Rx<bool> rememberAccount = true.obs;
  Rx<bool> isOk = false.obs;
  Rx<bool> isLoading = false.obs;
  int? statusLoginLocation;
  List<TextEditingController> textEdits = [
    TextEditingController(),
    TextEditingController(),
  ];

  LoginPwdController(this.apiManager);

  @override
  void onInit() {
    // TODO: implement onInit
    textEdits.first.text = PreferenceUtils.getString(phoneAccess) ?? '';
    if (PreferenceUtils.getBool(rememberKey) != null &&
        PreferenceUtils.getBool(rememberKey) == true) {
      textEdits.last.text = PreferenceUtils.getString(pwdKey) ?? '';
    }
    rememberAccount.value = PreferenceUtils.getBool(rememberKey) ?? false;
    checkShowAction();
    super.onInit();
  }

  changeShowPass() {
    hidePass.value = !hidePass.value;
  }

  validatePhoneNumber({String? phoneNumber}) {
    errNumberPhone?.value = Application.checkPhoneNumberText(phoneNumber);
    if (errNumberPhone?.value == null) {
      return isOk.value = true;
    }
    return isOk.value = true;
  }

  validatePass({String? password}) {
    errPass?.value = Application.checkEmptyErr(param: password);
    if (errPass?.value != null) {
      return isOk.value = true;
    }
    return isOk.value = false;
  }
 /// TODO hiển thi đăng nhập
  checkShowAction() {
    if (errPass?.value == null &&
        errNumberPhone?.value == null &&
        textEdits.last.text.isNotEmpty &&
        textEdits.first.text.isNotEmpty) {
      return isOk.value = true;
    }
    return isOk.value = false;
  }

  checkExistByPhone() async {
    try {
      isLoading.value = true;
      final query = {"phoneNumber": textEdits.first.text.trim()};
      final request = await apiManager?.checkExistByPhone(
        queryParam: query,
      );
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading.value = false;
        if (result.data == CREATED) {
          login();
          statusLoginLocation = CREATED;
        }
        if (result.data == GUEST) {
          statusLoginLocation = GUEST;
          warningNotification(context: Get.context!, message: "Không tìm thấy tài khoản của bạn. Vui lòng đăng ký tài khoản");
        }
        if (result.data == STAFF) {
          // login();
          statusLoginLocation = STAFF;
          warningNotification(context: Get.context!, message: "Số điện thoại này đã đăng ký nhân sự. Vui lòng đăng nhập bằng app nhân viên");
        }
      } else {
        isLoading.value = false;
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!, message: baseErr.message, description: baseErr.error);
      }
    } catch (ex) {
      isLoading.value = false;
      errNotification(context: Get.context!);
    }
  }

  login() async {
    // đã map to json rồi
    final object = {
      "grant_type": "password",
      "scope": "read",
      "username": textEdits.first.text.trim(),
      "password": textEdits.last.text.trim(),
    };
    try {
      final request = await apiManager?.login(object);
      final result = BaseApiResponse.fromJson(request);
      final accessToken = AccessDto.fromJson(result.data);
      if (result.code == successful || result.code == create) {
        await PreferenceUtils.setString(keyAccessToken, accessToken.accessToken.toString());
        await apiManager?.updateToken();
        isLoading.value = false;
        await PreferenceUtils.setString(phoneAccess, textEdits.first.text.trim().toString());
        await PreferenceUtils.setString(customerCode, accessToken.employeeCode);
        /// TODO lưu tài khoản
        if(rememberAccount.value){
          PreferenceUtils.setString(phoneAccess, textEdits.first.text.trim());
          PreferenceUtils.setString(pwdKey, textEdits.last.text.trim());
          PreferenceUtils.setBool(rememberKey, rememberAccount.value);
        }
        else{
          // không lưu thì xóa đi
          PreferenceUtils.remove(pwdKey);
          PreferenceUtils.setBool(rememberKey, rememberAccount.value);
        }
        await checkExpired();
      } else {
        isLoading.value = false;
        if (result.code == expiredToken) {
          errNotification(
              context: Get.context!, message: 'Tài khoản đăng nhập hoặc mật khẩu không chính xác!');
        }
        else {
          BaseErrLogin baseErr = BaseErrLogin.fromJson(result.data);
          errNotification(
              context: Get.context!, message: baseErr.error_description);
        }
      }
    } catch (ex) {
      errNotification(context: Get.context!);
      isLoading.value = false;
    }
  }

  Future<void> checkExpired() async {
    try {
      final request = await apiManager?.checkTypeUser();
      final result = BaseApiResponse.fromJson(request);
      if (result.code != null && result.code == expiredToken) {
        errNotificationNoAppBar(context: Get.context!,message: 'Tài khoản đăng nhập hoặc mật khẩu không chính xác!');
      }
      else {
        UserData userData = UserData.fromJson(result.data);
        if(userData.position == null && userData.position == ''){
          userData.positionShow = departmentType(userData.departmentType ??'');
          PreferenceUtils.setString(position, departmentType(userData.departmentType ??'')!);
        }
        else{
          userData.positionShow = positionType(userData.position ??'');
          PreferenceUtils.setString(position, positionType(userData.position ??'')!);
        }
        PreferenceUtils.setString(userKey, userData.fullName ?? '');
        PreferenceUtils.setString(phoneData, userData.phoneNumber ?? '');
        PreferenceUtils.setString(avatar, userData.imageStore?.first.fileId ?? '');
        PreferenceUtils.setString(address, userData.fullAddress ?? 'Chưa xác định');
        PreferenceUtils.setString(idAccount, userData.id.toString());
        PreferenceUtils.setString(myUserName, userData.username);
        var encodedString = jsonEncode(userData);
        PreferenceUtils.setString(userDataKey, encodedString);
        if(userData.userType == CREATED){
          Get.offAllNamed(Routers.home);
        }
        getSubNotify();
        getUserCreate();
      }
    } catch (ex) {
      errNotificationNoAppBar(context: Get.context!);
    }
  }
  getUserCreate() async{
    final request = await apiManager?.createUser();
     final response = BaseApiResponse.fromJson(request);
     if(response.code == successful){
       print('tao tai khoan chat thanh cong');
     }
  }
  getSubNotify() async {
    // check keyAccesstoken
    if (PreferenceUtils.getString(keyAccessToken) != null) {
      final object = {
        "tokens": ["${PreferenceUtils.getString(tokenFcm)}"]
      };
      final request = await apiManager?.subscribeNotify(data: object);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        if (kDebugMode) {
          print('Đã ký FCM');
        }
      } else {
        if (kDebugMode) {
          print('ký lỗi');
        }
      }
    }
  }
}
