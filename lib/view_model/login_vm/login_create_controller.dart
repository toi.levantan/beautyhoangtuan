import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import '../../main.dart';
import '../../model/login/access_dto.dart';
import '../../model/login/user_data.dart';
import '../../utils/app/application.dart';
import '../../utils/app/share_pref.dart';
import '../../utils/app/size_app.dart';
import '../../utils/path/const_key.dart';

class LoginCreateController extends GetxController {
  final ApiManager? apiManager;

  LoginCreateController(this.apiManager);

  TextEditingController edtNumberPhone = TextEditingController();
  TextEditingController otpController = TextEditingController();
  RxInt second = 0.obs;
  RxnString? errTextPhone = RxnString();
  Rx<String?>? errTextOTP = RxnString();
  Rx<bool?> isOk = false.obs;
  Rx<bool?> isOkOtp = false.obs;
  RxString resendText = 'Gửi lại OTP sau'.obs;
  Rx<bool> isSend = true.obs;
  bool isSentOtp = true;
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  var verificationIDReceivedValue = ''.obs;
  late String verificationIDValue;
  int resendTokenTime = 0;
  var messageOtpCode = '000000'.obs;
  RxInt timerCount = 0.obs;
  Rx<bool?> isLoading = false.obs;
  Rx<bool?> isLoadingOTP = false.obs;
  int? valueCheckCreate;
  String? oldPhone = '';
  bool isSuccessOtp = false;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  String token = '';
  var argument = Get.arguments[0] ?? '';
  var phoneFromForgot = Get.arguments[1] ?? "";

  // late TwilioPhoneVerify twilioPhoneVerify;

  @override
  void onInit() {
    // TODO: implement onInit
    if(phoneFromForgot.toString().isNotEmpty){
      edtNumberPhone.text = phoneFromForgot.toString().trim();
      validatePhoneNumber(phoneNumber: edtNumberPhone.text);
    }
    subscribleTokenFcm();
    getIdBranch();
    super.onInit();
  }

  getIdBranch() async {
    try {
      final request = await apiManager?.getLoyaltyOrganizationId();
      final result = BaseApiResponse.fromJson(request);
      await PreferenceUtils.setString(organizationId, result.data.toString());
      if (kDebugMode) {
        print('organizationIdPhone : ${PreferenceUtils.getString(organizationId)}');
      }
    } catch (ex) {
      if (kDebugMode) {
        print('có lỗi xảy ra');
      }
    }
  }
   ///TODO login OTP
    login() async {
    int timeStamp = DateTime.now().millisecondsSinceEpoch;
    // đã map to json rồi
    final object = {
       "grant_type": "password",
       "scope": "read",
       "username": edtNumberPhone.text.trim(),
       "accessKey": accessKey,
       "timeStamp" : timeStamp,
       "checksum" : sha1.convert(utf8.encode("$timeStamp$secretKey")).toString(),
    };
     try {
      final request = await apiManager?.login(object);
      final result = BaseApiResponse.fromJson(request);
      final accessToken = AccessDto.fromJson(result.data);
      if(result.code == expiredToken){
       return errNotification(context: Get.context!,message: 'Tài khoản đăng nhập hoặc mật khẩu không chính xác!');
      }
      if(result.code == successful || result.code == create){
        await PreferenceUtils.setString(keyAccessToken, accessToken.accessToken.toString());
        await apiManager?.updateToken();
        isLoading.value = false;
        await PreferenceUtils.setString(phoneAccess, edtNumberPhone.text.trim().toString());
        await PreferenceUtils.setString(customerCode, accessToken.employeeCode);
        await checkExpired();
        // Get.offAllNamed(Routers.home);
      }
      else {
        isLoading.value = false;
          BaseErrLogin baseErr = BaseErrLogin.fromJson(result.data);
          errNotification(context: Get.context!, message: baseErr.error_description);
      }
    } catch (ex) {
      errNotification(context: Get.context!);
      isLoading.value = false;
    }
  }
  checkPhoneExistLogin()async{
    try {
      isLoading.value = true;
      final query = {"phoneNumber": edtNumberPhone.text.trim()};
      final request = await apiManager?.checkExistByPhone(
        queryParam: query,
      );
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        valueCheckCreate = result.data;
        isLoading.value = false;
        //TODO check sdt là khách hàng
        if(valueCheckCreate == STAFF){
          ///TODO thay đổi nội dung thông báo
         return warningNotification(context: Get.context!, message: "Số điện thoại này đã đăng ký nhân sự. Vui lòng đăng nhập bằng app nhân viên");
        }
      } else {
        isLoading.value = false;
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!, message: baseErr.message, description: baseErr.error);
      }
    } catch (ex) {
      errNotification(context: Get.context!);
      isLoading.value = false;
    }
  }
  // keim tra sdt trong otp
  checkExistByPhone() async {
    try {
      isLoading.value= true;
      isLoadingOTP.value = true;
      final query = {"phoneNumber": edtNumberPhone.text.trim()};
      final request = await apiManager?.checkExistByPhone(
        queryParam: query,
      );
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoadingOTP.value = false;
        isLoading.value = false;

        if (result.data == CREATED )  {
          if(argument == CheckUser.create){
              return warningNotification(context: Get.context!,message: "Số điện thoại đã được đăng ký. Vui lòng đăng nhập");
          }
          if(phoneFromForgot.toString().isNotEmpty && argument == CheckUser.forgotPwd){
              Get.toNamed(Routers.forgot_pwd);
          }
          else{
            login();
          }
        }
        if(result.data == STAFF){
          return warningNotification(context: Get.context!,
              message: "Số điện thoại này đã đăng ký nhân sự. Vui lòng đăng nhập bằng app nhân viên");
        }
        if(result.data == GUEST ){
          Get.toNamed(Routers.updateInfo,arguments: edtNumberPhone.text.trim());
        }
      } else {
        isLoadingOTP.value = false;
        isLoading.value = false;
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!, message: baseErr.message, description: baseErr.error);
      }
    } catch (ex) {
      isLoadingOTP.value = false;
      errNotification(context: Get.context!);
    }
  }
  validatePhoneNumber({String? phoneNumber}) {
    errTextPhone?.value = Application.checkPhoneNumberText(phoneNumber);
    if (errTextPhone?.value == null) {
      return isOk.value = true;
    }
    return isOk.value = false;
  }

  validateOTPNumber({String? otp}) {
    errTextOTP?.value = Application.checkEmptyErr(param: otp);
    if (errTextOTP?.value == null) {
      return isOkOtp.value = true;
    }
    return isOkOtp.value = false;
  }
  // TODO kiểm tra hạn token
  Future<void> checkExpired() async {
    try {
      final request = await apiManager?.getExpired();
      final result = BaseApiResponse.fromJson(request);
      if (result.code != null && result.code == expiredToken) {
        // getUnSubcrisedNotify();
        // Get.offAllNamed(Routers.login);
        // PreferenceUtils.clearAllSharePre();
      }
      else {
        getSubcrisedNotify();
        getUserCreate();
        UserData user = UserData.fromJson(result.data);
        PreferenceUtils.setString(userKey, user.fullName ?? '');
        PreferenceUtils.setString(phoneData, user.phoneNumber ?? '');
        PreferenceUtils.setString(avatar, user.imageStore?.first.fileId ?? '');
        PreferenceUtils.setString(address, user.fullAddress??'Chưa xác định');
        PreferenceUtils.setString(idAccount, user.id.toString());
        PreferenceUtils.setString(myUserName, user.username.toString());
        Get.offAllNamed(Routers.home);
      }
    }
    catch (ex) {
      Get.offAllNamed(Routers.login);
    }
  }
  getUserCreate() async{
    final request = await apiManager?.createUser();
    final response = BaseApiResponse.fromJson(request);
    if(response.code == successful){
      print('tao tai khoan chat thanh cong');
    }
  }
  subscribleTokenFcm(){
    _firebaseMessaging.getToken().then((value) => {
      // call api send token
      token = value!,
       PreferenceUtils.setString(tokenFcm, token),
    });
    // show notifiy
    //FirebaseMessaging.onMessage.listen(showFlutterNotification);

  }
  void showFlutterNotification(RemoteMessage message) {
    RemoteNotification? notification = message.notification;
    AndroidNotification? android = message.notification?.android;
    if (notification != null && android != null) {
      flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channelDescription: channel.description,
            // TODO add a proper drawable resource to android, for now using
            //      one that already exists in example app.
            icon: 'launch_background',
          ),
        ),
      );
    }
  }
  getSubcrisedNotify() async{
    // check keyAccesstoken
    if(PreferenceUtils.getString(keyAccessToken) != null){
      final object = {
        "tokens": [
          "${PreferenceUtils.getString(tokenFcm)}"
        ]
      };
      final request = await apiManager?.subscribeNotify(data: object);
      final result = BaseApiResponse.fromJson(request);
      if(result.code == successful || result.code == create){
        if (kDebugMode) {
          print('Đã ký FCM');
        }
      }
      else{
        if (kDebugMode) {
          print('ký lỗi');
        }
      }
    }
  }
  getUnSubcribedNotify() async{
    // check keyAccesstoken
    if(PreferenceUtils.getString(keyAccessToken) != null){
      final object = {
        "tokens": [
          "${PreferenceUtils.getString(tokenFcm)}"
        ]
      };
      final request = await apiManager?.unsubscribeNotify(data: object);
      final result = BaseApiResponse.fromJson(request);
      if(result.code == successful || result.code == create){
        if (kDebugMode) {
          print('Đã huy ký FCM');
        }
      }
    }
  }
  Future<void> phoneAuth(BuildContext context) async {
    isLoading.value = true;
    await firebaseAuth.verifyPhoneNumber(
        phoneNumber: "+84${edtNumberPhone.text.trim().substring(1)}",
        verificationCompleted: (credential) {
          // onlyAndroid
        },
        verificationFailed: (err) {
          isLoading.value = false;
          //TODO chajy dduowjc mo command và xóa get to ơ day di
          if(err.code == "too-many-requests"){
            errNotification(context: context, message: 'Chúng tôi đã chặn tất cả các yêu cầu từ thiết bị này do hoạt động bất thường. Thử lại sau ');
          }
          else{
            errNotification(context: context, message: "Gửi OTP không thành công\n${err.code.toString().capitalizeFirst}");
          }
          debugPrint(err.code);
          isSuccessOtp = false;
          // timeFunc();
          // PreferenceUtils.clearAllSharePre();
          // apiManager?.updateToken();
          if (kDebugMode) {
            print('$err');
          }
        },
        codeSent: (verificationId, resendToken) async {
          verificationIDValue = verificationId;
          isLoading.value = false;
          timeFunc();
          isSuccessOtp = true;
          Get.toNamed(Routers.otp);
        },
        timeout: const Duration(seconds: 90),
        codeAutoRetrievalTimeout: (verificationId) {
          verificationIDValue = verificationId;
        });
  }
  ///send SMS otp from twilio
  // Future<void> sendCodeFromTwilio({String? numberPhone}) async{
  //   isLoading.value = true;
  //   isLoadingOTP.value = true;
  //   TwilioResponse twilioResponse = await twilioPhoneVerify.sendSmsCode(numberPhone.toString());
  //   if (twilioResponse.successful! )  {
  //     //code sent
  //     // verificationIDValue =  verificationId;
  //     isLoading.value = false;
  //     isLoadingOTP.value = false;
  //     successNotification(context: Get.context!, message: 'Đã gửi OTP');
  //     isSuccessOtp = true;
  //     otpController.clear();
  //     timeFunc();
  //   } else {
  //     print(twilioResponse.errorMessage);
  //   }
  // }
  /// send sms otp from firebase
  Future<void> phoneAuthResend(BuildContext context) async {
    isLoading.value = true;
    isLoadingOTP.value = true;
    await firebaseAuth.verifyPhoneNumber(
        phoneNumber: "+84${edtNumberPhone.text.trim().substring(1)}",
        verificationCompleted: (credential) {
          // onlyAndroid
        },
        verificationFailed: (err) {
          isLoading.value = false;
          isLoadingOTP.value = false;
          if(err.code == "too-many-requests"){
            errNotification(context: context, message: 'Chúng tôi đã chặn tất cả các yêu cầu từ thiết bị này do hoạt động bất thường. Thử lại sau ');
          }
          else{
            errNotification(context: context, message: "Gửi OTP không thành công\n${err.code.toString().capitalizeFirst}");
          }
          isSuccessOtp = false;
        },
        codeSent: (verificationId, resendToken) async {
          verificationIDValue =  verificationId;
          isLoading.value = false;
          isLoadingOTP.value = false;
          successNotification(context: context, message: 'Đã gửi OTP');
          isSuccessOtp = true;
          otpController.clear();
          timeFunc();
        },
        codeAutoRetrievalTimeout: (verificationId) {
          //verificationIDValue = verificationId;
        });
  }

  void timeFunc() {
    timerCount.value = 90;
    Timer.periodic(const Duration(seconds: 1), (timer) {
      timerCount.value--;
      if (timerCount.value <= 0) {
        timerCount.value = 0;
        if(isSuccessOtp == false){
          timer.cancel();
        }
        timer.cancel();
      }
    });
  }
  // Future<bool> verifyOtpTwilio()async {
  //
  //     var twilioResponse = await twilioPhoneVerify.verifySmsCode(
  //         phone: "+84${edtNumberPhone.text.trim().substring(1)}", code: 'code');
  //
  //     if (twilioResponse.successful!) {
  //       if (twilioResponse.verification!.status == VerificationStatus.approved) {
  //         //print('Phone number is approved');
  //         isLoadingOTP.value = false;
  //         isSuccessOtp = true;
  //         //timeFunc();
  //         return true;
  //       } else {
  //         return false;
  //       }
  //     } else {
  //       errNotification(
  //           context: Get.context!, message: twilioResponse.errorMessage);
  //       return false;
  //     }
  //   }
  Future<bool> verifyOtp() async {
    try {
      isLoadingOTP.value = true;
      var credential = await firebaseAuth.signInWithCredential(PhoneAuthProvider.credential(verificationId: verificationIDValue,
          smsCode: otpController.text.trim()));
      if (credential.user != null) {
        isLoadingOTP.value = false;
        isSuccessOtp = true;
        //timeFunc();
        return true;
      }
      else{
        errNotification(context: Get.context!, message: 'Mã OTP chưa đúng');
        isSuccessOtp = false;
        isLoadingOTP.value = false;
        return false;
      }
    } catch (err) {
      errNotification(context: Get.context!, message: 'Lỗi xác thực OTP');
      otpController.clear();
      isLoadingOTP.value = false;
      isSuccessOtp = false;
      return false;
    }
  }

}
