import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/login/checkinInfo_dto.dart';
import 'package:myhoangtuan/view_model/home_staff_vm/home_staff_controller.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';

class DetailCheckinController extends GetxController with GetTickerProviderStateMixin{
  ApiManager? apiManager;
  DetailCheckinController(this.apiManager);
 RefreshController refreshController = RefreshController(initialRefresh: false);
 DateTime? startTime;
 DateTime? endTime = DateTime.now();
 RxList<CheckinInfoDto> checkins = RxList<CheckinInfoDto>();
 RxList<CheckinInfoDto> checkinsRevertData = RxList<CheckinInfoDto>();
 RxBool isLoading = false.obs;
 RxBool isFilterAll = true.obs;
  RxList<File?> imageFile = <File>[].obs;
  HomeStaffController homeStaffController = Get.find<HomeStaffController>();
 late TabController tabController;
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'checkin'.toUpperCase()),
    Tab(text: 'nhận diện'.toUpperCase()),
  ];

  @override
  void onInit() {
    // TODO: implement onInit
    tabController = TabController(length: 2, vsync: this);
    startTime = DateTime.now().subtract(const Duration(days: 31));
    isLoading.value = true;
    getListInfoCheckin();
    super.onInit();
  }
  onRefresh(){
      isLoading.value = true;
      refreshController.refreshCompleted();
     checkinsRevertData.clear();
     if(isFilterAll.value){
       endTime = DateTime.now();
       startTime = DateTime.now().subtract(const Duration(days: 31));
     }
     getListInfoCheckin();
  }
  onLoad(){
    if(isFilterAll.value){
      endTime = startTime;
      startTime = endTime?.subtract(const Duration(days: 31));
      getListInfoCheckin();
    }
    else{
      refreshController.loadComplete();
    }
  }
  getListInfoCheckin()async{
    checkins.clear();
    final body = {
      "start": startTime?.toIso8601String(),
      "end"  : endTime?.toIso8601String()
    };
    final request = await apiManager?.checkinInfo(body);
    final response = BaseApiResponse.fromJsonList(request);
     response.data.forEach((element){
      CheckinInfoDto checkinInfoDto = CheckinInfoDto.fromJson(element);
      checkins.add(checkinInfoDto);
      });
     isLoading.value = false;
     refreshController.loadComplete();
     checkinsRevertData.addAll(checkins.reversed);
  }
}