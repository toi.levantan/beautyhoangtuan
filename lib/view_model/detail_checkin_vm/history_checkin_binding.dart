import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/detail_checkin_vm/history_checkin_controller.dart';

class HistoryCheckinBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<HistoryCheckinController>(() => HistoryCheckinController(Get.find<ApiManager>()),fenix: true);
  }
}