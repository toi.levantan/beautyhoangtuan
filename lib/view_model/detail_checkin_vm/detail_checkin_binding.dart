import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/detail_checkin_vm/detail_checkin_controller.dart';

class DetailCheckinBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<DetailCheckinController>(() => DetailCheckinController(Get.find<ApiManager>()),fenix: true);
  }
}