import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/login/History_checkin.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../model/login/checkinInfo_dto.dart';

class HistoryCheckinController extends GetxController{
  ApiManager? apiManager;
  RxBool isLoading = false.obs;
  RefreshController refreshController = RefreshController(initialRefresh: false);
  RxList<HistoryCheckin> hisCheckin = RxList<HistoryCheckin>();
  bool isLoad = false;
  CheckinInfoDto checkinInfoDto = Get.arguments as CheckinInfoDto;
  HistoryCheckinController(this.apiManager);
  String? startTime;
  String? endTime;

  @override
  void onInit() {
     startTime = strToDate(valueTimeString: checkinInfoDto.date.toString()).toIso8601String();
     endTime = strToDate(valueTimeString: checkinInfoDto.date.toString()).add(const Duration(hours: 23,minutes: 59)).toIso8601String();
    // TODO: implement onInit
    getHistoryCheckin();
    super.onInit();
  }
  onRefresh(){
    refreshController.refreshCompleted();
    isLoad = false;
  }
  onLoad(){
    isLoad = true;
  }
  getHistoryCheckin() async{
   final body = {
     "start" : startTime,
     "end"   : endTime,
   };
   final request = await apiManager?.historyCheckin(body);
   final response = BaseApiResponse.fromJsonList(request);
   response.data.forEach((element){
      HistoryCheckin historyCheckin = HistoryCheckin.fromJson(element);
      hisCheckin.add(historyCheckin);
   });
  }
}