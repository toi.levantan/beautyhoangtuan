import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/notify/Notification_dto.dart';
import 'package:myhoangtuan/model/notify/notify_emp.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../utils/path/const_key.dart';
import '../../utils/widget/snack_bar.dart';

class NotificationController extends GetxController {
  RxList<Data> notificationList = RxList();
  RefreshController refreshController = RefreshController(initialRefresh: false);
  ApiManager? apiManager;
  Rx<bool> isLoading = false.obs;
  String? arg  = Get.arguments;
  bool isLoad = false;
  int numberPageIndex = pageNumberValue;
  int numberPageSize = pageSizeValue;
  int totalPage = pageNumberValue;
  String? argument = Get.arguments;
  RxList<NotifyEmpWithBody> listNotyEmp = RxList<NotifyEmpWithBody>();

  NotificationController(this.apiManager);
  @override
  void onInit() {
    if(argument == fromHomeToNotify){
      getNotification();
    }
    else{
      getNotificationEmp();
    }
    super.onInit();
  }
  void onRefresherNotification(){
    isLoad = false;
    refreshController.refreshCompleted();
    if(argument == fromHomeToNotify){
      getNotification();
    }
    else{
      getNotificationEmp();
    }
  }
 void onLoadNotification() {
   isLoad = true;
   if (argument == fromHomeToNotify) {
     if (numberPageIndex < totalPage) {
       getNotification();
     }
     else{
       refreshController.refreshCompleted();
     }
   }
   else {
     getNotificationEmp();
   }

 }
 getNotification()async{
   if(!isLoad){
     isLoading.value = true;
     notificationList.clear();
   }
   else{
    if(pageNumberValue < totalPage){
      numberPageIndex = ++numberPageIndex;
    }
   }
    try{
      isLoading.value = true;
      final requestData = {
        'pageNumber' : numberPageIndex,
        'pageSize' : numberPageSize,
      };
      refreshController.loadComplete();
      final request = await apiManager?.getListNotification(data: requestData);
      final result = BaseApiResponse.fromJson(request);
      if(result.code == successful || result.code == create){
         NotificationDto notificationDto = NotificationDto.fromJson(result.data);
         addBadge(totalUnseen: notificationDto.totalNotSeen);
         countNotifyHome.value = notificationDto.totalNotSeen ?? 0;
         totalPage = notificationDto.totalPages!;
         notificationDto.data?.forEach((element) {
           notificationList.add(element);
         });
         isLoading.value = false;
      }
      else{
        isLoading.value = false;
        BaseErr err = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!,message: err.message);
      }
    }
    catch(ex){
      errNotification(context :Get.context!,);
    }
 }
  void addBadge({int? totalUnseen}) {
    FlutterAppBadger.updateBadgeCount(totalUnseen ?? 0);
  }
 seenNotify({int? id})async{
    try{
      isLoading.value = true;
      final request = await apiManager?.seenNotification(id: id);
      final result = BaseApiResponse.fromJson(request);
      if(result.code == successful || result.code == create){
        if (kDebugMode) {
          print('đã seen thông báo');
        }
      }
      else{
        isLoading.value = false;
        BaseErr err = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!,message: err.message);
      }
    }
    catch(ex){
      errNotification(context :Get.context!);
    }
 }
  getNotificationEmp()async{
    if(!isLoad){
      isLoading.value = true;
      listNotyEmp.clear();
      numberPageIndex = pageNumberValue;
      numberPageSize = pageSizeValue;
    }
    else{
      numberPageIndex = numberPageIndex + numberPageSize;
      numberPageSize = pageSizeValue;
    }
    try{
      final requestData = {
        'start' : numberPageIndex,
        'length' : numberPageSize,
      };
      refreshController.loadComplete();
      final request = await apiManager?.getListNotificationEmp(data: requestData);
      final result = BaseApiResponse.fromJsonList(request);
      if(result.code == successful || result.code == create){
        isLoading.value = false;
        result.data.forEach((element){
          NotifyEmp notifyEmp = NotifyEmp.fromJson(element);
          Map<String, dynamic> json = jsonDecode(notifyEmp.body ?? '');
          BodyNotify? body = BodyNotify.fromJson(json);
          NotifyEmpWithBody notifyEmpWithBody = NotifyEmpWithBody(
            id: notifyEmp.id,
            title: notifyEmp.title,
            totalNotSeen: notifyEmp.totalNotSeen,
            body: body,
            createdDate: notifyEmp.createdDate,
            seenStatus: notifyEmp.seenStatus,
          );
          listNotyEmp.add(notifyEmpWithBody);
        });
        addBadge(totalUnseen: listNotyEmp.first.totalNotSeen??0);
        countNotifyHome.value = listNotyEmp.first.totalNotSeen??0;
      }
      else{
        BaseErr err = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!,message: err.message);
      }
    }
    catch(ex){
      isLoading.value = false;
      errNotification(context :Get.context!,);
    }
  }
}

