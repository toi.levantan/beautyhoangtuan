import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/forgot_pwd_vm/forgot_pwd_controller.dart';

class ForgotPasswordBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<ForgotPasswordController>(() => ForgotPasswordController(Get.find<ApiManager>()));
  }

}