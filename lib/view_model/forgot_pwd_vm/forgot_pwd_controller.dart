import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';

import '../../core/repository/api_manager.dart';
import '../../core/routes/routers.dart';
import '../../utils/app/application.dart';
import '../../utils/path/const_key.dart';
import '../login_vm/login_create_controller.dart';

class ForgotPasswordController extends GetxController {
  ApiManager? apiManager;
  Rx<bool?> isLoading = false.obs;
  LoginCreateController loginController = Get.find<LoginCreateController>();
  List<TextEditingController> listEdtController = <TextEditingController>[
    TextEditingController(),
    TextEditingController(),
  ];
  Rx<bool> hidePass = true.obs;
  Rx<bool> hidePassConfirm = true.obs;
  Rx<String?>? validatePassword = RxnString();
  Rx<String?>? validateConfirmPassword = RxnString();
  Rx<bool?>? isOk = false.obs;
  String? edtNumberPhone;

  validateCheckPassword() {
    validatePassword?.value =
        Application.checkValidatePassword(pwd: listEdtController.first.text);
  }

  checkConfirmPass() {
    if (listEdtController.last.text != listEdtController.first.text) {
      return validateConfirmPassword?.value = 'Mật khẩu mới chưa khớp';
    }
    return validateConfirmPassword?.value = null;
  }

  validateButton() {
    if (listEdtController.first.text.isNotEmpty &&
        listEdtController.last.text.isNotEmpty) {
      if (validatePassword?.value == null &&
          validateConfirmPassword?.value == null) {
        return isOk?.value = true;
      }
      return isOk?.value = false;
    }
    return isOk?.value = false;
  }

  setPassword() async {
    isLoading.value = true;
    int timeStamp = DateTime.now().millisecondsSinceEpoch;
    try {
      final body = {
        "phoneNumber": edtNumberPhone?.trim() ?? '',
        "accessKey": accessKey,
        "timeStamp": timeStamp,
        "checksum" :  sha1.convert(utf8.encode("$timeStamp$secretKey")).toString(),
        "password" : listEdtController.first.text.trim(),
      };
      final request = await apiManager?.setNewPassword(body);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading.value = false;
        await successNotification(context: Get.context!,message: 'Mật khẩu thay đổi thành công');
        await Future.delayed(const Duration(seconds: 2));
        Get.offNamedUntil(Routers.intro_page, (route) => false);
      }
      else{
        BaseErr baseErr = BaseErr.fromJson(request);
        errNotification(context: Get.context!,message: baseErr.message);
      }
    } catch (ex) {
      isLoading.value = false;
      errNotification(context: Get.context!);
    }
  }

  ForgotPasswordController(this.apiManager);

  @override
  void onInit() {
    // TODO: implement onInit
    edtNumberPhone = loginController.edtNumberPhone.text;
    super.onInit();
  }
}
