import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../model/base_api.dart';
import '../../model/booking/new_dto.dart';
import '../../model/service/res_services_dto.dart';
import '../../utils/path/const_key.dart';
import '../../utils/widget/snack_bar.dart';

class NewsController extends GetxController{
  ApiManager? apiManager;
  NewsController(this.apiManager);
  RxList<Content> listData = RxList();
  Rx<bool?> isLoading = false.obs;
  RefreshController refreshController = RefreshController(initialRefresh: false);
  bool isLoadMore = false;
  var argument = Get.arguments;
  int pageSize = pageSizeValue;
  int pageNumber = pageNumberValue;
  int totalPage =  pageNumberValue;

  TextEditingController newSearchControllerEdt = TextEditingController();
 @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getNews();

  }
  onRefresh(){
    isLoadMore = false;
    getNews();
    refreshController.refreshCompleted();
  }
  onLoadMore(){
     isLoadMore = true;
     getNews();
  }
  getNews() async {
    isLoading.value =true;
    try {
    if(!isLoadMore){
      pageNumber = pageNumberValue;
      listData.clear();
      isLoading.value = true;
    }
    else{
      if(pageNumber < totalPage){
        pageNumber = ++pageNumber;
      }
      else{
        refreshController.loadComplete();
      }
    }
      final cancelRequestDto = {
        "type": argument[1],
        "pageNumber": pageNumber,
         "pageSize": pageSize,
         "keySearch": newSearchControllerEdt.text.trim(),
         "activeStatus": 1,
      };
      final request = await apiManager?.getHotNews(data: cancelRequestDto);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        refreshController.loadComplete();
        isLoading.value = false;
        NewDto newDto = NewDto.fromJson(result.data);
        totalPage = newDto.totalPages!;
        for (var element in newDto.content!) {
            listData.add(element);
        }
        isLoading.value =false;
      } else {
        isLoading.value = false;
        BaseErr err = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(context: Get.context!, message: err.message);
        isLoading.value =false;
      }
    } catch (ex) {
      errNotificationNoAppBar(context: Get.context!);
      isLoading.value =false;
    }
  }
}