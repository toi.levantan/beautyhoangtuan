import 'package:get/get.dart';
import 'package:myhoangtuan/view_model/news_vm/html_view_controller.dart';

class ViewHtmlBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<HtmlViewController>(() => HtmlViewController());
  }

}