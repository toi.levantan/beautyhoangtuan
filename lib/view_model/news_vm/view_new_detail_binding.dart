import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/news_vm/view_new_detail_controller.dart';

class ViewNewDetailBinding implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut<ViewNewDetailController>(() => ViewNewDetailController(Get.find<ApiManager>()),fenix: true);
  }
}