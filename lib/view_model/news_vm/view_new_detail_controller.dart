import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

import '../../model/service/res_services_dto.dart';

class ViewNewDetailController extends GetxController{
   Content? argument = Get.arguments;
  late final PlatformWebViewControllerCreationParams params;
  late final WebViewController webViewController;
   ApiManager? apiManager;
   Content? content = Content();
   Rx<bool> isLoading = true.obs;
   ViewNewDetailController(this.apiManager);

  @override
  void onInit() {
    // TODO: implement onInit
     getDetailNews();
    //viewHtml();
    super.onInit();
  }
  getDetailNews() async{
   try{
     isLoading.value = true;
     int? idNews = argument?.id ?? -1;
     final request = await apiManager?.getDetailNews(idNews);
     final response = BaseApiResponse.fromJson(request);
     if(response.code == successful || response.code == create){
       content = Content.fromJson(response.data);
       isLoading.value = false;
     }
     else{
       BaseErr baseErr = BaseErr.fromJson(request);
       errNotification(context: Get.context!,message: baseErr.message);
     }
   }
   catch(ex){
     errNotification(context: Get.context!);
   }
  }
  viewHtml(){
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }
    webViewController = WebViewController.fromPlatformCreationParams(params);
    webViewController..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {

          },
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
        ),
      )
      ..loadRequest(Uri.parse('http://192.168.1.42:5778/api/auth/html-page'));
  }
}