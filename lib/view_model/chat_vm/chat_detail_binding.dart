import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/chat_vm/chat_detail_controller.dart';

class ChatDetailBinding implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut<ChatDetailController>(() => ChatDetailController(Get.find<ApiManager>()),fenix: true);
  }

}