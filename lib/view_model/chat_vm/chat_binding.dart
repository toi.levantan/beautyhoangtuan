import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/chat_vm/chat_controller.dart';

class ChatBinding implements Bindings{
  @override
  void dependencies() {
   Get.lazyPut<ChatController>(() => ChatController(Get.find<ApiManager>()));
  }
}