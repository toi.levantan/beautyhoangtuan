import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';

class ChatController extends GetxController{
  ApiManager? apiManager;

  ChatController(this.apiManager);

  List<types.Message> messages = [];
  final user = const types.User(
    id: '82091008-a484-4a89-ae75-a22bf8d6f3ac',
  );
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }
  getHistoryMessage() async{
    try{
     final request = await apiManager?.getHistoryChat();
      final response = BaseApiResponse.fromJson(request);
    }
    catch(ex){
    errNotificationNoAppBar(context: Get.context!);
    }
  }
}