import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:get/route_manager.dart';
import 'package:get/state_manager.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/login/checkinInfo_dto.dart';
import 'package:myhoangtuan/model/permission_model/permission_model.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../core/repository/url.dart';
import '../../model/base_api.dart';
import '../../model/booking/history_booking_dto.dart';
import '../../model/fcm_body.dart';
import '../../model/feedback/feedback_dto.dart';
import '../../model/login/user_data.dart';
import '../../model/notify/notify_emp.dart';
import '../../utils/app/share_pref.dart';
import '../../utils/app/size_app.dart';
import '../../utils/app/util_logger.dart';
import '../../utils/path/const_key.dart';
import '../../utils/session/helper_notification.dart';
import '../../utils/widget/snack_bar.dart';

class HomeStaffController extends GetxController {
  RxInt indexPage = 0.obs;
  PersistentTabController? persistentTabController;
  Rx<bool?>? isLoading = false.obs;
  RefreshController refreshControllerHome = RefreshController(
      initialRefresh: false);
  RxList<Content> hotNew = RxList();
  RxList<Widget> imgList = RxList();
  RxList<ContentHis> historyListHome = RxList<ContentHis>();
  TextEditingController reasonControllerEdt = TextEditingController();
  Rx<bool> isShowBook = false.obs;
  Rx<bool> isLoadingAccount = false.obs;
  ApiManager? apiManager = Get.find<ApiManager>();
  RxList<File?> imageFile = <File>[].obs;
  UserData? userData;
  RxString avatarValue = "${PreferenceUtils.getString(avatar)}".obs;
  late Rx<bool> isLogin;
  List<NotifyEmp> listNotyEmp = [];
  List<CheckinInfoDto> listCheckin = [];
  RxString startTimeCheckin = RxString("");
  RxString endTimeCheckin = RxString("");
  RxString workStartAt = RxString("2023-07-07T01:30:00.004+0000");
  RxString workEndAt = RxString("2023-07-07T01:30:00.004+0000");
  // TODO Tài khoản
  RxnString? languageNote = RxnString('Tiếng Việt');
  Rx<String>? infoCheckin = Rx<String>("");
  List<PermissionModel> workSpaces = <PermissionModel>[];

  @override
  void onInit() {
    Map<String, dynamic> valueMap = json.decode(PreferenceUtils.getString(userDataKey) ?? '');
    userData = UserData.fromJson(valueMap);
    persistentTabController = PersistentTabController(initialIndex: 0);
    isLogin = checkHasLogin().value.obs;
    if (checkHasLogin().value) {
      getNotification();
      initialFeature();
    }
    getNotification();
    getMessageFcm();
    checkInfoCheckin();
    super.onInit();
  }

  initialFeature(){
   final listFeature = [
     PermissionModel(id: 'donbao',title: 'Đơn báo',image: ImagePath.donbao,subTitle: 'Viết đơn xin nghỉ',),
     PermissionModel(id: 'danhba',title: 'Danh bạ',image: ImagePath.phonebook,subTitle: 'Danh bạ điện thoại nhân viên'),
   ];
   workSpaces.addAll(listFeature);
  }
  getNotification() async {
    try {
      //total getTotal Notify Employee
      final requestData = {
        'pageNumber': pageNumberValue,
        'pageSize': 10,
      };
      final request = await apiManager?.getListNotificationEmp(
          data: requestData);
      final result = BaseApiResponse.fromJsonList(request);
      if (result.code == successful || result.code == create) {
        result.data.forEach((element) {
          NotifyEmp notifyEmp = NotifyEmp.fromJson(element);
          listNotyEmp.add(notifyEmp);
        });
        addBadge(totalUnseen: listNotyEmp.first.totalNotSeen ?? 0);
        countNotifyHomeEmployee.value = listNotyEmp.first.totalNotSeen ?? 0;
      }
      else {
        BaseErr err = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(context: Get.context!, message: err.message);
      }
    }
    catch (ex) {
      errNotificationNoAppBar(context: Get.context!,);
    }
  }

  void addBadge({int? totalUnseen}) {
    FlutterAppBadger.updateBadgeCount(totalUnseen ?? 0);
  }

  getUnSubNotify() async {
    // check keyAccesstoken
    if (PreferenceUtils.getString(keyAccessToken) != null) {
      final object = {
        "tokens": [
          "${PreferenceUtils.getString(tokenFcm)}"
        ]
      };
      final request = await apiManager?.unsubscribeNotifyEmp(data: object);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        if (kDebugMode) {
          print('Đã huy ký FCM');
        }
      }
    }
  }

  getMessageFcm() {
    final FirebaseMessaging fcm = FirebaseMessaging.instance;
    fcm.getInitialMessage().then((RemoteMessage? message) async {
      RemoteNotification? notification = message?.notification;
      FcmBody? fcmBody = FcmBody.fromJson(notification);
      Map<String, dynamic> json = await jsonDecode(fcmBody.body ?? '');
      Body body = Body.fromJson(json);
      FlutterAppBadger.updateBadgeCount(body.totalNotSeen ?? 0);
      countNotifyHomeEmployee.value = body.totalNotSeen ?? 0;
    });
  }

  updateAvatar(File? fileImage) async {
    try {
      isLoadingAccount.value = true;
      String fileName = fileImage!.path.split("/").last;
      String filePath = fileImage.path;
      final formData = FormData.fromMap(<String, dynamic>{
        "files": [
          await MultipartFile.fromFile(
            filePath,
            filename: fileName,
          ),
        ],
      });
      final request = await apiManager?.updateAvatarEmp(data: formData);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        checkExpired();
        isLoadingAccount.value = false;
        successNotificationNoAppBar(context: Get.context!, message: "Cập nhật ảnh thành công");
      } else {
        BaseErr baseErr = BaseErr.fromJson(result.data);
        isLoadingAccount.value = false;
        errNotificationNoAppBar(
            context: Get.context!, message: baseErr.message ?? baseErr.data);
      }
    } catch (ex) {
      errNotificationNoAppBar(context: Get.context!);
      isLoadingAccount.value = false;
    }
  }

  checkExpired() async {
    try {
      final request = await apiManager?.checkTypeUser();
      final result = BaseApiResponse.fromJson(request);
      if (result.code != null && result.code != expiredToken) {
        final user = UserData.fromJson(result.data);
        avatarValue.value = "${user.imageStore?.first.fileId}";
        PreferenceUtils.setString(avatar, user.imageStore?.first.fileId ?? '');
        PreferenceUtils.setString(myUserName, user.username.toString());
      }
    }
    catch (ex) {
      UtilLogger.log(ex.toString());
    }
  }
  checkInfoCheckin()async{
    try {
      listCheckin.clear();
      isLoading?.value = true;
      final startTime = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 0, 0, 0).toIso8601String();
      final endTime = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, 23, 59, 59).toIso8601String();
      final body = {
        "start": startTime,
        "end": endTime,
      };
      final request = await apiManager?.checkinInfo(body);
      final response = BaseApiResponse.fromJsonList(request);
      if (response.code == successful || response.code == create) {
        response.data.forEach((element) {
          CheckinInfoDto checkInDto = CheckinInfoDto.fromJson(element);
          listCheckin.add(checkInDto);
        });
        isLoading?.value = false;
        startTimeCheckin.value = listCheckin.last.checkinTime ?? '';
        endTimeCheckin.value   = listCheckin.last.checkoutTime ?? '';
        workStartAt.value      = listCheckin.last.workStartAt ?? '';
        workEndAt.value        = listCheckin.last.workEndAt ?? '';
      }
      else{
        BaseErr baseApiResponse = BaseErr.fromJson(response.data);
        errNotificationNoAppBar(context: Get.context!,message: baseApiResponse.message);
      }
    }
    catch (ex) {
      errNotificationNoAppBar(context: Get.context!);
    }
  }

  onRefresherHome() {
    checkInfoCheckin();
    refreshControllerHome.refreshCompleted();
  }

  onLoadHome() {
    refreshControllerHome.loadComplete();
  }
}