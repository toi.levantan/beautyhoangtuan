import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/booking/history_booking_dto.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../utils/app/share_pref.dart';

class HistoryBookController extends GetxController{
  ApiManager? apiManager;
 RefreshController refreshController = RefreshController(initialRefresh: false);
 Rx<bool?> isLoading = false.obs;
  RxInt isSelected = 0.obs;
  RxList<ContentHis> historyList = RxList<ContentHis>();
  RxList<ContentHis> newBookList = RxList<ContentHis>();
  bool isLoad = false;
  var argument = Get.arguments;
  int pageNumber = pageNumberValue;
  int pageSize = pageSizeValue;
  int totalPage = pageNumberValue;
  TextEditingController reasonControllerNewBookEdt  = TextEditingController();
  TextEditingController reasonControllerHistoryBookEdt  = TextEditingController();
  HistoryBookController(this.apiManager);


  @override
  void onInit() {
    if(argument != null){
      isSelected.value = 1;
    }
    getNewBooking();
    getHistoryBooking();
    // TODO: implement onInit
    super.onInit();
  }
  void onRefresh() async{
    isLoad = false;
    pageNumber = pageNumberValue;
    totalPage  = pageNumberValue;
    isSelected.value == 0 ? getNewBooking() : getHistoryBooking();
    refreshController.refreshCompleted();
  }
  void onLoading() async{
   if(pageNumber < totalPage){
     isLoad = true;
     isSelected.value == 0 ? getNewBooking() : getHistoryBooking();
   }
   else{
     refreshController.loadComplete();
   }
  }
  getHistoryBooking()async{
    if(!isLoad){
     historyList.clear();
     pageNumber = pageNumberValue;
     isLoading.value = true;
    }
    else{
    if(pageNumber < totalPage){
      pageNumber = ++pageNumber;
    }
    }
    try{
      final dataRequest = {
        "customerCode": PreferenceUtils.getString(customerCode),
        "pageNumber" :pageNumber,
        "pageSize" : pageSizeValue,
      };
      final request = await apiManager?.getHistoryBooking(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      HistoryBookingDto historyBookingdto = HistoryBookingDto.fromJson(result.data);
      if(result.code == successful || result.code == create || result.code == create) {
        isLoading.value = false;
        if(isLoad){
          refreshController.loadComplete();
        }
        if (historyBookingdto.content!.isNotEmpty) {
          totalPage = historyBookingdto.totalPages ?? 0;
          historyBookingdto.content?.forEach((element) {
            historyList.add(element);
          });
        }
      }
      else{
        isLoading.value = false;
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!,message: baseErr.message);
      }
    }
    catch(ex){
      isLoading.value = false;
      errNotification(context: Get.context!);
    }
  }
  getNewBooking()async{
    if(!isLoad){
      newBookList.clear();
      isLoading.value = true;
      pageNumber = pageNumberValue;
    }
    else{
     if(pageNumber < totalPage){
       pageNumber = ++pageNumber;
     }
    }
    try{
      refreshController.loadComplete();
      final dataRequest = {
        "customerCode": PreferenceUtils.getString(customerCode),
        "pageNumber" :pageNumber,
        'pageSize' : pageSizeValue,
        "startTime": DateTime.now().toIso8601String(),
        "isNearest": true,
      };
      final request = await apiManager?.getHistoryBooking(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      HistoryBookingDto historyBookingdto = HistoryBookingDto.fromJson(result.data);
      if(result.code == successful || result.code == create || result.code == create) {
        if (historyBookingdto.content!.isNotEmpty) {
          totalPage = historyBookingdto.totalPages!;
          historyBookingdto.content?.forEach((element) {
            newBookList.add(element);
          });
        }
        isLoading.value = false;
      }
      else{
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!,message: baseErr.message);
        isLoading.value = false;
      }
    }
    catch(ex){
      errNotification(context: Get.context!);
      isLoading.value = false;
    }
  }
  cancelBooking({String? reason,int? id})async {
    Get.back();
    try{
      isLoading.value = true;
      final cancelRequestDto = {
        "cancelReason": reason,
        "id" : id
      };
      final request = await apiManager?.cancelBooking(data: cancelRequestDto);
      final result = BaseApiResponse.fromJson(request);
      if(result.code == successful){
        isLoading.value = false;
        successNotificationNoAppBar(context: Get.context!,message: 'Lịch hẹn đã được hủy');
       getNewBooking() ;
       getHistoryBooking();
      }
      else{
        isLoading.value = false;
        BaseErr err = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(context: Get.context!,message: err.message);
      }
    }
    catch(ex){
      isLoading.value = false;
      errNotificationNoAppBar(context: Get.context!);
    }
  }
}