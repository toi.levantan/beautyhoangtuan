import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/booking/booking_dto.dart';
import 'package:myhoangtuan/model/booking/doctor_booking_dto.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:myhoangtuan/view_model/home_vm/home_controller.dart';
import '../../core/repository/api_manager.dart';
import '../../core/routes/routers.dart';
import '../../model/booking/booking_branch_dto.dart';
import '../../model/service/get_services_model.dart';
import '../../model/service/res_services_dto.dart';
import '../../utils/path/color_path.dart';

class BookController extends GetxController {
  final ApiManager? apiManager;
  BookController(this.apiManager);

  RxList listBranch = RxList<Data>();
  RxList listContent = RxList<Content>();
  RxList listCombo = RxList<Content>();
  RxList listContentServices = RxList<BeautyServicePackage>();
  RxList listDoctorDto = RxList<Data>();
  GetServicesModel dataRequestServices = GetServicesModel();
  GetServicesModel dataRequestServicesCombo = GetServicesModel();
  DateTime now = DateTime.now();
  List<DateTimeRange> converted = [];
  FocusNode branchFocus = FocusNode();
  List<TextEditingController> textEdtControllers = [];
  RxList<Data> dataTime = RxList<Data>();
  Rx<Color> colorPick = Colors.white.obs;
  Rx<Color> colorTextPick = colorIcon.obs;
  Rx<Color> colorIconDef = colorIcon.obs;
  Rx<Color> colorBackGroundBorderDef = colorBackGroundBorder.obs;
  Rx<Color> colorBackGrPickTimeDef = colorBackGrPickTime.obs;
  Rx<bool>? isLoading = false.obs;
  Rx<bool>? isCheckButton = false.obs;
  RxInt selectedRadio = 1.obs;
  String? hourBook;
  List<BeautyBookingDetails> listBookingDetail = [];
  List<BeautyBookingDetails> listBookingDetailCombo = [];
  Rx<String?>? chiNhanhErr = RxnString();
  Rx<String?>? dichVuErr = RxnString();
  Rx<String?>? goiDichVuErr = RxnString();
  Rx<String?>? ngayDatLichErr = RxnString();
  Rx<String?>? gioDatlichErr = RxnString();
  Rx<String?>? comboIdErr = RxnString();
  int valueChiNhanh = 0;
  bool isShowPickService = true;
  var argument = Get.arguments;
  HomeController homeController = Get.find<HomeController>();

  @override
  void onInit() {
    // TODO: implement onInit
    for (int i = 0; i <= 6; i++) {
      textEdtControllers.add(TextEditingController());
    }
    if (argument == null) {
      getBranchApi();
    }
    else {
      if (argument[0] == bookFromService) {
        BeautyBookingDetails data = BeautyBookingDetails(
            beautyServicePackageId: argument[1]);
        getBranchFromBookService(data: data);
        isShowPickService = false;
      }
      if (argument[0] == bookFromCombo) {
        isShowPickService = false;
        BeautyBookingDetails data = BeautyBookingDetails(
            beautyPackageComboId: argument[1]);
        getBranchFromBookService(data: data);
        selectedRadio.value = 2;
      }
    }
    super.onInit();
  }

  // TODO lấy danh sách chi nhánh từ chi nhánh và combo
  getBranchFromBookService({var data}) async {
    listBranch.clear();
    final request = await apiManager?.getBranchService(data: data);
    final result = BaseApiResponse.fromJson(request);
    Data bookingBranchDto = Data.fromJson(result.data);
    bookingBranchDto.organizationBranchResponseDTOS?.forEach((element) {
      Data data = Data(id: element.id,
          organizationBranchName: element.organizationBranchName);
      listBranch.add(data);
    });
  }

  // TODO lấy danh sách chi nhánh
  getBranchApi() async {
    listBranch.clear();
    try {
      final request = await apiManager?.getBranch();
      final result = BaseApiResponse.fromJson(request);
      BookingBranchDto bookingBranchDto =
      BookingBranchDto.fromJson(result.data);
      bookingBranchDto.data?.forEach((element) {
        listBranch.add(element);
      });
    } catch (err) {}
  }

  ///TODO lấy danh sách dịch vụ
  getServices(int organizationBranchId) async {
    try {
      isLoading?.value = true;
      listContent.clear();
      GetServicesModel dataRequest = GetServicesModel(
          organizationBranchId: organizationBranchId);
      final request = await apiManager?.getBeautyServices(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      ResServicesDto resServiceDto = ResServicesDto.fromJson(result.data);
      resServiceDto.content?.forEach((element) {
        listContent.add(element);
      });
      isLoading?.value = false;
    } catch (err) {
      isLoading?.value = false;
    }
  }

  /// lấy danh sách gói dịch vụ
  getPackageService() async {
    try {
      isLoading?.value = true;
      listContentServices.clear();
      final request = await apiManager?.getPackageBook(dataRequestServices);
      final result = BaseApiResponse.fromJson(request);
      ResServicesDto resServicesDtoPack = ResServicesDto.fromJson(result.data);
      resServicesDtoPack.content?.forEach((element) {
        listContentServices.add(element.beautyServicePackage);
      });
      isLoading?.value = false;
    } catch (err) {
      isLoading?.value = true;
    }
  }

  ///TODO lấy nhân viên
  getDoctor() async {
    try {
      listDoctorDto.clear();
      isLoading?.value = true;
      final param = <String, dynamic>{
        "organizationBranchId": dataRequestServices.organizationBranchId,
        "departmentId": dataRequestServices.departmentId,
      };
      final request = await apiManager?.getDoctor(param);
      final result = BaseApiResponse.fromJson(request);
      DoctorBookingDto doctorBookingDto =
      DoctorBookingDto.fromJson(result.data);
      doctorBookingDto.data?.forEach((element) {
        listDoctorDto.add(element);
      });
      isLoading?.value = false;
    } catch (err) {
      isLoading?.value = false;
      errNotification(context: Get.context!);
    }
  }

  //TODO getDoctorFromService
  getDoctorServices() async {
    try {
      listDoctorDto.clear();
      isLoading?.value = true;
      final param = <String, dynamic>{
        "organizationBranchId": dataRequestServices.organizationBranchId,
        "beautyServicePackageId": dataRequestServices.beautyServicePackageId,
      };
      final request = await apiManager?.getDoctorFromService(param: param);
      final result = BaseApiResponse.fromJson(request);
      DoctorBookingDto doctorBookingDto = DoctorBookingDto.fromJson(
          result.data);
      doctorBookingDto.data?.forEach((element) {
        listDoctorDto.add(element);
      });
      isLoading?.value = false;
    } catch (err) {
      isLoading?.value = false;
      errNotification(context: Get.context!);
    }
  }

  ///TODO lấy danh sách combo
  getCombo(int organizationBranchId) async {
    isLoading?.value = true;
    listCombo.clear();
    GetServicesModel dataRequest = GetServicesModel(
        organizationBranchId: organizationBranchId);
    final request = await apiManager?.getComboBook(dataRequest);
    final result = BaseApiResponse.fromJson(request);
    if (result.code == successful || result.code == create ||
        result.code == create) {
      ResServicesDto resServiceDto = ResServicesDto.fromJson(result.data);
      resServiceDto.content?.forEach((element) {
        listCombo.add(element);
      });
      isLoading?.value = false;
    }
    else {
      BaseErr baseErr = BaseErr.fromJson(request);
      errNotification(context: Get.context!, message: baseErr.message);
      isLoading?.value = false;
    }
  }

  validateBooking() {
    chiNhanhErr?.value = null;
    dichVuErr?.value = null;
    goiDichVuErr?.value = null;
    comboIdErr?.value = null;
    ngayDatLichErr?.value = null;

    /// selected == 1 là dịch vụ
    if (selectedRadio.value == 1) {
      // text 0 : chi nhánh
      // 1 : là dịch vụ
      // 2: là gói dịch vụ
      // 3 kỹ thuật viên ( no required)
      // 4 : là ngày đặt lịch
      // 5 : khung giờ
      if (textEdtControllers.first.text.isEmpty) {
        return chiNhanhErr?.value = 'Vui lòng chọn chi nhánh';
      }
      if (isShowPickService) {
        if (textEdtControllers[1].text.isEmpty) {
          return dichVuErr?.value = "Vui lòng chọn dịch vụ";
        }
        if (textEdtControllers[2].text.isEmpty) {
          return goiDichVuErr?.value = "Vui lòng chọn gói dịch vụ";
        }
      }
    }

    /// selected == 2 là combo
    ///  // text 6 : danh sách combo
    if (selectedRadio.value == 2) {
      if (textEdtControllers.first.text.isEmpty) {
        return chiNhanhErr?.value = ' Vui lòng chọn chi nhánh';
      }
      if (textEdtControllers[6].text.isEmpty && isShowPickService) {
        return comboIdErr?.value = ' Vui lòng chọn combo';
      }
    }
    if (textEdtControllers[4].text.isEmpty) {
      return ngayDatLichErr?.value = "Vui lòng chọn ngày đặt lịch";
    }
    return null;
  }

  /// đặt lịch
  booking() async {
    try {
      Get.back();
      listBookingDetail.clear();
      listBookingDetailCombo.clear();
      isLoading?.value = true;
      BeautyBookingDetails dataDetail = BeautyBookingDetails(
        beautyServicePackageId: dataRequestServices.beautyServicePackageId,
        bookingDate: dataRequestServices.bookingDate,
        performerId: dataRequestServices.performerId,
        startTime: dataRequestServices.startTime,
      );
      listBookingDetail.add(dataDetail);
      BookingDto bookingDto = BookingDto(
          organizationBranchId: dataRequestServices.organizationBranchId ?? -1,
          packageType: selectedRadio.value,
          beautyBookingDetails: listBookingDetail
      );

      BeautyBookingDetails dataDetailCombo = BeautyBookingDetails(
        bookingDate: dataRequestServicesCombo.bookingDate,
        beautyPackageComboId: dataRequestServicesCombo.beautyPackageComboId,
        startTime: dataRequestServicesCombo.startTime,
      );
      listBookingDetailCombo.add(dataDetailCombo);
      BookingDto bookingDtoCombo = BookingDto(
          organizationBranchId: dataRequestServicesCombo.organizationBranchId ??
              -1,
          packageType: selectedRadio.value,
          beautyBookingDetails: listBookingDetailCombo
      );
      final request = await apiManager?.booking(
          selectedRadio.value == 1 ? bookingDto : bookingDtoCombo);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading?.value = false;
        homeController.onRefresherHome();
        notify();
      } else {
        final result = BaseApiResponse.fromJson(request);
        final resultErr = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!,
            description: resultErr.error,
            message: resultErr.message);
        isLoading?.value = false;
      }
    } catch (err) {
      errNotification(context: Get.context!);
      isLoading?.value = false;
    }
  }

  notify() async {
   await successNotification(context: Get.context!, message: 'Đăng ký lịch thành công');
   Get.offNamed(Routers.historyBook, arguments: booked);
  }
}