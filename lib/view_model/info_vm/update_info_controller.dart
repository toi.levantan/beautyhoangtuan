import 'dart:convert';
import 'dart:io';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/route_manager.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import '../../model/booking/doctor_booking_dto.dart';
import '../../model/login/access_dto.dart';
import '../../model/login/user_data.dart';

class UpdateInfoController extends GetxController {
  ApiManager? apiManager;

  UpdateInfoController(this.apiManager);

  Rx<bool?> isLoading = false.obs;
  RxList<Data> listBranch = RxList<Data>();
  List<TextEditingController> listEdtController = <TextEditingController>[
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
  ];
  final now = DateTime.now();
  List<DateTime?>? valueTime = [];
  Rx<String?>? validateName = RxnString();
  Rx<String?>? validateBranch = RxnString();
  Rx<String?>? validatePassword = RxnString();
  Rx<String?>? validateConfirmPassword = RxnString();
  Rx<String?>? validateDate = RxnString();
  Rx<bool?>? isOk = false.obs;
  RxList<File?> imageFile = <File>[].obs;
  Data dataRequestServices = Data();
  num timeStamp = 0;
  String? phone = Get.arguments;
  Rx<bool> hidePass = true.obs;
  Rx<bool> hidePassConfirm = true.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    //checkExistByPhone();
    getBranchOriginApi();
  }

  validateCheckName() {
    validateName?.value =
        Application.checkEmptyErrAndShort(param: listEdtController[1].text);
  }
  validateCheckPassword() {
    validatePassword?.value = Application.checkValidatePassword(pwd:listEdtController[2].text);
  }

  validateCheckBranch() {
    validateBranch?.value =
        Application.checkEmptyErrAndShort(param: listEdtController.first.text);
  }
  checkConfirmPass(){
    if(listEdtController[2].text != listEdtController[3].text){
      return validateConfirmPassword?.value = 'Mật khẩu chưa khớp';
    }
    return validateConfirmPassword?.value = null;
  }
  validateDoB() {
    validateDate?.value =
        Application.checkEmptyErr(param: listEdtController.last.text);
  }

  /// lay chi nhanh khong can bearToken
  getBranchOriginApi() async {
    final query = {
      "organizationId": "${PreferenceUtils.getString(organizationId)}"
    };
    final request = await apiManager?.getOriginalId(queryParam: query);
    final result = BaseApiResponse.fromJsonList(request);
    result.data.forEach((element) {
      Data data = Data.fromJson(element);
      listBranch.add(data);
    });
  }

  validateButton() {
    if (listEdtController.first.text.isNotEmpty &&
        listEdtController[1].text.isNotEmpty && listEdtController[2].text.isNotEmpty&& listEdtController[3].text.isNotEmpty&&
        listEdtController.last.text.isNotEmpty) {
      if (validateName?.value == null &&
          validateBranch?.value == null &&
          validateDate?.value == null &&
          validatePassword?.value == null && validateConfirmPassword?.value == null &&
          imageFile.isNotEmpty) {
        return isOk?.value = true;
      }
      return isOk?.value = false;
    }
    return isOk?.value = false;
  }

  createByNumberPhone(File? fileImage) async {
    try {
      isLoading.value = true;
      String fileName = fileImage!.path.split("/").last;
      String filePath = fileImage.path;
      DateTime time = strToDate(valueTimeString: listEdtController.last.text);
      timeStamp = time.millisecondsSinceEpoch;
      final formData = FormData.fromMap(<String, dynamic>{
        "images": [
          await MultipartFile.fromFile(
            filePath,
            filename: fileName,
          ),
        ],
      });
      formData.fields.addAll([
        MapEntry("birthDay", timeStamp.toString()),
        MapEntry("organizationBranchId", dataRequestServices.organizationBranchId.toString()),
        MapEntry("organizationId", PreferenceUtils.getString(organizationId).toString()),
        MapEntry("fullName", listEdtController[1].text.toString().trim()),
        MapEntry("phoneNumber", phone.toString()),
        MapEntry("password", listEdtController[2].text.toString()),
      ]);
      final request = await apiManager?.createByNumberPhone(data: formData);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading.value = false;
        loginUpdate();
      } else {
        BaseErr baseErr = BaseErr.fromJson(result.data);
        isLoading.value = false;
        errNotification(context: Get.context!, message: baseErr.message ?? baseErr.data);
      }
    } catch (ex) {
      errNotification(context: Get.context!);
      isLoading.value = false;
    }
  }

  loginUpdate() async {
    // đã map to json rồi
    int timeStamp = DateTime.now().millisecondsSinceEpoch;
    // đã map to json rồi
    final object = {
      "grant_type": "password",
      "scope": "read",
      "username": phone,
      "accessKey": accessKey,
      "timeStamp" : timeStamp,
     "checksum" : sha1.convert(utf8.encode("$timeStamp$secretKey")).toString(),
    };
    try {
      final request = await apiManager?.login(object);
      final result = BaseApiResponse.fromJson(request);
      final accessToken = AccessDto.fromJson(result.data);
      await PreferenceUtils.setString( keyAccessToken, accessToken.accessToken.toString());
      await apiManager?.updateToken();
      if (result.code == successful || result.code == create) {
        await PreferenceUtils.setString(phoneAccess, phone.toString());
        await PreferenceUtils.setString(customerCode, accessToken.employeeCode);
        checkExpired();
      }
      else {
          BaseErrLogin baseErr = BaseErrLogin.fromJson(result.data);
          errNotification(
              context: Get.context!, message: baseErr.error_description);
      }
    } catch (ex) {
      errNotification(context: Get.context!);
      isLoading.value = false;
    }
  }

   checkExpired() async {
    try {
      final request = await apiManager?.getExpired();
      final result = BaseApiResponse.fromJson(request);
      if (result.code != null && result.code != expiredToken) {
        final user = UserData.fromJson(result.data);
         await PreferenceUtils.setString(userKey, user.fullName ?? '');
         await PreferenceUtils.setString(phoneData, user.phoneNumber ?? '');
        PreferenceUtils.setString(avatar, user.imageStore?.first.fileId ?? '');
        PreferenceUtils.setString(address, user.fullAddress??'Chưa xác định');
        PreferenceUtils.setString(idAccount, user.id.toString());
         await getSubcrisedNotify();
         Get.offAllNamed(Routers.home, arguments: true);
      }
    }
    catch (ex) {
      rethrow;
    }
  }
  getSubcrisedNotify() async{
    // check keyAccesstoken
    if(PreferenceUtils.getString(keyAccessToken) != null){
      final object = {
        "tokens": [
          "${PreferenceUtils.getString(tokenFcm)}"
        ]
      };
      final request = await apiManager?.subscribeNotify(data: object);
      final result = BaseApiResponse.fromJson(request);
      if(result.code == successful || result.code == create){
      }
    }
  }
}
