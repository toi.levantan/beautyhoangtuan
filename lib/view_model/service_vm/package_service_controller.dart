import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/service/get_services_model.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../core/repository/api_manager.dart';
import '../../model/service/res_services_dto.dart';

class PackageServiceController extends GetxController {
  ApiManager? apiManager;
  PackageServiceController(this.apiManager);
  TextEditingController textEditingController = TextEditingController();
  Content argument = Get.arguments;
  RxList<Content> listContent = RxList();
  RefreshController refreshController = RefreshController(initialRefresh: false);
  bool isLoad = false;
  int numberPageIndex = pageNumberValue;
  int numberPageSize = pageSizeValue;
  int totalPage = pageNumberValue;
  String? keySearch;
  Rx<bool>? isLoading = false.obs;
  Content? valueDetail = Content();
  @override
  void onInit() {
   getDetailPackService();
    // TODO: implement onInit
    super.onInit();
  }
  void onRefreshService() async{
    // monitor network fetch
    numberPageIndex = pageNumberValue;
    isLoad = false;
    getDetailPackService();
    refreshController.refreshCompleted();
  }
  void onLoadingService() async{
    // monitor network fetch
    isLoad = true;
    if(numberPageIndex < totalPage){
      getDetailPackService();
    }
    else{
      refreshController.loadComplete();
    }
  }
  getDetailPackService()async{
    if(!isLoad){
      isLoading?.value = true;
      listContent.clear();
    }
    else{
      numberPageIndex = ++numberPageIndex;
    }
    try{
        refreshController.loadComplete();
        GetServicesModel requestData = GetServicesModel(
        beautyServiceId: argument.id,
        keySearch: keySearch,
        pageSize: numberPageSize,
        pageNumber: numberPageIndex,
      );
      final request = await apiManager?.getBeautyIdService(requestData);
      final result = BaseApiResponse.fromJson(request);
      final detailPackage = ResServicesDto.fromJson(result.data);
        totalPage = detailPackage.totalPages ?? pageNumberValue;
        refreshController.loadComplete();
       detailPackage.content?.forEach((element) {
          listContent.add(element);
       });
       isLoading?.value = false;
    }
    catch(ex){
     isLoading?.value = false;
    }
  }
}
