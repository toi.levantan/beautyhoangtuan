import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/chat/recently_chat_dto.dart';
import 'package:myhoangtuan/model/chat/user_chat_dto.dart';
import 'package:myhoangtuan/model/new_feeds/new_feed_dto.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/app/util_logger.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../core/repository/api_manager.dart';
import '../../core/routes/routers.dart';
import '../../model/booking/history_booking_dto.dart';
import '../../model/booking/new_dto.dart';
import '../../model/fcm_body.dart';
import '../../model/login/user_data.dart';
import '../../model/notify/Notification_dto.dart';
import '../../model/service/get_services_model.dart';
import '../../model/service/res_services_dto.dart';
import '../../utils/app/share_pref.dart';
import '../../utils/app/size_app.dart';
import '../../utils/path/const_key.dart';
import '../../utils/session/helper_notification.dart';
import '../../utils/widget/images_render.dart';
import 'package:web_socket_client/web_socket_client.dart';

class HomeController extends GetxController {
  ApiManager? apiManager;

  HomeController(this.apiManager);

  RxList<Widget> imgList = RxList();
  RxList<Content> hotNew = RxList();
  RxList<ContentFeed> newFeeds = RxList<ContentFeed>();
  Rx<double> bottom = 10.0.obs;
  Rx<double> right = 10.0.obs;
  Rx<bool> isRefresher = true.obs;
  // late final flutterVideoCompress;
  // late final subscription;
  // RxList<ItemCard> items = <ItemCard>[
  //   ItemCard(content: Text('Xác thực bằng sinh trắc học'), val: false.obs),
  //   ItemCard(content: Text('Ngôn ngữ'), val: false.obs),
  //   ItemCard(content: Text('Giao diện'), val: false.obs),
  // ].obs;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  TextEditingController searchServiceEdt = TextEditingController();
  TextEditingController reasonControllerEdt = TextEditingController();
  TextEditingController newSearchControllerEdt = TextEditingController();
  RxInt indexPage = 0.obs;
  RxInt indexDot = 0.obs;
  RxBool isOnBiometric = false.obs;

  // final LocalAuthentication auth = LocalAuthentication();
  late bool isBiometricSupported;
  late bool canCheckBiometrics;
  String? keySearch = "";
  RxList<Content> listContent = RxList();
  RxList<Content> listContentCombo = RxList();
  final random = Random();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  RefreshController newsRefreshController =
      RefreshController(initialRefresh: false);
  RefreshController newFeedRefreshController =
      RefreshController(initialRefresh: false);
  bool isLoad = false;
  bool isLoadNew = false;
  bool isLoadMoreNewFeed = false;
  int totalPageNewFeed = 0;
  int? totalPageServices;
  int pageNumber = pageNumberValue;
  int pageSize = pageSizeValue;
  int pageNumberCombo = pageNumberValue;
  int pageSizeCombo = pageSizeValue;
  Rx<bool?>? isSerVicesLoading = false.obs;
  Rx<bool?>? isLoading = false.obs;
  Rx<bool?>? isLoadingNewFeed = false.obs;
  Rx<bool> isShowBook = false.obs;
  Rx<bool> isShowHotBeauty = false.obs;
  Rx<bool> isLoadingAccount = false.obs;
  RxString avatarValue = Application.imageNetworkPath(filedId: PreferenceUtils.getString(avatar)).obs;
  var argument = Get.arguments;
  RxInt isSelectedServices = 0.obs;
  RxList<ContentHis> historyList = RxList<ContentHis>();
  RxList<ContentHis> historyListHome = RxList<ContentHis>();
  RxList<Content> listHotBeauty = RxList();
  RefreshController refreshControllerHome =
      RefreshController(initialRefresh: false);
  RxString titleNew = enumMapNewsLabel[News.all]!.obs;


  // danh sách chủ đề trong chủ đề
  RxList<Obj> listNewGroup = RxList([
    Obj(
        title: enumMapNewsLabel[News.all],
        color: colorAll,
        status: true,
        type: enumMapNews[News.all]),
    Obj(
        title: enumMapNewsLabel[News.news],
        color: colorNews,
        status: false,
        type: enumMapNews[News.news]),
    Obj(
        title: enumMapNewsLabel[News.event],
        color: colorEvent,
        status: false,
        type: enumMapNews[News.event]),
    Obj(
        title: enumMapNewsLabel[News.document],
        color: colorIcon,
        status: false,
        type: enumMapNews[News.document]),
    Obj(
        title: enumMapNewsLabel[News.promotion],
        color: Colors.deepOrangeAccent,
        status: false,
        type: enumMapNews[News.promotion]),
  ]);
  RxList<Content> listAllNews = RxList<Content>();
  RxList<Content> listNews = RxList<Content>();
  RxList<Content> listEvent = RxList<Content>();
  RxList<Content> listDocument = RxList<Content>();
  RxList<Content> listPromotion = RxList<Content>();
  RxList<Content> listNewFeeds = RxList<Content>();
  RxList<File> fileUploadNewFeed = RxList<File>();
  int? typeNew;
  Rx<bool> isLoadingNews = false.obs;

  ///TODO lich hen
  RefreshController refreshControllerLichhen =
      RefreshController(initialRefresh: false);
  Rx<bool?> isLoadingLichhen = false.obs;
  RxInt isSelected = 0.obs;
  RxList<ContentHis> historyListLichhen = RxList<ContentHis>();
  RxList<ContentHis> newBookListLichhen = RxList<ContentHis>();

  // bool isLoad = false;
  // var argument = Get.arguments;
  TextEditingController reasonControllerNewBookEdt = TextEditingController();
  TextEditingController reasonControllerHistoryBookEdt =
      TextEditingController();
  RxList<File?> imageFile = <File>[].obs;
  /// list user chat
  RxList<DataUserChat> listUser = RxList<DataUserChat>();
  RxList<DataRecentlyChat> listRecentlyChat = RxList<DataRecentlyChat>();

  /// Tài khoản
  RxnString? languageNote = RxnString('Tiếng Việt');
  late Rx<bool> isLogin;
  late StreamSubscription streamSubscription;
  late ConnectivityResult result;
  Rx<bool> isDeviceConnected = false.obs;
  final Connectivity connectivity = Connectivity();

  /// TODO bảng tin
  RxList<String> filePickers = RxList<String>([]);
  int pageNumberNewFeed = pageNumberValue;

  @override
  void onInit() {
    checkHasInternet();
    startStreaming();
    isLogin = checkHasLogin().value.obs;
    isSelectedServices.value = 0;
    newFeeds.refresh();
    // TODO: implement onInit

    if (checkHasLogin().value) {
      getHistoryBooking();
      getHistoryBookingHome();
      getNotification();
      getNewFeeds();
      /// TODO get danh sách user
      getAllUser();
      ///TODO get danh sach nhom chat
      getAllRecentlyChat();
    }
    getMessageFcm();
    callServicesApi();
    callComboApi();
    getHotNew();
    // call api goị danh sách tin tức
    getNewShort();
    callBeautyHot();
    connectionWebSocket();

    super.onInit();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (argument != null && argument == true) {
        successNotificationNoAppBar(
            context: Get.context!, message: "Tài khoản đã được tạo");
      }
    });
    if (argument.toString() == Routers.post) {
      ///TODo phai sua tab tin tuc chuyen thanh man hinh
      indexPage.value = 2;
    }
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
  }
  /// check internet
  startStreaming() {
    streamSubscription = Connectivity().onConnectivityChanged.listen((event) {
      checkHasInternet();
    });
  }
  connectionWebSocket() async{
    // Create a WebSocket client.
    final socket = WebSocket(Uri.parse('wss://api.chatengine.io/person/?publicKey=$publicKeyChat&username=${PreferenceUtils.getString(myUserName)}&secret=$defaultConnectChat'));
    // socket.connection.listen((state) => print('state: "$state"'));
    // Listen for incoming messages.
    socket.messages.listen((message) {
      // print('message: "$message"');
      // Send a message to the server.
      socket.send('ping');
    });
  }
  showDialogBox() {
    showDialog(
        context: Get.context!,
        barrierDismissible: false,
        builder: (_) {
          return CupertinoAlertDialog(
            title: const Text('Không có internet'),
            content: const Text("Vui lòng kiểm tra kết nối internet"),
            actions: [
              CupertinoButton.filled(
                onPressed: () {
                  Navigator.pop(Get.context!);
                  checkHasInternet();
                },
                borderRadius: BorderRadius.zero,
                child: const Text('Thử lại'),
              )
            ],
          );
        });
  }

  Future<void> checkHasInternet() async {
    result = await Connectivity().checkConnectivity();
    if (result != ConnectivityResult.none) {
      isDeviceConnected = true.obs;
    } else {
      isDeviceConnected = false.obs;
      Get.back();
      showDialogBox();
    }
  }

  getMessageFcm() {
    final FirebaseMessaging fcm = FirebaseMessaging.instance;
    fcm.getInitialMessage().then((RemoteMessage? message) async {
      RemoteNotification? notification = message?.notification;
      FcmBody? fcmBody = FcmBody.fromJson(notification);
      Map<String, dynamic> json = await jsonDecode(fcmBody.body ?? '');
      Body body = Body.fromJson(json);
      FlutterAppBadger.updateBadgeCount(body.totalNotSeen ?? 0);
      countNotifyHome.value = body.totalNotSeen ?? 0;
    });
  }

  /// check support biometric
  // Future<void> checkSupportBiometric() async {
  //   bool isBiometricSupported = await auth.isDeviceSupported();
  //
  //   ///Now, verify whether biometric authentication is accessible from the app, check it using:
  // }

  ///TODO loading loadmore servicesScreen
  void onRefreshService() async {
    // monitor network fetch
    pageNumber = pageNumberValue;
    isLoad = false;
    isSelectedServices.value == 0 ? callServicesApi() : callComboApi();
    refreshController.refreshCompleted();
  }

  void onLoadingService() async {
    // monitor network fetch
    isLoad = true;
    if (pageNumber < totalPageServices!) {
      isLoad = true;
      isSelectedServices.value == 0 ? callServicesApi() : callComboApi();
    } else {
      refreshController.loadComplete();
    }
  }

  ///TODO call api tab Index = 1
  callServicesApi() async {
    try {
      if (!isLoad) {
        pageNumber = pageNumberValue;
        listContent.clear();
        refreshController.refreshCompleted();
        isSerVicesLoading?.value = true;
      } else {
        pageNumber = ++pageNumber;
      }
      refreshController.loadComplete();
      GetServicesModel dataRequest = GetServicesModel();
      dataRequest.keySearch = keySearch;
      dataRequest.pageSize = pageSizeValue;
      dataRequest.pageNumber = pageNumber;
      final request = await apiManager?.getBeautyServices(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      ResServicesDto resServiceDto = ResServicesDto.fromJson(result.data);
      totalPageServices = resServiceDto.totalPages;

      resServiceDto.content?.forEach((element) {
        listContent.add(element);
      });
      isSerVicesLoading?.value = false;
    } catch (ex) {
      isSerVicesLoading?.value = false;
    }
  }

  // lay notify o man home
  getNotification() async {
    try {
      final requestData = {
        'pageNumber': pageNumberValue,
        'pageSize': 200,
      };
      final request = await apiManager?.getListNotification(data: requestData);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        NotificationDto notificationDto = NotificationDto.fromJson(result.data);
        addBadge(totalUnseen: notificationDto.totalNotSeen);
        countNotifyHome.value = notificationDto.totalNotSeen ?? 0;
      } else {
        BaseErr err = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(context: Get.context!, message: err.message);
      }
    } catch (ex) {
      errNotificationNoAppBar(
        context: Get.context!,
      );
    }
  }

  callComboApi() async {
    try {
      if (!isLoad) {
        pageNumberCombo = pageNumberValue;
        listContentCombo.clear();
        refreshController.refreshCompleted();
        isSerVicesLoading?.value = true;
      } else {
        if (pageNumberCombo <= totalPageServices!) {
          pageNumber = ++pageNumber;
        }
      }
      refreshController.loadComplete();
      GetServicesModel dataRequest = GetServicesModel();
      dataRequest.keySearch = keySearch;
      dataRequest.pageSize = pageSize;
      dataRequest.pageNumber = pageNumber;
      final request = await apiManager?.getComboList(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      ResServicesDto resServiceDto = ResServicesDto.fromJson(result.data);
      totalPageServices = resServiceDto.totalPages;
      if (result.code == successful || result.code == create) {
        resServiceDto.content?.forEach((element) {
          listContentCombo.add(element);
        });
        isSerVicesLoading?.value = false;
      } else {
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(
            context: Get.context!,
            description: baseErr.error,
            message: baseErr.message);
        isSerVicesLoading?.value = false;
      }
    } catch (ex) {
      isSerVicesLoading?.value = false;
      errNotificationNoAppBar(
        context: Get.context!,
      );
    }
  }

  getHistoryBooking() async {
    historyList.clear();
    try {
      final dataRequest = {
        "customerCode": PreferenceUtils.getString(customerCode),
        "pageNumber": pageNumber,
        "pageSize": 1,
        "startDate": dateToString(
            dateTime: DateTime.now(),
            dateFormatTime: dOEMap[DateFormatCustom.HH_UTC]),
      };
      final request = await apiManager?.getHistoryBooking(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      HistoryBookingDto historyBookingdto =
          HistoryBookingDto.fromJson(result.data);
      if (result.code == successful || result.code == create) {
        historyBookingdto.content?.forEach((element) {
          historyList.add(element);
        });
        if (historyList.isEmpty) {
          isShowBook.value = false;
        } else {
          isShowBook.value = true;
        }
      } else {
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotification(context: Get.context!, message: baseErr.message);
      }
    } catch (ex) {
      isShowBook.value = false;
      errNotification(context: Get.context!);
    }
  }

  getHistoryBookingHome() async {
    historyListHome.clear();
    try {
      isLoading?.value = true;
      final dataRequest = {
        "customerCode": PreferenceUtils.getString(customerCode),
        "pageNumber": pageNumber,
        "pageSize": 1,
        "startTime": DateTime.now().toIso8601String(),
        "isNearest": true,
      };
      final request = await apiManager?.getHistoryBooking(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      HistoryBookingDto historyBookingdto =
          HistoryBookingDto.fromJson(result.data);
      if (result.code == successful || result.code == create) {
        isLoading?.value = false;
        historyBookingdto.content?.forEach((element) {
          historyListHome.add(element);
        });
        if (historyListHome.isEmpty) {
          isShowBook.value = false;
        } else {
          isShowBook.value = true;
        }
      } else {
        isShowBook.value = false;
        isLoading?.value = false;
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(
            context: Get.context!, message: baseErr.message);
      }
    } catch (ex) {
      isShowBook.value = false;
      isLoading?.value = false;
      errNotificationNoAppBar(context: Get.context!);
    }
  }

  // lấy danh sách gợi ý
  callBeautyHot() async {
    listHotBeauty.clear();
    try {
      isLoading?.value = true;
      GetServicesModel dataRequest = GetServicesModel();
      dataRequest.pageSize = 6;
      dataRequest.pageNumber = pageNumber;
      final request = await apiManager?.getHostBeautyApi(dataRequest);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading?.value = false;
        ResServicesDto resServiceDto = ResServicesDto.fromJson(result.data);
        resServiceDto.content?.forEach((element) {
          listHotBeauty.add(element);
        });
        if (listHotBeauty.isEmpty) {
          isShowHotBeauty.value = false;
        } else {
          isShowHotBeauty.value = true;
        }
      } else {
        isLoading?.value = false;
        BaseErr baseErr = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(
            context: Get.context!, message: baseErr.message);
      }
    } catch (ex) {
      isLoading?.value = false;
      errNotificationNoAppBar(context: Get.context!);
    }
  }

  onRefresherHome() {
    if (checkHasLogin().value) {
      getHistoryBookingHome();
    }
    callBeautyHot();
    getHotNew();
    refreshControllerHome.refreshCompleted();
  }

  onLoadHome() {
    refreshControllerHome.loadComplete();
  }

  onRefresherNew() {
    isLoadNew = false;
    newsRefreshController.refreshCompleted();
    getNewShort();
  }

  onLoadNew() {
    newsRefreshController.loadComplete();
  }

  void addBadge({int? totalUnseen}) {
    FlutterAppBadger.updateBadgeCount(totalUnseen ?? 0);
  }

  cancelBooking(int id) async {
    try {
      isLoading?.value = true;
      final cancelRequestDto = {
        "cancelReason": reasonControllerEdt.text.trim(),
        "id": id
      };
      final request = await apiManager?.cancelBooking(data: cancelRequestDto);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful) {
        isLoading?.value = false;
        successNotificationNoAppBar(
            context: Get.context!, message: 'Lịch hẹn đã được hủy');
        getHistoryBookingHome();
      } else {
        isLoading?.value = false;
        BaseErr err = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(context: Get.context!, message: err.message);
      }
    } catch (ex) {
      isLoading?.value = false;
      errNotificationNoAppBar(context: Get.context!);
    }
  }

  checkLogin() {
    isLogin.value = checkHasLogin().value;
  }

  getHotNew() async {
    try {
      hotNew.clear();
      imgList.clear();
      isLoading?.value = true;
      final cancelRequestDto = {
        "type": null,
        "pageNumber": pageNumber,
        "pageSize": 5,
        "activeStatus": 1,
      };
      final request = await apiManager?.getHotNews(data: cancelRequestDto);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading?.value = false;
        NewDto newDto = NewDto.fromJson(result.data);
        for (var element in newDto.content!) {
          hotNew.add(element);
          imgList.add(ImagesRender.circleNetWorkImage(
              Application.imageNetworkPath(
                  filedId: element.imageStores?.first.fileId ?? ""),
              width: Get.width,
              boxFit: BoxFit.fill));
        }
      } else {
        isLoading?.value = false;
        BaseErr err = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(context: Get.context!, message: err.message);
      }
    } catch (ex) {
      UtilLogger.log(ex.toString());
    }
  }

  getNewShort() async {
    isLoadingNews.value = true;
    if (titleNew.value == enumMapNewsLabel[News.all]) {
      typeNew = enumMapNews[News.all];
      listAllNews.clear();
    }
    if (titleNew.value == enumMapNewsLabel[News.news]) {
      typeNew = enumMapNews[News.news];
      listNews.clear();
    }
    if (titleNew.value == enumMapNewsLabel[News.event]) {
      typeNew = enumMapNews[News.event];
      listEvent.clear();
    }
    if (titleNew.value == enumMapNewsLabel[News.document]) {
      typeNew = enumMapNews[News.document];
      listDocument.clear();
    }
    if (titleNew.value == enumMapNewsLabel[News.promotion]) {
      typeNew = enumMapNews[News.promotion];
      listPromotion.clear();
    }
    try {
      isLoading?.value = true;
      final cancelRequestDto = {
        "type": typeNew,
        "pageNumber": pageNumberValue,
        "pageSize": 5,
        "keySearch": newSearchControllerEdt.text.trim(),
        "activeStatus": 1,
      };
      final request = await apiManager?.getHotNews(data: cancelRequestDto);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        isLoading?.value = false;
        NewDto newDto = NewDto.fromJson(result.data);
        for (var element in newDto.content!) {
          if (titleNew.value == enumMapNewsLabel[News.all]) {
            listAllNews.add(element);
          }
          if (titleNew.value == enumMapNewsLabel[News.news]) {
            listNews.add(element);
          }
          if (titleNew.value == enumMapNewsLabel[News.event]) {
            listEvent.add(element);
          }
          if (titleNew.value == enumMapNewsLabel[News.document]) {
            listDocument.add(element);
          }
          if (titleNew.value == enumMapNewsLabel[News.promotion]) {
            listPromotion.add(element);
          }
        }
        isLoadingNews.value = false;
      } else {
        isLoading?.value = false;
        BaseErr err = BaseErr.fromJson(result.data);
        errNotificationNoAppBar(context: Get.context!, message: err.message);
        isLoadingNews.value = false;
      }
    } catch (ex) {
      errNotificationNoAppBar(context: Get.context!);
      isLoadingNews.value = false;
    }
  }

  getUnSubNotify() async {
    // check keyAccesstoken
    if (PreferenceUtils.getString(keyAccessToken) != null) {
      final object = {
        "tokens": ["${PreferenceUtils.getString(tokenFcm)}"]
      };
      final request = await apiManager?.unsubscribeNotify(data: object);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        if (kDebugMode) {
          print('Đã huy ký FCM');
        }
      }
    }
  }

  updateAvatar(File? fileImage) async {
    try {
      isLoadingAccount.value = true;
      String fileName = fileImage!.path.split("/").last;
      String filePath = fileImage.path;
      final formData = FormData.fromMap(<String, dynamic>{
        "images": [
          await MultipartFile.fromFile(
            filePath,
            filename: fileName,
          ),
        ],
      });
      formData.fields.addAll([
        MapEntry("id", PreferenceUtils.getString(idAccount).toString()),
        MapEntry("fullName", PreferenceUtils.getString(userKey) ?? ""),
        // MapEntry("organizationBranchId", dataRequestServices.organizationBranchId.toString()),
      ]);
      final request = await apiManager?.updateAvatar(data: formData);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        checkExpired();
        isLoadingAccount.value = false;
        successNotificationNoAppBar(
            context: Get.context!, message: "Cập nhật ảnh thành công");
      } else {
        BaseErr baseErr = BaseErr.fromJson(result.data);
        isLoadingAccount.value = false;
        errNotificationNoAppBar(
            context: Get.context!, message: baseErr.message ?? baseErr.data);
      }
    } catch (ex) {
      errNotificationNoAppBar(context: Get.context!);
      isLoadingAccount.value = false;
    }
  }

  checkExpired() async {
    try {
      final request = await apiManager?.getExpired();
      final result = BaseApiResponse.fromJson(request);
      if (result.code != null && result.code != expiredToken) {
        final user = UserData.fromJson(result.data);
        avatarValue.value = "${user.imageStore?.first.fileId}";
        PreferenceUtils.setString(avatar, user.imageStore?.first.fileId ?? '');
      }
    } catch (ex) {
      UtilLogger.log(ex.toString());
    }
  }

  void removeBadge() {
    FlutterAppBadger.removeBadge();
  }

  onRefreshNewFeed() {
    newFeedRefreshController.refreshCompleted();
    isLoadMoreNewFeed = false;
    isRefresher.value = true;
    getNewFeeds();
  }

  onLoadMoreNewFeed() {
    isLoadMoreNewFeed = true;
    getNewFeeds();
  }

  getNewFeeds() async {
    try {
      if (!isLoadMoreNewFeed) {
        isLoadingNewFeed?.value = true;
        pageNumberNewFeed = pageNumberValue;
        newFeeds.clear();
      } else {
        if (pageNumberNewFeed >= totalPageNewFeed) {
          newFeedRefreshController.loadComplete();
          return;
        }
        pageNumberNewFeed = pageNumberNewFeed + 1;
      }
      final body = {
        "pageNumber": pageNumberNewFeed,
        "pageSize": pageSizeValue,
      };
      final request = await apiManager?.getNewFeed(data: body);
      final response = BaseApiResponse.fromJson(request);
      if (response.code == create || response.code == successful) {
        NewFeedDto newFeedDto = NewFeedDto.fromJson(response.data);
        totalPageNewFeed = newFeedDto.totalPages ?? 0;
        newFeedDto.content?.forEach((element) {
          newFeeds.add(element);
        });
        isLoadingNewFeed?.value = false;
        newFeedRefreshController.loadComplete();
      } else {
        isLoadingNewFeed?.value = false;
        newFeedRefreshController.loadComplete();
        BaseErr baseErr = BaseErr.fromJson(response.data);
        errNotificationNoAppBar(
          context: Get.context!,
          message: baseErr.message,
        );
      }
    } catch (ex) {
      isLoadingNewFeed?.value = false;
      newFeedRefreshController.loadComplete();
      errNotificationNoAppBar(context: Get.context!);
    }
  }
  delNewFeed({int? id})async {
    try {
      final request = await apiManager?.delPost(id: id);
      final response = BaseApiResponse.fromJson(request);
      if (response.code == successful) {
        successNotificationNoAppBar(context: Get.context!,message: 'Đã xóa bài viết');
        newFeeds.removeWhere((element) => element.id == id);
        newFeeds.refresh();
      }
      else{
        BaseErr baseErr = BaseErr.fromJson(response.data);
        errNotificationNoAppBar(context: Get.context!,message: baseErr.message);
      }
    }
    catch(ex){
      errNotificationNoAppBar(context: Get.context!);
    }
  }
  postLikeNewFeed({int? newsFeedId,int? status}) async{
  final body = [
      {
        "newsFeedId": newsFeedId,
        "status": status
      }
    ];
    final request = await apiManager?.likeOrUnlike(data: body);
    final response = BaseApiResponse.fromJson(request);
    if(response.code == successful){
      print('like thanh cong');
    }
  }
  getAllUser() async{
    try{
      listUser.clear();
     final request = await apiManager?.getAllUserChat();
     final response = BaseApiResponse.fromJson(request);
     if(response.code == successful){
       UserChatDto userChatDto = UserChatDto.fromJson(response.data);
       userChatDto.data?.forEach((element) {
          listUser.add(element);
       });
     }
     else{
       BaseErr baseErr = BaseErr.fromJson(request);
       errNotificationNoAppBar(context: Get.context!,message:baseErr.message);
     }
    }
    catch(ex){
      errNotificationNoAppBar(context: Get.context!);
    }
  }
  getAllRecentlyChat() async{
    try{
      listRecentlyChat.clear();
     final request = await apiManager?.getRecentlyChat();
     final response = BaseApiResponse.fromJson(request);
     if(response.code == successful){
       RecentlyChatDto recentlyChatDto = RecentlyChatDto.fromJson(response.data);
       recentlyChatDto.data?.forEach((element) {
          listRecentlyChat.add(element);
       });
     }
     else{
       BaseErr baseErr = BaseErr.fromJson(request);
       errNotificationNoAppBar(context: Get.context!,message:baseErr.message);
     }
    }
    catch(ex){
      errNotificationNoAppBar(context: Get.context!);
    }
  }
}

class Obj {
  int? index;
  String? title;
  Color? colorText;
  Color? color;
  bool? status;
  int? type;

  Obj(
      {this.index,
      this.title,
      this.color,
      this.colorText,
      this.status,
      this.type});
}

class ItemCard {
  Widget? widget;
  Widget content;
  RxBool? val;

  ItemCard({this.widget, required this.content, this.val});
}
