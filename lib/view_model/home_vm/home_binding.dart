import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'home_controller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController(Get.find<ApiManager>()),fenix: true);
  }
}
