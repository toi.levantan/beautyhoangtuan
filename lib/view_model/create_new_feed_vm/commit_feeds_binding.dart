import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';

import 'commit_feed_controllers.dart';

class CommitFeedBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<CommitFeedController>(() => CommitFeedController(Get.find<ApiManager>()),fenix: true);
  }
}