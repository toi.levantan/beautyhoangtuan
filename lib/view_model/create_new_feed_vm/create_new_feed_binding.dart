import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/create_new_feed_vm/create_new_feed_controller.dart';

class CreateNewFeedBinding implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut<CreateNewFeedController>(() => CreateNewFeedController(Get.find<ApiManager>()),fenix: true);
  }
}