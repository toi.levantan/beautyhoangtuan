import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get/get_rx/get_rx.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/new_feeds/comment_feedback_dto.dart';
import 'package:myhoangtuan/model/new_feeds/new_feed_dto.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CommitFeedController extends GetxController{
  ApiManager? apiManager;
  CommitFeedController(this.apiManager);
  ContentFeed? contentFeed = Get.arguments;
  RefreshController refreshController = RefreshController(initialRefresh: false);
  RxList<BeautyAttachments> beautyAttachments = RxList();
  RxList<String> filePickers = RxList<String>();
   TextEditingController commentEdtController = TextEditingController();
  Rx<String> avatarValue = RxString('');
  Rx<bool> isLoading = false.obs;
  RxList<ContentFeedBack> comments = RxList<ContentFeedBack>();
  int pageNumber = pageNumberValue;
  int pageSize = pageSizeValue;
  int totalPage = 0;
  bool isLoadMore = false;
  int? parentId;
  String? responseUsername;
  @override
  void onInit() {
    // TODO: implement onInit
    contentFeed?.beautyAttachments?.forEach((element) {
      beautyAttachments.add(element);
    });
    loadCommit();
    super.onInit();
  }
  onRefresher(){
    refreshController.refreshCompleted();
    isLoadMore = false;
    loadCommit();
  }
  onLoad(){
    refreshController.loadComplete();
    isLoadMore = true;
    loadCommit();
  }
  /// TODO parendId laf id cha
  /// TODO is Response la id binh luan phan hoi
  createCommit({String? image, String? content,int? parentId, int? idResponse})async{
    try{
       FormData formData = FormData();
       formData.fields.add(MapEntry('content', content ??''));
       image != null && image.isNotEmpty ?formData.files.add(MapEntry('images',MultipartFile.fromFileSync(image,filename: image.split("/").last))): null;
       final request = await apiManager?.createCommit(newsFeedId: contentFeed?.id ??0,data: formData,parentId: parentId);
       final response = BaseApiResponse.fromJson(request);
       if(response.code == successful){
           onRefresher();
       }
       else{}
    }
    catch(ex){
      errNotification(context: Get.context!);
    }
  }
  updateCommit(){}

  loadCommit() async{
    try{
      if(!isLoadMore){
        comments.clear();
        pageNumber = pageNumberValue;
      }
      else{
        if(pageNumber >= totalPage){
          pageNumber = totalPage;
        }
        else{
          pageNumber = pageNumber+1;
        }
      }
      isLoading.value = true;
      final body = {
        "beautyNewsFeedId": contentFeed?.id,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
      };
      final request = await apiManager?.getListCommit(data: body);
      final response = BaseApiResponse.fromJson(request);
      if(response.code == successful) {
        isLoading.value = false;
        CommentFeedbackDto commentFeedbackDto = CommentFeedbackDto.fromJson(response.data);
        totalPage = commentFeedbackDto.totalPages ??0;
        commentFeedbackDto.content?.forEach((element) {
          comments.add(element);
        });
      }
      else{
        BaseErr baseErr = BaseErr.fromJson(response.data);
        errNotificationNoAppBar(context: Get.context!,message: baseErr.message);
        isLoading.value = false;
      }
    }
    catch(ex){
      isLoading.value = false;
    }
  }
}