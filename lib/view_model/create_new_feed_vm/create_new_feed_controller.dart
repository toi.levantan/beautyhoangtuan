import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get/state_manager.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/model/new_feeds/MapVideoThumbnailDto.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:video_compress/video_compress.dart';

class CreateNewFeedController extends GetxController {
  ApiManager? apiManager;
  RxList<File?> filePickers = RxList<File>();
  List<File?> fileVideoThumbnail = [];
  List<MapVideoThumbnailDto?> mapVideoThumbs = [];
  FilePickerResult? result;
  String? destinationPath = "";
  Rx<String?> desFile = RxnString();
  Rx<String?> displayedFile = RxnString();
  Rx<int>? durationTime = 0.obs;
  String? failureMessage;
  String? filePath;
  bool? isVideoCompressed = false;
  TextEditingController textEditingController = TextEditingController();
  RxString ? textContentCheck =''.obs;
  RxBool isLoading = false.obs;
  // files medias đẩy cho api
  RxList<File?> medias = RxList<File>();
  int maxSizeInBytes = 300 * 1024 * 1024; // Giới hạn dung lượng tệp là 10 MB
  int currentSize = 0;
  Rx<bool> isPickFile = false.obs;
  Rx<int> percent = 100.obs;
  Timer? timerCheck;
  CreateNewFeedController(this.apiManager);


// Pick multiple images and videos.
  pickFileMedia() async {
    try {
      final response = await FilePicker.platform.pickFiles(
        allowMultiple: true, type: FileType.image);
      if (response != null) {
        percent.value = 0;
        for (int i = 0; i <= response.files.length -1; i++) {
          currentSize += response.files[i].size;
        }
          if (currentSize >= maxSizeInBytes) {
            if (timerCheck != null) {
              timerCheck?.cancel();
            }
            warningNotification(context: Get.context!,message: 'Giới hạn dung lượng tệp là 300Mb');
            filePickers.clear();
            medias.clear();
            currentSize = 0;
            response.paths.clear();
          } else {
            currentSize =0;
            for (int i = 0; i <= response.paths.length - 1; i++) {
              File file = File(response.paths[i]?? "");
              filePickers.add(file);
              medias.add(file);
            }
          }
        }
    }catch(ex){
      errNotification(context: Get.context!,message: '$ex');
    }
  }

  pickFileVideo() async {
    try {
      result = await FilePicker.platform.pickFiles(
          allowMultiple: true,
          type: FileType.video,
          allowCompression: true,
          onFileLoading: (status) {
            if (status == FilePickerStatus.picking) {
              isPickFile.value = true;
              if (percent.value == 1) {
                return;
              }
              percent.value = 0;
              isPickFile.value = true;
              timerCheck = Timer.periodic(const Duration(milliseconds: 600), (timer) {
                    timer.tick;
                    percent.value = percent.value + 1;
                    if (percent.value >= 100) {
                      percent.value = 100;
                      timer.cancel();
                    }});
            }});
      if (result != null && result!.files.isNotEmpty ) {
        for (int i = 0; i <= result!.files.length - 1; i++) {
          currentSize += result!.files[i].size;
        }
        if(currentSize > maxSizeInBytes){
          //check file quas lớn;
          fileTooLarge();
        }
        else{
          for (var element in result!.paths) {
            compressFile(sourcePath: element);
          }
        }
      }
    }catch(ex){
      errNotification(context: Get.context!,message: '$ex');
    }
  }
  fileTooLarge(){
    percent.value = 100;
    timerCheck?.cancel();
    warningNotification(context: Get.context!, message: 'File bạn đã chọn đã vượt quá hạn dung lượng(300Mb). Vui lòng thử lại');
    isPickFile.value = false;
    filePickers.clear();
    medias.clear();
    currentSize = 0;
  }
  compressFile({String? sourcePath}) async {
    try {
      final response = await VideoCompress.compressVideo(
        sourcePath ?? "",
        quality: VideoQuality.Res640x480Quality,
      );
      desFile.value = response?.path ?? "";
      displayedFile = desFile;
      isVideoCompressed = true;
        File file = File(response?.path ??'');
        File? fileThumb = await VideoCompress.getFileThumbnail(file.path);
        filePickers.add(fileThumb);
        fileVideoThumbnail.add(fileThumb);
        MapVideoThumbnailDto mapVideoThumbnailDto = MapVideoThumbnailDto();
        mapVideoThumbnailDto.thumbnailName = fileThumb.path.split("/").last;
        mapVideoThumbnailDto.videoName = response?.path?.split("/").last ??"";
        mapVideoThumbs.add(mapVideoThumbnailDto);
        medias.add(file);

      /// TODO nen xong la 100 %
      percent.value = 100;
      currentSize = 0;
      timerCheck?.cancel();
      isPickFile.value = false;
    }
    catch(ex){
      errNotificationNoAppBar(context: Get.context!, message: 'Có lỗi trong quá trình nén video');
    }
  }
  getFileSize(String filepath, int decimals) async {
    var file = File(filepath);
    int bytes = await file.length();
    if (bytes <= 0) return "0 B";
    const suffixes = ["B", "KB", "MB", "GB", "TB", "PB"];
    var i = (log(bytes) / log(1024)).floor();
    return '${(bytes / pow(1024, i)).toStringAsFixed(decimals)} ${suffixes[i]}';
  }
  createNewFeed() async{
    try {
      isLoading.value = true;
      FormData formData = FormData();
      formData.fields.add(MapEntry('content', textEditingController.text.trim()));
      formData.fields.add(MapEntry('mapVideoThumbnail', json.encode(mapVideoThumbs)));
      if (medias.isNotEmpty) {
        for (int i = 0; i <= medias.length-1; i++) {
          if(Application.isTypeVideo(srcPath: medias[i]?.path).value){
           formData.files.add(MapEntry("videos", MultipartFile.fromFileSync(medias[i]?.path ??'', filename: medias[i]?.path.split("/").last)));
         }
         else{
           formData.files.add(MapEntry("images", MultipartFile.fromFileSync(medias[i]?.path ??'', filename: medias[i]?.path)));
         }
        }
        if(fileVideoThumbnail.isNotEmpty) {
          for (int i = 0; i <= fileVideoThumbnail.length - 1; i++) {
            formData.files.add(MapEntry('thumbnails', MultipartFile.fromFileSync(fileVideoThumbnail[i]?.path??'',filename: fileVideoThumbnail[i]?.path.split("/").last
            )));
          }
        }
      }
      final request = await apiManager?.createNewFeed(data: formData);
      final response = BaseApiResponse.fromJson(request);
      if (response.code == successful) {
         isLoading.value= true;
         Get.back(result: true);
      }
      else{
        isLoading.value= false;
        BaseErr baseErr = BaseErr.fromJson(response.data);
        errNotificationNoAppBar(context: Get.context!, message: baseErr.message);
      }
    }catch(ex){
      isLoading.value= false;
      errNotificationNoAppBar(context: Get.context!);
    }
  }
  @override
  void dispose() {
    /// TODO: implement dispose
    timerCheck?.cancel();
    dispose();
    super.dispose();
  }
}
