import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:video_player/video_player.dart';
import '../../model/new_feeds/new_feed_dto.dart';

class VideoPreviewController extends GetxController{
  late VideoPlayerController videoPlayerController;
  BeautyAttachments? beautyAttachments = Get.arguments;
  Rx<bool> isPlay = true.obs;
  @override
  void onInit() {
    videoPlayerController = VideoPlayerController.networkUrl(Uri.parse(Application.imageNetworkPath(filedId: "${beautyAttachments?.fileId}.mp4")))..initialize().then((_) => {
       videoPlayerController.play()
    });

    // TODO: implement onInit
    super.onInit();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  void onClose() {
    // TODO: implement onClose
    videoPlayerController.pause();
    videoPlayerController.dispose();
    super.onClose();
  }
}