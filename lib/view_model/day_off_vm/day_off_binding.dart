import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/day_off_vm/day_off_controller.dart';

class DayOffBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<DayOffController>(() => DayOffController(Get.find<ApiManager>()),fenix: true);
  }

}