import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';

import '../../model/feedback/feedback_dto.dart';

class DayOffController extends GetxController with GetTickerProviderStateMixin{
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'Đăng ký'.toUpperCase()),
    Tab(text: 'Lịch sử'.toUpperCase()),
  ];
  late TabController tabController;
  ApiManager? apiManager;
  DayOffController(this.apiManager);
  Rx<String?>? dichVuErr = RxnString();
  List<TextEditingController> textEdtControllers = [];
  RxList listContent = RxList<Content>();

  @override
  void onInit() {
    // TODO: implement onInit
    tabController = TabController(length: myTabs.length, vsync: this);
    for(int i =0; i<=4 ;i++){
      textEdtControllers.add(TextEditingController());
    }
    super.onInit();
  }
}