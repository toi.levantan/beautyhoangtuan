import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import '../../core/repository/api_manager.dart';
import '../../core/routes/routers.dart';
import '../../model/base_api.dart';
import '../../model/login/access_dto.dart';
import '../../model/login/user_data.dart';
import '../../utils/app/application.dart';
import '../../utils/app/share_pref.dart';
import '../../utils/app/size_app.dart';
import '../../utils/path/const_key.dart';
import '../../utils/session/date_formatter.dart';
import '../../utils/session/enum_session.dart';
import '../../utils/widget/snack_bar.dart';

class IntroController extends GetxController{
  var argument = Get.arguments;
  PersistentTabController? persistentTabController;
  RxInt indexPage = 0.obs;
  ApiManager? apiManager;
  Rx<bool> hidePass = true.obs;
  Rx<String?>? errNumberPhone = RxnString();
  Rx<String?>? errPass = RxnString();
  Rx<bool> rememberAccountEmp = true.obs;
  Rx<bool> isLoading = false.obs;
  int? statusLoginLocation;
  Rx<bool> isOk = false.obs;
  List<TextEditingController> textEdits = [
    TextEditingController(),
    TextEditingController(),
  ];
  IntroController(this.apiManager);

  @override
  void onInit() {
    // TODO: implement onInit
    persistentTabController = PersistentTabController(initialIndex: 0);
    textEdits.first.text = PreferenceUtils.getString(userName) ?? '';
    if (PreferenceUtils.getBool(rememberKeyEmp) != null &&
        PreferenceUtils.getBool(rememberKeyEmp) == true) {
      textEdits.last.text = PreferenceUtils.getString(pwdKeyEmp) ?? '';
    }
    rememberAccountEmp.value = PreferenceUtils.getBool(rememberKeyEmp) ?? false;
    checkShowAction();
    super.onInit();
  }
  login() async {
    isLoading.value = true;
    // đã map to json rồi
    final object = {
      "grant_type": "password",
      "scope": "read",
      "username": textEdits.first.text.trim(),
      "password": textEdits.last.text.trim(),
    };
    try {
      final request = await apiManager?.login(object);
      final result = BaseApiResponse.fromJson(request);
      final accessToken = AccessDto.fromJson(result.data);
      if (result.code == successful || result.code == create) {
        await PreferenceUtils.setString(keyAccessToken, accessToken.accessToken.toString());
        await apiManager?.updateToken();
        isLoading.value = false;
        await PreferenceUtils.setString(userName, textEdits.first.text.trim().toString());
        await PreferenceUtils.setString(customerCode, accessToken.employeeCode);
        /// TODO lưu tài khoản
        if(rememberAccountEmp.value){
          PreferenceUtils.setString(userName, textEdits.first.text.trim());
          PreferenceUtils.setString(pwdKeyEmp, textEdits.last.text.trim());
          PreferenceUtils.setBool(rememberKeyEmp, rememberAccountEmp.value);
        }
        else{
          // không lưu thì xóa đi
          PreferenceUtils.remove(pwdKey);
          PreferenceUtils.setBool(rememberKeyEmp, rememberAccountEmp.value);
        }
        await checkExpired();
      } else {
        isLoading.value = false;
        if (result.code == expiredToken) {
          errNotification(
              context: Get.context!, message: 'Tài khoản đăng nhập hoặc mật khẩu không chính xác!');
        }
        else {
          BaseErrLogin baseErr = BaseErrLogin.fromJson(result.data);
          errNotification(
              context: Get.context!, message: baseErr.error_description);
        }
      }
    } catch (ex) {
      errNotification(context: Get.context!);
      isLoading.value = false;
    }
  }
  validateUserName({String? userName}) {
    errNumberPhone?.value = Application.checkEmptyErr(param: userName);
    if (errNumberPhone?.value == null) {
      return isOk.value = true;
    }
    return isOk.value = true;
  }

  validatePass({String? password}) {
    errPass?.value = Application.checkEmptyErr(param: password);
    if (errPass?.value != null) {
      return isOk.value = true;
    }
    return isOk.value = false;
  }
  Future<void> checkExpired() async {
    try {
      final request = await apiManager?.checkTypeUser();
      final result = BaseApiResponse.fromJson(request);
      if (result.code != null && result.code == expiredToken) {
        errNotificationNoAppBar(context: Get.context!,message: 'Tài khoản đăng nhập hoặc mật khẩu không chính xác!');
      }
      else {
        UserData userData = UserData.fromJson(result.data);
         if(userData.userType != STAFF){
           await PreferenceUtils.remove(keyAccessToken);
           await apiManager?.updateToken();
           return warningNotification(
               context: Get.context!,
               message: "Tài khoản này không phải là tài khoản nhân sự. Vui lòng đăng nhập với tư cách là nhân sự");
         }
        if(userData.position == null && userData.position == ''){
          userData.positionShow = departmentType(userData.departmentType ??'');
          PreferenceUtils.setString(position, departmentType(userData.departmentType ??'')!);
        }
        else{
          userData.positionShow = positionType(userData.position ??'');
          PreferenceUtils.setString(position, positionType(userData.position ??'')!);
        }
        PreferenceUtils.setString(userKey, userData.fullName ?? '');
        PreferenceUtils.setString(phoneData, userData.phoneNumber ?? '');
        PreferenceUtils.setString(avatar,userData.imageStore!.isNotEmpty ? userData.imageStore?.first.fileId : "");
        PreferenceUtils.setString(address, userData.fullAddress ?? 'Chưa xác định');
        PreferenceUtils.setString(idAccount, userData.id.toString());
        PreferenceUtils.setString(department, userData.departmentDTO?.departmentName.toString());
        PreferenceUtils.setString(birthday, "${timeStampToString(timeStamp: userData.birthday,dateTimeFormat: dOEMap[DateFormatCustom.DD_MM_YYYY])}");
        var encodedString = jsonEncode(userData);
        PreferenceUtils.setString(userDataKey, encodedString);
        PreferenceUtils.setString(email, userData.email ?? 'Chưa xác định');
        PreferenceUtils.setString(userAccount, userData.username ?? '');
        if(userData.userType == STAFF){
          Get.offAllNamed(Routers.home_staff);
        }
        getSubNotifyEmployee();
      }
    } catch (ex) {
      errNotificationNoAppBar(context: Get.context!);
    }
  }
  getSubNotifyEmployee() async {
    // check keyAccesstoken
    if (PreferenceUtils.getString(keyAccessToken) != null) {
      final object = {
        "tokens": ["${PreferenceUtils.getString(tokenFcm)}"]
      };
      final request = await apiManager?.subscribeNotifyEmployee(data: object);
      final result = BaseApiResponse.fromJson(request);
      if (result.code == successful || result.code == create) {
        if(kDebugMode){
          print('Đã ký FCM nhaân viên');
        }
      } else {

      }
    }
  }
  changeShowPass() {
    hidePass.value = !hidePass.value;
  }
  checkShowAction() {
    if (errPass?.value == null &&
        errNumberPhone?.value == null &&
        textEdits.last.text.isNotEmpty &&
        textEdits.first.text.isNotEmpty) {
      return isOk.value = true;
    }
    return isOk.value = false;
  }
}