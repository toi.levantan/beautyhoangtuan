import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/view_model/intro_vm/intro_controller.dart';
import 'package:get/get.dart';

class IntroBinding implements Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(IntroController(Get.find<ApiManager>()));
  }
}