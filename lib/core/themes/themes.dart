import 'package:flutter/material.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import '../../utils/app/size_app.dart';

class Themes {
  static const sfProDisplay = 'SF Pro Display';
  static const sfProText = 'SF Pro Text';

  // sf pro display TODO size 8
  static TextStyle get sfText8_500 {
    return TextStyle(
      fontSize: sizeTxt_8,
      fontWeight: FontWeight.w500,
      fontFamily: sfProText,
      color: colorTextApp,
    );
  }


  // sf pro display TODO size 9
  static TextStyle get sfText9_500 {
    return TextStyle(
      fontSize: sizeTxt_9,
      fontWeight: FontWeight.w500,
      fontFamily: sfProText,
      color: colorTextApp,
    );
  }
  static TextStyle get sfTextHint9_400 {
    return TextStyle(
      fontSize: sizeTxt_9,
      fontWeight: FontWeight.w400,
      fontFamily: sfProText,
      color: colorTextHint,
    );
  }
  ///TODO size 10
  static TextStyle get sfText10_500 {
    return TextStyle(
      fontSize: sizeTxt_10,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w500,
    );
  }
  static TextStyle get sfText10_400 {
    return TextStyle(
      fontSize: sizeTxt_10,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get sfTextHint10_400 {
    return TextStyle(
      fontSize: sizeTxt_10,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get sfTextHint10_600 {
    return TextStyle(
      fontSize: sizeTxt_10,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w600,
    );
  }
  ///TODO size 11
  static TextStyle get sfHintText11_500 {
    return TextStyle(
        fontSize: sizeTxt_11,
        fontFamily: sfProText,
        color: colorTextHint,
        fontWeight: FontWeight.w500);
  }
  static TextStyle get sfText11_500 {
    return TextStyle(
        fontSize: sizeTxt_11,
        fontFamily: sfProText,
        color: colorTextApp,
        fontWeight: FontWeight.w500);
  }
  static TextStyle get sfTextHint11_400 {
    return TextStyle(
        fontSize: sizeTxt_11,
        fontFamily: sfProText,
        color: colorTextHint,
        fontWeight: FontWeight.w400);
  }
  static TextStyle get sfText11_400 {
    return TextStyle(
        fontSize: sizeTxt_11,
        fontFamily: sfProText,
        color: colorTextApp,
        fontWeight: FontWeight.w400);
  }
  //TODO size 12
  static TextStyle get sfText12_700 {
    return TextStyle(
      fontSize: sizeTxt_12,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w700,
    );
  }
  static TextStyle get sfText12_300 {
    return TextStyle(
      fontSize: sizeTxt_12,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w300,
    );
  }
  static TextStyle get sfText12_400 {
    return TextStyle(
      fontSize: sizeTxt_12,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get sfText12_500 {
    return TextStyle(
      fontSize: sizeTxt_12,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfTextHint12_500 {
    return TextStyle(
      fontSize: sizeTxt_12,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfTextHint12_400 {
    return TextStyle(
      fontSize: sizeTxt_12,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w400,
    );
  }
  //TODO size 16

  static TextStyle get sfText16_500 {
    return TextStyle(
      fontSize: sizeTxt_16,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfText16_400 {
    return TextStyle(
      fontSize: sizeTxt_16,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w400,
    );
  }

  static TextStyle get sfText16_600 {
    return TextStyle(
      fontSize: sizeTxt_16,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w600,
    );

  }
  static TextStyle get sfText16_700 {
    return TextStyle(
      fontSize: sizeTxt_16,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w700,
    );
  }
  static TextStyle get sfText16_800 {
    return TextStyle(
      fontSize: sizeTxt_16,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w800,
    );
  }
  static TextStyle get sfTextHint16_400 {
    return TextStyle(
      fontSize: sizeTxt_16,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w400,
    );
  }
  //TODO 14 size
  static TextStyle get sfText14_500 {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfText14_400 {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get sfText14_300 {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w300,
    );
  }
  static TextStyle get labelNote {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get sfText14_800 {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w800,
    );
  }
  static TextStyle get sfTextHint14_400 {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get sfTextHint14_500 {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w600,
    );

  }
  static TextStyle get sfText14_700 {
    return TextStyle(
      fontSize: sizeTxt_14,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w700,
    );
  }
  //TODO 15 size
  static TextStyle get textAppBar {
    return TextStyle(
      fontSize: sizeTxt_15,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfText15_500 {
    return TextStyle(
      fontSize: sizeTxt_15,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfText15_400 {
    return TextStyle(
      fontSize: sizeTxt_15,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w400,
    );
  }
  static TextStyle get sfText15_700 {
    return TextStyle(
      fontSize: sizeTxt_15,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w700,
    );
  }
  //TODO size 17
  static TextStyle get sfText17_700 {
    return TextStyle(
      fontSize: sizeTxt_17,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w700,
    );
  }
  static TextStyle get sfText17_400 {
    return TextStyle(
      fontSize: sizeTxt_17,
      fontFamily: sfProText,
      color: colorTextApp,
      fontWeight: FontWeight.w400,
    );
  }
  //TODO 18 size
  static TextStyle get sfText18_500 {
    return TextStyle(
      fontSize: sizeTxt_18,
      fontFamily: sfProText,
      fontWeight: FontWeight.w500,
    );
  }
  static TextStyle get sfText18_700 {
    return TextStyle(
      fontSize: sizeTxt_18,
      fontFamily: sfProText,
      fontWeight: FontWeight.w700,
    );
  }
  static TextStyle get sfHintText18_500 {
    return TextStyle(
      fontSize: sizeTxt_18,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w500,
    );
  }
  //TODO size 20
  static TextStyle get sfText20_500 {
    return TextStyle(
      fontSize: sizeTxt_20,
      color: colorTextApp,
      fontFamily: sfProText,
      fontWeight: FontWeight.w500,
    );
  }
  static TextStyle get sfHintText20_500 {
    return TextStyle(
      fontSize: sizeTxt_20,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w500,
    );
  }
  //TODO size 22
  static TextStyle get sfText22_500 {
    return TextStyle(
      fontSize: sizeTxt_22,
      color: colorTextApp,
      fontFamily: sfProText,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfHintText22_500 {
    return TextStyle(
      fontSize: sizeTxt_22,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w500,
    );
  }
  //TODO size 23
  static TextStyle get sfText23_500 {
    return TextStyle(
      fontSize: sizeTxt_22,
      color: colorTextApp,
      fontFamily: sfProText,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfText23_700 {
    return TextStyle(
      fontSize: sizeTxt_22,
      color: colorTextApp,
      fontFamily: sfProText,
      fontWeight: FontWeight.w700,
    );
  }
  static TextStyle get sfHintText23_500 {
    return TextStyle(
      fontSize: sizeTxt_22,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w500,
    );
  }
  //TODO size 24
  static TextStyle get sfText24_500 {
    return TextStyle(
      fontSize: sizeTxt_24,
      color: colorTextApp,
      fontFamily: sfProText,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get sfText24_700 {
    return TextStyle(
      fontSize: sizeTxt_24,
      color: colorTextApp,
      fontFamily: sfProText,
      fontWeight: FontWeight.w700,
    );
  }
  static TextStyle get sfHintText24_500 {
    return TextStyle(
      fontSize: sizeTxt_24,
      fontFamily: sfProText,
      color: colorTextHint,
      fontWeight: FontWeight.w500,
    );
  }
}
