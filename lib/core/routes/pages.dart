import 'package:myhoangtuan/utils/widget/image_preview.dart';
import 'package:myhoangtuan/view/customer/chat/chat_detail.dart';
import 'package:myhoangtuan/view/customer/new_feeds/commit_new_feed.dart';
import 'package:myhoangtuan/view/customer/news/view_html.dart';
import 'package:myhoangtuan/view/customer/report/detail_report_page.dart';
import 'package:myhoangtuan/view/customer/share_friend/share_friend.dart';
import 'package:myhoangtuan/view/employee/checkin_detail/detail_checkin_page.dart';
import 'package:myhoangtuan/view/employee/checkin_detail/detail_history.dart';
import 'package:myhoangtuan/view/employee/checkin_detail/history_checkin.dart';
import 'package:myhoangtuan/view/employee/day_off/day_off_page.dart';
import 'package:myhoangtuan/view_model/change_pass_vm/change_pass_binding.dart';
import 'package:myhoangtuan/view_model/create_new_feed_vm/commit_feeds_binding.dart';
import 'package:myhoangtuan/view_model/create_new_feed_vm/create_new_feed_binding.dart';
import 'package:myhoangtuan/view_model/day_off_vm/day_off_binding.dart';
import 'package:myhoangtuan/view_model/detail_checkin_vm/detail_checkin_binding.dart';
import 'package:myhoangtuan/view_model/detail_checkin_vm/detail_history_binding.dart';
import 'package:myhoangtuan/view_model/home_staff_vm/home_staff_binding.dart';
import 'package:myhoangtuan/view_model/intro_vm/intro_binding.dart';
import 'package:myhoangtuan/view_model/report_vm/detail_report_binding.dart';
import 'package:myhoangtuan/view_model/service_vm/detail_sevices_binding.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/view_model/book_vm/book_binding.dart';
import 'package:myhoangtuan/view_model/news_vm/news_binding.dart';
import 'package:myhoangtuan/view_model/report_vm/create_report_binding.dart';
import 'package:myhoangtuan/view_model/report_vm/report_binding.dart';
import 'package:myhoangtuan/view_model/service_vm/package_combo_binding.dart';
import 'package:myhoangtuan/view_model/service_vm/package_service_binding.dart';
import 'package:myhoangtuan/view_model/share_friend_vm/share_friend_binding.dart';
import 'package:myhoangtuan/view_model/video_vm/video_preview_binding.dart';
import '../../view/customer/book/book_page.dart';
import '../../view/customer/change_pass/change_pass.dart';
import '../../view/customer/forgot_pwd/forgotpassword_page.dart';
import '../../view/customer/hdsd/hdsd_page.dart';
import '../../view/customer/history_book/history_book_page.dart';
import '../../view/customer/home/page_screen/home_page.dart';
import '../../view/customer/home/page_screen/account_setting_page.dart';
import '../../view/customer/home/services_screen/detail_package_service_screen.dart';
import '../../view/customer/home/services_screen/package_combo_screen.dart';
import '../../view/customer/home/services_screen/package_services_screen.dart';
import '../../view/customer/intro/intro_page.dart';
import '../../view/customer/login/login_page_create.dart';
import '../../view/customer/login/login_password.dart';
import '../../view/customer/login/otp_page.dart';
import '../../view/customer/new_feeds/create_new_feed.dart';
import '../../view/customer/news/develop_view.dart';
import '../../view/customer/news/news_detail.dart';
import '../../view/customer/news/view_detail_news.dart';
import '../../view/customer/notification/notification_page.dart';
import '../../view/customer/preview_media/video_preview.dart';
import '../../view/customer/report/create_report_page.dart';
import '../../view/customer/report/report_page.dart';
import '../../view/customer/splash/splash_page.dart';
import '../../view/customer/update_info/update_info_page.dart';
import '../../view/employee/contact/contact_page.dart';
import '../../view/employee/home/page_screen_staff/home_page_staff.dart';
import '../../view_model/book_vm/history_book_binding.dart';
import '../../view_model/chat_vm/chat_detail_binding.dart';
import '../../view_model/contact_vm/contact_binding.dart';
import '../../view_model/detail_checkin_vm/history_checkin_binding.dart';
import '../../view_model/forgot_pwd_vm/forgot_password_binding.dart';
import '../../view_model/home_vm/home_binding.dart';
import '../../view_model/info_vm/update_info_binding.dart';
import '../../view_model/login_vm/login_create_binding.dart';
import '../../view_model/login_vm/login_password_binding.dart';
import '../../view_model/news_vm/html_binding.dart';
import '../../view_model/news_vm/view_new_detail_binding.dart';
import '../../view_model/notification/notification_binding.dart';
import '../../view_model/splash_vm/splash_binding.dart';

class Pages {
  Pages._();
  static final pages = [
    GetPage(name: Routers.initialRoute, page: () => const SplashPage(), binding: SplashBinding()),
    GetPage(name: Routers.home, page: () => const HomePage(), binding: HomeBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.login_create, page: () => const LoginPageCreate(), binding: LoginCreateBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.otp, page: () => const OtpPage(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.report, page: () => const ReportPage(),binding: ReportBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.createReport, page: () => const CreateReportPage(),binding: CreateReportBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.book, page: () =>  const BookPage(),binding: BookBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.updateInfo, page: () => const UpdateInfoPage(),binding: UpdateInfoBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.packageService, page: () => const PackageServiceScreen(),binding: PackageServiceBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.packageCombo, page: () => const PackageComboScreen(),binding: PackageComboBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.detailService, page: () => const DetailPackageServiceScreen(), binding: DetailServicesBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.accSetting, page: () => const AccountSettingPage(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.historyBook, page: () => const HistoryBookPage(),binding: HistoryBookBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.notification, page: () => const NotificationPage(),binding: NotificationBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.news, page: () => const NewsDetail(),binding: NewsBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.news_detail, page: () => const ViewHtmlDetailNews(),binding: ViewNewDetailBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.develop_page, page: () => const DevelopPage(),binding: ViewNewDetailBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.intro_page, page: () => const IntroPage(),binding: IntroBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.login_pwd, page: () => const LoginPwdPage(),binding: LoginPwdBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.change_pass, page: () => const ChangePassPage(),binding: ChangePassBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.forgot_pwd, page: () => const ForgotPasswordPage(),binding: ForgotPasswordBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.hdsd, page: () => const HDSD(),binding: ViewNewDetailBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.home_staff, page: () => const HomePageStaff(),binding: HomeStaffBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.detail_report, page: () => const DetailReportPage(),binding: DetailReportBinding(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.image_preview, page: () => const ImagePreview(),transitionDuration: const Duration(milliseconds: 620)),
    GetPage(name: Routers.detail_checkin, page: () => const DetailCheckIn(),transitionDuration: const Duration(milliseconds: 620),binding: DetailCheckinBinding()),
    GetPage(name: Routers.history_checkin, page: () => const HistoryCheckinPage(),transitionDuration: const Duration(milliseconds: 620),binding: HistoryCheckinBinding()),
    GetPage(name: Routers.html_view, page: () => const ViewHtml(),transitionDuration: const Duration(milliseconds: 620),binding: ViewHtmlBinding()),
    GetPage(name: Routers.share_friend, page: () => const ShareFriend(),transitionDuration: const Duration(milliseconds: 620),binding: ShareFriendBinding()),
    GetPage(name: Routers.create_new_feeds, page: () => const CreateNewFeed(),transitionDuration: const Duration(milliseconds: 620),binding: CreateNewFeedBinding()),
    GetPage(name: Routers.video_preview, page: () => const VideoPreview(),transitionDuration: const Duration(milliseconds: 620),binding: VideoPreviewBinding()),
    GetPage(name: Routers.commit_feed, page: () => const CommitFeed(),transitionDuration: const Duration(milliseconds: 620),binding: CommitFeedBinding()),
    GetPage(name: Routers.detail_history, page: () => const DetailHistory(),transitionDuration: const Duration(milliseconds: 620),binding: DetailHistoryBinding()),
    GetPage(name: Routers.danhba, page: () => const ContactPage(),transitionDuration: const Duration(milliseconds: 620),binding: ContactBinding()),
    GetPage(name: Routers.donbao, page: () => const DayOffPage(),transitionDuration: const Duration(milliseconds: 620),binding: DayOffBinding()),
    GetPage(name: Routers.chat_detail, page: () => const ChatDetail(),transitionDuration: const Duration(milliseconds: 620),binding: ChatDetailBinding()),
  ];
}
