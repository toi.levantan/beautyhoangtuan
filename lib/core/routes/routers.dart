class Routers{
  static const initialRoute = '/initial';
  static const login = '/login';
  static const login_create = '/login_create';
  static const home = '/home';
  static const otp = '/otp';
  static const qr = '/qr';
  static const report = '/report';
  static const book = '/book';
  static const createReport = '/createReport';
  static const updateInfo = '/updateInfo';
  static const packageService = '/packageService';
  static const detailService = '/detailService';
  static const packageCombo = '/packageCombo';
  static const change_pass = '/change_pass';
  static const accSetting = '/accSetting';
  static const historyBook = '/historyBook';
  static const notification = '/notification';
  static const news = '/news';
  /// chi tiết  tin tức
  static const news_detail = '/news_detail';
  static const profile = '/profile';
  static const develop_page = '/develop_page';
  static const intro_page = '/intro_page';
  static const login_pwd = '/login_pwd';
  static const forgot_pwd = '/forgot_pwd';
  /// danh sách tin tức
  static const post = '/post';
  static const hdsd = '/hdsd';
  static const home_staff = '/home_staff';
  ///TODO toi chi tiết phản ánh
  static const detail_report = '/detail_report';
  static const image_preview = '/image_preview';
  static const video_preview = '/video_preview';
  static const detail_checkin = '/detail_checkin';
  static const history_checkin = '/history_checkin';
  static const html_view = '/html_view';
  static const share_friend = '/share_friend';
  ///TODO newfeeds
  static const create_new_feeds = '/create_new_feeds';
  static const commit_feed = '/commit_feed';

  //lich su ra vao
  static const detail_history = '/detail_history';
  ///TODO danh ba
  static const danhba = '/danhba';
 ///TODO don xin nghi
  static const donbao = '/donbao';
  ///TODO chi tiết tin nhắn
  static const chat_detail = '/chat_detail';
}