import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/model/base_api.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/session/helper_notification.dart';
import '../../utils/app/share_pref.dart';
import '../../utils/app/util_logger.dart';
import '../../utils/widget/snack_bar.dart';

/// TODO bắt handle dio
Map<String, dynamic> dioErrorHandle(DioError error) {
  UtilLogger.log("ERROR", error);
  switch (error.type) {
    // TODO return dữ liệu lỗi và code lỗi
    case DioErrorType.connectionError:
      return errNotificationNoAppBar(context: Get.context!, message: "Lỗi kết nối");
    case DioErrorType.connectionTimeout:
      return errNotificationNoAppBar(
          context: Get.context!, message: "Không thể kết nối đến server");
    case DioErrorType.badResponse:
      return {
        "success": false,
        "code": error.response?.statusCode,
        "data": error.response?.data
      };
    case DioErrorType.sendTimeout:
      return errNotificationNoAppBar(
          context: Get.context!, message: "Hết hạn gửi yêu cầu");
    case DioErrorType.receiveTimeout:
      return errNotificationNoAppBar(
          context: Get.context!, message: "Hết hạn lấy dữ liệu từ server");
    case DioErrorType.unknown:
      return {
        "success": false,
        "code": error.response?.statusCode,
        "data": error.response?.data
      };
    default:
      return {
        "success": false,
        "code": error.response?.statusCode,
        "data": error.response?.data
      };
  }
}

class HttpManager {
  // khai báo dio
  final Dio _noAuthurizeDio;
  final Dio _noAuthDio;
  final Dio _authDio;
  final Dio _dio;
  final Dio _dioToken;

  ///TODO truyền dio cho http
  HttpManager(this._noAuthurizeDio, this._noAuthDio, this._authDio, this._dio,
      this._dioToken) {
    _noAuthurizeDio.options = baseOptionNoAuth;
    _noAuthDio.options = baseNoAuth;
    initApiClient();
  }

  // cau hinh authConfig
  // final authConfig = {'clientId': "clientMobile", "clientSecret": "secret"};

  /// mã hóa (encode) base64
  final authHeader = 'Basic ${base64.encode(utf8.encode("clientMobile:n55pVJdXRpKM7btkQsbhZBddQS3mQUxra"))}';

  ///khoi tao client
  Future<void> initApiClient() async {
    /// exportOption to do set header
    _authDio.options = exportOptionOauth(baseOptionsOauth);
    _dio.options = exportOption(baseOptions);
    _dioToken.options = exportOption(baseOptionsExpiredToken);
  }

  /// call api
  final baseOptions = BaseOptions(
    baseUrl: baseEndPoint,
    connectTimeout: const Duration(seconds: 30),
    receiveTimeout: const Duration(minutes: 3),
    contentType: Headers.jsonContentType,
    responseType: ResponseType.json,
  );

  /// call api check token
  BaseOptions baseOptionsExpiredToken = BaseOptions(
    baseUrl: authUrl,
    connectTimeout: const Duration(seconds: 30),
    receiveTimeout: const Duration(minutes: 3),
    contentType: Headers.jsonContentType,
    responseType: ResponseType.json,
  );

  /// baseOption NoAuthCrm
  BaseOptions baseOptionNoAuth = BaseOptions(
    baseUrl: baseEndPoint,
    connectTimeout: const Duration(seconds: 30),
    receiveTimeout: const Duration(minutes: 3),
    contentType: Headers.jsonContentType,
    responseType: ResponseType.json,
  );

  /// baseOption NoAuth
  BaseOptions baseNoAuth = BaseOptions(
    baseUrl: authUrl,
    connectTimeout: const Duration(seconds: 30),
    receiveTimeout: const Duration(minutes: 3),
    contentType: Headers.jsonContentType,
    responseType: ResponseType.json,
  );

  /// baseOption OAuth2
  BaseOptions baseOptionsOauth = BaseOptions(
    baseUrl: authUrl,
    connectTimeout: const Duration(seconds: 30),
    receiveTimeout: const Duration(minutes: 3),
    contentType: Headers.formUrlEncodedContentType,
    responseType: ResponseType.json,
  );

  /// Setup option
  BaseOptions exportOption(BaseOptions options) {
    options.headers["Authorization"] =
    "Bearer ${PreferenceUtils.getString(keyAccessToken)}";
    log("token ${PreferenceUtils.getString(keyAccessToken)}");
    return options;
  }

  /// Setup option
  // BaseOptions exportOptionNoAuth(BaseOptions options) {
  //   options.headers["Accept-Language"] = "vi-VN";
  //   options.headers.remove('Authorization');
  //   log("headers ${options.headers}");
  //   return options;
  // }

  ///Setup Option OauthToken Generate
  BaseOptions exportOptionOauth(BaseOptions options) {
    options.headers["Authorization"] = authHeader;
    options.headers["Accept-Language"] = "vi-VN";
    if (kDebugMode) {
      print('headersAuth ${options.headers}');
    }
    return options;
  }


  // setup method
  ///post method
  Future<dynamic> post({
    required String url,
    Map<String, dynamic>? data,
    Map<String, dynamic>? queryParams,
  }) async {
    try {
      UtilLogger.log("POST URL", baseEndPoint + url);
      UtilLogger.log("DATA", const JsonEncoder().convert(data));
      // tạo request
      final response = await _dio.post(
          url, data: data, queryParameters: queryParams);

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(code: response.statusCode,
            data: null,
            message: '',
            success: false);
      }

      UtilLogger.log(
          "POST RESPONSE", const JsonEncoder().convert(response.data));
      UtilLogger.log("token", "${PreferenceUtils.getString(keyAccessToken)}");
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      if (err.response?.statusCode == expiredToken) {
        Get.dialog(
          AlertDialog(
            title: const Text(
              'Đăng xuất',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            content: const Text(
                'Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại để tiếp tục sử dụng !'),
            actions: <Widget>[
              MaterialButton(
                minWidth: 120,
                height: 40,
                onPressed: () {
                  PreferenceUtils.remove(keyAccessToken);
                  // Kiem tra con login khong
                  ///TODO khong con login thi dang xuat nhung van o trang chu
                  Get.back();
                  Get.offAllNamed(Routers.intro_page);
                },
                color: colorIcon,
                textColor: Colors.white,
                child: const Text('Xác nhận'),
              ),
            ],
          ),
          barrierDismissible: false,
        );
      }
      return dioErrorHandle(err);
    }
  }
  Future<dynamic> postList({
    required String url,
    var data,
    Map<String, dynamic>? queryParams,
  }) async {
    try {
      UtilLogger.log("POST URL", baseEndPoint + url);
     // UtilLogger.log("DATA", const JsonEncoder().convert(data));
      // tạo request
      final response = await _dio.post(
          url, data: data, queryParameters: queryParams);

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(code: response.statusCode,
            data: null,
            message: '',
            success: false);
      }

      UtilLogger.log(
          "POST RESPONSE", const JsonEncoder().convert(response.data));
      UtilLogger.log("token", "${PreferenceUtils.getString(keyAccessToken)}");
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      if (err.response?.statusCode == expiredToken) {
        Get.dialog(
          AlertDialog(
            title: const Text(
              'Đăng xuất',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            content: const Text(
                'Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại để tiếp tục sử dụng !'),
            actions: <Widget>[
              MaterialButton(
                minWidth: 120,
                height: 40,
                onPressed: () {
                  PreferenceUtils.remove(keyAccessToken);
                  // Kiem tra con login khong
                  ///TODO khong con login thi dang xuat nhung van o trang chu
                  Get.back();
                  Get.offAllNamed(Routers.intro_page);
                },
                color: colorIcon,
                textColor: Colors.white,
                child: const Text('Xác nhận'),
              ),
            ],
          ),
          barrierDismissible: false,
        );
      }
      return dioErrorHandle(err);
    }
  }

  Future<dynamic> postNoNotifyExpired({
    required String url,
    Map<String, dynamic>? data,
    Map<String, dynamic>? queryParams,
  }) async {
    try {
      UtilLogger.log("POST URL", baseEndPoint + url);
      UtilLogger.log("DATA", const JsonEncoder().convert(data));
      // tạo request
      final response = await _dio.post(
          url, data: data, queryParameters: queryParams);

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(code: response.statusCode,
            data: null,
            message: '',
            success: false);
      }

      UtilLogger.log(
          "POST RESPONSE", const JsonEncoder().convert(response.data));
      UtilLogger.log("token", "${PreferenceUtils.getString(keyAccessToken)}");
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      return dioErrorHandle(err);
    }
  }

  Future<dynamic> postFormData({
    required String url,
    dynamic data,
    Map<String, dynamic>? queryParams,
  }) async {
    try {
      UtilLogger.log("POST URL", baseEndPoint + url);
      // tạo request
      final response = await _dio.post(url, data: data, queryParameters: queryParams);

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(code: response.statusCode,
            data: null,
            message: '',
            success: false);
      }
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      if (err.response?.statusCode == expiredToken) {
        Get.dialog(
          AlertDialog(
            title: const Text(
              'Đăng xuất',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            content: const Text(
                'Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại để tiếp tục sử dụng !'),
            actions: <Widget>[
              MaterialButton(
                minWidth: 120,
                height: 40,
                onPressed: () {
                  PreferenceUtils.remove(keyAccessToken);
                  // Kiem tra con login khong
                  ///TODO khong con login thi dang xuat nhung van o trang chu
                  Get.back();
                  Get.offAllNamed(Routers.intro_page);
                },
                color: colorIcon,
                textColor: Colors.white,
                child: const Text('Xác nhận'),
              ),
            ],
          ),
          barrierDismissible: false,
        );
      }
      return dioErrorHandle(err);
    }
  }

  ///
  ///post method no Auth Crm header
  Future<dynamic> postNoAuth({
    required String url,
    dynamic data,
    Map<String, dynamic>? queryParam,
  }) async {
    try {
      // tạo request
      //UtilLogger.log("DATA", const JsonEncoder().convert(data));
      if (kDebugMode) {
        print('');
      }
      final response = await _noAuthurizeDio.post(
          url, data: data, queryParameters: queryParam);
      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(code: response.statusCode,
            data: null,
            message: '',
            success: false);
      }
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      return dioErrorHandle(err);
    }
  }

  ///post method no Auth
  Future<dynamic> postNoAuthorize({
    required String url,
    dynamic data,
    Map<String, dynamic>? queryParam,
  }) async {
    try {
      // tạo request
      //UtilLogger.log("POST URL", authUrl + url);
      final response = await _noAuthDio.post(
          url, data: data, queryParameters: queryParam);

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(code: response.statusCode,
            data: null,
            message: '',
            success: false);
      }
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      return dioErrorHandle(err);
    }
  }


  Future<dynamic> postOauth({
    required String url,
    required Map<String, dynamic> data,
    Options? options,
  }) async {
    try {
      // tạo request
      final response = await _authDio.post(url, data: data, options: options);
      UtilLogger.log("POST URL", baseEndPoint + url);
      UtilLogger.log("DATA", const JsonEncoder().convert(data));

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(
            code: response.statusCode, data: null, message: '', success: false);
      }
      UtilLogger.log(
          "POST RESPONSE", const JsonEncoder().convert(response.data));
      if (kDebugMode) {
        print('data ${response.data}');
      }
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true).toJson();
    } on DioError catch (err) {
      if (err.response?.statusCode == expiredToken) {
        return Get.dialog(
          AlertDialog(
            title: const Text(
              'Đăng xuất',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            content: const Text(
                'Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại để tiếp tục sử dụng !'),
            actions: <Widget>[
              MaterialButton(
                minWidth: 120,
                height: 40,
                onPressed: () {
                  PreferenceUtils.remove(keyAccessToken);
                  // Kiem tra con login khong
                  ///TODO khong con login thi dang xuat nhung van o trang chu
                  Get.back();
                  Get.offAllNamed(Routers.intro_page);
                },
                color: colorIcon,
                textColor: Colors.white,
                child: const Text('Xác nhận'),
              ),
            ],
          ),
          barrierDismissible: false,
        );
      }
      return dioErrorHandle(err);
    }
  }

  Future<dynamic> postOauthDio({
    required String url,
    required Map<String, dynamic> data,
    Options? options,
  }) async {
    try {
      // tạo request
      final response = await _dioToken.post(url, data: data, options: options);
      UtilLogger.log("POST URL", baseEndPoint + url);
      UtilLogger.log("DATA", const JsonEncoder().convert(data));

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(
            code: response.statusCode, data: null, message: '', success: false);
      }
      UtilLogger.log(
          "POST RESPONSE", const JsonEncoder().convert(response.data));
      if (kDebugMode) {
        print('data ${response.data}');
      }
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true).toJson();
    } on DioError catch (err) {
      if (err.response?.statusCode == expiredToken) {
        return Get.dialog(
          AlertDialog(
            title: const Text(
              'Đăng xuất',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            content: const Text(
                'Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại để tiếp tục sử dụng !'),
            actions: <Widget>[
              MaterialButton(
                minWidth: 120,
                height: 40,
                onPressed: () {
                  PreferenceUtils.remove(keyAccessToken);
                  // Kiem tra con login khong
                  ///TODO khong con login thi dang xuat nhung van o trang chu
                 checkHasLogin();
                 
                  Get.back();
                },
                color: colorIcon,
                textColor: Colors.white,
                child: const Text('Xác nhận'),
              ),
            ],
          ),
          barrierDismissible: false,
        );
      }
      return dioErrorHandle(err);
    }
  }

  Future<dynamic> postLogin({
    required String url,
    required Map<String, dynamic> data,
    Options? options,
  }) async {
    try {
      // tạo request
      final response = await _authDio.post(url, data: data, options: options);
      UtilLogger.log("POST URL", baseEndPoint + url);
      UtilLogger.log("DATA", const JsonEncoder().convert(data));

      /// check request code
      if (response.statusCode != successful) {
        return BaseApiResponse(
            code: response.statusCode, data: null, message: '', success: false);
      }
      UtilLogger.log(
          "POST RESPONSE", const JsonEncoder().convert(response.data));
      if (kDebugMode) {
        print('data ${response.data}');
      }
      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true).toJson();
    } on DioError catch (err) {
      return dioErrorHandle(err);
    }
  }

  /// method get
  Future<dynamic> get({required String url,
    required Map<String, dynamic> parameters,
    Options? options}) async {
    try {
      UtilLogger.log("GET URL", baseEndPoint + url);
      UtilLogger.log("PARAMS", parameters);

      final response = await _dio.get(url,
          queryParameters: parameters,
          options: options ?? Options(method: 'GET'));
      if (response.statusCode != successful) {
        return BaseApiResponse(
            code: response.statusCode,
            data: null,
            message: '',
            success: false)
            .toJson();
      }
      if (kDebugMode) {
        print(response);
      }
      UtilLogger.log(
          "GET RESPONSE", const JsonEncoder().convert(response.data));

      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      if (err.response?.statusCode == expiredToken) {
        if (err.response?.statusCode == expiredToken) {
          return Get.dialog(
            AlertDialog(
              title: const Text(
                'Đăng xuất',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              content: const Text(
                  'Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại để tiếp tục sử dụng !'),
              actions: <Widget>[
                MaterialButton(
                  minWidth: 120,
                  height: 40,
                  onPressed: () async{
                    await PreferenceUtils.remove(keyAccessToken);
                     // Kiem tra con login khong
                    ///TODO khong con login thi dang xuat nhung van o trang chu
                    checkHasLogin();
                   
                    Get.back();
                  },
                  color: colorIcon,
                  textColor: Colors.white,
                  child: const Text('Xác nhận'),
                ),
              ],
            ),
            barrierDismissible: false,
          );
        }
        return dioErrorHandle(err);
      }
    }

    // Future<dynamic> getAuth({required String url,
    //   required Map<String, dynamic> parameters,
    //   Options? options}) async {
    //   try {
    //     UtilLogger.log("GET URL", authUrl + url);
    //     UtilLogger.log("PARAMS", parameters);
    //
    //     final response = await _dioToken.get(url,
    //         queryParameters: parameters,
    //         options: options ?? Options(method: 'GET'));
    //     if (response.statusCode != successful) {
    //       return BaseApiResponse(
    //           code: response.statusCode,
    //           data: null,
    //           message: '',
    //           success: false)
    //           .toJson();
    //     }
    //     if (kDebugMode) {
    //       print(response);
    //     }
    //     UtilLogger.log(
    //         "GET RESPONSE", const JsonEncoder().convert(response.data));
    //
    //     return BaseApiResponse(
    //         code: successful,
    //         data: response.data,
    //         message: response.statusMessage,
    //         success: true)
    //         .toJson();
    //   } on DioError catch (err) {
    //     ///TODO check respose trả về thực hiện cái gì
    //     if (err.response?.statusCode == expiredToken) {
    //       return Get.dialog(
    //         AlertDialog(
    //           title: const Text(
    //             'Đăng xuất',
    //             style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
    //           ),
    //           content: const Text(
    //               'Phiên làm việc của bạn đã hết. Vui lòng đăng nhập lại để tiếp tục sử dụng !'),
    //           actions: <Widget>[
    //             MaterialButton(
    //               minWidth: 120,
    //               height: 40,
    //               onPressed: () {
    //                 PreferenceUtils.remove(keyAccessToken);
    //                 // Kiem tra con login khong
    //                 ///TODO khong con login thi dang xuat nhung van o trang chu
    //                checkHasLogin();
    //
    //                 Get.back();
    //               },
    //               color: colorIcon,
    //               textColor: Colors.white,
    //               child: const Text('Xác nhận'),
    //             ),
    //           ],
    //         ),
    //         barrierDismissible: false,
    //       );
    //     }
    //     return dioErrorHandle(err);
    //   }
    // }
  }
  /// method get
  Future<dynamic> getUnNotify({required String url,
    required Map<String, dynamic> parameters,
    Options? options}) async {
    try {
      UtilLogger.log("GET URL", baseEndPoint + url);
      UtilLogger.log("PARAMS", parameters);

      final response = await _dio.get(url,
          queryParameters: parameters,
          options: options ?? Options(method: 'GET'));
      if (response.statusCode != successful) {
        return BaseApiResponse(
            code: response.statusCode,
            data: null,
            message: '',
            success: false)
            .toJson();
      }
      if (kDebugMode) {
        print(response);
      }


      return BaseApiResponse(
          code: successful,
          data: response.data,
          message: response.statusMessage,
          success: true)
          .toJson();
    } on DioError catch (err) {
      if (err.response?.statusCode == expiredToken) {
        return dioErrorHandle(err);
      }
    }
  }
    Future<dynamic> getAuth({required String url,
      required Map<String, dynamic> parameters,
      Options? options}) async {
      try {
        UtilLogger.log("GET URL", authUrl + url);
        UtilLogger.log("PARAMS", parameters);

        final response = await _dioToken.get(url,
            queryParameters: parameters,
            options: options ?? Options(method: 'GET'));
        if (response.statusCode != successful) {
          return BaseApiResponse(
              code: response.statusCode,
              data: null,
              message: '',
              success: false)
              .toJson();
        }
        if (kDebugMode) {
          print(response);
        }
        UtilLogger.log(
            "GET RESPONSE", const JsonEncoder().convert(response.data));

        return BaseApiResponse(
            code: successful,
            data: response.data,
            message: response.statusMessage,
            success: true)
            .toJson();
      } on DioError catch (err) {
        return dioErrorHandle(err);
      }
    }
    Future<dynamic> getNoAuth({
      required String url,
      required Map<String, dynamic> parameters,
      Options? options
    }) async {
      try {
        UtilLogger.log("GET URL", authUrl + url);
        UtilLogger.log("Header", _noAuthurizeDio.options);

        final response = await _noAuthurizeDio.get(url,
            queryParameters: parameters,
            options: options ?? Options(method: 'GET'));
        if (response.statusCode != successful) {
          return BaseApiResponse(
              code: response.statusCode,
              data: null,
              message: '',
              success: false)
              .toJson();
        }
        if (kDebugMode) {
          print(response);
        }
        UtilLogger.log(
            "GET RESPONSE", const JsonEncoder().convert(response.data));
        return BaseApiResponse(
            code: successful,
            data: response.data,
            message: response.statusMessage,
            success: true)
            .toJson();
      } on DioError catch (err) {
        return dioErrorHandle(err);
      }
    }
  }
