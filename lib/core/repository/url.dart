import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/session/environment.dart';

String authUrl             = Environment.test[EnvironmentEnum.authUrl] ?? "";
String baseEndPoint        = Environment.test[EnvironmentEnum.baseEndPoint] ??"";
String linkImage           = Environment.test[EnvironmentEnum.linkImage] ??"";
String linkImageProcessor  = Environment.product[EnvironmentEnum.linkImageProcessor] ??"";

/// doi pass
const String changePassUri = '/auth-user/api/v1/user/change-password';
//call token
const String authLogin = '/auth-user/oauth/token';
// /api/v1/beauty-public
///Todo dịch vụ đặt lịch
const String getBeautyService = '/crm/api/v1/beauty-public/beauty-service/find-by-condition';
const String getBeautyServiceId = '/crm/api/v1/beauty-public/beauty-package/find-by-condition';
const String branchId = '/crm/api/v1/beauty-booking/get-all-organization-branch';
const String beautyServiceBook = '/crm/api/v1/beauty-service-package-department/find-by-condition';
const String beautyDoctor = '/crm/api/v1/beauty-booking/get-list-doctor-department';
const String beautyTime = '/crm/api/v1/beauty-booking/get-list-work-shift-doctor';
const String beautyBooking = '/crm/api/v1/beauty-booking/create';
const String createByPhone = '/crm/api/v1/loyalty/customer/create-by-phone-number';
const String existPhone = '/crm/api/v1/loyalty/customer/check-exist-by-phone-number';
const String getOrganizationId = '/crm/api/v1/organization-branch/by-organization-id';
const String branchServiceId = '/crm/api/v1/beauty-booking/get-info-package-or-combo';
///TODO combo đặt lịch
const String comboBook = '/crm/api/v1/beauty-public/beauty-combo/find-by-condition';
//getComb0
const String getCombo = '/crm/api/v1/beauty-public/beauty-combo/find-by-condition';
//getDetailCombo
const String getComboIdPackage = '/crm/api/v1/beauty-public/beauty-combo/find-by-id';
// todo lich su dat lich lich sap toi
const String historyBooking = '/crm/api/v1/beauty-booking/find-by-condition';
// check expired token
const String expiredTokenUri = '/crm/api/v1/loyalty/customer/find-by-id';
const String getHostBeauty = '/crm/api/v1/beauty-public/get-list-hot-booking';
const String getDoctorFromServiceUri = '/crm/api/v1//beauty-booking/get-list-doctor-by-organization_branch-and-service-package';
const String cancelBookingUri = '/crm/api/v1/beauty-booking/cancel-booking';
const String hotNewUri = '/crm/api/v1/beauty-public/beauty-post/find-by-condition';
const String getRoomUri = '/crm/api/v1/department/find-by-condition';
const String getPersonInChargeUri = '/crm/api/v1/employee/admin/find-by-department';
const String createReportUri = '/crm/api/v1/beauty-booking-feedback/create';
const String getListFeedBack = '/crm/api/v1/beauty-booking-feedback/find-by-condition';
const String getListNotify = '/crm/api/v1/beauty-notification/find-by-condition';
const String getListNotifyEmpUri = '/crm/api/v1/notification/list';
const String seenNotify = '/crm/api/v1/beauty-notification/seen';
const String seenNotifyEmp = '/crm/api/v1/notification/seen';
const String subscribeNotifyUri = '/crm/api/v1/notification/loyalty-customer-subscribe';
const String subscribeNotifyEmployeeUri = '/crm/api/v1/notification/subscribe';
const String unsubscribeNotifyUri = '/crm/api/v1/notification/loyalty-customer-unsubscribe';
const String unsubscribeNotifyEmployeeUri = '/crm/api/v1/notification/unsubscribe';
const String getOrganizationIdUri = '/crm/api/v1/loyalty/customer/get-organization-id';
const String setNewPasswordUri = '/auth-user/api/v1/user/reset-password-by-otp';
const String updateAvatarUri = '/crm/api/v1/loyalty/customer/mobile-update';
const String updateAvatarEmpUri = '/crm/api/v1/employee/face/upload';
/// getType user
const String getTypeUser = '/crm/api/v1/loyalty/customer/get-user-type';
/// getDetail Feedback
const String getDetailFeedback  = '/crm/api/v1/beauty-booking-feedback/find-by-id';
const String getDetailCheckin  = '/crm/api/v1/time-keeping/find';
const String getHistoryCheckin  = '/processor/api/v1/processor/all-in-out-history';
const String getDetailNewsUri  = '/crm/api/v1/beauty-public/beauty-post/find-by-id';
const String getPhoneBookUri  = '/crm/api/v1/phonebook';
/// newfeeds
const String createNewFeedUri  = '/crm/api/v1/beauty-news-feed/create';
const String createNewComment  = '/crm/api/v1/beauty-comment-history/create';
const String getNewFeedsUri  = '/crm/api/v1/beauty-news-feed/find-by-condition';
const String likeOrUnlikeUri  = '/crm/api/v1/beauty-foul-keyword/interact';
const String listCommitUri  = '/crm/api/v1/beauty-comment-history/find-by-condition';
const String delPostUri  = '/crm/api/v1/beauty-news-feed/delete';
/// TODO tao user chat
const String createUserChat  = '/crm/api/v1/chat-user/get-or-create-user';
/// TODO getUserChat
const String getUserChat  = '/crm/api/v1/chat-user/get-all';
const String getRecentlyChatUri  = '/crm/api/v1/group-chat/get-current-user-chats';
const String getHistoryChatUri  = '/crm/api/v1/chat-message/get-chat-message-history';

