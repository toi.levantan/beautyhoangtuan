import 'package:dio/dio.dart';
import 'package:myhoangtuan/core/repository/http_manager.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/model/booking/booking_dto.dart';
import 'package:myhoangtuan/model/service/get_services_model.dart';

class ApiManager {
  // call method api
  HttpManager httpManager;

  // gọi tới httpMaganer để call request
  ApiManager(this.httpManager);

  /// call repo restful api here
  Future<dynamic> login(dynamic request) async {
    //authLogin là path url
    return httpManager.postLogin(url: authLogin, data: request);
  }

  /// call services restful api here
  Future<dynamic> getBeautyServices(GetServicesModel request) async {
    //authLogin là path url
    return httpManager.postNoAuth(url: getBeautyService, data: request.toJson());
  }

  Future<dynamic> getBeautyIdService(GetServicesModel request) async {
    //authLogin là path url
    return httpManager.postNoAuth(url: getBeautyServiceId, data: request.toJson());
  }

  Future<dynamic> getBranch() async {
    //authLogin là path url
    return httpManager.post(url: branchId);
  }

  Future<dynamic> getPackageBook(GetServicesModel request) async {
    //authLogin là path url
    return httpManager.post(url: beautyServiceBook, data: request.toJson());
  }

  Future<dynamic> getDoctor(var request) async {
    //authLogin là path url
    return httpManager.get(url: beautyDoctor, parameters: request);
  }

  Future<dynamic> getTime(GetServicesModel request) async {
    //authLogin là path url
    return httpManager.post(url: beautyTime, data: request.toJson());
  }

  Future<dynamic> booking(BookingDto request) async {
    //authLogin là path url
    return httpManager.post(url: beautyBooking, data: request.toJson());
  }
  Future<dynamic> changePass(var data) async {
    //authLogin là path url
    return httpManager.postOauthDio(url: changePassUri, data: data);
  }

  Future<dynamic> updateToken() async {
    return httpManager.initApiClient();
  }


  Future<dynamic> checkExistByPhone({var queryParam}) async {
    return httpManager.postNoAuth(
      url: existPhone,
      queryParam: queryParam,
    );
  }

  Future<dynamic> getOriginalId({var queryParam}) async {
    return httpManager.postNoAuth(
        url: getOrganizationId, data: {}, queryParam: queryParam);
  }

  ///get Danh sách goi combo dat licj
  Future<dynamic> getComboBook(GetServicesModel request) async {
    return httpManager.postNoAuth(url: comboBook, data: request.toJson());
  }
  // lay danh sach goi combo trong dich vu
  Future<dynamic> getComboList(GetServicesModel request) async {
    return httpManager.postNoAuth(url: getCombo, data: request.toJson());
  }
  Future<dynamic> getBeautyIdCombo({int? id}) async {
    //authLogin là path url
    return httpManager.getNoAuth(url:"$getComboIdPackage/$id",parameters: {});
  }
  ///get Danh sách lịch đã đặt // sắp tới
  Future<dynamic> getHistoryBooking(var request) async {
    return httpManager.post(url: historyBooking, data: request);
  }
  ///get Danh sách lịch đã đặt // sắp tới
  Future<dynamic> getHostBeautyApi(var request) async {
    return httpManager.postNoAuth(url: getHostBeauty, data: request.toJson());
  }
  ///get check hạn token
  Future<dynamic> getExpired() async {
    return httpManager.post(url:expiredTokenUri);
  }
  Future<dynamic> getExpiredSplash() async {
    return httpManager.postNoNotifyExpired(url:expiredTokenUri);
  }
  //todo get branch from service
  Future<dynamic> getBranchService({var data}) async {
    //authLogin là path url
    return httpManager.post(url: branchServiceId,data: data.toJson());
  }
  //todo get doctor booking fromService
  Future<dynamic> getDoctorFromService({var param}) async {
    //authLogin là path url
    return httpManager.get(url: getDoctorFromServiceUri,parameters: param);
  }
  //todo huy lich
  Future<dynamic> cancelBooking({var data}) async {
    //authLogin là path url
    return httpManager.post(url: cancelBookingUri,data: data);
  }
  Future<dynamic> getHotNews({var data}) async {
    //authLogin là path url
    return httpManager.postNoAuth(url: hotNewUri,data: data);
  }
  ///TODO get Phong ban
  Future<dynamic> getRoom({var data}) async {
    //authLogin là path url
    return httpManager.post(url: getRoomUri,data: data);
  }
  ///TODO get nguoi phu trach
  Future<dynamic> getPersonInCharge({var data}) async {
    //authLogin là path url
    return httpManager.get(url: getPersonInChargeUri,parameters: data);
  }
  Future<dynamic> createByNumberPhone({FormData? data, var queryParam}) async {
    return httpManager.postNoAuth(
        url: createByPhone, data: data);
  }
  Future<dynamic> createReport({FormData? data, var queryParam}) async {
    return httpManager.postFormData(
        url: createReportUri, data: data);
  }
  Future<dynamic> getFeedBack({var data}) async {
    return httpManager.post(url: getListFeedBack, data: data);
  }
  Future<dynamic> getListNotification({var data}) async {
    return httpManager.post(url: getListNotify, data: data);
  }
  Future<dynamic> getListNotificationEmp({var data}) async {
    return httpManager.post(url: getListNotifyEmpUri, data: data);
  }
  Future<dynamic> seenNotification({int? id}) async {
    return httpManager.get(url: "$seenNotify/$id",parameters: {});
  }
  Future<dynamic> seenNotificationEmp({int? id}) async {
    return httpManager.get(url: "$seenNotifyEmp/$id",parameters: {});
  }
  Future<dynamic> subscribeNotify({var data}) async {
    return httpManager.postNoNotifyExpired(url: subscribeNotifyUri, data: data);
  }
  Future<dynamic> subscribeNotifyEmployee({var data}) async {
    return httpManager.postNoNotifyExpired(url: subscribeNotifyEmployeeUri, data: data);
  }
  Future<dynamic> unsubscribeNotify({var data}) async {
    return httpManager.post(url: unsubscribeNotifyUri, data: data);
  }
  Future<dynamic> unsubscribeNotifyEmp({var data}) async {
    return httpManager.post(url: unsubscribeNotifyEmployeeUri, data: data);
  }
  /// dungf cho splash khong hien thi dang xuat khi het token
  Future<dynamic> unsubscribeNotifySplash({var data}) async {
    return httpManager.postNoAuthorize(url: unsubscribeNotifyUri, data: data);
  }
  Future<dynamic> getLoyaltyOrganizationId() async {
    return httpManager.getNoAuth(url: getOrganizationIdUri, parameters: {});
  }
  Future<dynamic> setNewPassword(var data) async {
   return httpManager.postNoAuthorize(url: setNewPasswordUri, data: data);
  }
  Future<dynamic> updateAvatar({FormData? data, var queryParam}) async {
    return httpManager.postFormData(
        url: updateAvatarUri, data: data);
  }
  Future<dynamic> updateAvatarEmp({FormData? data, var queryParam}) async {
    return httpManager.postFormData(
        url: updateAvatarEmpUri, data: data);
  }
  /// Lay thong tin la khah hang hay nhan su
  Future<dynamic> checkTypeUser() async {
    return httpManager.getUnNotify(url: getTypeUser, parameters: {});
  }
  /// call api detail feedback
  Future<dynamic> detailFeedback(int? iDFeedback) async {
    //authLogin là path url
    return httpManager.get(url: "$getDetailFeedback/$iDFeedback", parameters: {});
  }
  /// lay thong tin check in
  Future<dynamic> checkinInfo(var body) async {
    //authLogin là path url
    return httpManager.post(url: getDetailCheckin, data: body);
  }
  // lay lich su ra vao
  Future<dynamic> historyCheckin(var body) async {
    //authLogin là path url
    return httpManager.post(url: getHistoryCheckin, data: body);
  }
  /// call api detail feedback
  Future<dynamic> getDetailNews(int? idNews) async {
    //authLogin là path url
    return httpManager.getNoAuth(url: "$getDetailNewsUri/$idNews", parameters: {});
  }
  /// call api get danh ba
  Future<dynamic> getPhoneBook() async {
    //authLogin là path url
    return httpManager.get(url: getPhoneBookUri, parameters: {});
  }
  /// tao bai viet
  Future<dynamic> createNewFeed({FormData? data}) async {
    //authLogin là path url
    return httpManager.postFormData(url: createNewFeedUri, data: data);
  }
  /// lay bai viet
  Future<dynamic> getNewFeed({var data}) async {
    //authLogin là path url
    return httpManager.post(url: getNewFeedsUri, data: data);
  }
  Future<dynamic> likeOrUnlike({var data}) async {
    //authLogin là path url
    return httpManager.postFormData(url: likeOrUnlikeUri, data: data);
  }
  /// lay danh sach binh luan
  Future<dynamic> getListCommit({var data}) async {
    //authLogin là path url
    return httpManager.post(url: listCommitUri, data: data);
  }
  /// lay danh sach binh luan
  Future<dynamic> delPost({int? id}) async {
    //authLogin là path url
    return httpManager.get(url: "$delPostUri/$id", parameters: {},);
  }
  /// tao user chat
  Future<dynamic>createUser()async{
    return httpManager.post(url: createUserChat,data: {});
  }

  Future<dynamic>createCommit({int? newsFeedId, int? parentId,FormData? data}) async {
    //authLogin là path url
    String uri = '';
    if(parentId != null){
     uri = '$createNewComment?newsFeedId=$newsFeedId&parentId=$parentId';
    }
    else{
      uri ='$createNewComment?newsFeedId=$newsFeedId';
    }
    return httpManager.postFormData(url: uri, data: data);
  }
  /// get user chat
  Future<dynamic>getAllUserChat()async{
    return httpManager.post(url: getUserChat,data: {});
  }
  /// get history chat
  Future<dynamic>getHistoryChat()async{
    return httpManager.post(url: getHistoryChatUri,data: {});
  }
  /// get list chat gan day
  Future<dynamic>getRecentlyChat()async{
    return httpManager.post(url: getRecentlyChatUri,data: {});
  }
}
