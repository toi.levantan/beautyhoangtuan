import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/model/booking/performer.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/contact_vm/contact_controller.dart';

import '../../../core/themes/themes.dart';
import '../../../utils/app/size_app.dart';
import '../../../utils/widget/textfieldcommon.dart';

class ContactPage extends GetWidget<ContactController> {
  const ContactPage({super.key});

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        appBar: appBarButtonEmp(title: 'Danh bạ nhân viên'),
        body: bodyWidget(
            listDataCheck: controller.contacts,
            isLoading: controller.isLoading,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SearchFormField(
                  editingController: controller.textEditingController,
                  onChangedFocus: (focus) {
                    if (!focus) {

                    }
                  },
                  onChange: (value){
                    controller.filterContact();
                  },
                ),
                Expanded(
                  child: Obx(() => Padding(
                    padding:  EdgeInsets.symmetric(vertical: size12),
                    child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: controller.contacts.length,
                          itemBuilder: (context, index) {
                            return itemContact(
                                performer: controller.contacts[index],context: context);
                          },
                        ),
                  )),
                ),
              ],
            )),
      ),
    );
  }

  Widget itemContact({Performer? performer,required BuildContext context}) {
    return Card(
      child: Padding(
        padding:  EdgeInsets.all(size16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              children: [
                ImagesRender.circleNetWorkImage("${performer?.imageStores?.first.fileId}",
                    circular: size100, width: ic_46, height: ic_46,boxFit: BoxFit.cover),
                SizedBox(width: size16),
                Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${performer?.fullName}',
                          style: Themes.sfText14_500,
                        ),
                        SizedBox(
                          height: size4,
                        ),
                        Row(
                          children: [
                            Text('${performer?.phoneNumber}',style: Themes.sfText12_400,),
                            SizedBox(width: size16),
                            InkWell(
                                onTap: (){
                                  Application.showSnackCopy(context: context);
                                  Clipboard.setData(ClipboardData(text: performer?.phoneNumber ??''));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Icon(CupertinoIcons.square_on_square,size: ic_12,color: colorFontStaff2,),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: size4,
                        ),
                        Text('${performer?.email}',style: Themes.sfText12_400,),
                        SizedBox(
                          height: size4,
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
            const Divider(color: colorIcon),
             Row(
               children: [
               Expanded(child: itemInfoShowEndTitleNoIcon(data: 'Gọi điện',img: ImagePath.phone_green,textStyle: Themes.sfText14_400. copyWith(color: colorComplete),
               onTap: (){
                 Application.makePhoneCall(performer?.phoneNumber ??'');
               })),
               Expanded(child: itemInfoShowEndTitleNoIcon(data: 'Nhắn tin',img: ImagePath.email,textStyle: Themes.sfText14_400.copyWith(color: colorMessage),onTap: (){
                 Application.makeMessage(performer?.phoneNumber ??'');
               })),
             ],)
          ],
        ),
      ),
    );
  }
}
