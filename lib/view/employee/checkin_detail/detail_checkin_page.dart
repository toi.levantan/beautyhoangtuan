import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/model/login/checkinInfo_dto.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/infinity.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/detail_checkin_vm/detail_checkin_controller.dart';
import '../../../core/routes/routers.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/path/image_paths.dart';
import '../../../utils/session/bottom_sheet.dart';
import '../../../utils/session/file_picker.dart';
import '../../../utils/widget/images_render.dart';

class DetailCheckIn extends GetWidget<DetailCheckinController> {
  const DetailCheckIn({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBarButtonTabBar(
            title: 'Chi tiết',
            tabBar: TabBar(
              tabs: controller.myTabs,
              controller: controller.tabController,
              labelColor: Colors.white,
              indicatorColor: colorIcon,
            )),
        body: TabBarView(
            controller: controller.tabController,
            children: [body(),face()]));
  }

  Widget body() {
    return Column(
      children: [
        Container(height: size56,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          border: Border.all(color: colorBackGroundAppBar,width: 2),
        ),
        child: buttonPickRangeTime(onPickTime: (dateTimes){
          controller.startTime =  dateTimes?.first ?? DateTime.now().subtract(const Duration(days: 31));
          controller.endTime = dateTimes?.last ?? DateTime.now();
          controller.onRefresh();
        },isShowFunction: (value){
          controller.isFilterAll.value = value ?? true;
        })),
        Expanded(
          child: bodyWidget(
              padding: EdgeInsets.zero,
              isLoading: controller.isLoading,
              listDataCheck: controller.checkinsRevertData,
              child: InfinityRefresh(
                  refreshController: controller.refreshController,
                  onRefresh: controller.onRefresh,
                  onLoading: controller.onLoad,
                  padding: EdgeInsets.zero,
                  child: Obx(() => ListView.builder(
                        itemCount: controller.checkinsRevertData.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return itemInfoCheckin(checkinInfoDto: controller.checkinsRevertData[index],index: index);},
                      )))),
        ),
      ],
    );
  }

  Widget face() {
    return bodyWidget(
      padding: EdgeInsets.zero,
      isLoading: controller.homeStaffController.isLoadingAccount,
      child: Column(
        children: [
          SizedBox(
            height: Get.height /5,
            child:  Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(alignment: Alignment.center, children: [
                  InkWell(
                    onTap: () {
                      BottomSheetCommon.getBottomPickCamera(
                        cameraTap: () async {
                          Get.back();
                          XFile? image = await ImagePicker()
                              .pickImage(source: ImageSource.camera);
                          if (image != null) {
                            CroppedFile? cropFile = await ImageCropper()
                                .cropImage(
                                sourcePath: image.path,
                                uiSettings: [
                                  AndroidUiSettings(
                                      toolbarTitle: 'Cropper',
                                      toolbarColor: Colors.deepOrange,
                                      toolbarWidgetColor: Colors.white,
                                      initAspectRatio:
                                      CropAspectRatioPreset.square,
                                      lockAspectRatio: false),
                                  IOSUiSettings(
                                    title: 'Cropper',
                                    minimumAspectRatio: 300.0,
                                    rectX: 300.0,
                                    rectY: 300.0,
                                    rectWidth: Get.width,
                                    rectHeight: Get.width,
                                  ),
                                ],
                                aspectRatioPresets: [
                                  CropAspectRatioPreset.square,
                                ]);
                            if (cropFile?.path != null) {
                              controller.imageFile.clear();
                              File file = File(cropFile!.path);
                              controller.imageFile.add(file);
                              controller.homeStaffController.updateAvatar(
                                  controller.imageFile.first);
                            }
                          }
                        },
                        galleryTap: () async {
                          Get.back();
                          File? filePicker =
                          await PickerFile.singleImageFilePicker();
                          if (filePicker != null &&
                              filePicker.path != '') {
                            CroppedFile? cropFile =
                            await ImageCropper().cropImage(
                                sourcePath: filePicker.path,
                                aspectRatioPresets: [
                                  CropAspectRatioPreset.square,
                                ],
                                uiSettings: [
                                  AndroidUiSettings(
                                      toolbarTitle: 'Cropper',
                                      toolbarColor: Colors.deepOrange,
                                      toolbarWidgetColor:
                                      Colors.white,
                                      initAspectRatio:
                                      CropAspectRatioPreset
                                          .square,
                                      lockAspectRatio: false),
                                  IOSUiSettings(
                                    title: 'Cropper',
                                    minimumAspectRatio: 300.0,
                                    rectX: 300.0,
                                    rectY: 300.0,
                                    rectWidth: Get.width,
                                    rectHeight: Get.width,
                                  ),
                                ],
                                maxHeight: 300,
                                maxWidth: 300);

                            if (cropFile?.path != null) {
                              File fileCrop = File(cropFile!.path);
                              controller.imageFile.clear();
                              controller.imageFile.add(fileCrop);
                              controller.homeStaffController.updateAvatar(
                                  controller.imageFile.first);
                            }
                          }
                        },
                      );
                    },
                    child: Stack(
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: SizedBox(
                                width: ic_86,
                                height: ic_86,
                                child: Obx(() =>
                                    ImagesRender.circleNetWorkImage(
                                        "${controller.homeStaffController.avatarValue}",
                                        circular: 100,
                                        width: ic_86,
                                        height: ic_86,
                                        boxFit: BoxFit.cover)))),
                        Positioned(
                            right: 10,
                            bottom: 10,
                            child: ImagesRender.svgPicture(
                                src: ImagePath.edit, height: ic_20)),
                      ],
                    ),
                  ),
                ]),
                SizedBox(height: size8,),
                Text(PreferenceUtils.getString(userKey) ??"",style: Themes.sfText18_700)
              ],
            ),
          ),
          Card(
            child: Column(
              children: [
                itemInfoShowEndTitleNoIcon(img:  ImagePath.user_account,data: "Tên đăng nhập",endTitle: "${PreferenceUtils.getString(userAccount)}"),
                itemInfoShowEndTitleNoIcon(img:  ImagePath.birthday,data: "Sinh nhật",endTitle: "${PreferenceUtils.getString(birthday)}"),
                itemInfoShowEndTitleNoIcon(img:  ImagePath.position_account,data: "Chức vụ",endTitle: "${PreferenceUtils.getString(position)}"),
                itemInfoShowEndTitleNoIcon(img:  ImagePath.department,data: "Phòng ban",endTitle: "${PreferenceUtils.getString(department)}"),
                itemInfoShowEndTitleNoIcon(img:  ImagePath.email,data: "Email",endTitle: "${PreferenceUtils.getString(email)}"),
                itemInfoShowEndTitleNoIcon(img:  ImagePath.phone,data: "Điện thoại",endTitle: "${PreferenceUtils.getString(phoneData)}"),
                itemInfoShowEndTitleNoIcon(img:  ImagePath.address,data: "Địa chỉ",endTitle: "${PreferenceUtils.getString(address)}"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget itemInfoCheckin({CheckinInfoDto? checkinInfoDto, required int index}) {
    return Padding(
      padding:  EdgeInsets.only(left: size8,right: size8,top: size8),
      child: InkWell(
        onTap: (){
          Get.toNamed(Routers.history_checkin,arguments: checkinInfoDto);
        },
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
            width: size56,
            height: size56,
            alignment: Alignment.center,
            decoration: BoxDecoration(
               color: colorIcon,
              borderRadius: BorderRadius.circular(size82),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [Text("${getValueDOEShort(dateToString(dateTime: strToDate(valueTimeString: checkinInfoDto?.date ??""), dateFormatTime: dOEMap[DateFormatCustom.EEEE]))}",style: Themes.sfText10_400.copyWith(color: Colors.white)),
                SizedBox(height: size2,),
                Text(dateToString(dateTime: strToDate(valueTimeString: checkinInfoDto?.date ??""), dateFormatTime: dOEMap[DateFormatCustom.DD_MM]),style: Themes.sfText10_500.copyWith(color: Colors.white))],
            ),
          ),
          SizedBox(width: size8,),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  color: colorFontStaff,
                  borderRadius: BorderRadius.circular(size8)),
              child: Padding(
                padding: EdgeInsets.all(size12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(checkinInfoDto?.groupName ?? '',
                                style: Themes.sfText15_700),
                            SizedBox(height: size4),
                            Text(checkinInfoDto?.fullName ?? ''),
                          ],
                        ),
                        RichText(
                            text: TextSpan(
                                text:
                                    '${isoDateToString(isoDate: checkinInfoDto?.workStartAt, dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])}',
                                children: [
                                  TextSpan(
                                      text: ' - ',
                                      style: Themes.sfHintText11_500),
                                  TextSpan(
                                      text:
                                          '${isoDateToString(isoDate: checkinInfoDto?.workEndAt, dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])}',
                                      style: Themes.sfHintText11_500),
                                ],
                                style: Themes.sfHintText11_500))
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: size8),
                      child: Divider(
                        height: size1,
                        color: Colors.white,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: size10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(children:  [
                                    Icon(CupertinoIcons.arrow_up_square_fill,color: colorCheckin,size: ic_24),
                                    SizedBox(width: size8),
                                    Obx(() => Text(controller.checkinsRevertData[index].checkinTime != null ? controller.checkinsRevertData[index].checkinTime.toString() :"--:--",style: Themes.sfText14_400.copyWith(color: colorCheckin)))
                                  ],),
                                  SizedBox(width: size16,),
                                  Row(children: [
                                    Icon(CupertinoIcons.arrow_down_square_fill,color: Colors.deepOrangeAccent,size: ic_24),
                                    SizedBox(width: size8),
                                    Obx(() => Text(controller.checkinsRevertData[index].checkoutTime != null ? controller.checkinsRevertData[index].checkoutTime.toString() :"--:--",style: Themes.sfText14_400.copyWith(color: Colors.deepOrangeAccent)),)
                                  ],)
                                ],
                              ),
                            ),
                          ],
                        ),
                        // trang thai cham cong
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ]),
      ),
    );
  }
}
