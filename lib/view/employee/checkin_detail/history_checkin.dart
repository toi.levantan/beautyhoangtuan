import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/widget/infinity.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/detail_checkin_vm/history_checkin_controller.dart';
import '../../../core/routes/routers.dart';
import '../../../model/login/History_checkin.dart';
import '../../../utils/session/date_formatter.dart';
import '../../../utils/session/enum_session.dart';
import '../../../utils/widget/button_common.dart';

class HistoryCheckinPage extends GetWidget<HistoryCheckinController> {
  const HistoryCheckinPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBarButtonEmp(title: 'Lịch sử ra vào', actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: size16),
            child: const Icon(Icons.note_alt_outlined),
          )
        ]),
        body: InfinityRefresh(
          refreshController: controller.refreshController,
          onLoading: controller.onLoad,
          onRefresh: controller.onRefresh,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              infoDay(time: controller.checkinInfoDto.date??''),
              bodyWidget(
                listDataCheck: controller.hisCheckin,
                isLoading: controller.isLoading,
                padding: EdgeInsets.zero,
                child: Obx(() => ListView.builder(
                      shrinkWrap: true,
                      itemCount: controller.hisCheckin.length,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return itemHistoryCheckin(controller.hisCheckin[index]);
                      },
                    )),
              ),
            ],
          ),
        ));
  }

  Widget itemHistoryCheckin(HistoryCheckin? checkinInfoDto) {
    return InkWell(
      onTap: (){
        Get.toNamed(Routers.detail_history,
        arguments: [
          controller.checkinInfoDto,
          checkinInfoDto,
        ]);
      },
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(size12),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      CupertinoIcons.info_circle,
                      color: colorIcon,
                      size: size20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: size8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            checkinInfoDto?.fullName ?? '',
                            style: Themes.sfText16_400,
                          ),
                          SizedBox(height: size6),
                          RichText(
                              text: TextSpan(
                                  text:
                                      "${controller.checkinInfoDto.workShiftName}   ",
                                  children: [
                                    TextSpan(
                                        text: '(',
                                        style: Themes.sfHintText11_500),
                                    TextSpan(
                                        text:
                                            '${isoDateToString(isoDate: controller.checkinInfoDto.workStartAt, dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])}',
                                        style: Themes.sfHintText11_500),
                                    TextSpan(
                                        text: ' - ',
                                        style: Themes.sfHintText11_500),
                                    TextSpan(
                                        text:
                                            '${isoDateToString(isoDate: controller.checkinInfoDto.workEndAt, dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])}',
                                        style: Themes.sfHintText11_500),
                                    TextSpan(
                                        text: ')',
                                        style: Themes.sfHintText11_500),
                                  ],
                                  style: Themes.sfHintText11_500
                                      .copyWith(color: colorIcon))),
                        ],
                      ),
                    ),
                    Container(height: size42, width: 1, color: colorRoundBorder),
                  ],
                ),
              ),
              SizedBox(
                  width: size76w,
                  child: Padding(
                    padding: EdgeInsets.only(left: size16),
                    child: Text(
                      "${isoDateToString(isoDate: checkinInfoDto?.createdAt ?? "", dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])}",
                      style: Themes.sfText16_700,
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
