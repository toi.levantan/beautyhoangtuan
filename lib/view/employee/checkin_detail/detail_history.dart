import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import '../../../core/themes/themes.dart';
import '../../../utils/app/size_app.dart';
import '../../../utils/session/date_formatter.dart';
import '../../../utils/widget/images_render.dart';
import '../../../view_model/detail_checkin_vm/detail_history_controller.dart';

class DetailHistory extends GetWidget<DetailHistoryController> {
  const DetailHistory({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: appBarButtonEmp(title: 'Chi tiểt lịch sử ra vào'),
    body: body(context));
  }
  Widget body(BuildContext context){
   return bodyWidget(
     padding: EdgeInsets.all(size4),
     child: Column(
       children: [
         Padding(
           padding:  EdgeInsets.symmetric(horizontal: size12),
           child: infoDay(time: controller.checkinInfoDto?.date),
         ),
         Column(
           children: [
             itemInfoOnlyTitle(data: 'Tên nhân viên',endTitle: controller.historyCheckin?.fullName),
             itemInfoOnlyTitle(data: 'Ca',endTitle: 'Ca hành chính'),
             itemInfoOnlyTitle(data: 'Ghi chú',),
             itemInfoOnlyTitle(data: 'Thời gian',endTitle: isoDateToString(isoDate: controller.historyCheckin?.createdAt,dateTimeFormat: dOEMap[DateFormatCustom.HH_DD_YYYY] )),
             Padding(
               padding: const EdgeInsets.all(12.0),
               child: Row(
                 mainAxisSize: MainAxisSize.max,
                 children: [
                   Expanded(
                     child: Text(
                      'Ảnh nhận diện',
                       style: Themes.sfTextHint14_400,
                     ),
                   ),
                   ImagesRender.circleNetWorkImage(
                       circular: 8,
                       '${linkImageProcessor}storage/${controller.historyCheckin?.imageUrl}',
                       width: size160,
                       height: size120,
                       boxFit: BoxFit.cover),
                 ],
               ),
             ),
           ],
         )
       ],
     ),
   );
  }}
