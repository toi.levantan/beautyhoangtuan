import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/view/employee/day_off/signin_day_off.dart';
import 'package:myhoangtuan/view_model/day_off_vm/day_off_controller.dart';

import '../../../utils/path/color_path.dart';

class DayOffPage extends GetWidget<DayOffController> {
  const DayOffPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButtonTabBar(title: 'Đơn xin nghỉ phép',
      tabBar: TabBar(tabs: controller.myTabs,controller: controller.tabController,
        labelColor: Colors.white,
        indicatorColor: colorIcon,)),
      body: body(),
    );
  }
  Widget body(){
    return TabBarView(children: [
      SignDayOff(),
      SignDayOff(),
    ],controller: controller.tabController,);
  }
}
