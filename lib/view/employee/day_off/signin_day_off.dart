import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/view_model/day_off_vm/day_off_controller.dart';
import '../../../model/feedback/feedback_dto.dart';
import '../../../utils/app/size_app.dart';
import '../../../utils/widget/textfieldcommon.dart';
import '../../../utils/widget/utils_common.dart';

class SignDayOff extends GetWidget<DayOffController>{
  const SignDayOff({super.key});

  @override
  Widget build(BuildContext context) {
   return  Expanded(
       child: bodyWidget(
         child: SingleChildScrollView(
           child: Column(
             children: [
               markTitle(title: 'Số ngày nghỉ phép còn lại:', value: '1'),
               SizedBox(height: size16,),
               textFieldDropDown(
                   errorText: controller.dichVuErr,
                   onTap: (){
                     List<SelectedListItem> listConvert = [];
                     if (controller.listContent.isNotEmpty) {
                       for (Content element in controller.listContent) {
                         SelectedListItem selectItem =
                         SelectedListItem(
                             name: element.files ?? '',
                             value: element.id.toString());
                         listConvert.add(selectItem);
                       }
                     }
                     dropDownSearch(
                         sourceList: listConvert,
                         context: Get.context!,
                         bottomSheetTitle: "Loại nghỉ phép",
                         onSelectedValue: (name, value) {
                             // TODO xử lý khi chọn
                         });
                   },
                   label: 'Loại nghỉ phép*',
                   hintText: 'Nghỉ phép',
                   editingController: controller.textEdtControllers.first),
                   SizedBox(height: size16,),
               textFieldDropDown(
                   errorText: controller.dichVuErr,
                   onTap: (){
                     List<SelectedListItem> listConvert = [];
                     if (controller.listContent.isNotEmpty) {
                       for (Content element in controller.listContent) {
                         SelectedListItem selectItem =
                         SelectedListItem(
                             name: element.files ?? '',
                             value: element.id.toString());
                         listConvert.add(selectItem);
                       }
                     }
                     dropDownSearch(
                         sourceList: listConvert,
                         context: Get.context!,
                         bottomSheetTitle: "Loại nghỉ phép",
                         onSelectedValue: (name, value) {
                           // TODO xử lý khi chọn
                         });
                   },
                   label: 'Loại nghỉ phép*',
                   hintText: 'Nghỉ phép',
                   editingController: controller.textEdtControllers.first),
                   SizedBox(height: size16,),
               textFieldDropDown(
                   errorText: controller.dichVuErr,
                   onTap: (){
                     List<SelectedListItem> listConvert = [];
                     if (controller.listContent.isNotEmpty) {
                       for (Content element in controller.listContent) {
                         SelectedListItem selectItem =
                         SelectedListItem(
                             name: element.files ?? '',
                             value: element.id.toString());
                         listConvert.add(selectItem);
                       }
                     }
                     dropDownSearch(
                         sourceList: listConvert,
                         context: Get.context!,
                         bottomSheetTitle: "Loại nghỉ phép",
                         onSelectedValue: (name, value) {
                           // TODO xử lý khi chọn
                         });
                   },
                   label: 'Loại nghỉ phép*',
                   hintText: 'Nghỉ phép',
                   editingController: controller.textEdtControllers.first),
                   SizedBox(height: size16,),
               textFieldDropDown(
                   errorText: controller.dichVuErr,
                   onTap: (){
                     List<SelectedListItem> listConvert = [];
                     if (controller.listContent.isNotEmpty) {
                       for (Content element in controller.listContent) {
                         SelectedListItem selectItem =
                         SelectedListItem(
                             name: element.files ?? '',
                             value: element.id.toString());
                         listConvert.add(selectItem);
                       }
                     }
                     dropDownSearch(
                         sourceList: listConvert,
                         context: Get.context!,
                         bottomSheetTitle: "Loại nghỉ phép",
                         onSelectedValue: (name, value) {
                           // TODO xử lý khi chọn
                         });
                   },
                   label: 'Thời gian',
                   hintText: 'Nghỉ phép',
                   editingController: controller.textEdtControllers.first),
                   SizedBox(height: size16,),
             ],
           ),
         ),
       ),
   );
  }

}