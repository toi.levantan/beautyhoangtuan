import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/home_staff_vm/home_staff_controller.dart';
import 'package:share_plus/share_plus.dart';
import '../../../../core/routes/routers.dart';
import '../../../../core/themes/themes.dart';
import '../../../../model/service/res_services_dto.dart';
import '../../../../utils/app/share_pref.dart';
import '../../../../utils/path/color_path.dart';
import '../../../../utils/path/const_key.dart';
import '../../../../utils/path/image_paths.dart';
import '../../../../utils/session/bottom_sheet.dart';
import '../../../../utils/session/file_picker.dart';
import '../../../../utils/session/helper_notification.dart';

class ProfileScreenStaff extends GetWidget<HomeStaffController> {
  const ProfileScreenStaff({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return bodyWidget(
      padding: EdgeInsets.zero,
      isLoading: controller.isLoadingAccount,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Stack(
              children: [
                ImagesRender.imagesAssets(src: ImagePath.account_background,width: Get.width,height: Get.width *(3/5)),
                Column(
                  children: [
                    SizedBox(height: size40,),
                    Stack(alignment: Alignment.topRight, children: [
                      InkWell(
                        onTap: () {
                          BottomSheetCommon.getBottomPickCamera(
                            cameraTap: () async {
                              Get.back();
                              XFile? image = await ImagePicker()
                                  .pickImage(source: ImageSource.camera);

                              if (image != null) {
                                CroppedFile? cropFile = await ImageCropper()
                                    .cropImage(
                                    sourcePath: image.path,
                                    uiSettings: [
                                      AndroidUiSettings(
                                          toolbarTitle: 'Cropper',
                                          toolbarColor: Colors.deepOrange,
                                          toolbarWidgetColor: Colors.white,
                                          initAspectRatio:
                                          CropAspectRatioPreset.square,
                                          lockAspectRatio: false),
                                      IOSUiSettings(
                                        title: 'Cropper',
                                        minimumAspectRatio: 300.0,
                                        rectX: 300.0,
                                        rectY: 300.0,
                                        rectWidth: Get.width,
                                        rectHeight: Get.width,
                                      ),
                                    ],
                                    aspectRatioPresets: [
                                      CropAspectRatioPreset.square,
                                    ]);
                                if (cropFile?.path != null) {
                                  controller.imageFile.clear();
                                  File file = File(cropFile!.path);
                                  controller.imageFile.add(file);
                                  controller.updateAvatar(
                                      controller.imageFile.first);
                                }
                                Get.back();
                                // controller.validateButton();
                              }
                            },
                            galleryTap: () async {
                              Get.back();
                              File? filePicker =
                              await PickerFile.singleImageFilePicker();
                              if (filePicker != null &&
                                  filePicker.path != '') {
                                CroppedFile? cropFile =
                                await ImageCropper().cropImage(
                                    sourcePath: filePicker.path,
                                    aspectRatioPresets: [
                                      CropAspectRatioPreset.square,
                                    ],
                                    uiSettings: [
                                      AndroidUiSettings(
                                          toolbarTitle: 'Cropper',
                                          toolbarColor: Colors.deepOrange,
                                          toolbarWidgetColor:
                                          Colors.white,
                                          initAspectRatio:
                                          CropAspectRatioPreset
                                              .square,
                                          lockAspectRatio: false),
                                      IOSUiSettings(
                                        title: 'Cropper',
                                        minimumAspectRatio: 300.0,
                                        rectX: 300.0,
                                        rectY: 300.0,
                                        rectWidth: Get.width,
                                        rectHeight: Get.width,
                                      ),
                                    ],
                                    maxHeight: 300,
                                    maxWidth: 300);

                                if (cropFile?.path != null) {
                                  File fileCrop = File(cropFile!.path);
                                  controller.imageFile.clear();
                                  controller.imageFile.add(fileCrop);
                                  controller.updateAvatar(
                                      controller.imageFile.first);
                                }
                                // controller.validateButton();
                                Get.back();
                              }
                            },
                          );
                        },
                        child: Stack(
                          children: [
                            Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: SizedBox(
                                    width: ic_86,
                                    height: ic_86,
                                    child: Obx(() =>
                                        ImagesRender.circleNetWorkImage(
                                            "${controller.avatarValue}",
                                            circular: 100,
                                            width: ic_86,
                                            height: ic_86,
                                            boxFit: BoxFit.cover)))),
                            Positioned(
                                right: 10,
                                bottom: 10,
                                child: ImagesRender.svgPicture(
                                    src: ImagePath.edit, height: ic_20)),
                          ],
                        ),
                      ),
                    ]),

                    //TODO điều khoản và quy định
                    Column(
                      children: [
                        Text(
                          PreferenceUtils.getString(userKey)?.toUpperCase() ?? "",
                          style: Themes.sfText16_700.copyWith(color: Colors.white),
                        ),
                        SizedBox(
                          height: size8,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 32),
                          child: itemInfo(
                              img: ImagePath.dia_chi,
                              data: PreferenceUtils.getString(address)),
                        ),
                        SizedBox(
                          height: size4,
                        ),
                        itemInfo(
                            img: ImagePath.calling,
                            data:
                            "(+84) ${PreferenceUtils.getString(phoneData) == null || PreferenceUtils.getString(phoneData).toString().isEmpty ? "" :
                            PreferenceUtils.getString(phoneData)?.toUpperCase().substring(1)}"),
                        SizedBox(height: size20,)
                      ],
                    ),
                    SizedBox(
                      height: size24,
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(bottom: size16),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                EdgeInsets.only(left: size16, right: size16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: size8,
                                    ),
                                    Text(
                                      "Cài đặt",
                                      style: Themes.sfText18_700,
                                    ),
                                    SizedBox(
                                      height: size10,
                                    ),
                                    itemInfoShowAsset(
                                        img: ImagePath.sercurity_lock,
                                        data: "Đổi mật khẩu",
                                        onTap: () {
                                          Get.toNamed(Routers.change_pass);
                                        }),
                                    itemInfoShowEndTitle(
                                        img: ImagePath.global,
                                        data: "Ngôn ngữ",
                                        endTitle: controller.languageNote,
                                        onTap: () {}),
                                  ],
                                ),
                              ),
                              Container(
                                height: size6,
                                color: colorBackGroundSearch,
                              ),
                              SizedBox(
                                height: size6,
                              ),
                              Padding(
                                padding:
                                EdgeInsets.only(left: size16, right: size16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: size8,
                                    ),
                                    Text(
                                      "Điều khoản và quy định",
                                      style: Themes.sfText18_700,
                                    ),
                                    SizedBox(
                                      height: size10,
                                    ),
                                    itemInfoShow(
                                        img: ImagePath.alert,
                                        data: "Thông tin ứng dụng",
                                        onTap: () {
                                          Get.toNamed(Routers.html_view,
                                              arguments: Content(
                                                  title: "Thông tin ứng dụng",
                                                  content:
                                                  "<h4><strong>Chính Sách Bảo Mật</strong></h4><p>Ứng dụng MyHoangTuan thuộc sở hữu của Hoàng Tuấn, là bên kiểm soát dữ liệu đối với dữ liệu cá nhân của bạn.</p><p>Chúng tôi đã thông qua Chính Sách Bảo Mật này, xác định cách chúng tôi xử lý thông tin do https://drhoangtuan.vn/ thu thập, đồng thời cung cấp lý do tại sao chúng tôi phải thu thập một số dữ liệu cá nhân nhất định về bạn. Do đó, bạn phải đọc Chính Sách Bảo Mật này trước khi sử dụng trang web https://drhoangtuan.vn/.</p><p>Chúng tôi chăm sóc dữ liệu cá nhân của bạn và cam kết đảm bảo tính bảo mật và an toàn của dữ liệu đó.</p><h4><strong>Thông tin cá nhân chúng tôi thu thập:</strong></h4><p>Khi bạn truy cập ứng dụng MyHoangTuan, chúng tôi tự động thu thập một số thông tin nhất định về thiết bị của bạn, bao gồm thông tin về trình duyệt web, địa chỉ IP, múi giờ và một số cookie đã cài đặt trên thiết bị của bạn. Ngoài ra, khi bạn duyệt trang web, chúng tôi thu thập thông tin về các trang web hoặc sản phẩm riêng lẻ mà bạn xem, những trang web hoặc cụm từ tìm kiếm nào đã giới thiệu bạn đến trang web và cách bạn tương tác với trang web. Chúng tôi gọi thông tin được thu thập tự động này là “Thông tin thiết bị”. Hơn nữa, chúng tôi có thể thu thập dữ liệu cá nhân mà bạn cung cấp cho chúng tôi (bao gồm nhưng không giới hạn ở Tên, Họ, Địa Chỉ, Thông Tin Thanh Toán, v.v.) trong quá trình đăng ký để có thể thực hiện thỏa thuận.</p><h4><strong>Tại sao chúng tôi xử lý dữ liệu của bạn?</strong></h4><p>Ưu tiên hàng đầu của chúng tôi là bảo mật dữ liệu khách hàng và do đó, chúng tôi có thể chỉ xử lý dữ liệu người dùng tối thiểu, chỉ ở chừng mực cần thiết để duy trì trang web. Thông tin thu thập tự động chỉ được sử dụng để xác định các trường hợp lạm dụng tiềm ẩn và thiết lập thông tin thống kê liên quan đến việc sử dụng trang web. Thông tin thống kê này không được tổng hợp theo cách khác để xác định bất kỳ người dùng cụ thể nào của hệ thống.</p><p>Bạn có thể truy cập trang web mà không cần cho chúng tôi biết bạn là ai hoặc tiết lộ bất kỳ thông tin nào mà ai đó có thể xác định bạn là một cá nhân cụ thể, có thể nhận dạng được. Tuy nhiên, nếu bạn muốn sử dụng một số tính năng của trang web hoặc bạn muốn nhận bản tin của chúng tôi hoặc cung cấp các chi tiết khác bằng cách điền vào biểu mẫu, bạn có thể cung cấp dữ liệu cá nhân cho chúng tôi, chẳng hạn như email, họ, tên, thành phố của bạn, nơi cư trú, tổ chức, số điện thoại. Bạn có thể chọn không cung cấp cho chúng tôi dữ liệu cá nhân của mình, nhưng khi đó bạn có thể không tận dụng được một số tính năng của trang web. Ví dụ: bạn sẽ không thể nhận bản tin của chúng tôi hoặc liên hệ trực tiếp với chúng tôi từ trang web. Người dùng không chắc chắn về những thông tin bắt buộc có thể liên hệ với chúng tôi qua thammyhoangtuan@gmail.com.</p><h4><strong>Quyền của bạn:</strong></h4><p>Nếu bạn là công dân Châu Âu, bạn có các quyền sau liên quan đến dữ liệu cá nhân của mình:</p><ul><li>Quyền được thông báo.</li><li>Quyền truy cập.</li><li>Quyền sửa chữa.</li><li>Quyền xóa bỏ.</li><li>Quyền hạn chế xử lý.</li><li>Quyền đối với tính khả chuyển của dữ liệu.</li><li>Quyền phản đối.</li><li>Các quyền liên quan đến việc ra quyết định và lập hồ sơ tự động.</li></ul><p>Nếu bạn muốn thực hiện quyền này, vui lòng liên hệ với chúng tôi qua thông tin liên hệ bên dưới.</p><p>Ngoài ra, nếu bạn là cư dân Châu Âu, chúng tôi lưu ý rằng chúng tôi đang xử lý thông tin của bạn để thực hiện các hợp đồng mà chúng tôi có thể có với bạn (ví dụ: nếu bạn đặt hàng qua trang web) hoặc theo cách khác để theo đuổi lợi ích kinh doanh hợp pháp của chúng tôi được liệt kê ở trên. Ngoài ra, xin lưu ý rằng thông tin của bạn có thể được chuyển ra ngoài Châu Âu, bao gồm cả Canada và Hoa Kỳ.</p><h4><strong>Liên kết đến các trang web khác:</strong></h4><p>Trang web của chúng tôi có thể chứa các liên kết đến các trang web khác không do chúng tôi sở hữu hoặc kiểm soát. Xin lưu ý rằng chúng tôi không chịu trách nhiệm về các trang web khác như vậy hoặc các thông lệ về quyền riêng tư của bên thứ ba. Chúng tôi khuyến khích bạn lưu ý khi bạn rời khỏi trang web của chúng tôi và đọc các điều khoản về quyền riêng tư của mỗi trang web có thể thu thập thông tin cá nhân.</p><h4><strong>Bảo mật thông tin:</strong></h4><p>Chúng tôi bảo mật thông tin bạn cung cấp trên máy chủ trong một môi trường được kiểm soát, an toàn, được bảo vệ khỏi sự truy cập, sử dụng hoặc tiết lộ trái phép. Chúng tôi giữ các biện pháp bảo vệ hành chính, kỹ thuật và vật lý hợp lý để bảo vệ chống lại việc truy cập, sử dụng, sửa đổi trái phép và tiết lộ dữ liệu cá nhân trong sự kiểm soát và lưu giữ của chúng tôi. Tuy nhiên, không thể đảm bảo việc truyền dữ liệu qua Internet hoặc mạng không dây.</p><h4><strong>Tiết lộ pháp lý:</strong></h4><p>Chúng tôi sẽ tiết lộ bất kỳ thông tin nào chúng tôi thu thập, sử dụng hoặc nhận được nếu pháp luật yêu cầu hoặc cho phép, chẳng hạn như để tuân thủ khi hầu tòa hoặc quy trình pháp lý tương tự và khi chúng tôi tin rằng việc tiết lộ là cần thiết để bảo vệ quyền lợi của chúng tôi, bảo vệ sự an toàn của bạn hoặc sự an toàn của những người khác, điều tra gian lận hoặc đáp ứng yêu cầu của chính phủ.</p><h4><strong>Thông tin liên lạc:</strong></h4><p>Nếu bạn muốn liên hệ với chúng tôi để hiểu thêm về Chính sách này hoặc muốn liên hệ với chúng tôi về bất kỳ vấn đề nào liên quan đến quyền cá nhân và Thông tin cá nhân của bạn, bạn có thể gửi email tới thammyhoangtuan@gmail.com</p>"));
                                        }),
                                    itemInfoShow(
                                        img: ImagePath.book,
                                        data: "Điều khoản sử dụng",
                                        onTap: () {
                                          Get.toNamed(Routers.html_view,
                                              arguments: Content(
                                                  title: "Điều khoản sử dụng",
                                                  content:
                                                  "<h4><strong>Chính Sách Bảo Mật</strong></h4><p>Ứng dụng MyHoangTuan thuộc sở hữu của Hoàng Tuấn, là bên kiểm soát dữ liệu đối với dữ liệu cá nhân của bạn.</p><p>Chúng tôi đã thông qua Chính Sách Bảo Mật này, xác định cách chúng tôi xử lý thông tin do https://drhoangtuan.vn/ thu thập, đồng thời cung cấp lý do tại sao chúng tôi phải thu thập một số dữ liệu cá nhân nhất định về bạn. Do đó, bạn phải đọc Chính Sách Bảo Mật này trước khi sử dụng trang web https://drhoangtuan.vn/.</p><p>Chúng tôi chăm sóc dữ liệu cá nhân của bạn và cam kết đảm bảo tính bảo mật và an toàn của dữ liệu đó.</p><h4><strong>Thông tin cá nhân chúng tôi thu thập:</strong></h4><p>Khi bạn truy cập ứng dụng MyHoangTuan, chúng tôi tự động thu thập một số thông tin nhất định về thiết bị của bạn, bao gồm thông tin về trình duyệt web, địa chỉ IP, múi giờ và một số cookie đã cài đặt trên thiết bị của bạn. Ngoài ra, khi bạn duyệt trang web, chúng tôi thu thập thông tin về các trang web hoặc sản phẩm riêng lẻ mà bạn xem, những trang web hoặc cụm từ tìm kiếm nào đã giới thiệu bạn đến trang web và cách bạn tương tác với trang web. Chúng tôi gọi thông tin được thu thập tự động này là “Thông tin thiết bị”. Hơn nữa, chúng tôi có thể thu thập dữ liệu cá nhân mà bạn cung cấp cho chúng tôi (bao gồm nhưng không giới hạn ở Tên, Họ, Địa Chỉ, Thông Tin Thanh Toán, v.v.) trong quá trình đăng ký để có thể thực hiện thỏa thuận.</p><h4><strong>Tại sao chúng tôi xử lý dữ liệu của bạn?</strong></h4><p>Ưu tiên hàng đầu của chúng tôi là bảo mật dữ liệu khách hàng và do đó, chúng tôi có thể chỉ xử lý dữ liệu người dùng tối thiểu, chỉ ở chừng mực cần thiết để duy trì trang web. Thông tin thu thập tự động chỉ được sử dụng để xác định các trường hợp lạm dụng tiềm ẩn và thiết lập thông tin thống kê liên quan đến việc sử dụng trang web. Thông tin thống kê này không được tổng hợp theo cách khác để xác định bất kỳ người dùng cụ thể nào của hệ thống.</p><p>Bạn có thể truy cập trang web mà không cần cho chúng tôi biết bạn là ai hoặc tiết lộ bất kỳ thông tin nào mà ai đó có thể xác định bạn là một cá nhân cụ thể, có thể nhận dạng được. Tuy nhiên, nếu bạn muốn sử dụng một số tính năng của trang web hoặc bạn muốn nhận bản tin của chúng tôi hoặc cung cấp các chi tiết khác bằng cách điền vào biểu mẫu, bạn có thể cung cấp dữ liệu cá nhân cho chúng tôi, chẳng hạn như email, họ, tên, thành phố của bạn, nơi cư trú, tổ chức, số điện thoại. Bạn có thể chọn không cung cấp cho chúng tôi dữ liệu cá nhân của mình, nhưng khi đó bạn có thể không tận dụng được một số tính năng của trang web. Ví dụ: bạn sẽ không thể nhận bản tin của chúng tôi hoặc liên hệ trực tiếp với chúng tôi từ trang web. Người dùng không chắc chắn về những thông tin bắt buộc có thể liên hệ với chúng tôi qua thammyhoangtuan@gmail.com.</p><h4><strong>Quyền của bạn:</strong></h4><p>Nếu bạn là công dân Châu Âu, bạn có các quyền sau liên quan đến dữ liệu cá nhân của mình:</p><ul><li>Quyền được thông báo.</li><li>Quyền truy cập.</li><li>Quyền sửa chữa.</li><li>Quyền xóa bỏ.</li><li>Quyền hạn chế xử lý.</li><li>Quyền đối với tính khả chuyển của dữ liệu.</li><li>Quyền phản đối.</li><li>Các quyền liên quan đến việc ra quyết định và lập hồ sơ tự động.</li></ul><p>Nếu bạn muốn thực hiện quyền này, vui lòng liên hệ với chúng tôi qua thông tin liên hệ bên dưới.</p><p>Ngoài ra, nếu bạn là cư dân Châu Âu, chúng tôi lưu ý rằng chúng tôi đang xử lý thông tin của bạn để thực hiện các hợp đồng mà chúng tôi có thể có với bạn (ví dụ: nếu bạn đặt hàng qua trang web) hoặc theo cách khác để theo đuổi lợi ích kinh doanh hợp pháp của chúng tôi được liệt kê ở trên. Ngoài ra, xin lưu ý rằng thông tin của bạn có thể được chuyển ra ngoài Châu Âu, bao gồm cả Canada và Hoa Kỳ.</p><h4><strong>Liên kết đến các trang web khác:</strong></h4><p>Trang web của chúng tôi có thể chứa các liên kết đến các trang web khác không do chúng tôi sở hữu hoặc kiểm soát. Xin lưu ý rằng chúng tôi không chịu trách nhiệm về các trang web khác như vậy hoặc các thông lệ về quyền riêng tư của bên thứ ba. Chúng tôi khuyến khích bạn lưu ý khi bạn rời khỏi trang web của chúng tôi và đọc các điều khoản về quyền riêng tư của mỗi trang web có thể thu thập thông tin cá nhân.</p><h4><strong>Bảo mật thông tin:</strong></h4><p>Chúng tôi bảo mật thông tin bạn cung cấp trên máy chủ trong một môi trường được kiểm soát, an toàn, được bảo vệ khỏi sự truy cập, sử dụng hoặc tiết lộ trái phép. Chúng tôi giữ các biện pháp bảo vệ hành chính, kỹ thuật và vật lý hợp lý để bảo vệ chống lại việc truy cập, sử dụng, sửa đổi trái phép và tiết lộ dữ liệu cá nhân trong sự kiểm soát và lưu giữ của chúng tôi. Tuy nhiên, không thể đảm bảo việc truyền dữ liệu qua Internet hoặc mạng không dây.</p><h4><strong>Tiết lộ pháp lý:</strong></h4><p>Chúng tôi sẽ tiết lộ bất kỳ thông tin nào chúng tôi thu thập, sử dụng hoặc nhận được nếu pháp luật yêu cầu hoặc cho phép, chẳng hạn như để tuân thủ khi hầu tòa hoặc quy trình pháp lý tương tự và khi chúng tôi tin rằng việc tiết lộ là cần thiết để bảo vệ quyền lợi của chúng tôi, bảo vệ sự an toàn của bạn hoặc sự an toàn của những người khác, điều tra gian lận hoặc đáp ứng yêu cầu của chính phủ.</p><h4><strong>Thông tin liên lạc:</strong></h4><p>Nếu bạn muốn liên hệ với chúng tôi để hiểu thêm về Chính sách này hoặc muốn liên hệ với chúng tôi về bất kỳ vấn đề nào liên quan đến quyền cá nhân và Thông tin cá nhân của bạn, bạn có thể gửi email tới thammyhoangtuan@gmail.com.</p>"));
                                        }),
                                    itemInfoShow(
                                        img: ImagePath.policy,
                                        data: "Chính sách sử dụng",
                                        onTap: () {
                                          Get.toNamed(Routers.html_view,
                                              arguments: Content(
                                                  title: "Chính sách sử dụng",
                                                  content:
                                                  "<h4><strong>Chính Sách Bảo Mật</strong></h4><p>Ứng dụng MyHoangTuan thuộc sở hữu của Hoàng Tuấn, là bên kiểm soát dữ liệu đối với dữ liệu cá nhân của bạn.</p><p>Chúng tôi đã thông qua Chính Sách Bảo Mật này, xác định cách chúng tôi xử lý thông tin do https://drhoangtuan.vn/ thu thập, đồng thời cung cấp lý do tại sao chúng tôi phải thu thập một số dữ liệu cá nhân nhất định về bạn. Do đó, bạn phải đọc Chính Sách Bảo Mật này trước khi sử dụng trang web https://drhoangtuan.vn/.</p><p>Chúng tôi chăm sóc dữ liệu cá nhân của bạn và cam kết đảm bảo tính bảo mật và an toàn của dữ liệu đó.</p><h4><strong>Thông tin cá nhân chúng tôi thu thập:</strong></h4><p>Khi bạn truy cập ứng dụng MyHoangTuan, chúng tôi tự động thu thập một số thông tin nhất định về thiết bị của bạn, bao gồm thông tin về trình duyệt web, địa chỉ IP, múi giờ và một số cookie đã cài đặt trên thiết bị của bạn. Ngoài ra, khi bạn duyệt trang web, chúng tôi thu thập thông tin về các trang web hoặc sản phẩm riêng lẻ mà bạn xem, những trang web hoặc cụm từ tìm kiếm nào đã giới thiệu bạn đến trang web và cách bạn tương tác với trang web. Chúng tôi gọi thông tin được thu thập tự động này là “Thông tin thiết bị”. Hơn nữa, chúng tôi có thể thu thập dữ liệu cá nhân mà bạn cung cấp cho chúng tôi (bao gồm nhưng không giới hạn ở Tên, Họ, Địa Chỉ, Thông Tin Thanh Toán, v.v.) trong quá trình đăng ký để có thể thực hiện thỏa thuận.</p><h4><strong>Tại sao chúng tôi xử lý dữ liệu của bạn?</strong></h4><p>Ưu tiên hàng đầu của chúng tôi là bảo mật dữ liệu khách hàng và do đó, chúng tôi có thể chỉ xử lý dữ liệu người dùng tối thiểu, chỉ ở chừng mực cần thiết để duy trì trang web. Thông tin thu thập tự động chỉ được sử dụng để xác định các trường hợp lạm dụng tiềm ẩn và thiết lập thông tin thống kê liên quan đến việc sử dụng trang web. Thông tin thống kê này không được tổng hợp theo cách khác để xác định bất kỳ người dùng cụ thể nào của hệ thống.</p><p>Bạn có thể truy cập trang web mà không cần cho chúng tôi biết bạn là ai hoặc tiết lộ bất kỳ thông tin nào mà ai đó có thể xác định bạn là một cá nhân cụ thể, có thể nhận dạng được. Tuy nhiên, nếu bạn muốn sử dụng một số tính năng của trang web hoặc bạn muốn nhận bản tin của chúng tôi hoặc cung cấp các chi tiết khác bằng cách điền vào biểu mẫu, bạn có thể cung cấp dữ liệu cá nhân cho chúng tôi, chẳng hạn như email, họ, tên, thành phố của bạn, nơi cư trú, tổ chức, số điện thoại. Bạn có thể chọn không cung cấp cho chúng tôi dữ liệu cá nhân của mình, nhưng khi đó bạn có thể không tận dụng được một số tính năng của trang web. Ví dụ: bạn sẽ không thể nhận bản tin của chúng tôi hoặc liên hệ trực tiếp với chúng tôi từ trang web. Người dùng không chắc chắn về những thông tin bắt buộc có thể liên hệ với chúng tôi qua thammyhoangtuan@gmail.com.</p><h4><strong>Quyền của bạn:</strong></h4><p>Nếu bạn là công dân Châu Âu, bạn có các quyền sau liên quan đến dữ liệu cá nhân của mình:</p><ul><li>Quyền được thông báo.</li><li>Quyền truy cập.</li><li>Quyền sửa chữa.</li><li>Quyền xóa bỏ.</li><li>Quyền hạn chế xử lý.</li><li>Quyền đối với tính khả chuyển của dữ liệu.</li><li>Quyền phản đối.</li><li>Các quyền liên quan đến việc ra quyết định và lập hồ sơ tự động.</li></ul><p>Nếu bạn muốn thực hiện quyền này, vui lòng liên hệ với chúng tôi qua thông tin liên hệ bên dưới.</p><p>Ngoài ra, nếu bạn là cư dân Châu Âu, chúng tôi lưu ý rằng chúng tôi đang xử lý thông tin của bạn để thực hiện các hợp đồng mà chúng tôi có thể có với bạn (ví dụ: nếu bạn đặt hàng qua trang web) hoặc theo cách khác để theo đuổi lợi ích kinh doanh hợp pháp của chúng tôi được liệt kê ở trên. Ngoài ra, xin lưu ý rằng thông tin của bạn có thể được chuyển ra ngoài Châu Âu, bao gồm cả Canada và Hoa Kỳ.</p><h4><strong>Liên kết đến các trang web khác:</strong></h4><p>Trang web của chúng tôi có thể chứa các liên kết đến các trang web khác không do chúng tôi sở hữu hoặc kiểm soát. Xin lưu ý rằng chúng tôi không chịu trách nhiệm về các trang web khác như vậy hoặc các thông lệ về quyền riêng tư của bên thứ ba. Chúng tôi khuyến khích bạn lưu ý khi bạn rời khỏi trang web của chúng tôi và đọc các điều khoản về quyền riêng tư của mỗi trang web có thể thu thập thông tin cá nhân.</p><h4><strong>Bảo mật thông tin:</strong></h4><p>Chúng tôi bảo mật thông tin bạn cung cấp trên máy chủ trong một môi trường được kiểm soát, an toàn, được bảo vệ khỏi sự truy cập, sử dụng hoặc tiết lộ trái phép. Chúng tôi giữ các biện pháp bảo vệ hành chính, kỹ thuật và vật lý hợp lý để bảo vệ chống lại việc truy cập, sử dụng, sửa đổi trái phép và tiết lộ dữ liệu cá nhân trong sự kiểm soát và lưu giữ của chúng tôi. Tuy nhiên, không thể đảm bảo việc truyền dữ liệu qua Internet hoặc mạng không dây.</p><h4><strong>Tiết lộ pháp lý:</strong></h4><p>Chúng tôi sẽ tiết lộ bất kỳ thông tin nào chúng tôi thu thập, sử dụng hoặc nhận được nếu pháp luật yêu cầu hoặc cho phép, chẳng hạn như để tuân thủ khi hầu tòa hoặc quy trình pháp lý tương tự và khi chúng tôi tin rằng việc tiết lộ là cần thiết để bảo vệ quyền lợi của chúng tôi, bảo vệ sự an toàn của bạn hoặc sự an toàn của những người khác, điều tra gian lận hoặc đáp ứng yêu cầu của chính phủ.</p><h4><strong>Thông tin liên lạc:</strong></h4><p>Nếu bạn muốn liên hệ với chúng tôi để hiểu thêm về Chính sách này hoặc muốn liên hệ với chúng tôi về bất kỳ vấn đề nào liên quan đến quyền cá nhân và Thông tin cá nhân của bạn, bạn có thể gửi email tới thammyhoangtuan@gmail.com</p>"));
                                        }),
                                  ],
                                ),
                              ),
                              Container(
                                height: size6,
                                color: colorBackGroundSearch,
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: size16),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Expanded(
                                          child: Row(
                                            children: [
                                              ImagesRender.imagesAssets(
                                                  src: ImagePath.gear,
                                                  width: ic_18,
                                                  height: ic_18,
                                                  boxFit: BoxFit.contain),
                                              SizedBox(
                                                width: size18,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Phiên bản',
                                                    style: Themes.sfText14_400,
                                                  ),
                                                  SizedBox(
                                                    height: size8,
                                                  ),
                                                  Text(
                                                    'v1.0',
                                                    style: Themes.sfText14_400,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // const Divider(height: 1,),
                                ],
                              ),
                              Container(
                                height: size6,
                                color: colorBackGroundSearch,
                              ),
                              Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 12.0, horizontal: size16),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        InkWell(
                                          onTap: () async {
                                            if (Platform.isIOS) {
                                              Share.share(PreferenceUtils.getString(
                                                  urlShareIOS) ??
                                                  '');
                                            } else {
                                              Share.share(PreferenceUtils.getString(
                                                  urlShareAndroid) ??
                                                  '');
                                            }
                                          },
                                          child: Row(
                                            children: [
                                              ImagesRender.imagesAssets(
                                                  src: ImagePath.share,
                                                  width: ic_18,
                                                  height: ic_18),
                                              SizedBox(
                                                width: size18,
                                              ),
                                              Text(
                                                'Chia sẻ ứng dụng',
                                                style: Themes.sfText14_400,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // const Divider(height: 1,),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  bottom: size33, left: size16, right: size16),
              child: SizedBox(
                height: size44,
                child: buttonCommon(
                    title: "Đăng xuất",
                    color: Colors.red,
                    isOK: true.obs,
                    function: () async {
                      //Get.offAllNamed(Routers.intro_page);
                      showDialog(barrierDismissible : false,context: context, builder: (_){
                        return AlertDialog(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(size24)),
                          title: Row(
                            children: [
                              Expanded(child: Text("Đăng xuất",style: Themes.sfText15_700)),
                              InkWell(
                                  onTap : (){
                                    Get.back();
                                  },
                                  child: Icon(CupertinoIcons.clear_circled_solid,color: colorIcon,size: ic_18,)),
                            ],
                          ),
                          content: SizedBox(
                            height: size74,
                            width: Get.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Bạn có chắc chắn muốn đăng xuất?',
                                  style: Themes.sfText12_400,),
                                SizedBox(height: size16,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    buttonLogout(title: "Có",
                                        colorText: Colors.black,
                                        function: () async{
                                          Get.back();
                                          FlutterAppBadger.removeBadge();
                                          PreferenceUtils.remove(keyAccessToken);
                                          checkHasLogin();
                                          controller.getUnSubNotify();
                                          Get.offAllNamed(Routers.home);
                                        }),
                                    buttonLogout(
                                        colorButton: colorIcon,
                                        title: 'Không',
                                        function: (){
                                          Get.back();
                                        }
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                    }),
              )),
        ],
      ),
    );
  }
  buttonLogout({Function? function,Color? colorButton,
    String? title,Color? colorText}){
    return Expanded(
      child: InkWell(
        onTap: (){
          if(function != null){
            function.call();
          }
        },
        child: Container(
          height: size40,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(size8),color: colorButton),
          child: Center(child: Text(title??'',style: Themes.sfText12_500.copyWith(color: colorText ?? Colors.white),)),
        ),
      ),
    );
  }
  Widget itemInfo({String? img, String? data}) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
            child: Text(data ?? '',
                style: Themes.sfText12_400.copyWith(color: Colors.white), overflow: TextOverflow.ellipsis)),
      ],
    );
  }

  Widget itemInfoShow({String? img, String? data, Function? onTap}) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap.call();
        }
      },
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      ImagesRender.imagesAssets(
                          src: img ?? '',
                          width: ic_18,
                          height: ic_18,
                          boxFit: BoxFit.contain),
                      SizedBox(
                        width: size18,
                      ),
                      Text(
                        data ?? '',
                        style: Themes.sfText14_400,
                      ),
                    ],
                  ),
                ),
                const Icon(Icons.keyboard_arrow_right_outlined),
              ],
            ),
          ),
          //  const Divider(height: 1,),
        ],
      ),
    );
  }

  Widget itemInfoShowAsset({String? img, String? data, Function? onTap}) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap.call();
        }
      },
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      ImagesRender.imagesAssets(
                          src: img ?? '',
                          width: ic_18,
                          height: ic_18,
                          boxFit: BoxFit.contain),
                      SizedBox(
                        width: size18,
                      ),
                      Text(
                        data ?? '',
                        style: Themes.sfText14_400,
                      ),
                    ],
                  ),
                ),
                const Icon(Icons.keyboard_arrow_right_outlined),
              ],
            ),
          ),
          //  const Divider(height: 1,),
        ],
      ),
    );
  }

  Widget itemInfoShowEndTitle(
      {String? img, String? data, Function? onTap, RxnString? endTitle}) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap.call();
        }
      },
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Row(
                    children: [
                      ImagesRender.imagesAssets(
                          src: img ?? '',
                          width: ic_18,
                          height: ic_18,
                          boxFit: BoxFit.contain),
                      SizedBox(
                        width: size18,
                      ),
                      Text(
                        data ?? '',
                        style: Themes.sfText14_400,
                      ),
                    ],
                  ),
                ),
                Obx(() => Text(
                  endTitle?.value ?? ''.obs.value,
                  style: Themes.sfTextHint14_400,
                )),
                const Icon(Icons.keyboard_arrow_right_outlined),
              ],
            ),
          ),
          //  const Divider(height: 1,),
        ],
      ),
    );
  }
}
