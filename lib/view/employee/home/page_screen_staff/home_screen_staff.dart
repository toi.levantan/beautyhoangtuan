import 'package:badges/badges.dart' as badges;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/core/translate/translation.dart';
import 'package:myhoangtuan/model/permission_model/permission_model.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import '../../../../core/routes/routers.dart';
import '../../../../utils/app/share_pref.dart';
import '../../../../utils/path/color_path.dart';
import '../../../../utils/path/const_key.dart';
import '../../../../utils/path/image_paths.dart';
import '../../../../utils/session/date_formatter.dart';
import '../../../../utils/session/enum_session.dart';
import '../../../../utils/widget/dialog_common.dart';
import '../../../../utils/widget/infinity.dart';
import '../../../../view_model/home_staff_vm/home_staff_controller.dart';

class HomeScreenStaff extends GetWidget<HomeStaffController> {
  const HomeScreenStaff({Key? key}) : super(key: key);
  final Color colorThemeText = colorBackGroundBorder;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: bodyWidget(
            padding: EdgeInsets.zero,
            isLoading: controller.isLoading,
            child: Stack(children: [
              ImagesRender.imagesAssets(
                  src: ImagePath.home_background,
                  width: Get.width,
                  height: Get.width * (3 / 5)),
              bodyWidget(
                  isLoading: controller.isLoading,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    SizedBox(
                      height: size60,
                    ),
                    Row(children: [
                      Obx(() => ImagesRender.circleNetWorkImage(
                          controller.avatarValue.value,
                          width: ic_56,
                          height: ic_56,
                          circular: size100w,
                          boxFit: BoxFit.cover)),
                      SizedBox(width: size12),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(Translate.hello.tr,
                                style: Themes.sfTextHint14_400
                                    .copyWith(color: colorThemeText)),
                            Row(children: [
                              Text(
                                controller.userData?.positionShow ?? '',
                                style: Themes.sfText16_700
                                    .copyWith(color: colorThemeText),
                              ),
                              SizedBox(
                                height: size6,
                              ),
                              Obx(() => Text(
                                    "${PreferenceUtils.getString(userKey).obs}",
                                    style: Themes.sfText16_700
                                        .copyWith(color: colorThemeText),
                                  )),
                            ])
                          ],
                        ),
                      ),
                      InkWell(
                          onTap: () async {
                            if (PreferenceUtils.getString(keyAccessToken) ==
                                null) {
                              showDialog(
                                  context: context,
                                  builder: (_) {
                                    return AlertDialog(
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(24.0),
                                          ),
                                        ),
                                        content: Builder(builder: (context) {
                                          return RequiredLoginDialog(
                                            loginButton: () async {
                                              Get.toNamed(Routers.intro_page);
                                            },
                                          );
                                        }));
                                  });
                            } else {
                              var result = await Get.toNamed(
                                  Routers.notification,
                                  arguments: fromHomeStaffToNotify);
                              if (result == Routers.post) {
                                controller.indexPage.value = 2;
                              }
                            }
                          },
                          child: Obx(() => badges.Badge(
                              showBadge: !controller.isLogin.value
                                  ? false.obs.value
                                  : true.obs.value,
                              position: badges.BadgePosition.topEnd(end: -6),
                              ignorePointer: true,
                              badgeStyle: badges.BadgeStyle(
                                  padding: EdgeInsets.all(size4)),
                              badgeContent: Obx(() => Text(
                                    '${countNotifyHomeEmployee.value >= 10 ? "9+" : countNotifyHomeEmployee.value}',
                                    style: Themes.sfText8_500
                                        .copyWith(color: Colors.white),
                                  )),
                              badgeAnimation:
                                  const badges.BadgeAnimation.fade(),
                              child: iconCircleCommon(
                                  ImagesRender.svgPicture(
                                      src: ImagePath.notification,
                                      width: ic_18,
                                      height: ic_18),
                                  padding: size6)))),
                    ]),
                    infoStaff(),
                    //TODO bac si hien thi thong tin lich hen
                    // Obx(() => Visibility(
                    //     visible: controller.isShowBook.value,
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //       children: [
                    //         Text(
                    //           'Lịch hẹn',
                    //           style: Themes.sfText16_600
                    //               .copyWith(color: colorFontStaff2),
                    //         ),
                    //         Text(
                    //           'Xem tất cả',
                    //           style: Themes.sfText11_400
                    //               .copyWith(color: colorFontStaff2),
                    //         ),
                    //       ],
                    //     ))),
                    Text('WorkSpace'.toUpperCase(),style: Themes.sfText14_700.copyWith(color: colorFontStaff2),),
                    Expanded( 
                      child: InfinityRefresh(
                          padding: EdgeInsets.zero,
                          refreshController: controller.refreshControllerHome,
                          onRefresh: controller.onRefresherHome,
                          onLoading: controller.onLoadHome,
                          child: GridView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: controller.workSpaces.length,
                              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                              padding: EdgeInsets.only(top: size12),
                              itemBuilder: (context, index) {
                                return itemWorkSpace(
                                    index: index,
                                    permissionModel: controller.workSpaces[index]);
                              })),
                    )
                  ]))
            ])));
  }

  Widget itemWorkSpace({PermissionModel? permissionModel, int? index}) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: InkWell(
        onTap: (){
          switch(permissionModel?.id){
            case 'danhba' : Get.toNamed(Routers.danhba);
            break;
            case 'donbao' : Get.toNamed(Routers.donbao);
            break;
          }
        },
        child: Container(
          padding: EdgeInsets.all(size16),
          decoration: BoxDecoration(boxShadow: const [
            BoxShadow(color: colorBackGroundAppBar,offset: Offset(-0.2,1),spreadRadius: 1,blurRadius: 1),
          ],borderRadius: BorderRadius.circular(size8),  color: Colors.white),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment:  CrossAxisAlignment.start,
            children: [
              Expanded(child: Center(child: ImagesRender.svgPicture(src: permissionModel?.image ??'',height: size82,width: Get.width))),
              Text(permissionModel?.title ??'',style: Themes.sfText12_400,),
              Text(permissionModel?.subTitle ??'',style: Themes.sfTextHint9_400.copyWith(fontStyle: FontStyle.italic),),
            ],
          ),
        ),
      ),
    );
  }

  Widget infoStaff() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: size16),
      child: InkWell(
        onTap: () {
          Get.toNamed(Routers.detail_checkin);
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(size16),
            color: colorFontStaff,
          ),
          child: Padding(
            padding: EdgeInsets.all(size16),
            child: Obx(() =>Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: size12),
                  child:  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(CupertinoIcons.checkmark_seal_fill,size: ic_16,color: colorFontStaff2),
                      SizedBox(height: size10,),
                      Text(controller.startTimeCheckin.value.isEmpty ? 'Bạn chưa check in ngày hôm nay' :'Bạn đã check in ngày hôm nay',style: Themes.sfText14_500.copyWith(color: colorFontStaff2)),
                      SizedBox(height: size10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RichText(
                              text: TextSpan(
                                  style:  Themes.sfText12_300.copyWith(color: colorFontStaff2),
                                  text: getValueDOE(dateToString(
                                      dateTime: DateTime.now(),
                                      dateFormatTime:
                                      dOEMap[DateFormatCustom.EEEE])),
                                  children: [
                                    const TextSpan(text: " "),
                                    const TextSpan(text: 'ngày'),
                                    const TextSpan(text: " "),
                                    TextSpan(
                                        text: dateToString(
                                            dateTime: DateTime.now(),
                                            dateFormatTime:
                                            dOEMap[DateFormatCustom.DD_MM_YYYY])),
                                  ])),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: size10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(children:  [
                              Icon(CupertinoIcons.arrow_up_square_fill,color: colorCheckin,size: ic_24),
                              SizedBox(width: size8),
                              Obx(() => Text(controller.startTimeCheckin.value.isNotEmpty ? controller.startTimeCheckin.value :"--:--",style: Themes.sfText15_700.copyWith(color: colorCheckin)))
                            ],),
                            SizedBox(width: size16,),
                            Row(children: [
                              Icon(CupertinoIcons.arrow_down_square_fill,color: Colors.deepOrangeAccent,size: ic_24),
                              SizedBox(width: size8),
                              Obx(() => Text(controller.endTimeCheckin.value.isNotEmpty ? controller.endTimeCheckin.value :"--:--",style: Themes.sfText15_700.copyWith(color: Colors.deepOrangeAccent)),)
                            ],)
                          ],
                        ),
                      ),
                      Obx(() => Text("(Hành Chính: ${isoDateToString(isoDate: controller.workStartAt.value,dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])} - ${isoDateToString(isoDate: controller.workEndAt.value,dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])})",style: Themes.sfText12_300.copyWith(color: colorFontStaff2))),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }
}
