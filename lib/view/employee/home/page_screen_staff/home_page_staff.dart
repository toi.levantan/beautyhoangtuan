import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/view/employee/home/page_screen_staff/home_screen_staff.dart';
import 'package:myhoangtuan/view/employee/home/page_screen_staff/profile_screen_staff.dart';
import 'package:myhoangtuan/view_model/home_staff_vm/home_staff_controller.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

import '../../../../utils/app/size_app.dart';
import '../../../../utils/path/image_paths.dart';
import '../../../../utils/widget/images_render.dart';


class HomePageStaff extends GetWidget<HomeStaffController> {
  const HomePageStaff({Key? key}) : super(key: key);

  //trang chủ
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          resizeToAvoidBottomInset: false,
          body: PersistentTabView(
            context,
            controller: controller.persistentTabController,
            navBarStyle: NavBarStyle.style9,
            backgroundColor: colorBottomBar,
            screenTransitionAnimation: const ScreenTransitionAnimation(animateTabTransition: true,curve: Curves.linear,duration: Duration(milliseconds: 420)),
            onItemSelected: (value) {
              controller.indexPage.value = value;
            },
            screens: const [
              HomeScreenStaff(),
              ProfileScreenStaff(),
            ],
            items: _navBarsItems()));
  }

  List<PersistentBottomNavBarItem>? _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: ImagesRender.svgPicture(
            src: ImagePath.home, width: ic_16, height: ic_16),
        inactiveIcon: ImagesRender.svgPicture(
            src: ImagePath.unselectedHome, width: ic_16, height: ic_16),
        activeColorPrimary: colorIcon,
        inactiveColorPrimary: CupertinoColors.systemGrey,
        title: ("Trang chủ"),
        iconSize: ic_18
      ),
      PersistentBottomNavBarItem(
        inactiveIcon: ImagesRender.svgPicture(
            src: ImagePath.unselectedUser, width: ic_16, height: ic_16),
        icon: ImagesRender.svgPicture(
            src: ImagePath.user, width: ic_16, height: ic_16),
        title: ("Tài khoản"),
        activeColorPrimary: colorIcon,
        inactiveColorPrimary: CupertinoColors.systemGrey,
          iconSize: ic_18
      ),
    ];
  }
}
