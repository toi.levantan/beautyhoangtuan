import 'package:flutter/cupertino.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/view_model/intro_vm/intro_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/routes/routers.dart';
import '../../../core/themes/themes.dart';
import '../../../utils/path/image_paths.dart';
import '../../../utils/widget/images_render.dart';
import '../../../utils/widget/textfieldcommon.dart';
import '../../../utils/widget/utils_common.dart';


class IntroPage extends GetWidget<IntroController> {
  const IntroPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: appBarButton(
                  title: 'Hồ sơ sức khỏe',
                  showBottomBorder: false),
                 body: customerWidget));
  }

  Widget get employeeWidget =>
      bodyWidget(
        isLoading: controller.isLoading,
        child: Column(
          children: [
            Text(
              'Đăng nhập với tư cách là nhân sự của HT Beauty', style: Themes.sfText12_400,),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  itemInfo(label: 'Tài khoản',
                      prefixIcon: ImagesRender.svgPicture(
                          src: ImagePath.person, height: ic_11),
                      errText: controller.errNumberPhone,
                      keyboardType: TextInputType.multiline,
                      onFocusChanged: (focus){
                      if(!focus){
                        controller.validateUserName(userName:  controller.textEdits.first.text.trim());
                        controller.checkShowAction();
                      }
                      },
                      onChange: (value){
                        controller.validateUserName(userName:  controller.textEdits.first.text.trim());
                        controller.checkShowAction();
                      },
                      textEditingController: controller.textEdits.first),
                  SizedBox(height: size16,),
                  itemInfo(label: 'Mật khẩu',
                      prefixIcon: ImagesRender.svgPicture(
                          src: ImagePath.lock, height: ic_11),
                      obscureText: controller.hidePass,
                      suffixIcon: true,
                      errText: controller.errPass,
                      textEditingController: controller.textEdits.last,
                      onFocusChanged: (focus){
                    if(!focus){
                      controller.validatePass(password: controller.textEdits.last.text.trim());
                      controller.checkShowAction();
                      }},
                      onChange: (value){
                        controller.validatePass(password: value.trim());
                        controller.checkShowAction();
                      }),
                  SizedBox(height: size24,),
                  Row(
                    children: [
                      SizedBox(
                        height: ic_16,
                        width: ic_16,
                        child: InkWell(
                            onTap: () {
                              controller.rememberAccountEmp.value = !controller.rememberAccountEmp.value;
                            },
                            child: Obx(() =>
                            controller.rememberAccountEmp.value
                                ? ImagesRender.svgPicture(
                                src: ImagePath.checkbox, height: ic_16)
                                : Container(decoration: BoxDecoration(
                                border: Border.all(
                                    strokeAlign: BorderSide.strokeAlignOutside,
                                    color: colorIcon,
                                    width: 0.8),
                                borderRadius: BorderRadius.circular(size4)
                            ),))),
                      ),
                      SizedBox(width: size8,),
                      Expanded(child: Text('Ghi nhớ mật khẩu',
                          style: Themes.sfText12_500.copyWith(
                              color: colorIcon))),
                      InkWell(
                          onTap: () {
                            //TODO đi đến màn đăng nhâp với gia tri la quen mat khau
                            FocusScope.of(Get.context!).unfocus();
                            Get.toNamed(Routers.login_create, arguments: [
                              CheckUser.forgotPwd,
                              controller.textEdits.first.text.trim(),
                            ]);
                          },
                          child: Text('Quên mật khẩu',
                              style: Themes.sfText12_500.copyWith(
                                  color: colorIcon))),
                    ],
                  ),
                  SizedBox(height: size24,),
                  buttonCommon(title: 'Tiếp tục',
                      color: colorIcon,
                      isOK: controller.isOk,
                      function: () {
                        FocusScope.of(Get.context!).unfocus();
                        controller.login();
                        // controller.
                      }),
                ],),
            ),
          ],
        ),
      );

  Widget get customerWidget {
    return bodyWidget(child:
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Nếu chưa có hồ sơ, vui lòng tạo hồ sơ', style: Themes.sfText12_400,),
        Expanded(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Bạn đã sử dụng dịch vụ tại HT Beauty',
                  style: Themes.sfText14_700,),
                SizedBox(height: size22),
                buttonCommon(title: 'Đã từng sử dụng - Kết nối hồ sơ',
                    isOK: true.obs,
                    color: colorIcon,
                    function: () async {
                      Get.toNamed(
                        Routers.login_pwd, /*arguments: CheckUser.used*/);
                    }),
                SizedBox(height: size10),
                buttonCommon(title: 'Chưa từng sử dụng - Tạo hồ sơ',
                    isOK: true.obs,
                    color: colorIcon,
                    function: () async {
                      Get.toNamed(Routers.login_create,
                          arguments: [CheckUser.create, ""]);
                      // print(result);
                    }),
              ]),
        ),
      ],
    ));
  }

  Widget itemInfo(
      {String? label, Widget? prefixIcon, bool? suffixIcon, String? hintText, Rx<
          bool>? obscureText, TextEditingController? textEditingController, Rx<
          String?>? errText,
        Function(bool)? onFocusChanged, Function(String)? onChange, TextInputType? keyboardType}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label ?? '', style: Themes.sfText12_700),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            prefixIcon ?? Container(),
            SizedBox(width: size16,),
            Expanded(child: FormFieldLabelNoBorder(title: '',
              editingController: textEditingController,
              errorText: RxnString(),
              hintText: hintText,
              obscureText: obscureText,
              keyboardType: keyboardType,

              onChange: (value) {
                if (onChange != null) {
                  onChange.call(value);
                }
              },
              onFocusChanged: (value) {
                if (onFocusChanged != null) {
                  onFocusChanged.call(value);
                }
              },)),
            Visibility(visible: suffixIcon ?? false, child: InkWell(
              onTap: () {
                controller.changeShowPass();
              },
              child: Obx(() =>
              controller.hidePass.value ? Icon(
                CupertinoIcons.eye_slash_fill, size: ic_16,
                color: colorTextHint,) : Icon(
                CupertinoIcons.eye_solid, size: ic_16,
                color: colorTextHint,)),
            ),),
          ],),
        SizedBox(height: size8),
        const Divider(color: colorTextHint, height: 1,),
        Obx(() =>
            Visibility(
                visible: errText?.value != null,
                child: Column(
                  children: [
                    SizedBox(height: size4),
                    Text(errText?.value ?? "",
                        style: Themes.sfText10_400.copyWith(color: Colors.red)),
                  ],
                )))
      ],);
  }
}
