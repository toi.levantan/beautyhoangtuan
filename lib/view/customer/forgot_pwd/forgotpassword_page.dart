import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/forgot_pwd_vm/forgot_pwd_controller.dart';

import '../../../core/routes/routers.dart';
import '../../../utils/app/size_app.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/widget/button_common.dart';
import '../../../utils/widget/textfieldcommon.dart';


class ForgotPasswordPage extends GetWidget<ForgotPasswordController> {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        appBar: appBarButton(title: 'Đặt lại mật khẩu',
        leading: (){
         Get.offNamedUntil(Routers.intro_page,(route) => false);
        }),
        body: bodyWidget(
          isLoading: controller.isLoading,
            child: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  SizedBox(height: size47),
                  FormFieldLabel(
                      editingController: controller.listEdtController.first,
                      title: 'Mật khẩu',
                      obscureText: controller.hidePass,
                      isRequired: true,
                      suffixIcon: InkWell(
                        onTap: (){
                          controller.hidePass.value = ! controller.hidePass.value;
                        },
                        child:  Obx(() => controller.hidePass.value ?  Icon(CupertinoIcons.eye_slash_fill,size: ic_16,color: colorTextHint,) :  Icon(CupertinoIcons.eye_solid,size: ic_16,
                          color: colorTextHint,)),
                      ),
                      onChange: (value){
                        controller.validateCheckPassword();
                        controller.validateButton();
                      },
                      onFocusChanged: (onFocus) {
                        if (!onFocus) {
                          controller.validateCheckPassword();
                          controller.validateButton();
                        }
                      },
                      errorText: controller.validatePassword),
                  SizedBox(
                    height: size24,
                  ),
                  FormFieldLabel(
                      editingController: controller.listEdtController.last,
                      title: 'Xác nhận mật khẩu mới',
                      obscureText: controller.hidePassConfirm,
                      isRequired: true,
                      suffixIcon: InkWell(
                        onTap: (){
                          controller.hidePassConfirm.value = ! controller.hidePassConfirm.value;
                        },
                        child:  Obx(() => controller.hidePassConfirm.value ?  Icon(CupertinoIcons.eye_slash_fill,size: ic_16,color: colorTextHint,) :  Icon(CupertinoIcons.eye_solid,size: ic_16,
                          color: colorTextHint,)),
                      ),
                      onChange: (value){
                        controller.checkConfirmPass();
                        controller.validateButton();
                      },
                      onFocusChanged: (onFocus) {
                        if (!onFocus) {
                          controller.checkConfirmPass();
                          controller.validateButton();
                        }
                      },
                      errorText: controller.validateConfirmPassword),
                ],
              ),
            ),
            buttonCommon(title: 'Lưu thông tin',isOK: controller.isOk,
            function: (){
              FocusScope.of(context).unfocus();
             controller.setPassword();
            }),
          ],
        )),
      ),
    );
  }
}
