import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/model/booking/history_booking_dto.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:popover/popover.dart';

import '../../../core/themes/themes.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/path/image_paths.dart';
import '../../../utils/session/enum_session.dart';
import '../../../utils/widget/images_render.dart';


class ItemHistoryBook extends GetWidget {
  final BuildContext? context;
  final ContentHis? contentHistoryBook;
  final Function(int? idLich)? onTap;
  final Function()? onTapReport;
  const ItemHistoryBook({this.context,this.contentHistoryBook,this.onTap,this.onTapReport, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Rx<bool> showCancel = true.obs;
    Rx<bool> showReport = true.obs;

    ///TODO hiển thị hủy lịch
    contentHistoryBook!.status == 4 || contentHistoryBook!.status == 3 ? showCancel = false.obs: showCancel = true.obs;

   // contentHistoryBook!.status == mapEnumStatusBook[StatusBook.COMPLETED] ? showReport = true.obs : showReport = false.obs;
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(size18), color: backGroundBook),
      child: SizedBox(
        height: getDeviceType() == DeviceType.Tablet ? ic_200 : size180,
        child:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            // chia hàng ngày hẹn
            Expanded(
              child: Row(
                children: [
                  Container(decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(size8)),
                    child: Column(
                      children: [
                        Text("${isoDateToString(isoDate: contentHistoryBook?.beautyBookingDetails?.first.bookingDate ??'',dateTimeFormat: dOEMap[DateFormatCustom.HH_mm])}"),
                      ],
                    ),),
                  Expanded(
                    child: Padding(
                      padding:  EdgeInsets.all(size16),
                      child: Column(
                        ///TODO chia cột ngày hẹn và thời gian
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:  [
                          SizedBox(
                            width:  Get.width - size90w ,
                            child: Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      Text('Lịch hẹn',style: Themes.sfText12_400.copyWith(color: colorIconSearch),),
                                       Text(' - ',style: Themes.sfTextHint10_600,),
                                      Text(contentHistoryBook?.packageType == 1 ?  "Dịch vụ" : "Combo",style: Themes.sfText12_400.copyWith(color: colorIconSearch),),
                                      const Text(' - '),
                                      Expanded(
                                        child: Text('${contentHistoryBook?.packageType == 1 ? contentHistoryBook?.beautyBookingDetails?.first.beautyServicePackage?.name :
                                        contentHistoryBook?.beautyBookingDetails?.first.beautyPackageCombo?.name}',style: Themes.sfText12_500.copyWith(color: colorTextBook),overflow: TextOverflow.ellipsis,),
                                      ),
                                    ],
                                  ),
                                ),
                                //TODO
                                // Visibility(
                                //   visible: false,
                                //   child: GestureDetector(
                                //           onTap: (){
                                //             showPopover(context: context,
                                //                 height: size56,
                                //                 width: Get.width /3,
                                //                 arrowHeight: size8,
                                //                 arrowWidth: size16,
                                //                 arrowDxOffset: 160,
                                //                 direction: PopoverDirection.top,
                                //                 bodyBuilder: (context) {
                                //                   return InkWell(
                                //                     onTap: (){
                                //                       if(onTapReport != null){
                                //                         onTapReport?.call();
                                //                       }
                                //                     },
                                //                     child: Row(
                                //                       mainAxisAlignment: MainAxisAlignment.spaceAround,
                                //                       children: [
                                //                         Text('Phản ánh',style: Themes.sfText14_500.copyWith(color: colorIcon)),
                                //                         Icon(Icons.send_and_archive_outlined,color: colorIcon,size: ic_16,)
                                //                       ],
                                //                     ),
                                //                   );
                                //                 });
                                //           },
                                //           child: ImagesRender.svgPicture(src: ImagePath.more_vertical,width: ic_12)),
                                // ),
                                Obx(() => Visibility(
                                    visible: showCancel.value || showReport.value,
                                    child: GestureDetector(
                                      onTap: (){
                                        showPopover(
                                            context: context,
                                            width: Get.width *(3/5),
                                            arrowHeight: size8,
                                            arrowWidth: size16,
                                            backgroundColor: Colors.white,
                                            arrowDxOffset: 150,
                                            direction: PopoverDirection.top,
                                            bodyBuilder: (context) {
                                            return Padding(
                                              padding: EdgeInsets.symmetric(vertical: size8),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Obx(() =>Visibility(
                                                    visible: showReport.value,
                                                    child: InkWell(
                                                      onTap: (){
                                                        if(onTap != null){
                                                          Get.back();
                                                          onTapReport?.call();
                                                        }
                                                      },
                                                      child: Padding(
                                                        padding: EdgeInsets.symmetric(vertical: size8),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                          children: [
                                                            Text('Phản ánh',style: Themes.sfText16_400),
                                                            Icon(Icons.send_outlined,color: colorComplete,size: ic_16,)
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  )),
                                                  Visibility(
                                                     visible: showCancel.value,
                                                    child: Column(
                                                      children: [
                                                        const Divider(),
                                                        InkWell(
                                                          onTap: (){
                                                            if(onTap != null){
                                                              Get.back();
                                                              onTap?.call(contentHistoryBook?.id);
                                                            }
                                                          },
                                                          child: Padding(
                                                            padding: EdgeInsets.symmetric(vertical: size8),
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                              children: [
                                                                Text('Huỷ lịch  ',style: Themes.sfText16_400),
                                                                Icon(CupertinoIcons.delete,color: Colors.redAccent,size: ic_16,)
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                        });
                                      },
                                        child: ImagesRender.svgPicture(src: ImagePath.more_vertical,width: ic_12))))
                              ],
                            ),
                          ),
                          SizedBox(height: size4,),
                          Row(
                            children: [
                              SizedBox(child: ImagesRender.svgPicture(src: ImagePath.clock,height: size16,width: size16)),
                              SizedBox(width: size8),
                              Text(contentHistoryBook?.beautyBookingDetails?.first.bookingDateStr ?? "",style: Themes.sfText12_500.copyWith(color: colorTextBook),),
                              Visibility(
                                visible: contentHistoryBook?.beautyBookingDetails?.first.bookingDateStr != null,
                                child: Padding(
                                  padding:  EdgeInsets.symmetric(horizontal: size8),
                                  child: ImagesRender.svgPicture(src: ImagePath.dot),
                                ),
                              ),
                              Text(contentHistoryBook?.beautyBookingDetails?.first.startTime ?? "",style: Themes.sfText12_500.copyWith(color: colorTextBook),),
                              Visibility(
                                  visible: contentHistoryBook?.beautyBookingDetails?.first.endTime != null,
                                  child: const Text(" - ")),
                              Text(contentHistoryBook?.beautyBookingDetails?.first.endTime ?? "",style: Themes.sfText12_500.copyWith(color: colorTextBook),),
                             const Text(" - "),
                              Obx(() =>Text('${statusBookToString(statusBook: contentHistoryBook?.status)}',style: Themes.sfText12_500.copyWith(
                                color: statusBook(statusBook: contentHistoryBook?.status)
                              ),))
                            ],),
                        ],
                      ),
                    ),
                  )],
              ),
            ),
             Padding(
               padding:  EdgeInsets.symmetric(horizontal: size16),
               child: Container(width:  getDeviceType() == DeviceType.Tablet ? Get.width - size100 :  Get.width - size90w,color: colorBorderTextField,height: 0.5,),
             ),
            Expanded(child: Padding(
              padding:  EdgeInsets.all(size16),
              child: Row(
                children: [
                  InkWell(
                    onTap: (){
                      if( contentHistoryBook?.beautyBookingDetails?.first.performer != null){
                       ///TODO huy lich
                         contentHistoryBook?.beautyBookingDetails?.first.performer?.phoneNumber;
                      }
                    },
                    child: Stack(
                      children: [
                        contentHistoryBook?.beautyBookingDetails?.first.performer?.imageStore != null && contentHistoryBook!.beautyBookingDetails!.first.performer!.imageStore!.isNotEmpty ?
                        ImagesRender.circleNetWorkImage(contentHistoryBook?.beautyBookingDetails?.first.performer?.imageStore?.first.url??"",circular: 100,width: ic_50,height: ic_50,
                        boxFit: BoxFit.fill) :
                        ImagesRender.imagesAssetsCircle(src: ImagePath.doctor_icon,circular: 100,width: ic_50,height: ic_50,boxFit: BoxFit.cover),
                        Positioned(
                            bottom: 0,
                            right: -2,
                            child: ImagesRender.imagesAssetsCircle(src: ImagePath.telephone_call,width: ic_14,height: ic_14)),
                      ],
                    ),
                  ),
                  SizedBox(width: size16,),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                     contentHistoryBook?.beautyBookingDetails != null ? Text(contentHistoryBook?.beautyBookingDetails?.first.performer?.fullName ?? "Chưa xác định",
                         style: Themes.sfText16_500.copyWith(color: colorTextBook)) : const Text('Chưa xác định'),
                      SizedBox(height: size6,),
                      Text("Bác sĩ",style: Themes.sfText14_400.copyWith(color: colorDoctor)),
                    ],)
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }
}
      Color? statusBook({int? statusBook}){
        switch(statusBook){
          ///TODO mới tạo - chờ xác nhận
          case 1: return Colors.grey;
        ///TODO đã xác nhân
          case 2: return Colors.green;
        ///TODO đã hoàn thành
          case 3: return  Colors.purple;
        ///TODO đã hủy
          case 4: return colorCancel;
        }
        return Colors.grey;
      }
      Rx<String>? statusBookToString({int? statusBook}){
        switch(statusBook){
          ///TODO mới tạo - chờ xác nhận
          case 1: return 'Chờ xác nhận'.obs;
        ///TODO đã xác nhân
          case 2: return 'Đã xác nhận'.obs;
        ///TODO đã hoàn thành
          case 3: return 'Đã hoàn thành'.obs;
        ///TODO đã hủy
          case 4: return 'Đã hủy'.obs;
        }
        return ''.obs;
      }
