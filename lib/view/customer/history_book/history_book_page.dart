import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view/customer/history_book/item_history_book.dart';
import 'package:myhoangtuan/view_model/book_vm/history_book_controller.dart';
import '../../../core/routes/routers.dart';
import '../../../utils/widget/dialog_common.dart';
import '../../../utils/widget/infinity.dart';
import '../../../utils/widget/snack_bar.dart';
import '../../../utils/widget/textfieldcommon.dart';


class HistoryBookPage extends GetWidget<HistoryBookController> {
  ///TODO Dịch vụ
  const HistoryBookPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      child: Scaffold(
        appBar: appBarButton(title: 'Lịch đặt'),
        resizeToAvoidBottomInset: false,
        body:Obx(()=> bodyWidget(
          isLoading: controller.isLoading,
          listDataCheck: controller.isSelected.value == 0 ? controller.newBookList: controller.historyList,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: size44,
                width: Get.width,
                decoration: BoxDecoration(
                  color: colorBackGroundSearch,
                  borderRadius: BorderRadius.circular(size8),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                        child: InkWell(
                            onTap: () {
                              controller.isSelected.value = 0;
                            //  controller.searchServiceEdt.clear();
                            },
                            child: Obx(() => radioButtonCustom(
                                isSelect: controller.isSelected == 0.obs ? true : false,
                                title: "Lịch sắp tới")))),
                    Expanded(
                        child: InkWell(
                            onTap: () {
                              controller.isSelected.value = 1;
                            //  controller.searchServiceEdt.clear();
                            },
                            child: Obx(() => radioButtonCustom(
                                isSelect: controller.isSelected == 1.obs ? true : false,
                                title: "Đã đặt")))),
                  ],
                ),
              ),
              SizedBox(
                height: size12,
              ),
              /// từ textfiledcommon
              /// search textfield
              // SearchFormField(
              //   editingController: controller.searchServiceEdt,
              //   onFieldSubmit: (value) {
              //     controller.keySearch = value;
              //     if (controller.isSelectedServices.value == 0) {
              //       controller.callServicesApi();
              //     }
              //     if (controller.isSelectedServices.value == 1) {
              //       controller.callComboApi();
              //     }
              //   },
              // ),
              SizedBox(
                height: size16,
              ),
              // GridviewBuilder
              Expanded(
                child: InfinityRefresh(
                  refreshController: controller.refreshController,
                  onLoading: controller.onLoading,
                  onRefresh: controller.onRefresh,
                  padding: EdgeInsets.zero,
                  child: Obx(() => controller.isSelected.value == 0 ? ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: controller.newBookList.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding:  EdgeInsets.symmetric(vertical: size6),
                          child: ItemHistoryBook(contentHistoryBook: controller.newBookList[index],
                              onTapReport: () async{
                                var result = await  Get.toNamed(Routers.createReport,arguments: controller.newBookList[index]);
                                if(result != null){
                                  successNotification(context: Get.context!,message: 'Gửi phản ánh thành công');
                                }
                              },
                              onTap: (idBooking) async{
                                await Future.delayed(const Duration(milliseconds: 500));
                                showDialog(context: Get.context!,barrierDismissible: false, builder: (_){
                                  return  AlertDialog(
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(12.0),
                                      ),
                                    ),
                                    content: Builder(builder:  (context) {
                                      return  ConfirmDialogBooking(
                                        content: "Xác nhận hủy lịch?",
                                        onTapClose: (){
                                          controller.reasonControllerNewBookEdt.clear();
                                          Get.back();
                                        },
                                        title: Column(
                                          children: [
                                            RichText(
                                                maxLines: 3,
                                                text:  TextSpan(text: 'Bạn có đồng ý hủy lịch hẹn ',
                                                    style: Themes.sfText14_400.copyWith(color: colorTextApp),
                                                    children: [
                                                      TextSpan(text: controller.newBookList[index].packageType == 1? "có gói dịch vụ ":"có gói combo "),
                                                      TextSpan(text: controller.newBookList[index].packageType == 1? controller.newBookList[index].beautyBookingDetails?.first.beautyServicePackage?.name??'':
                                                      controller.historyList[index].beautyBookingDetails?.first.beautyPackageCombo?.name??"",style: Themes.sfText14_700.copyWith(color: colorCancel)),
                                                      const TextSpan(text: " không?"),
                                                    ])),
                                            SizedBox(height: size6),
                                            FormFieldLabel(title: 'Lý do hủy', editingController: controller.reasonControllerNewBookEdt, maxLine: 1,errorText: RxnString()),
                                            SizedBox(height: size6),
                                          ],
                                        ),
                                        confirmText: "Hủy lịch",
                                        onTap: (){
                                          Get.back();
                                          controller.reasonControllerNewBookEdt.clear();
                                          controller.cancelBooking(reason: controller.reasonControllerNewBookEdt.text.trim(),id: controller.newBookList[index].id??-1);
                                        },
                                      );
                                    },),
                                  );
                                });
                                {
                                  showDialog(context: Get.context!,barrierDismissible: false, builder: (_){
                                    return  AlertDialog(
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(24.0),
                                        ),
                                      ),
                                      content: Builder(builder:  (context) {
                                        return  ConfirmDialogBooking(
                                          content: "Xác nhận hủy lịch?",
                                          onTapClose: (){
                                            controller.reasonControllerNewBookEdt.clear();
                                            Get.back();
                                          },
                                          title: Column(
                                            children: [
                                              RichText(
                                                  maxLines: 3,
                                                  text:  TextSpan(text: 'Bạn có đồng ý hủy lịch hẹn ',
                                                      style: Themes.sfText14_400.copyWith(color: colorTextApp),
                                                      children: [
                                                        TextSpan(text: controller.newBookList[index].packageType == 1? "có gói dịch vụ ":"có gói combo "),
                                                        TextSpan(text: controller.newBookList[index].packageType == 1? controller.newBookList[index].beautyBookingDetails?.first.beautyServicePackage?.name??'':
                                                        controller.historyList[index].beautyBookingDetails?.first.beautyPackageCombo?.name??"",style: Themes.sfText14_700.copyWith(color: colorCancel)),
                                                        const TextSpan(text: " không?"),
                                                      ])),
                                              SizedBox(height: size6),
                                              FormFieldLabel(title: 'Lý do hủy', editingController: controller.reasonControllerNewBookEdt, maxLine: 1,errorText: RxnString()),
                                              SizedBox(height: size6),
                                            ],
                                          ),
                                          confirmText: "Hủy lịch",
                                          onTap: (){
                                            Get.back();
                                            controller.reasonControllerNewBookEdt.clear();
                                            controller.cancelBooking(reason: controller.reasonControllerNewBookEdt.text.trim(),id: controller.newBookList[index].id??-1);
                                          },
                                        );
                                      },),
                                    );
                                  });
                                }
                          }),
                        );
                      }) :
                      Obx(() =>ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: controller.historyList.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding:  EdgeInsets.symmetric(vertical: size6),
                          child: ItemHistoryBook(contentHistoryBook: controller.historyList[index],
                              onTapReport: () async{
                                var result = await Get.toNamed(Routers.createReport,arguments: controller.historyList[index]);
                                if(result != null){
                                  successNotification(context: Get.context!,message: 'Gửi phản ánh thành công');
                                }
                              },
                              onTap: (idBooking) async{
                                showDialog(context: context, builder: (_){
                                  return  AlertDialog(
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(24.0),
                                      ),
                                    ),
                                    content: Builder(builder:  (context) {
                                      return  ConfirmDialogBooking(
                                        content: "Xác nhận hủy lịch?",
                                        title: Column(
                                          children: [
                                            RichText(
                                                maxLines: 3,
                                                text:  TextSpan(text: 'Bạn có đồng ý hủy lịch hẹn ',
                                                    style: Themes.sfText14_400.copyWith(color: colorTextApp),
                                                    children: [
                                                      TextSpan(text: controller.historyList[index].packageType == 1? "có gói dịch vụ ":"có gói combo "),
                                                      TextSpan(text: controller.historyList[index].packageType == 1? controller.historyList[index].beautyBookingDetails?.first.beautyServicePackage?.name??'':
                                                      controller.historyList[index].beautyBookingDetails?.first.beautyPackageCombo?.name??"",style: Themes.sfText14_700.copyWith(color: colorCancel)),
                                                      const TextSpan(text: " không?"),
                                                    ])),
                                            SizedBox(height: size6),
                                            FormFieldLabel(title: 'Lý do hủy', editingController: controller.reasonControllerHistoryBookEdt, maxLine: 1,errorText: RxnString()),
                                            SizedBox(height: size6),
                                          ],
                                        ),
                                        confirmText: "Hủy lịch",
                                        onTap: (){
                                          controller.cancelBooking(reason: controller.reasonControllerHistoryBookEdt.text.trim(),id: controller.historyList[index].id??-1);
                                        },
                                      );
                                    },),
                                  );
                                });
                              }),
                        );
                      }),
                  )),
                ),
              ),
            ],
          ),
        ),
        ),),
      context: context,
    );
  }

  Widget radioButtonCustom({String? title, required bool isSelect}) {
    return isSelect
        ? Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(size8)),
      elevation: 2,
      margin: EdgeInsets.all(size2),
          child: Padding(
      padding: EdgeInsets.all(size2),
      child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(size8),
            color: Colors.white,
          ),
          child: Text(
            title ?? '',
            style: Themes.sfText12_700,
          ),
      ),
    ),
        )
        : Center(
        child: Text(title ?? '',
            style: Themes.sfText12_700.copyWith(color: colorTextBook)));
  }
}
