import 'dart:io';
import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/model/booking/doctor_booking_dto.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/session/bottom_sheet.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/info_vm/update_info_controller.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../../../utils/path/color_path.dart';
import '../../../utils/session/date_formatter.dart';
import '../../../utils/session/enum_session.dart';
import '../../../utils/session/file_picker.dart';
import '../../../utils/widget/textfieldcommon.dart';

class UpdateInfoPage extends GetWidget<UpdateInfoController> {
  const UpdateInfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
        context: context,
        child: Scaffold(
          appBar: appBarButton(title: 'Thông tin cá nhân', isBack: true),
          body: bodyWidget(
            isLoading: controller.isLoading,
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ImagesRender.svgPicture(src: ImagePath.toast, width: Get.width,boxFit: BoxFit.fitWidth),
                        SizedBox(
                          height: size12,
                        ),
                        InkWell(
                          onTap: (){
                            Uri url = Uri.parse("https://hoangtuan.inviv.vn/pages/guide");
                             Application.launchInBrowser(url);
                          },
                          child: Row(
                            children: [
                              Icon(CupertinoIcons.question_circle,size: ic_12,color: colorIcon,),
                              SizedBox(width: size4,),
                              Text(
                                'Hướng dẫn chụp ảnh nhận diện',
                                style: Themes.sfText12_700.copyWith(color: colorIcon),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: size12,
                        ),
                        Text(
                          'Ảnh đại diện',
                          style: Themes.sfText12_700,
                        ),
                        SizedBox(
                          height: size6,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Stack(alignment: Alignment.topRight, children: [
                              Obx(() => InkWell(
                                    onTap: () {
                                      BottomSheetCommon.getBottomPickCamera(
                                        cameraTap: () async {
                                          Get.back();
                                          XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);

                                          if (image != null) {
                                            CroppedFile? cropFile = await ImageCropper().cropImage(
                                                    sourcePath: image.path,
                                                uiSettings: [
                                                  AndroidUiSettings(
                                                      toolbarTitle: 'Cropper',
                                                      toolbarColor: Colors.deepOrange,
                                                      toolbarWidgetColor: Colors.white,
                                                      initAspectRatio: CropAspectRatioPreset.square,
                                                      lockAspectRatio: true),
                                                  IOSUiSettings(
                                                    title: 'Cropper',
                                                    minimumAspectRatio: 300.0,
                                                    rectX: 300.0,
                                                    rectY: 300.0,
                                                    rectWidth: Get.width,
                                                    rectHeight: Get.width,

                                                  ),
                                                ],
                                                    aspectRatioPresets: [
                                                  CropAspectRatioPreset.square,
                                                ]);
                                            controller.imageFile.clear();
                                            File file = File(cropFile!.path);
                                            controller.imageFile.add(file);
                                            // Get.back();
                                            controller.validateButton();
                                          }
                                        },
                                        galleryTap: () async {
                                          Get.back();
                                          File? filePicker = await PickerFile.singleImageFilePicker();
                                          if (filePicker != null && filePicker.path != '') {
                                            CroppedFile? cropFile = await ImageCropper().cropImage(
                                                    sourcePath: filePicker.path,
                                                    aspectRatioPresets: [
                                                  CropAspectRatioPreset.square,
                                                ],
                                                uiSettings: [
                                                  AndroidUiSettings(
                                                      toolbarTitle: 'Cropper',
                                                      toolbarColor: Colors.deepOrange,
                                                      toolbarWidgetColor: Colors.white,
                                                      initAspectRatio: CropAspectRatioPreset.square,
                                                      lockAspectRatio: true),
                                                  IOSUiSettings(
                                                    title: 'Cropper',
                                                     minimumAspectRatio: 300.0,
                                                     rectX: 300.0,
                                                     rectY: 300.0,
                                                    rectWidth: Get.width,
                                                    rectHeight: Get.width,
                                                  ),
                                                ],
                                                    maxHeight: 300,
                                                    maxWidth: 300);

                                            File fileCrop = File(cropFile!.path);
                                            controller.imageFile.clear();
                                            controller.imageFile.add(fileCrop);
                                            controller.validateButton();
                                            // Get.back();
                                          }
                                        },
                                      );
                                    },
                                    child: controller.imageFile.isNotEmpty ? Image.file(
                                      File(controller.imageFile.first!.path),
                                        // src: controller.imageFile.isNotEmpty
                                        //     ? controller.imageFile.first!.path
                                        //     : ImagePath.imagePickDef,
                                        width: ic_80,
                                        height: ic_80) : ImagesRender.imagesAssets(src: ImagePath.imagePickDef,height: ic_80,width: ic_80),
                                  )),
                              Obx(() => Visibility(
                                  visible: controller.imageFile.isNotEmpty,
                                  child: InkWell(
                                      onTap: () {
                                        controller.imageFile.clear();
                                        controller.validateButton();
                                      },
                                      child: Icon(
                                        CupertinoIcons.clear_circled_solid,
                                        size: ic_18,
                                        color: Colors.red,
                                      )))),
                            ]),
                          ],
                        ),
                        SizedBox(
                          height: size12,
                        ),
                        FormFieldLabel(
                            editingController:
                                controller.listEdtController.first,
                            title: 'Chi nhánh',
                            isRequired: true,
                            onTap: () {
                              List<SelectedListItem> listConvert = [];
                              for (Data element in controller.listBranch) {
                                SelectedListItem selectItem = SelectedListItem(
                                    name: element.organizationBranchName ?? '',
                                    value: element.id.toString());
                                listConvert.add(selectItem);
                              }
                              dropDownSearch(
                                  sourceList: listConvert,
                                  context: Get.context!,
                                  bottomSheetTitle: "Chi nhánh",
                                  onSelectedValue: (name, value) {
                                    controller.listEdtController.first.text = name ?? '';
                                    controller.dataRequestServices.organizationBranchId = int.parse(value);
                                    controller.dataRequestServices.id = int.parse(value);
                                    controller.validateCheckBranch();
                                    controller.validateButton();
                                  });
                            },
                            onFocusChanged: (onFocus) {
                              if (!onFocus) {
                                controller.validateCheckBranch();
                                controller.validateButton();
                              }
                            },
                            readOnly: true,
                            errorText: controller.validateBranch),
                        SizedBox(
                          height: size12,
                        ),
                        FormFieldLabel(
                            editingController: controller.listEdtController[1],
                            title: 'Họ tên',
                            isRequired: true,
                            onFocusChanged: (onFocus) {
                              if (!onFocus) {
                                controller.validateCheckName();
                                controller.validateButton();
                              }
                            },
                            errorText: controller.validateName),
                        SizedBox(
                          height: size6,
                        ),
                        FormFieldLabel(
                            editingController: controller.listEdtController[2],
                            title: 'Mật khẩu',
                            obscureText: controller.hidePass,
                            isRequired: true,
                            suffixIcon: InkWell(
                              onTap: (){
                               controller.hidePass.value = ! controller.hidePass.value;
                              },
                              child:  Obx(() => controller.hidePass.value ?  Icon(CupertinoIcons.eye_slash_fill,size: ic_16,color: colorTextHint,) :  Icon(CupertinoIcons.eye_solid,size: ic_16,
                                color: colorTextHint,)),
                            ),
                            onFocusChanged: (onFocus) {
                              if (!onFocus) {
                                controller.validateCheckPassword();
                                controller.validateButton();
                              }
                            },
                            errorText: controller.validatePassword),
                        SizedBox(
                          height: size6,
                        ),
                        FormFieldLabel(
                            editingController: controller.listEdtController[3],
                            title: 'Xác nhận mật khẩu mới',
                            obscureText: controller.hidePassConfirm,
                            isRequired: true,
                            suffixIcon: InkWell(
                              onTap: (){
                               controller.hidePassConfirm.value = ! controller.hidePassConfirm.value;
                              },
                              child:  Obx(() => controller.hidePassConfirm.value ?  Icon(CupertinoIcons.eye_slash_fill,size: ic_16,color: colorTextHint,) :  Icon(CupertinoIcons.eye_solid,size: ic_16,
                                color: colorTextHint,)),
                            ),
                            onFocusChanged: (onFocus) {
                              if (!onFocus) {
                                controller.checkConfirmPass();
                                controller.validateButton();
                              }
                            },
                            errorText: controller.validateConfirmPassword),
                        SizedBox(
                          height: size6,
                        ),
                        InkWell(
                          onTap: () {
                            Get.dialog(calendarPickerDialog(
                                dateTimeChange: (value) {
                                  // controller.dataRequestServices.bookingDate = value?.toIso8601String();
                                  // controller.getTimeInPack();
                                  controller.listEdtController.last.text =
                                      dateToString(dateFormatTime: dOEMap[DateFormatCustom.DD_MM_YYYY],
                                          dateTime: value);
                                  controller.validateDoB();
                                  controller.validateButton();
                                },
                                currentDate: controller.listEdtController.last.text.isNotEmpty ? strToDate(valueTimeString: controller.listEdtController.last.text)
                                    : DateTime.now(),
                                lastDate: DateTime.now(),
                                firstDate: DateTime(DateTime.now().year - 80),
                                context: context));
                          },
                          child: FormFieldLabel(
                              editingController:
                                  controller.listEdtController.last,
                              title: 'Ngày sinh',
                              readOnly: true,
                              enable: false,
                              errorText: controller.validateDate,
                              isRequired: true,
                              onFocusChanged: (onFocus) {
                                if (!onFocus) {
                                  controller.validateDoB();
                                  controller.validateButton();
                                }
                              },
                              suffixIcon: Container(
                                padding: EdgeInsets.all(size14),
                                child: ImagesRender.svgPicture(
                                    src: ImagePath.calendar_bold),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
                buttonCommon(
                    isOK: controller.isOk,
                    title: 'Lưu thông tin',
                    function: () async {
                      FocusScope.of(context).unfocus();
                      controller.createByNumberPhone(controller.imageFile.first);
                    }),
              ],
            ),
          ),
        ));
  }
}
