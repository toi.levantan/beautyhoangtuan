import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/textfieldcommon.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/login_vm/login_password_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/routes/routers.dart';
import '../../../utils/path/image_paths.dart';
import '../../../utils/session/enum_session.dart';
import '../../../utils/widget/images_render.dart';


class LoginPwdPage extends GetWidget<LoginPwdController> {
  const LoginPwdPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        body: bodyWidget(
          padding: EdgeInsets.zero,
          isLoading: controller.isLoading,
          child: SingleChildScrollView(
            child: SizedBox(
              height: Get.height,
              child: Stack(
                children: [
                  ImagesRender.imagesAssets(src: ImagePath.logo_spa,width: Get.width,height: Get.height/2,boxFit: BoxFit.fill),
                  Padding(
                    padding: EdgeInsets.only(top: size56,left: size16,),
                    child: InkWell(
                        onTap: (){
                          Get.back();
                        },
                        child: const Icon(CupertinoIcons.back,color: Colors.white)),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: size24),
                      child: Card(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(size12)),
                        margin: EdgeInsets.zero,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                            itemInfo(label: 'Tài khoản', prefixIcon: ImagesRender.svgPicture(src: ImagePath.person,height: ic_16),errText: controller.errNumberPhone,
                             keyboardType: TextInputType.phone,
                             onChange: (value){
                               controller.validatePhoneNumber(phoneNumber:  controller.textEdits.first.text.trim());
                               controller.checkShowAction();
                            },textEditingController: controller.textEdits.first),
                              SizedBox(height: size16,),
                            itemInfo(label: 'Mật khẩu', prefixIcon: ImagesRender.svgPicture(src: ImagePath.lock,height: ic_16),obscureText: controller.hidePass, suffixIcon: true,
                                errText: controller.errPass,
                            textEditingController: controller.textEdits.last,
                            onChange: (value){
                              controller.validatePass(password:  value.trim());
                              controller.checkShowAction();
                            }),
                              SizedBox(height: size24,),
                              Row(
                               children: [
                                 SizedBox(
                                  height: ic_16,
                                  width: ic_16,
                                  child: InkWell(
                                    onTap: (){
                                     controller.rememberAccount.value = ! controller.rememberAccount.value;
                                    },
                                      child: Obx(() => controller.rememberAccount.value ? ImagesRender.svgPicture(src: ImagePath.checkbox,height: ic_16) : Container(decoration: BoxDecoration(
                                        border: Border.all(strokeAlign: BorderSide.strokeAlignOutside,color: colorIcon,width: 0.8),
                                        borderRadius: BorderRadius.circular(size4)
                                      ),))),
                                ),
                                 SizedBox(width: size8,),
                                 Expanded(child: Text('Ghi nhớ mật khẩu',style: Themes.sfText12_500.copyWith(color:colorIcon))),
                                 InkWell(
                                   onTap: (){
                                     //TODO đi đến màn đăng nhâp với gia tri la quen mat khau
                                     FocusScope.of(context).unfocus();
                                     Get.toNamed(Routers.login_create,arguments: [
                                       CheckUser.forgotPwd,
                                       controller.textEdits.first.text.trim(),
                                     ] );
                                   },
                                     child: Text('Quên mật khẩu',style: Themes.sfText12_500.copyWith(color: colorIcon))),
                               ],
                             ),
                              SizedBox(height: size24,),
                              buttonCommon(title: 'Tiếp tục',color: colorIcon,isOK: controller.isOk,
                              function: (){
                                FocusScope.of(context).unfocus();
                                controller.checkExistByPhone();
                                // controller.
                              }),
                          ],),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding:  EdgeInsets.symmetric(vertical: size74),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                              onTap: () async {
                                FocusScope.of(context).unfocus();
                                Get.toNamed(Routers.login_create,arguments: [
                                  CheckUser.used,
                                  controller.textEdits.first.text.trim(),
                                ]);
                              },
                              child: Column(
                                children:  [
                                  const Icon(CupertinoIcons.chevron_up_circle_fill,color: colorFontStaff2,),
                                  SizedBox(height: size4w,),
                                  Text('Đăng nhập bằng số điện thoại',style: Themes.sfText14_400.copyWith(color: colorFontStaff2),)
                                ],
                              )),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
  Widget itemInfo({String? label,Widget? prefixIcon, bool? suffixIcon,String? hintText ,Rx<bool>? obscureText,TextEditingController? textEditingController, Rx<String?>? errText,
  Function(bool)? onFocusChanged, Function(String)? onChange,TextInputType? keyboardType}){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Text(label ??'',style: Themes.sfText12_700),
      Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
        prefixIcon ?? Container(),
          SizedBox(width: size16,),
         Expanded(child: FormFieldLabelNoBorder(title: '', editingController: textEditingController,errorText: RxnString(),hintText: hintText,obscureText: obscureText,
         keyboardType: keyboardType ,

         onChange: (value){
          if(onChange != null){
            onChange.call(value);
          }
         },
         onFocusChanged: (value){
             if(onFocusChanged != null){
               onFocusChanged.call(value);
             }
         },)),
            Visibility(visible: suffixIcon ?? false, child: InkWell(
                onTap: (){
                  controller.changeShowPass();
                },
                child:  Obx(() => controller.hidePass.value ?  Icon(CupertinoIcons.eye_slash_fill,size: ic_16,color: colorTextHint,) :  Icon(CupertinoIcons.eye_solid,size: ic_16,
                color: colorTextHint,)),
              ),),
      ],),
        SizedBox(height: size8),
        const Divider(color: colorTextHint,height: 1,),
       Obx(() => Visibility(
            visible: errText?.value != null,
            child: Column(
              children: [
                SizedBox(height: size4),
                Text(errText?.value ??"",style: Themes.sfText10_400.copyWith(color: Colors.red)),
              ],
            )))
    ],);
  }
}
