import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/login_vm/login_create_controller.dart';
import 'package:pinput/pinput.dart';
import '../../../utils/widget/button_common.dart';


class OtpPage extends GetView<LoginCreateController> {
  const OtpPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return  gestureFocus(
      context: context,
      child: Scaffold(
            appBar: appBarButton(
                title: 'Mã xác thực OTP',
                colorBackgroundAppbar: Colors.transparent,
                showBottomBorder: false),
            body: bodyWidget(
              isLoading: controller.isLoadingOTP,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Điền OTP vừa được gửi đến số điện thoại:',
                        style: Themes.sfTextHint14_400,
                      ),
                      const SizedBox(height: 16),
                      Text(controller.edtNumberPhone.text,
                          style: Themes.sfText14_500),
                      SizedBox(height: size47),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                              child:  Pinput(
                                controller: controller.otpController,
                                enabled: true,
                                androidSmsAutofillMethod: AndroidSmsAutofillMethod.smsUserConsentApi,
                                length: 6,
                                autofocus: true,
                                keyboardType: TextInputType.phone,
                                animationCurve: Curves.linear,
                                animationDuration: const Duration(microseconds: 420),
                                defaultPinTheme: PinTheme(decoration:   const BoxDecoration(color: Colors.transparent,border: Border(bottom: BorderSide(color: colorBackGrPickTime))),
                                textStyle: Themes.sfText22_500),
                                pinAnimationType: PinAnimationType.slide,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                onCompleted: (value){
                                  controller.validateOTPNumber(otp: value);
                                },
                              )),
                          Expanded(
                              flex: 2,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Obx(() => InkWell(
                                        onTap: () {
                                          // nếu thời gian bằng 0 thì gửi lại OTP
                                          if (controller.timerCount.value == 0) {
                                            FocusScope.of(context).unfocus();
                                            controller.phoneAuthResend(context);
                                          }
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(16.0),
                                          child: RichText(
                                            text: TextSpan(
                                                text: controller.timerCount.value != 0
                                                    ? "Gửi lại OTP sau(${controller.timerCount.value}s)"
                                                    : "Gửi lại OTP",
                                                style: Themes.sfTextHint12_500
                                                    .copyWith(
                                                        color: controller.timerCount.value != 0 ? colorTextHint : colorIcon)),
                                          ),
                                        ),
                                      )),
                                ],
                              )),
                        ],
                      )
                    ],
                  )),
                  buttonCommon(
                      isOK: controller.isOkOtp,
                      title: 'Tiếp tục',
                      titleColor: Colors.white,
                      function: () async {
                        FocusScope.of(context).unfocus();
                        controller.isLoadingOTP;
                        if(controller.isLoadingOTP.value == false){
                        bool isVerify = await controller.verifyOtp();
                        if (isVerify) {
                          ///TODO kiếm tra sdt đã tồn tại chưa? tồn tại rồi thì đưa ra màn home
                          controller.checkExistByPhone();
                        }
                      }}),
                ],
              ),
            ),
      ),
    );
  }
}
