import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/view_model/login_vm/login_create_controller.dart';

import '../../../core/themes/themes.dart';
import '../../../utils/widget/button_common.dart';
import '../../../utils/widget/utils_common.dart';


class LoginPageCreate extends GetView<LoginCreateController> {
  const LoginPageCreate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        appBar: appBarButton(
            title: 'Nhập số điện thoại',
            colorBackgroundAppbar: Colors.transparent,
            isBack: controller.argument == CheckUser.used ? false : true,
            showBottomBorder: false),
        body: bodyWidget(
          isLoading: controller.isLoading,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
             controller.argument  ==  CheckUser.used ? Wrap(children: [
                Text('Nhập số điện thoại để đăng nhập tài khoản hoặc ',style: Themes.sfText14_400.copyWith(color: colorTextHint),),
                InkWell(
                  onTap: (){
                    Get.back();
                  },
                    child: Padding(
                      padding:  EdgeInsets.only(top: size3),
                      child: Text( "Đăng nhập bằng tài khoản",style: Themes.sfText14_400.copyWith(color: colorIcon)),
                    ))])  : Text(
                'Nhập số điện thoại để đăng ký tài khoản',
                style: Themes.sfTextHint14_400,
              ),
              const SizedBox(height: 16),
              Obx(() => FocusScope(
                    child: Focus(
                      onFocusChange: (focus) {
                        if (!focus) {
                          controller.validatePhoneNumber(phoneNumber: controller.edtNumberPhone.text);
                        }
                      },
                      child: TextFormField(
                        controller: controller.edtNumberPhone,
                        maxLines: 1,
                        onChanged: (value) {
                          controller.validatePhoneNumber(
                              phoneNumber: controller.edtNumberPhone.text);
                        },
                        textAlignVertical: TextAlignVertical.center,
                        style: Themes.sfText18_500,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          fillColor: Colors.grey.shade100,
                          border: InputBorder.none,
                          errorText: controller.errTextPhone?.value,
                          hintText: "Số điện thoại của bạn",
                          hintStyle: Themes.sfHintText18_500,
                        ),
                      ),
                    ),
                  )),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.max,
                children: [
                  RichText(
                    text: TextSpan(
                        text: 'Bằng việc tiếp tục, bạn đã đồng ý với ',
                        style: Themes.sfTextHint14_500,
                        children: [
                          TextSpan(
                              text: "Điều khoản & Điều kiện ",
                              style: Themes.sfTextHint14_500
                                  .copyWith(color: colorPolicy)),
                          TextSpan(
                              text: 'cùng ', style: Themes.sfTextHint14_500),
                          TextSpan(
                              text: 'Chính sách bảo mật và chia sẻ thông tin ',
                              style: Themes.sfTextHint14_500
                                  .copyWith(color: colorPolicy)),
                          TextSpan(
                              text: 'của HT Beauty',
                              style: Themes.sfTextHint14_500),
                        ]),
                  ),
                  const SizedBox(height: 10),
                  buttonCommon(
                      isOK: controller.isOk,
                      title: 'Tiếp tục',
                      function: () async {
                        FocusScope.of(context).unfocus();
                        ///TODO chạy được bỏ comment ra
                       if (controller.edtNumberPhone.text.trim() == "0977450517") {
                           await controller.checkExistByPhone();
                        // Get.toNamed(Routers.forgot_pwd);
                        } else {
                          await controller.checkPhoneExistLogin();
                          ///TODO create = 1 la ton tai khach hang va di tu man hinh tao moi
                          if(controller.valueCheckCreate == CREATED && controller.argument == CheckUser.create){
                            return warningNotification(context: context,message: 'Số điện thoại đã được đăng ký. Vui lòng đăng nhập');
                          }
                          if(controller.valueCheckCreate == GUEST && controller.argument == CheckUser.forgotPwd){
                            return warningNotification(context: Get.context!, message: "Không tìm thấy tài khoản của bạn. Vui lòng đăng ký tài khoản");
                          }
                          /// TODO create = 0 la chua ton tai khach hang di tu dang nhap
                          if(controller.valueCheckCreate == GUEST && controller.argument == CheckUser.used){
                            return warningNotification(context: Get.context!, message: "Không tìm thấy tài khoản của bạn. Vui lòng đăng ký tài khoản");
                          }
                          if (controller.valueCheckCreate == 1 || controller.valueCheckCreate == 0) {
                            if (controller.oldPhone != controller.edtNumberPhone.text) {
                              controller.oldPhone = controller.edtNumberPhone.text;
                              controller.phoneAuth(Get.context!);
                              controller.otpController.clear();
                            } else {
                              if (controller.isSuccessOtp) {
                                Get.toNamed(Routers.otp, arguments:
                                  controller.argument,
                                );
                              } else {
                                controller.phoneAuth(Get.context!);
                              }
                            }
                          }
                        }
                      }),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}
