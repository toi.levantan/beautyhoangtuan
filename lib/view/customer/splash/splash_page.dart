import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';

import '../../../utils/widget/images_render.dart';
import '../../../view_model/splash_vm/splash_controller.dart';

class SplashPage extends GetView<SplashController> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: Get.width,
          height: Get.height,
          child: ImagesRender.imagesAssets(src: ImagePath.splash,boxFit: BoxFit.fill)),
    );
  }
}
