import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/model/service/res_services_dto.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';

import '../../../model/booking/doctor_booking_dto.dart';
import '../../../utils/session/date_formatter.dart';
import '../../../utils/session/enum_session.dart';
import '../../../utils/widget/textfieldcommon.dart';
import '../../../utils/widget/utils_common.dart';
import '../../../view_model/book_vm/book_controller.dart';


class BookPage extends GetWidget<BookController> {
  const BookPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        appBar: appBarButton(title: 'Đặt lịch'),
        body: bodyWidget(
          isLoading: controller.isLoading,
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  children: [
                    textFieldDropDown(
                      errorText: controller.chiNhanhErr,
                        label: "Chi nhánh*",
                        hintText: 'Chọn chi nhánh gần nhất',
                        onTap: (){
                          List<SelectedListItem> listConvert = [];
                          for (Data element in controller.listBranch) {
                            SelectedListItem selectItem = SelectedListItem(
                                name: element.organizationBranchName ?? '',
                                value: element.id.toString());
                            listConvert.add(selectItem);
                          }
                          dropDownSearch(
                              sourceList: listConvert,
                              context: Get.context!,
                              bottomSheetTitle: "Chi nhánh",
                              onSelectedValue: (name, value) {
                                int valueParse = int.parse(value);
                                controller.textEdtControllers.first.text = name ?? '';
                                controller.dataRequestServices.organizationBranchId = valueParse;
                                controller.dataRequestServicesCombo.organizationBranchId = valueParse;
                                if(!controller.isShowPickService){
                                  ///TODO chọn đặt lịch từ services
                                  ///gán luôn id gói và id combo luôn
                                  ///TODO call api gọi bác sĩ vì chọn từ services không có id phòng ban DepartmentId
                                  controller.dataRequestServices.beautyServicePackageId = controller.argument[1];
                                  controller.dataRequestServicesCombo.beautyPackageComboId = controller.argument[1];
                                  ///TODO selectedRadio ==1 là dịch vụ == 2 là combo
                                  if(controller.selectedRadio.value ==1){
                                    /// gọi api lấy danh sách bác sĩ
                                    controller.getDoctorServices();
                                  }
                                }
                                else{
                                  ///đặt lịch bình thường gọi api gói dịch vụ và dịch vụ
                                  controller.getServices(valueParse);
                                  controller.getCombo(valueParse);
                                }
                                controller.valueChiNhanh = valueParse;
                                controller.validateBooking();
                                for (int i = 1; i <= controller.textEdtControllers.length - 1; i++) {
                                  controller.textEdtControllers[i].clear();
                                }
                              });
                        },
                        editingController: controller.textEdtControllers.first),
                    Visibility(
                      visible: controller.isShowPickService,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: size12),
                          Obx(() => Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Row(
                                children: [
                                  Radio(
                                      value: 1.obs,
                                      activeColor: colorIcon,
                                      groupValue: controller.selectedRadio.value,
                                      onChanged: (value) {
                                        controller.selectedRadio.value = int.parse(value.toString());
                                        controller.textEdtControllers[6].clear();
                                        controller.textEdtControllers[4].clear();
                                        controller.textEdtControllers[5].clear();
                                      }),
                                  Text(
                                    'Dịch vụ',
                                    style: Themes.sfText14_500,
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Radio(
                                      value: 2.obs,
                                      activeColor: colorIcon,
                                      groupValue: controller.selectedRadio.value,
                                      onChanged: (value) {
                                        controller.selectedRadio.value = int.parse(value.toString());
                                        for (int i = 1; i <= 5; i++) {
                                          controller.textEdtControllers[i].clear();
                                        }
                                      }),
                                  Text(
                                    'Combo',
                                    style: Themes.sfText14_500,
                                  )
                                ],
                              )
                            ],
                          )),
                          SizedBox(height: size12),
                          ///TODO chọn dịch vụ
                          Obx(() => Visibility(visible: controller.selectedRadio.value == 1,
                              child: Column(
                                children: [
                                  textFieldDropDown(
                                      errorText: controller.dichVuErr,
                                      onTap: (){
                                        if (controller.textEdtControllers.first.text.isEmpty && controller.selectedRadio.value == 1 ) {
                                          return warningNotification(
                                              context: context,
                                              message: 'Hãy chọn chi nhánh');
                                        }
                                        List<SelectedListItem> listConvert = [];
                                        if (controller.listContent.isNotEmpty) {
                                          for (Content element in controller.listContent) {
                                            SelectedListItem selectItem =
                                            SelectedListItem(
                                                name: element.name ?? '',
                                                value: element.id.toString());
                                            listConvert.add(selectItem);
                                          }
                                        }
                                        dropDownSearch(
                                            sourceList: listConvert,
                                            context: Get.context!,
                                            bottomSheetTitle: "Dịch vụ",
                                            onSelectedValue: (name, value) {
                                              controller.textEdtControllers[1].text = name ?? '';
                                              controller.dataRequestServices.beautyServiceId = int.parse(value);
                                              controller.dataRequestServices.departmentId = null;
                                              controller.dataRequestServices.beautyServicePackageId =null;
                                              controller.dataRequestServices.performerId = null;
                                              controller.validateBooking();
                                              for (int i = 2; i <= controller.textEdtControllers.length - 1;i++) {
                                                controller.textEdtControllers[i].clear();
                                              }
                                              controller.getPackageService();
                                            });
                                      },
                                      label: 'Dịch vụ*',
                                      hintText: 'Chọn dịch vụ',
                                      editingController:
                                      controller.textEdtControllers[1]),
                                  SizedBox(height: size12),
                                  textFieldDropDown(
                                      onTap: (){
                                        if (controller.textEdtControllers[1].text.isEmpty && controller.selectedRadio.value ==1) {
                                          return warningNotification(
                                              context: context,
                                              message: 'Hãy chọn dịch vụ');
                                        }
                                        List<SelectedListItem> listConvert = [];
                                        for (BeautyServicePackage element in controller.listContentServices) {
                                          SelectedListItem selectItem = SelectedListItem(name:"${element.name}",
                                              value: "${element.departmentId.toString()}/${element.id.toString()}");
                                          listConvert.add(selectItem);
                                        }
                                        dropDownSearch(
                                            sourceList: listConvert,
                                            context: Get.context!,
                                            bottomSheetTitle: "Gói dịch vụ",
                                            onSelectedValue: (name, value) {
                                              controller.textEdtControllers[2].text = name ?? '';
                                              //TODO chọn gói
                                              // lay phong ban
                                              controller.dataRequestServices.beautyServicePackageId = int.tryParse(value.toString().split("/").last);
                                              controller.dataRequestServices.departmentId = int.parse(value.toString().split("/").first);
                                              controller.dataRequestServices.performerId = null;
                                              controller.dataRequestServices.bookingDate = null;
                                              controller.validateBooking();
                                              controller.getDoctor();
                                            });
                                      },
                                      hintText: 'Chọn gói dịch vụ',
                                      label: 'Gói dịch vụ*',
                                      errorText: controller.goiDichVuErr,
                                      editingController:
                                      controller.textEdtControllers[2]),
                                ],
                              ))),
                          ///TODO chọn combo
                          Obx(() => Visibility(
                              visible: controller.selectedRadio.value == 2,
                              child: Column(
                                children: [
                                  textFieldDropDown(
                                      errorText: controller.comboIdErr,
                                      label: 'Combo',
                                      hintText: 'Chọn combo',
                                      onTap: () {
                                        if (controller.textEdtControllers.first.text.isEmpty && controller.selectedRadio.value == 2) {
                                          return warningNotification(
                                              context: context,
                                              message: 'Hãy chọn chi nhánh');
                                        }
                                        List<SelectedListItem> listConvert = <SelectedListItem>[];
                                        for (var element in controller.listCombo) {
                                          SelectedListItem selectedListItem = SelectedListItem(name: element.name,value: element?.id.toString());
                                          listConvert.add(selectedListItem);
                                        }
                                        dropDownSearch(
                                            sourceList: listConvert,
                                            context: Get.context!,
                                            bottomSheetTitle: "Combo",
                                            onSelectedValue: (name, value) {
                                              controller.textEdtControllers[6].text = name ?? '';
                                              controller.dataRequestServicesCombo.beautyPackageComboId = int.parse(value);
                                              controller.validateBooking();
                                            });
                                      },editingController: controller.textEdtControllers[6]),
                                ],
                              ))),
                        ],
                      ),
                    ),
                   Obx(() => Visibility(
                      visible: controller.selectedRadio.value == 1,
                        child: Column(children: [
                      SizedBox(height: size12),
                      ///TODO : chọn kỹ thuật viên
                      textFieldDropDown(
                          errorText: RxnString(),
                          hintText: 'Chọn kỹ thuật viên',
                          label: 'Kỹ thuật viên',
                          onTap: () {
                            //TODO bắt validate kỹ thuật viên phải vừa là text rỗng và đặt lịch không từ gói
                            if(!controller.isShowPickService && controller.textEdtControllers.first.text.isEmpty){
                              return warningNotification(
                                  context: context,
                                  message: 'Hãy chọn chi nhánh gần nhất');
                            }
                            if (controller.textEdtControllers[2].text.isEmpty && controller.isShowPickService) {
                              return warningNotification(
                                  context: context,
                                  message: 'Hãy chọn gói dịch vụ');
                            }
                            List<SelectedListItem> listConvert = [];
                            for (Data element in controller.listDoctorDto) {
                              SelectedListItem selectItem = SelectedListItem(
                                  name: element.fullName ?? '',
                                  value: element.id.toString());
                              listConvert.add(selectItem);
                            }
                            dropDownSearch(
                                sourceList: listConvert,
                                context: Get.context!,
                                bottomSheetTitle: "Kỹ thuật viên",
                                onSelectedValue: (name, value) {
                                  controller.textEdtControllers[3].text = name ?? '';
                                  controller.dataRequestServices.performerId = int.parse(value);
                                  controller.dataRequestServices.bookingDate = null;
                                });
                          },
                          editingController: controller.textEdtControllers[3]),
                    ],))),
                    SizedBox(height: size12),
                    textFieldDropDown(
                      errorText: controller.ngayDatLichErr,
                        label: 'Ngày đặt lịch*',
                        hintText: 'Chọn ngày đặt lịch',
                        icon: Padding(
                          padding: const EdgeInsets.only(right: 4),
                          child: ImagesRender.svgPicture(
                              src: ImagePath.calendar_bold,
                              width: ic_12,
                              height: ic_12),
                        ),
                        editingController: controller.textEdtControllers[4],
                        onTap: () {
                          Get.dialog(calendarPickerDialog(
                              dateTimeChange: (value) {
                                controller.dataRequestServices.bookingDate = value?.toIso8601String();
                                controller.dataRequestServicesCombo.bookingDate = value?.toIso8601String();
                                controller.validateBooking();
                                if(controller.selectedRadio.value == 1){
                                  controller.dataRequestServices.startTime = null;
                                }
                                if(controller.selectedRadio.value != 1){
                                  controller.dataRequestServicesCombo.performerId = null;
                                }
                                controller.textEdtControllers[4].text = "${getValueDOE(dateToString(dateTime: value, dateFormatTime: dOEMap[DateFormatCustom.EEEE]))},${dateToString(dateFormatTime: dOEMap[DateFormatCustom.DD_MM_YYYY], dateTime: value)}";
                              },
                              lastDate: DateTime(DateTime.now().year + 5),
                              firstDate: DateTime.now(),
                              context: context)).then((value) => {
                              controller.dataRequestServices.startTime = "07:00",
                              controller.dataRequestServicesCombo.startTime = "07:00",
                                controller.validateBooking(),
                          });
                        }),
                    SizedBox(height: size12),
                    textFieldDropDown(
                      errorText: controller.gioDatlichErr,
                        label: 'Khung giờ',
                        hintText: 'Chọn khung giờ',
                        icon: Padding(
                          padding: const EdgeInsets.only(right: 4),
                          child: ImagesRender.svgPicture(
                              src: ImagePath.hours_clock,
                              width: ic_12,
                              height: ic_12),
                        ),
                        editingController: controller.textEdtControllers[5],
                        onTap: () async {
                          if (controller.textEdtControllers[4].text.isEmpty) {
                            return warningNotification(
                                context: context,
                                message: 'Hãy chọn ngày đặt lịch');}
                            showCupertinoModalPopup(context: context,
                                builder: (BuildContext context){
                              return Center(
                                child: Container(
                                  width: 300,
                                  height: 212,
                                  decoration:  BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(size12)),
                                  child: CupertinoDatePicker(mode: CupertinoDatePickerMode.time,
                                      use24hFormat: false,
                                      onDateTimeChanged: (dateTime){
                                       String time = dateToString(dateTime: dateTime,dateFormatTime: dOEMap[DateFormatCustom.HH_mm_a]);
                                       controller.textEdtControllers[5].text = time;
                                       controller.hourBook = dateToString(dateTime: dateTime,dateFormatTime: dOEMap[DateFormatCustom.HH_mm]);
                                       controller.dataRequestServices.startTime = controller.hourBook;
                                       controller.dataRequestServicesCombo.startTime = controller.hourBook;
                                      },initialDateTime:  DateTime.now(),
                                  ),
                                ),
                              );
                            }).then((value) => {  controller.validateBooking()});
                          },),
                  ],
                ),
              ),
              buttonCommon(
                  title: 'Đặt lịch',
                  function: () {
                    FocusScope.of(context).unfocus();
                    controller.validateBooking();
                    if(controller.validateBooking() == null){
                      showDialog(barrierDismissible : false,context: context, builder: (_){
                        return AlertDialog(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(size24)),
                          title: Row(
                            children: [
                              Expanded(child: Text("Đặt lịch",style: Themes.sfText15_700)),
                              InkWell(
                                  onTap : (){
                                    Get.back();
                                  },
                                  child: Icon(CupertinoIcons.clear_circled_solid,color: colorIcon,size: ic_18,)),
                            ],
                          ),
                          content: SizedBox(
                            height: size76w,
                            width: Get.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Bạn có chắc chắn muốn đặt lịch?',
                                  style: Themes.sfText12_400,),
                                SizedBox(height: size16,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    buttonLogout(title: "Không",
                                        colorText: Colors.black,
                                        function: () {
                                          Get.back();
                                        }),
                                    buttonLogout(
                                        colorButton: colorIcon,
                                        title: 'Có',
                                        function: (){
                                          if(!controller.isLoading!.value){
                                            controller.booking();
                                          }
                                        }
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                    }
                  },
                  isOK: true.obs),
            ],
          ),
        ),
      ),
    );
  }
  ///TODO lịch chọn giờ
  // List<Widget> listWidget() {
  //   return controller.dataTime
  //       .mapIndexed((index, element) => itemPickCalendar(index, element))
  //       .toList();
  // }
  //
  // Widget itemPickCalendar(int index, Data element) {
  //   return Obx(
  //     () => InkWell(
  //         onTap: () {
  //           controller.dataRequestServices.startTime = element.startTime;
  //           controller.dataTime.forEachIndexed((idx, element) {
  //             if (idx != index) {
  //               controller.dataTime[idx].colorPick?.value = Colors.white;
  //               controller.dataTime[idx].colorTextPick?.value = colorIcon;
  //             }
  //           });
  //           element.colorPick?.value = colorIcon;
  //           element.colorTextPick?.value = Colors.white;
  //         },
  //         child: Container(
  //           alignment: Alignment.center,
  //           height: size24,
  //           constraints: BoxConstraints(maxHeight: size24),
  //           decoration: BoxDecoration(
  //               borderRadius: BorderRadius.circular(size8),
  //               border: Border.all(
  //                   color: element.isActive ?? false
  //                       ? controller.colorIconDef.value
  //                       : controller.colorBackGroundBorderDef.value),
  //               color: element.isActive ?? false
  //                   ? element.colorPick?.value
  //                   : controller.colorBackGrPickTimeDef.value),
  //           child: Text(
  //             element.startTime ?? '',
  //             style: element.isActive ?? false
  //                 ? Themes.sfText12_500
  //                     .copyWith(color: element.colorTextPick?.value)
  //                 : Themes.sfTextHint12_500,
  //           ),
  //         )),
  //   );
  // }
}
