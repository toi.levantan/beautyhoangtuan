import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/chat_vm/chat_detail_controller.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

import '../../../utils/widget/textfieldcommon.dart';

class ChatDetail extends GetWidget<ChatDetailController> {
  const ChatDetail({super.key});
  final _user = const types.User(
    id: '82091008-a484-4a89-ae75-a22bf8d6f3ac',
  );

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
       context: context,
      child: Scaffold(
        appBar: appBarButton(title: '',actions: [
          ImagesRender.netWorkImage(),
        ],),
        body: Chat(messages: controller.messages, onSendPressed: (p0) {

        }, user: _user,
          usePreviewData: true,
        emojiEnlargementBehavior: EmojiEnlargementBehavior.multi,
        showUserAvatars: true,
        showUserNames: true,
          onAttachmentPressed: _handleAttachmentPressed,
        ),
      ),
    );
  }
  Widget customerButtonWidget(){
    return Container(
      constraints: const BoxConstraints(
        minHeight: 60,
        maxHeight: 300,
      ),
      child: Padding(
        padding: EdgeInsets.all(size16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(right: size16,top: size16),
              child: Icon(CupertinoIcons.add,color: colorIcon,size:ic_16),
            ),
            const Expanded(child: FormFieldLabelMessage(title: '', editingController: null,maxLine: 5,minLine: 1,)),
          ],
        ),
      ),
    );
  }
  void _handleAttachmentPressed() {
    showModalBottomSheet<void>(
      context: Get.context!,
      builder: (BuildContext context) => SafeArea(
        child: SizedBox(
          height: 144,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  // _handleImageSelection();
                },
                child: const Align(
                  alignment: AlignmentDirectional.centerStart,
                  child: Text('Photo'),
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  // _handleFileSelection();
                },
                child: const Align(
                  alignment: AlignmentDirectional.centerStart,
                  child: Text('File'),
                ),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: const Align(
                  alignment: AlignmentDirectional.centerStart,
                  child: Text('Cancel'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
