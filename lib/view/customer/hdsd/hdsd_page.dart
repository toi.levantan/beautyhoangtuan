import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../utils/widget/button_common.dart';
import '../../../view_model/news_vm/view_new_detail_controller.dart';


class HDSD extends GetWidget<ViewNewDetailController> {
  const HDSD({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButton(title: 'Hướng dẫn sử chụp ảnh nhận diện'),
      body: Scaffold(
        body: WebViewWidget(controller: controller.webViewController),
      ),
    );
  }
}
