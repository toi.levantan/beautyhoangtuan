import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/model/service/res_services_dto.dart';
import 'package:myhoangtuan/model/notify/Notification_dto.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/infinity.dart';
import 'package:myhoangtuan/view_model/notification/notification_controller.dart';
import 'package:collection/collection.dart';
import '../../../core/routes/routers.dart';
import '../../../core/themes/themes.dart';
import '../../../model/notify/notify_emp.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/widget/button_common.dart';
import '../../../utils/widget/images_render.dart';
import '../../../utils/widget/utils_common.dart';

class NotificationPage extends GetWidget<NotificationController> {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBarButton(
          title: "Thông báo",
        ),
        body: bodyWidget(
          padding: const EdgeInsets.symmetric(horizontal: 0),
          isLoading: controller.isLoading,
          listDataCheck: controller.notificationList,
            child: InfinityRefresh(
              padding: EdgeInsets.zero,
              refreshController: controller.refreshController,
              onLoading: controller.onLoadNotification,
              onRefresh:controller.onRefresherNotification,
              child: Obx(()=> ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: controller.notificationList
                      .mapIndexed((index, element) => Container(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                      decoration: BoxDecoration(color: element.seenStatus == 0 ? colorOtpBox : Colors.transparent),
                      child: Column(
                        children: [
                          InkWell(
                              onTap: () async{
                                await controller.seenNotify(id: element.id);
                                 controller.onRefresherNotification();
                                if(element.url == Routers.historyBook){
                                  Get.toNamed(Routers.historyBook,arguments: 1);
                                  return ;
                                }
                                else if(element.url == Routers.home){
                                  controller.onRefresherNotification();
                                  return ;
                                }
                                if (element.url == Routers.news_detail) {
                                  Content content = Content(id: element.objectId);
                                  Get.toNamed(element.url ?? Routers.notification, arguments: content);
                                  return;
                                }
                                if (element.url == Routers.detail_report) {
                                  Content content = Content(id: element.objectId);
                                  Get.toNamed(element.url ?? Routers.notification, arguments: content);
                                  return;
                                }
                               else {
                                  Get.toNamed(element.url!);
                                }
                              },
                              child: notificationItem(element.obs)),
                        ],
                      )))
                      .toList()),
            ))));
  }
}

Widget notificationItem(Rx<Data> notificationObject) {
  return Obx(() => Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Padding(
        padding:  EdgeInsets.symmetric(vertical: size16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             Padding(
               padding:  EdgeInsets.only(right: size10),
               child: ImagesRender.svgPicture(src: ImagePath.info,height: ic_20,width: ic_20),
             ),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(notificationObject.value.title ??"",style: Themes.sfText14_500,maxLines: 1,),
              const SizedBox(height: 4,),
              Text(notificationObject.value.message ??"",style: Themes.sfText14_300,maxLines: 4,overflow: TextOverflow.ellipsis),
            ],
            )),
            Padding(
              padding: EdgeInsets.only( left: size10),
              child: Text(notificationObject.value.createdDateStr ?? "",style: Themes.sfText10_400),
            ),
          ],
        ),
      ),
      Divider(height: size1,color: colorDoctor,),
    ],
  ));
}
Widget notificationItemStaff(Rx<NotifyEmpWithBody> notificationObject) {
  return Obx(() => Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Padding(
        padding:  EdgeInsets.symmetric(vertical: size16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             Padding(
               padding:  EdgeInsets.only(right: size10),
               child: ImagesRender.svgPicture(src: ImagePath.info,height: ic_20,width: ic_20),
             ),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(notificationObject.value.title ??"",style: Themes.sfText14_500,maxLines: 1,),
              const SizedBox(height: 4,),
              Text( "${notificationObject.value.body?.content}",style: Themes.sfText14_300,maxLines: 4,overflow: TextOverflow.ellipsis),
            ],
            )),
            Padding(
              padding: EdgeInsets.only( left: size10),
              child: Text(notificationObject.value.createdDateStr ?? "",style: Themes.sfText10_400),
            ),
          ],
        ),
      ),
      Divider(height: size1,color: colorDoctor,),
    ],
  ));
}
