import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import '../../../view_model/news_vm/view_new_detail_controller.dart';

class ViewHtmlDetailNews extends GetView<ViewNewDetailController> {
  const ViewHtmlDetailNews({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: controller.isLoading.value ? appBarButton(title: '') :appBarButton(title: controller.content?.title?.toString().capitalizeFirst??''),
        body:  bodyWidget(isLoading: controller.isLoading, child: SingleChildScrollView(child: Html(data: controller.content?.content ??'')))));
  }
}
