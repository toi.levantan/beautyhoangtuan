import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/infinity.dart';
import 'package:myhoangtuan/utils/widget/textfieldcommon.dart';
import 'package:myhoangtuan/view_model/news_vm/news_controller.dart';

import '../../../core/repository/url.dart';
import '../../../core/themes/themes.dart';
import '../../../model/service/res_services_dto.dart';
import '../../../utils/app/size_app.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/widget/images_render.dart';
import '../../../utils/widget/utils_common.dart';


class NewsDetail extends GetView<NewsController> {
  const NewsDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      child: Scaffold(
        appBar: appBarButton(title: "${controller.argument[0]}".toUpperCase(),),
        body: bodyWidget(isLoading: controller.isLoading,child:
       Obx(() => Column(
         children: [
           SearchFormField(editingController: controller.newSearchControllerEdt,
           onFieldSubmit: (_){
             controller.isLoadMore = false;
             controller.getNews();
           },
           onChangedFocus: (focus){
             if(!focus){
               controller.isLoadMore = false;
               controller.getNews();
             }
           }),
           Expanded(
             child: InfinityRefresh(
               padding: EdgeInsets.zero,
               refreshController: controller.refreshController,
               onLoading: controller.onLoadMore,
               onRefresh: controller.onRefresh,
               child: ListView.builder(
                 physics: const NeverScrollableScrollPhysics(),
                  itemCount: controller.listData.length,
                    itemBuilder: (context,index){
                  return  InkWell(
                    onTap: (){
                        Get.toNamed(Routers.news_detail,arguments: controller.listData[index]);
                    },
                      child: itemBuilderAll(content: controller.listData[index]));
                }),
             ),
           ),
         ],
       ))),
      ), context: context,
    );
  }
}
Widget itemBuilderAll({Content? content}) {
  return Column(
    children: [
      Padding(
        padding: EdgeInsets.symmetric(vertical: size16),
        child: Row(
          children: [
            ImagesRender.circleNetWorkImage(content?.imageStores != null &&
                content!.imageStores!.isNotEmpty ? Application.imageNetworkPath(filedId: content.imageStores?.first.fileId) : '', width: size64,
                height: size64),
            Expanded(child: Padding(
              padding:  EdgeInsets.symmetric(horizontal: size14),
              child: Text(content?.title ?? '',maxLines: 3,style: Themes.sfText15_500,),
            )),
          ],
        ),
      ),
      const Divider(height: 1, color: colorBorderTextField)
    ],
  );
}
