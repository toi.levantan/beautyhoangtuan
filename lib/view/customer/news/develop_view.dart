import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import '../../../core/themes/themes.dart';
import '../../../view_model/news_vm/view_new_detail_controller.dart';



class DevelopPage extends GetWidget<ViewNewDetailController> {
  const DevelopPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButton(title: controller.argument?.title??''),
      body: Scaffold(
        body:  Center(child: Text('Chức năng đang phát triển',style: Themes.sfText14_500,)),
      ),
    );
  }
}
