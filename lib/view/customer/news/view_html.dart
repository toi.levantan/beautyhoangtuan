import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import '../../../utils/widget/button_common.dart';
import '../../../utils/widget/utils_common.dart';
import '../../../view_model/news_vm/html_view_controller.dart';

class ViewHtml extends GetWidget<HtmlViewController> {
  const ViewHtml({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButton(title: controller.argument?.title?.toString().capitalizeFirst??''),
      body: Scaffold(
        body: bodyWidget(child: SingleChildScrollView(child: Html(data: controller.argument?.content ??''))),
      ),
    );
  }
}
