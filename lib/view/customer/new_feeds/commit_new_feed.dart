import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/model/new_feeds/comment_feedback_dto.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/infinity.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/create_new_feed_vm/commit_feed_controllers.dart';
import '../../../../core/routes/routers.dart';
import '../../../../core/themes/themes.dart';
import '../../../utils/widget/layout_grid_network.dart';

class CommitFeed extends GetWidget<CommitFeedController> {
  const CommitFeed({super.key});

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        appBar: appBarButton(title: 'Bình luận', actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: ic_16),
            child: Icon(
              Icons.more_horiz_rounded,
              size: ic_18,
              color: colorTextApp,
            ),
          ),
        ]),
        body: Column(
          children: [
            Expanded(child: InfinityRefresh(
                refreshController: controller.refreshController,
                onLoading: controller.onLoad,
                onRefresh: controller.onRefresher,
                padding: EdgeInsets.all(size16),
                child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                                child: Row(children: [
                                  ImagesRender.circleNetWorkImage(
                                      PreferenceUtils.getString(avatar) ?? "",
                                      width: ic_46,
                                      height: ic_46,
                                      circular: size100w),
                                  SizedBox(width: size24),
                                  Expanded(
                                      child: Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Text(
                                                        "${controller.contentFeed?.fullName}",
                                                        style: Themes.sfText14_500,
                                                      ),
                                                    ],
                                                  ),
                                                  // Thời gian đăng
                                                  SizedBox(height: size2),
                                                  Text('', style: Themes.sfTextHint12_400)
                                                ])
                                          ]))
                                ]))
                          ],
                        ),
                        SizedBox(
                          height: size16,
                        ),
                        InkWell(
                          onTap: () {
                            Get.toNamed(Routers.commit_feed);
                          },
                          child: linkifyUnlimited(
                              text: '${controller.contentFeed?.content}'),
                        ),
                        SizedBox(
                          height: size16,
                        ),
                        //TODO hiển thị ảnh hoặc video
                        layoutGridFileDetail(
                            beautyAttachments: controller.beautyAttachments),
                        Obx(() =>
                            Visibility(visible: controller.isLoading.value,
                                child: const Text('Đang tải bình luận...'))),
                        //TODO lam comment
                        Obx(() =>
                            ListView.builder(
                                itemCount: controller.comments.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return itemComments(contentFeedBack: controller.comments[index]);
                                }))
                      ]),
                )),
            ),
            /// TODO create comment
            addComment(
              textEditingController: controller.commentEdtController,
              function: ({comment, image}) {
                if (comment != null && comment.isNotEmpty || image != null && image.isNotEmpty) {
                  controller.createCommit(image: image, content: comment,parentId: controller.parentId);
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  /// item comment
  Widget itemComments({ContentFeedBack? contentFeedBack}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: size8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ImagesRender.circleNetWorkImage(
              '${contentFeedBack?.avatar ?? ''}',
              width: ic_46,
              height: ic_46,
              circular: 100),
          Container(color: Colors.redAccent, width: 1,),
          SizedBox(width: size6),
          Expanded(
              child: Column(
                children: [
                  Container(
                      width: Get.width,
                      decoration: BoxDecoration(
                          color: colorBackGroundBorder.withOpacity(0.6),
                          borderRadius: BorderRadius.circular(size8)
                      ),
                      padding: EdgeInsets.all(size6),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${contentFeedBack?.fullName}',
                              style: Themes.sfText12_700,),
                            SizedBox(height: size6),
                            Text(
                              contentFeedBack?.content ?? '',
                              style: Themes.sfText12_400,
                            ),
                            SizedBox(height: size6),
                          contentFeedBack!.beautyAttachments!.isEmpty ?  Container():  ImagesRender.circleNetWorkImage(Application.imageNetworkPath(filedId: contentFeedBack.beautyAttachments?.first.fileId),
                                width: ic_90,
                                height: ic_90,
                                boxFit: BoxFit.cover)
                          ])),
                  SizedBox(height: size8),

                  /// TODO phan hoi va chinh sua
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Visibility(
                        visible: contentFeedBack.createdBy == PreferenceUtils.getString(myUserName),
                        child: InkWell(onTap: () {
                          controller.commentEdtController.text = contentFeedBack.content ?? '';
                        },
                            child: Text('Chỉnh sửa',
                                style: Themes.sfText11_500.copyWith(color: colorFontStaff2))),
                      ),
                      InkWell(onTap: () {
                        controller.commentEdtController.text = contentFeedBack.fullName ?? '';
                        controller.parentId = contentFeedBack.id;
                      },
                          child: Text('Phản hồi', style: Themes.sfText11_500.copyWith(color: colorFontStaff2))),
                    ],
                  ),

                  ///TODO list phan hoi
                  ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: contentFeedBack.childList?.length,
                    itemBuilder: (context, index) {
                      return itemFeedback(childList: contentFeedBack.childList?[index]);
                    },)
                ],
              )),
        ],
      ),
    );
  }

  /// item phan hoi
  Widget itemFeedback({ChildList? childList}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: size8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ImagesRender.circleNetWorkImage(
                  '${childList?.avatar ?? ''}',
                  width: ic_38,
                  height: ic_38,
                  circular: 100),
              Container(color: Colors.redAccent, width: 1,),
              SizedBox(width: size6),
              Expanded(
                  child: Column(
                    children: [
                      Container(
                          width: Get.width,
                          decoration: BoxDecoration(
                              color: colorBackGroundBorder.withOpacity(0.6),
                              borderRadius: BorderRadius.circular(size8)
                          ),
                          padding: EdgeInsets.all(size6),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('${childList?.fullName}',
                                  style: Themes.sfText12_700,),
                                SizedBox(height: size6),
                                Text(
                                  childList?.content ?? '',
                                  style: Themes.sfText12_400,
                                ),
                                SizedBox(height: size6),
                              childList!.beautyAttachments!.isEmpty ? Container(): ImagesRender.circleNetWorkImage(
                                    Application.imageNetworkPath(filedId: childList.beautyAttachments?.first.fileId ??''),
                                    width: ic_60,
                                    height: ic_60,
                                    boxFit: BoxFit.cover),
                                SizedBox(height: size6)])),
                      SizedBox(height: size8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Visibility(
                            visible: childList.createdBy == PreferenceUtils.getString(myUserName),
                            child: InkWell(onTap: () {
                              controller.commentEdtController.text = childList.content ?? '';
                            }, child: Text('Chỉnh sửa', style: Themes.sfText11_500.copyWith(color: colorFontStaff2))),
                          ),
                          InkWell(onTap: () {
                            controller.commentEdtController.text = childList.fullName ?? '';
                            controller.parentId = childList.parentId;
                            controller.responseUsername = childList.createdBy;
                          }, child: Text('Phản hồi', style: Themes.sfText11_500.copyWith(color: colorFontStaff2))),
                        ],
                      )
                    ],
                  ))],
          ),
        ],
      ),
    );
  }
}
