import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/textfieldcommon.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/create_new_feed_vm/create_new_feed_controller.dart';
import '../../../core/themes/themes.dart';
import '../../../utils/app/application.dart';

class CreateNewFeed extends GetWidget<CreateNewFeedController> {
  const CreateNewFeed({super.key});

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: appBarButton(
            title: 'Tạo bài viết',
            showBottomBorder: false,
            actions: [
           InkWell(
                onTap: () {
                  if(controller.filePickers.isNotEmpty || controller.textContentCheck?.value != null && controller.textContentCheck!.value.isNotEmpty ) {
                    controller.createNewFeed();
                  }
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: size16),
                  child:  Obx(() => controller.filePickers.isNotEmpty || controller.textContentCheck?.value != null && controller.textContentCheck!.value.isNotEmpty ?
                  ImagesRender.svgPicture(src: ImagePath.send_active,boxFit: BoxFit.contain,height: size24,width: size24) : ImagesRender.svgPicture(src: ImagePath.send,boxFit: BoxFit.contain,height: size24,width: size24),
                ),
              )),
            ]),
        body: bodyWidget(
          isLoading: controller.isLoading,
          padding: EdgeInsets.zero,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: size16),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            FormFieldLabelNoBorder(
                              title: '',
                              editingController: controller.textEditingController,
                              errorText: RxnString(),
                              hintText: 'Bạn đang nghĩ gì',
                              onChange: (value){
                                controller.textContentCheck?.value = value;
                              },
                              minLine: 2,
                              textStyle: Themes.sfText17_400,
                              maxLine: 50,
                            ),
                            Visibility(
                                visible: true,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: size12),
                                  child: layoutGridFile(),
                                )),
                          ],
                        ),
                      ),
                    )),
                    pickFromImageAndVideo(pickImage: () {
                      controller.pickFileMedia();
                    }, pickVideo: () {
                      controller.pickFileVideo();
                    }),
                  ],
                ),
                Obx(() => Visibility(
                      visible: controller.isPickFile.value,
                      child: Container(
                        width: Get.width,
                        height: Get.height,
                        color: Colors.white.withOpacity(0.6),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                          ImagesRender.imagesAssets(src: ImagePath.uploading,height: ic_46,width: ic_46),
                            Text(
                              '${controller.isPickFile.value && controller.percent.value >= 100 ? 99 : controller.percent.value}%',
                              style: Themes.sfTextHint10_600
                                  .copyWith(color: colorFontStaff2),
                            )
                          ],
                        ),
                      ),
                    )),
              ]))));
  }

  ///TODO showLayoutGridFile
  Widget layoutGridFile() {
    return Obx(() => controller.filePickers.length == 1
        ? SizedBox(
            child: Stack(alignment: Alignment.topRight, children: [
            Stack(
              alignment: Alignment.center,
              children: [
                Image.file(
                  controller.filePickers.first!,
                  fit: BoxFit.cover,
                  width: Get.width,
                  height: Get.height / 2,
                ),
                Visibility(
                    visible: Application.isTypeVideo(srcPath: controller.medias.first!.path).value,
                    child: Icon(
                      CupertinoIcons.play_circle,
                      color: Colors.white,
                      size: ic_30,
                    ))
              ],
            ),
            closeStack(function: () {
              controller.filePickers.removeLast();
              controller.medias.removeLast();
            })
          ]))
        : controller.filePickers.length == 2
            ? SizedBox(
                width: Get.width,
                height: Get.width,
                child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: controller.filePickers.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return SizedBox(
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: [
                          Row(
                            children: [
                              Stack(
                                alignment: Alignment.center,
                                children: [
                                  Image.file(
                                    controller.filePickers[index]!,
                                    width: index == 0
                                        ? Get.width / 2 - size14
                                        : Get.width / 2 - size18,
                                    height: Get.width - size33,
                                    fit: BoxFit.cover,
                                  ),
                                  Visibility(
                                      visible: Application.isTypeVideo(
                                          srcPath: controller.medias[index]!.path)
                                          .value,
                                      child: Icon(
                                        CupertinoIcons.play_circle,
                                        color: Colors.white,
                                        size: ic_30,
                                      ))
                                ],
                              ),
                              Visibility(
                                  visible: index == 0,
                                  child: SizedBox(width: size4)),
                            ],
                          ),
                          closeStack(function: () {
                            controller.filePickers.removeAt(index);
                            controller.medias.removeAt(index);
                          })
                        ],
                      ),
                    );
                  },
                ))
            : controller.filePickers.length == 3
                ? SizedBox(
                    width: Get.width,
                    height: Get.width,
                    child: Row(
                      children: [
                        Expanded(
                            child:
                                Stack(alignment: Alignment.topRight, children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.file(
                                controller.filePickers.first!,
                                height: Get.width,
                                width: Get.width,
                                fit: BoxFit.cover,
                              ),
                              Visibility(
                                  visible: Application.isTypeVideo(
                                      srcPath: controller.medias.first!.path).value,
                                  child: Icon(
                                    CupertinoIcons.play_circle,
                                    color: Colors.white,
                                    size: ic_30,
                                  ))
                            ],
                          ),
                          closeStack(function: () {
                            controller.filePickers.removeAt(0);
                            controller.medias.removeAt(0);
                          })
                        ])),
                        SizedBox(width: size4),
                        Column(
                          children: [
                            Expanded(
                                child: Stack(
                              alignment: Alignment.topRight,
                              children: [
                                Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Image.file(
                                      controller.filePickers[1]!,
                                      width: Get.width / 3,
                                      height: Get.width,
                                      fit: BoxFit.cover,
                                    ),
                                    Visibility(
                                        visible: Application.isTypeVideo(
                                            srcPath: controller.medias[1]!.path).value,
                                        child: Icon(
                                          CupertinoIcons.play_circle,
                                          color: Colors.white,
                                          size: ic_30,
                                        ))
                                  ],
                                ),
                                closeStack(function: () {
                                  controller.filePickers.removeAt(1);
                                  controller.medias.removeAt(1);
                                })
                              ],
                            )),
                            SizedBox(
                              height: size4,
                            ),
                            Expanded(
                              child: Stack(
                                alignment: Alignment.topRight,
                                children: [
                                  Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Image.file(controller.filePickers.last!,
                                          width: Get.width / 3,
                                          fit: BoxFit.cover,
                                          height: Get.width),
                                      Visibility(
                                          visible: Application.isTypeVideo(
                                              srcPath: controller.medias.last!.path).value,
                                          child: Icon(
                                            CupertinoIcons.play_circle,
                                            color: Colors.white,
                                            size: ic_30,
                                          ))
                                    ],
                                  ),
                                  closeStack(function: () {
                                    controller.filePickers.removeLast();
                                    controller.medias.removeLast();
                                  })
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ))
                : controller.filePickers.length == 4
                    ? SizedBox(
                        width: Get.width,
                        height: Get.width,
                        child: Row(
                          children: [
                            Expanded(
                                flex: 3,
                                child: Stack(
                                    alignment: Alignment.topRight,
                                    children: [
                                      Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          Image.file(
                                            controller.filePickers.first!,
                                            height: Get.width,
                                            width: Get.width,
                                            fit: BoxFit.cover,
                                          ),
                                          Visibility(
                                              visible: Application.isTypeVideo(
                                                  srcPath: controller.medias.first!.path).value,
                                              child: Icon(
                                                CupertinoIcons.play_circle,
                                                color: Colors.white,
                                                size: ic_30,
                                              ))
                                        ],
                                      ),
                                      closeStack(function: () {
                                        controller.filePickers.removeAt(0);
                                        controller.medias.removeAt(0);
                                      })
                                    ])),
                            SizedBox(width: size4),
                            SizedBox(
                              height: Get.width,
                              width: Get.width / 3,
                              child: Column(
                                children: [
                                  Expanded(
                                      child: Stack(
                                    alignment: Alignment.topRight,
                                    children: [
                                      Stack(
                                        alignment: Alignment.center,
                                        fit: StackFit.expand,
                                        children: [
                                          Image.file(
                                            controller.filePickers[1]!,
                                            width: Get.width / 3,
                                            fit: BoxFit.cover,
                                          ),
                                          Visibility(
                                              visible: Application.isTypeVideo(
                                                  srcPath: controller.medias[1]!.path).value,
                                              child: Icon(
                                                CupertinoIcons.play_circle,
                                                color: Colors.white,
                                                size: ic_30,
                                              ))
                                        ],
                                      ),
                                      closeStack(function: () {
                                        controller.filePickers.removeAt(1);
                                        controller.medias.removeAt(1);
                                      })
                                    ],
                                  )),
                                  SizedBox(
                                    height: size4,
                                  ),
                                  Expanded(
                                      child: Stack(
                                    alignment: Alignment.topRight,
                                    children: [
                                      Stack(
                                        fit: StackFit.expand,
                                        alignment: Alignment.center,
                                        children: [
                                          Image.file(
                                            controller.filePickers[2]!,
                                            width: Get.width / 3,
                                            fit: BoxFit.cover,
                                          ),
                                          Visibility(
                                              visible: Application.isTypeVideo(
                                                  srcPath: controller.medias[2]!.path).value,
                                              child: Icon(
                                                CupertinoIcons.play_circle,
                                                color: Colors.white,
                                                size: ic_30,
                                              ))
                                        ],
                                      ),
                                      closeStack(function: () {
                                        controller.filePickers.removeAt(2);
                                        controller.medias.removeAt(2);
                                      })
                                    ],
                                  )),
                                  SizedBox(
                                    height: size4,
                                  ),
                                  Expanded(
                                      child: Stack(
                                    alignment: Alignment.topRight,
                                    children: [
                                      Stack(
                                        alignment: Alignment.center,
                                        fit: StackFit.expand,
                                        children: [
                                          Image.file(
                                            controller.filePickers.last!,
                                            width: Get.width / 3,
                                            fit: BoxFit.cover,
                                          ),
                                          Visibility(
                                              visible: Application.isTypeVideo(
                                                  srcPath: controller.medias.last!.path).value,
                                              child: Icon(
                                                CupertinoIcons.play_circle,
                                                color: Colors.white,
                                                size: ic_30,
                                              ))
                                        ],
                                      ),
                                      closeStack(function: () {
                                        controller.filePickers.removeLast();
                                        controller.medias.removeLast();
                                      })
                                    ],
                                  )),
                                ],
                              ),
                            ),
                          ],
                        ))
                    : controller.filePickers.length > 4
                        ? SizedBox(
                            width: Get.width,
                            height: Get.width,
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 3,
                                    child: Stack(
                                        alignment: Alignment.topRight,
                                        children: [
                                          Image.file(
                                            controller.filePickers.first!,
                                            height: Get.width,
                                            width: Get.width,
                                            fit: BoxFit.cover,
                                          ),
                                          closeStack(function: () {
                                            controller.filePickers.removeAt(0);
                                            controller.medias.removeAt(0);
                                          })
                                        ])),
                                SizedBox(width: size4),
                                SizedBox(
                                  height: Get.width,
                                  width: Get.width / 3,
                                  child: Column(
                                    children: [
                                      Expanded(
                                          child: Stack(
                                        alignment: Alignment.topRight,
                                        children: [
                                          Image.file(
                                            controller.filePickers[1]!,
                                            width: Get.width / 3,
                                            height: Get.width,
                                            fit: BoxFit.cover,
                                          ),
                                          closeStack(function: () {
                                            controller.filePickers.removeAt(1);
                                            controller.medias.removeAt(1);
                                          })
                                        ],
                                      )),
                                      SizedBox(
                                        height: size4,
                                      ),
                                      Expanded(
                                          child: Stack(alignment: Alignment.topRight,
                                        children: [
                                          Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Image.file(
                                                controller.filePickers[2]!,
                                                width: Get.width / 3,
                                                height: Get.width,
                                                fit: BoxFit.cover,
                                              ),
                                              Visibility(
                                                  visible: Application.isTypeVideo(
                                                      srcPath: controller.medias[2]!.path).value,
                                                  child: Icon(
                                                    CupertinoIcons.play_circle,
                                                    color: Colors.white,
                                                    size: ic_30,
                                                  ))
                                            ],
                                          ),
                                          closeStack(function: () {
                                            controller.filePickers.removeAt(2);
                                            controller.medias.removeAt(2);
                                          })
                                        ],
                                      )),
                                      SizedBox(
                                        height: size4,
                                      ),
                                      Expanded(
                                          child: Stack(alignment: Alignment.topRight,
                                        children: [
                                          Image.file(
                                            controller.filePickers[3]!,
                                            width: Get.width / 3,
                                            height: Get.width,
                                            fit: BoxFit.cover,
                                          ),
                                          Container(
                                              color: colorTextApp.withOpacity(0.2),
                                              width: Get.width,
                                              height: Get.height,
                                              alignment: Alignment.center,
                                              child: Text("+${controller.filePickers.length - 4}", style: Themes.sfText23_500.copyWith(color: colorRoundBorder)))],)),],),),],)) : Container());
  }
}
