import 'dart:io';
import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:marquee/marquee.dart';
import 'package:myhoangtuan/model/booking/person_in_charge.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/report_vm/create_report_controller.dart';
import 'package:collection/collection.dart';
import '../../../core/themes/themes.dart';
import '../../../model/booking/doctor_booking_dto.dart';
import '../../../utils/path/image_paths.dart';
import '../../../utils/session/bottom_sheet.dart';
import '../../../utils/session/file_picker.dart';
import '../../../utils/widget/images_render.dart';
import '../../../utils/widget/snack_bar.dart';
import '../../../utils/widget/textfieldcommon.dart';

class CreateReportPage extends GetWidget<CreateReportController> {
  const CreateReportPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      child: Scaffold(
          appBar: appBarButton(title: 'Tạo mới phản ánh'),
          body: bodyWidget(
             isLoading: controller.isLoading,
              child: Column(
                children: [
                  Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
            children: [
                    textFieldDropDown(
                        errorText: controller.errChiNhanh,
                        enabled: controller.argument != null ? false : null,
                        onTap: (){
                          // if (controller.textEdtControllers.first.text.isEmpty) {
                          //   return warningNotification(
                          //       context: context,
                          //       message: 'Hãy chọn chi nhánh gần nhất');
                          // }
                          List<SelectedListItem> listConvert = [];
                          for (Data element in controller.listBranch) {
                            SelectedListItem selectItem = SelectedListItem(
                                name: element.organizationBranchName ?? '',
                                value: element.id.toString());
                            listConvert.add(selectItem);
                          }
                          dropDownSearch(
                              sourceList: listConvert,
                              context: Get.context!,
                              bottomSheetTitle: "Chi nhánh",
                              onSelectedValue: (name, value) {
                                int valueParse = int.parse(value);
                                controller.textEdtControllers.first.text = name ?? '';
                                controller.dataRequestServices.organizationBranchId = valueParse;
                                ///TODO set gia tri cho model
                                controller.dataRequestServices.departmentId = null;
                                controller.dataRequestServices.performerId = null;
                                controller.textEdtControllers[1].clear();
                                controller.validateBooking();
                                for (int i = 1; i <= controller.textEdtControllers.length -1; i++) {
                                  controller.textEdtControllers[i].clear();
                                }
                               //TODO goi danh sách phòng ban
                                controller.getRoom(context: context);
                                controller.validateBooking();
                              });
                        },
                        label: 'Chi nhánh*',

                        hintText: 'Chọn chi nhánh gần nhất',
                        editingController:
                        controller.textEdtControllers.first),
                     SizedBox(height: size12),
                      Visibility(
                        visible: controller.isShowPban!,
                        child: Column(
                          children: [
                            //TODO widget phòng ban
                            textFieldDropDown(
                                errorText: controller.errPhongban,
                                enabled: controller.argument != null ? false : null,
                                onTap: (){
                                  if (controller.textEdtControllers.first.text.isEmpty) {
                                    return warningNotification(
                                        context: context,
                                        message: 'Hãy chọn chi nhánh gần nhất');
                                  }
                                  List<SelectedListItem> listConvert = [];
                                  for (DepartmentDto element in controller.listRoom) {
                                    SelectedListItem selectItem = SelectedListItem(
                                        name: element.departmentName ?? '',
                                        value: element.id.toString());
                                    //addd List để truyền vào dropdown
                                    listConvert.add(selectItem);
                                  }
                                  dropDownSearch(
                                      sourceList: listConvert,
                                      context: Get.context!,
                                      bottomSheetTitle: "Phòng ban",
                                      onSelectedValue: (name, value) {
                                        int valueParse = int.parse(value);
                                        controller.textEdtControllers[1].text = name ?? '';
                                        controller.textEdtControllers[2].clear();
                                        controller.dataRequestServices.departmentId = valueParse;
                                        controller.dataRequestServices.performerId = null;

                                        controller.validateBooking();
                                        for (int i = 2; i <= controller.textEdtControllers.length -1; i++) {
                                          controller.textEdtControllers[i].clear();
                                        }
                                        //TODO goi danh sách người phụ trách
                                        controller.getPersonal();
                                      });
                                },
                                label: 'Phòng ban*',
                                hintText: 'Chọn phòng ban',
                                editingController: controller.textEdtControllers[1]),
                            SizedBox(height: size12),
                            textFieldDropDown(
                                errorText: Rxn(),
                                enabled: controller.argument != null ? false : null,
                                onTap: (){
                                  if (controller.textEdtControllers[1].text.isEmpty) {
                                    return warningNotification(
                                        context: context,
                                        message: 'Hãy chọn phòng ban');
                                  }
                                  List<SelectedListItem> listConvertDoctor = [];
                                  for (PersonInCharge element in controller.listDoctor) {
                                    SelectedListItem selectItem = SelectedListItem(
                                        name: element.fullName ?? '',
                                        value: element.id.toString());
                                    //addd List để truyền vào dropdown
                                    listConvertDoctor.add(selectItem);
                                  }
                                  dropDownSearch(
                                      sourceList: listConvertDoctor,
                                      context: Get.context!,
                                      bottomSheetTitle: "Người phụ trách",
                                      onSelectedValue: (name, value) {
                                        int valueParse = int.parse(value);
                                        controller.textEdtControllers[2].text = name ?? '';
                                        controller.dataRequestServices.performerId = valueParse;
                                        controller.validateBooking();
                                      });
                                },
                                label: 'Phụ trách',
                                hintText: 'Chọn người phụ trách',
                                editingController: controller.textEdtControllers[2]),
                            SizedBox(height: size12),
                          ],
                        ),
                      ),
                    textFieldEdtDropDown(
                      label: 'Người liên quan khác',
                      hintText: "Thêm người liên quan khác",
                      minLine: 1,
                      maxLine: 2,
                      editingController: controller.textEdtControllers[3],
                      errorText: Rxn(),
                    ),
                    SizedBox(height: size12),
                    textFieldDropDown(
                        errorText: Rxn(),
                        onTap: (){
                          List<SelectedListItem> listConvertDoctor = [];
                          for (PersonInCharge element in controller.listTypeReport) {
                            SelectedListItem selectItem = SelectedListItem(
                                name: element.fullName ?? '',
                                value: element.type?.toString()??'');
                            //addd List để truyền vào dropdown
                            listConvertDoctor.add(selectItem);
                          }
                          dropDownSearch(
                              sourceList: listConvertDoctor,
                              context: Get.context!,
                              bottomSheetTitle: "Phản ánh của bạn",
                              onSelectedValue: (name, value) {
                                controller.textEdtControllers[4].text = name ?? '';
                                controller.dataRequestServices.type = int.tryParse(value);
                                controller.validateBooking();
                              });
                        },
                        label: 'Phản ánh của bạn*',
                        hintText: 'Chọn loại đánh giá',
                        editingController: controller.textEdtControllers[4]),
                    SizedBox(height: size12),
                    textFieldEdtDropDown(
                      label: 'Mô tả thêm (Tùy chọn)',
                      hintText: '',
                      minLine: 6,
                      maxLine: 6,
                      editingController: controller.textEdtControllers[5],
                      errorText: Rxn(),
                    ),
                    SizedBox(height: size12),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Đính kèm file (Âm thanh, hình ảnh, video. Kích thước các file không vượt quá 100MB)',
                          style: Themes.sfTextHint12_500,
                        ),
                        Text(
                          '',
                          style: Themes.sfTextHint12_500,
                        ),
                        SizedBox(
                          height: size6,
                        ),
                        Wrap(
                            children: [
                              //listVideo
                          Obx(()=>  Wrap(
                            children: controller.listFileAudio.mapIndexed((index,element) => itemAudio(index,element)).toList())),
                              //ListFile video
                              Obx(()=>  Wrap(
                                  children: controller.listFileVideo.mapIndexed((index,element) => itemVideo(index,element)).toList())),
                            //TODO list file hình ảnh
                          Obx(() =>  Wrap(
                             children: controller.listFileImage.mapIndexed((index,element) => itemImage(index,element)).toList())),
                            InkWell(
                            onTap: () {
                              BottomSheetCommon.getBottomPickFile(
                                //Chọn file từ tệp tin
                                cameraTap: () async {
                                  Get.back();
                                  controller.isLoading?.value = true;
                                  List<File?> file = await PickerFile.audio();
                                 if(file.isNotEmpty) {
                                     for (var element in file) {
                                       controller.listFileAudio.add(element!);
                                     }
                                      controller.isLoading?.value = false;
                                   }
                                  controller.isLoading?.value = false;
                                },
                                galleryTap: () async {
                                  Get.back();
                                  //Chọn từ video
                                  controller.isLoading?.value = true;
                                  List<File?> filePicker = await PickerFile.multiFilePickerImage();
                                  if(filePicker.isNotEmpty){
                                    for (var element in filePicker) {
                                     if(element!.path.endsWith('.jpg')|| element.path.endsWith('.png')|| element.path.endsWith('.heif') ||element.path.endsWith('.jpeg') ){
                                      controller.listFileImage.add(element);
                                     }
                                     if(element.path.endsWith('.mp4') || element.path.endsWith('.hevc')|| element.path.endsWith('.mov')){
                                       controller.listFileVideo.add(element);
                                     }
                                     controller.isLoading?.value = false;
                                    }
                                  }
                                  controller.isLoading?.value = false;
                                },
                              );
                            },
                            child:  Padding(
                              padding:  EdgeInsets.all(size4),
                              child: ImagesRender.imagesAssets(src: ImagePath.imagePickDef,height: ic_80,width: ic_80),
                            )),
                        ]),
                      ],
                    ),
              SizedBox(height: size12,),
                const Text("Đánh giá"),
                SizedBox(height: size12,),
                appRatingBar(updateStar:(value){
                  controller.dataRequestServices.rate = value ?? 5;
                } ,showStar: 5 ),
              SizedBox(height: size16,),
            ],
          ),
                  ),
                  buttonCommon(title: 'Gửi',isOK: controller.isShowAction,
                  function: (){
                   if(!controller.isLoading!.value){
                     controller.createReport();
                   }
                  })
                ],
              ))), context: context,
    );
  }
  Widget itemAudio(int index ,File? file){

    return Padding(
      padding:  EdgeInsets.all(size4),
      child: SizedBox(
        height: size86,
        width: size86,
        child: Card(
          margin: EdgeInsets.zero,
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Positioned(right: 0, child: InkWell(
                          onTap: (){
                            controller.listFileAudio.removeAt(index);
                          },
                          child: Icon(CupertinoIcons.clear_thick_circled,size: ic_18,color: Colors.red,))),
                    ],
                  ),
                ),
                ImagesRender.svgPicture(src: ImagePath.music,height: ic_24,width: ic_24),
                SizedBox(height: 20 ,child: Marquee(text: file?.path.split('/').last??'',style: Themes.sfText10_400,velocity: 20,blankSpace: size8,)),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget itemImage(int index ,File? file){
    return Padding(
      padding:  EdgeInsets.all(size4),
      child: SizedBox(
        height: size86,
        width: size86,
        child: Card(
          margin: EdgeInsets.zero,
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Positioned(right: 0, child: InkWell(
                        onTap: (){
                          controller.listFileImage.removeAt(index);
                        },
                          child: Icon(CupertinoIcons.clear_thick_circled,size: ic_18,color: Colors.red,))),
                    ],
                  ),
                ),
                ImagesRender.svgPicture(src: ImagePath.image,height: ic_24,width: ic_24),
                SizedBox(height: 20,child: Marquee(text :file?.path.split('/').last??'',style: Themes.sfText10_400,velocity: 20,blankSpace: size8,))
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget itemVideo(int index ,File? file){
    return Padding(
      padding:  EdgeInsets.all(size4),
      child: SizedBox(
        height: size86,
        width: size86,
        child: Card(
          margin: EdgeInsets.zero,
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Positioned(right: 0, child: InkWell(
                          onTap: (){
                            controller.listFileVideo.removeAt(index);
                          },
                          child: Icon(CupertinoIcons.clear_thick_circled,size: ic_18,color: Colors.red,))),
                    ],
                  ),
                ),
                ImagesRender.svgPicture(src: ImagePath.video,height: ic_24,width: ic_24),
                SizedBox(height: 20,child: Marquee(text: file?.path.split('/').last??'',style: Themes.sfText10_400,blankSpace: size8,startAfter: const Duration(seconds: 3),velocity: 20,)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
