import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/model/feedback/feedback_dto.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/report_vm/report_controller.dart';

import '../../../core/routes/routers.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/widget/images_render.dart';
import '../../../utils/widget/infinity.dart';


class ReportPage extends GetWidget<ReportController> {
  const ReportPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorFeedback,
        appBar: appBarButton(
          title: 'Phản ánh',
          actions: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: sizeTxt_16),
              child: Center(
                child: InkWell(
                  onTap: () => {
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
                    var result = await Get.toNamed(Routers.createReport);
                      if(result != null){
                        successNotification(context: context,message: 'Gửi phản ánh thành công');
                        controller.getFeedback();
                      }
                    })
                  },
                  child: Text("Tạo mới",
                      style: Themes.sfTextHint14_400.copyWith(
                        color: colorIcon,
                      )),
                ),
              ),
            ),
          ],
        ),
        body: bodyWidget(
          isLoading: controller.isLoading,
          listDataCheck: controller.items,
          child: InfinityRefresh(
            padding: EdgeInsets.zero,
              onRefresh: controller.onRefresh,
              onLoading: controller.onLoading,
              refreshController: controller.refreshController,
              child: Obx(() =>SingleChildScrollView(
                child: Column(
                    children: controller.items.map((e) => itemReport(e)).toList()),
              ))),
        ));
  }
}
Widget itemReport(Content? content) {
  String? day = content?.createdDateStr?.split(" ").first;
  String? time = content?.createdDateStr?.split(" ").last;
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 10),
    child: InkWell(
      onTap: (){
        Get.toNamed(Routers.detail_report,arguments: content);
      },
      child: Column(
        children: [
          // TODO hien thi gio
          timeHoursNullOrDefault(time: time??''),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              //TODO  hien thi avatar
              ImagesRender.circleNetWorkImage(
                  '${PreferenceUtils.getString(avatar)}',
                  width: ic_28,
                  boxFit: BoxFit.cover,
                  height: ic_28),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8, top: 6),
                  child: Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: Colors.white,
                    ),
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            content?.departmentName ??'',
                            style: Themes.sfText14_500,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Loại phản ánh : ${mapTypeFeedBack[content?.type]}",
                            style: Themes.sfText14_400,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          appRatingBar(readOnly: true,showStar: content?.rate),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            day!,
                            style: Themes.sfTextHint11_400,
                          ),
                        ]),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
