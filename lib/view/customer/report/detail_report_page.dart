import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:marquee/marquee.dart';
import '../../../core/themes/themes.dart';
import '../../../model/feedback/feedback_detail.dart';
import '../../../utils/app/application.dart';
import '../../../utils/app/size_app.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/path/image_paths.dart';
import '../../../utils/widget/button_common.dart';
import '../../../utils/widget/images_render.dart';
import '../../../utils/widget/textfieldcommon.dart';
import '../../../utils/widget/utils_common.dart';
import '../../../view_model/report_vm/detail_report_controller.dart';
import 'package:collection/collection.dart';

class DetailReportPage extends GetView<DetailReportController> {
  const DetailReportPage({super.key});

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      child: Scaffold(
          appBar: appBarButton(title: 'Thông tin phản ánh'),
          body: bodyWidget(
              isLoading: controller.isLoading,
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                children: [
                  textFieldDropDown(
                      errorText: Rxn(),
                      enabled: false,
                      label: 'Chi nhánh',
                      icon: Container(),
                      editingController: controller.textEdtControllers.first),
                  SizedBox(height: size12),
                  textFieldDropDown(
                      errorText: Rxn(),
                      label: 'Phòng ban',
                      icon: Container(),
                      editingController: controller.textEdtControllers[1]),
                  SizedBox(height: size12),
                  textFieldDropDown(
                      errorText: Rxn(),
                      label: 'Phụ trách',
                      icon: Container(),
                      editingController: controller.textEdtControllers[2]),
                  SizedBox(height: size12),
                  textFieldDropDown(
                    label: 'Người liên quan khác',
                    minLines: 1,
                    maxLines: 3,
                    icon: Container(),
                    editingController: controller.textEdtControllers[3],
                    errorText: Rxn(),
                  ),
                  SizedBox(height: size12),
                  textFieldDropDown(
                      errorText: Rxn(),
                      icon: Container(),
                      label: 'Phản ánh của bạn',
                      editingController: controller.textEdtControllers[4]),
                  SizedBox(height: size12),
                  textFieldDropDown(
                    label: 'Mô tả thêm',
                    hintText: '',
                    minLines: 1,
                    maxLines: 4,
                    icon: Container(),
                    editingController: controller.textEdtControllers[5],
                    errorText: Rxn(),
                  ),
                  SizedBox(height: size12),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'File phản ánh',
                        style: Themes.sfTextHint12_500,
                      ),
                      Text(
                        '',
                        style: Themes.sfTextHint12_500,
                      ),
                      SizedBox(
                        height: size6,
                      ),
                      Wrap(
                        children: [
                          Obx(() =>
                              Wrap(children: controller.images.mapIndexed((
                                  index, element) => itemImage(index, element))
                                  .toList())),
                          Obx(() =>
                              Wrap(children: controller.videos.mapIndexed((
                                  index, element) => itemVideo(index, element))
                                  .toList())),
                          Obx(() =>
                              Wrap(children: controller.records.mapIndexed((
                                  index, element) => itemAudio(index, element))
                                  .toList())),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: size12,
                  ),
                  const Text("Đánh giá"),
                  SizedBox(
                    height: size12,
                  ),
                  appRatingBar(showStar: 5),
                  SizedBox(height: size24),
                  Row(children: [
                    ImagesRender.svgPicture(src: ImagePath.dot),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: size4),
                      child: Text(
                        'Phản hồi từ hệ thống',
                        style: Themes.sfText16_600,
                      ),
                    ),
                  ]),
                  SizedBox(height: size4),
                  Obx(() =>Visibility(
                    visible: controller.isShowReply.value,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(color: colorIcon.withOpacity(0.2),borderRadius: BorderRadius.circular(ic_8)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: size4),
                          Obx(() =>Text(controller.reply.toString().capitalizeFirst ?? '',style: Themes.sfText14_400,)),
                          SizedBox(height: size4),
                          Obx(() =>
                              Wrap(children: controller.files.mapIndexed((index, element) => itemFile(index, element)).toList())),
                        ],
                      ),
                    ),
                  )),
                ],
              ))),
      context: context,
    );
  }

  Widget itemAudio(int index, Records records) {
    return Padding(
      padding: EdgeInsets.all(size4),
      child: InkWell(
        onTap: () {
          Uri url = Uri.parse(records.url ?? '');
          Application.launchInBrowser(url);
        },
        child: SizedBox(
          height: size90,
          width: size90,
          child: Card(
            margin: EdgeInsets.zero,
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Column(
                children: [
                  Expanded(
                    child: ImagesRender.svgPicture(src: ImagePath.music,
                        height: ic_24,
                        width: ic_24,
                        boxFit: BoxFit.contain),
                  ),
                  SizedBox(
                      height: 20,
                      child: Marquee(
                        text: records.fileName ?? '',
                        style: Themes.sfText10_400,
                        velocity: 20,
                        blankSpace: size8,
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
  Widget itemImage(int index, Images images) {
    return Padding(
      padding: EdgeInsets.all(size4),
      child: InkWell(
        onTap: () {
          Uri url = Uri.parse(images.url ?? '');
          Application.launchInBrowser(url);
        },
        child: SizedBox(
          height: size90,
          width: size90,
          child: Card(
            margin: EdgeInsets.zero,
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Column(children: [
                Expanded(child: ImagesRender.svgPicture(src: ImagePath.image,
                    width: ic_24,
                    height: ic_24,
                    boxFit: BoxFit.contain)),
                SizedBox(
                    height: 20,
                    child: Marquee(
                      text: images.fileName ?? '',
                      style: Themes.sfText10_400,
                      velocity: 20,
                      blankSpace: size8,
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }
  Widget itemVideo(int index, Videos videos) {
    return Padding(
      padding: EdgeInsets.all(size4),
      child: InkWell(
        onTap: (){
          Uri url = Uri.parse(videos.url ??'');
          Application.launchInBrowser(url);
        },
        child: SizedBox(
          height: size90,
          width: size90,
          child: Card(
            margin: EdgeInsets.zero,
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Column(children: [
                Expanded(child: ImagesRender.svgPicture(src: ImagePath.video, width: ic_24, height: ic_24,boxFit: BoxFit.contain)),
                SizedBox(
                    height: 20,
                    child: Marquee(
                      text: videos.fileName ??'',
                      style: Themes.sfText10_400,
                      velocity: 20,
                      blankSpace: size8,
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }
  Widget itemFile(int index, Files files) {
    return Padding(
      padding: EdgeInsets.all(size4),
      child: InkWell(
        onTap: (){
          Uri url = Uri.parse(files.url ??'');
          Application.launchInBrowser(url);
        },
        child: SizedBox(
          height: size90,
          width: size90,
          child: Card(
            margin: EdgeInsets.zero,
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Column(children: [
                Expanded(child: ImagesRender.imagesAssets(src: ImagePath.folder, width: ic_22, height: ic_22,boxFit: BoxFit.contain)),
                SizedBox(
                    height: 20,
                    child: Marquee(
                      text: files.fileName!= null ? files.fileName.toString().capitalizeFirst! :'',
                      style: Themes.sfText10_400,
                      velocity: 20,
                      blankSpace: size8,
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
