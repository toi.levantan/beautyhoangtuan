import 'package:card_swiper/card_swiper.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/model/new_feeds/new_feed_dto.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import '../../../core/themes/themes.dart';


class PreViewMedia extends GetView {
  final Rx<int>? index;
  final List<BeautyAttachments>? beautyAttachments;
  const PreViewMedia({ this.index,super.key,this.beautyAttachments});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Stack(
      alignment: Alignment.topRight,
      children: [
        Swiper(itemCount: beautyAttachments?.length ??0,
        index:  index?.value ?? 0,
        onIndexChanged: (indexChange){
          index?.value = indexChange;
        },
        itemBuilder: (context, index) {
            return InkWell(
             onTap: (){
               if(beautyAttachments?[index].fileType == fileTypeUriMap[FileTypeUri.videos]){
                Get.toNamed(Routers.video_preview,arguments:  beautyAttachments?[index]);
               }
             },
              child: Stack(
                fit: StackFit.passthrough,
                children: [
                  ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: beautyAttachments?[index].thumbnail),boxFit:  BoxFit.contain),
                  Visibility(
                      visible: beautyAttachments?[index].fileType == fileTypeUriMap[FileTypeUri.videos],
                      child: Icon(CupertinoIcons.play_circle,color: Colors.white,size: ic_24,)),
                ],
              ),
            );
        },),
         Padding(
           padding:  EdgeInsets.symmetric(vertical: size36,horizontal: size16),
           child: InkWell(
               onTap: (){
                 Get.back();
               },
               child: Padding(
                 padding: const EdgeInsets.all(8.0),
                 child: ImagesRender.svgPicture(src: ImagePath.close,width: ic_20,height: ic_20),
               )),
         ),
        Padding(
          padding:  EdgeInsets.symmetric(vertical: size48),
          child: Obx(() => Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('${index!.value + 1}/${beautyAttachments?.length}',style: Themes.sfText12_500),
            ],
          )),
        ),
      ],
    ));
  }
}
