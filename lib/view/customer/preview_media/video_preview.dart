import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:video_player/video_player.dart';
import '../../../view_model/video_vm/video_preview_controller.dart';

class VideoPreview extends GetWidget<VideoPreviewController> {
  const VideoPreview({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButton(title: controller.beautyAttachments?.fileName.toString() ??''),
      body: Center(
        child:  AspectRatio(
                aspectRatio: 1.8,
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    VideoPlayer(controller.videoPlayerController),
                    VideoProgressIndicator(controller.videoPlayerController, allowScrubbing: true,)
                  ],
                )))
    );
  }
}
