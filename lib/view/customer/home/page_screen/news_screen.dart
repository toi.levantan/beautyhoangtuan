import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/model/service/res_services_dto.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/textfieldcommon.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/home_vm/home_controller.dart';
import 'package:collection/collection.dart';
import '../../../../core/routes/routers.dart';
import '../../../../core/themes/themes.dart';
import '../../../../utils/widget/infinity.dart';

class NewScreen extends GetWidget<HomeController> {
  const NewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Obx(() => bodyWidget(
          isLoading: controller.isLoadingNews,
          listDataCheck:controller.titleNew.value == enumMapNewsLabel[News.all] ? controller.listAllNews : controller.titleNew.value == enumMapNewsLabel[News.news]
              ? controller.listNews  : controller.titleNew.value == enumMapNewsLabel[News.event] ? controller.listEvent :
             controller.titleNew.value == enumMapNewsLabel[News.document] ?  controller.listDocument: controller.listPromotion,
          viewNoData: ImageNotFoundBlogWidget(),
          child: InfinityRefresh(
            padding: EdgeInsets.zero,
            onRefresh: controller.onRefresherNew,
            onLoading: controller.onLoadNew,
            refreshController: controller.newsRefreshController,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SearchFormField(
                    editingController: controller.newSearchControllerEdt,
                    onChangedFocus: (focus){
                      if(!focus){
                        controller.getNewShort();
                      }
                    },
                    onFieldSubmit: (_) {
                      controller.getNewShort();
                      FocusScopeNode currentFocus = FocusScope.of(context);
                      if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
                        FocusManager.instance.primaryFocus!.unfocus();
                      }
                    }),
                SizedBox(height: size16),
                Text(
                  "Các chủ đề",
                  style: Themes.sfText23_700,
                ),
                SizedBox(height: size16),
                SizedBox(
                  height: size60,
                  child: Obx(() =>
                      ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listNewGroup.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return InkWell(
                              onTap: () {
                                controller.titleNew.value =
                                    controller.listNewGroup[index].title ??
                                        '';
                                if (controller.listNewGroup[index].title == enumMapNewsLabel[News.all]) {
                                  controller.typeNew = enumMapNews[News.all];
                                }
                                if (controller.listNewGroup[index].title ==
                                    enumMapNewsLabel[News.news]) {
                                  controller.typeNew = enumMapNews[News.news];
                                }
                                if (controller.listNewGroup[index].title ==
                                    enumMapNewsLabel[News.event]) {
                                  controller.typeNew = enumMapNews[News.event];
                                }
                                if(controller.listNewGroup[index].title == enumMapNewsLabel[News.document]){
                                  controller.typeNew = enumMapNews[News.document];
                                }
                                if(controller.listNewGroup[index].title == enumMapNewsLabel[News.promotion]){
                                  controller.typeNew = enumMapNews[News.promotion];
                                }
                                controller.getNewShort();
                              },
                              child: Padding(
                                  padding: EdgeInsets.only(
                                      right: index ==
                                          controller.listNewGroup.length - 1
                                          ? zeroSize
                                          : size16),
                                  child: buttonSelected(
                                      title: controller.listNewGroup[index]
                                          .title,
                                      color: controller.listNewGroup[index]
                                          .color,
                                      type: controller.listNewGroup[index]
                                          .type,
                                      isSelected: controller
                                          .listNewGroup[index].status?.obs)));
                        },
                      )),
                ),
                SizedBox(height: size16),
                Obx(() => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(controller.titleNew.value, style: Themes.sfText24_700),
                   Stack(children: [
                     Obx(() =>
                         Visibility(
                             visible: controller.titleNew.value == enumMapNewsLabel[News.all] && controller.listAllNews.isNotEmpty,
                             child:  InkWell(
                                 onTap: (){
                                   Get.toNamed(Routers.news,arguments: [
                                     enumMapNewsLabel[News.all],
                                     enumMapNews[News.all],
                                   ]);
                                 },
                                 child: Text("Xem tất cả",style: Themes.sfText14_500.copyWith(color: colorPolicy),)))),
                     Obx(() =>
                         Visibility(
                             visible: controller.titleNew.value == enumMapNewsLabel[News.news] &&controller.listNews.isNotEmpty,
                             child:  InkWell(
                                 onTap:(){
                                   Get.toNamed(Routers.news,arguments: [enumMapNewsLabel[News.news],
                                     enumMapNews[News.news],]);
                                 },
                                 child: Text("Xem tất cả",style: Themes.sfText14_500.copyWith(color: colorPolicy),)))),
                     Obx(() =>
                         Visibility(
                             visible: controller.titleNew.value == enumMapNewsLabel[News.event] && controller.listEvent.isNotEmpty,
                             child:  InkWell(
                                 onTap: (){
                                   Get.toNamed(Routers.news,arguments: [
                                     enumMapNewsLabel[News.event],
                                     enumMapNews[News.event],
                                   ]);
                                 },
                                 child: Text("Xem tất cả",style: Themes.sfText14_500.copyWith(color: colorPolicy),)))),
                   ],),
                    // Obx(() => Column(children: )),
                  ],
                )),
                SizedBox(height: size2),
                const Divider(height: 1, color: colorBorderTextField),
                Obx(() =>
                    Visibility(
                        visible: controller.titleNew.value == enumMapNewsLabel[News.all],
                        child: Column(
                            children: controller.listAllNews.mapIndexed((index, element) =>
                                itemBuilderAll(content: element)).toList()))),
                Obx(() =>
                    Visibility(
                        visible: controller.titleNew.value == enumMapNewsLabel[News.news],
                        child: Column(
                            children: controller.listNews.mapIndexed((index, element) =>
                                itemBuilderAll(content: element))
                                .toList()))),
                Obx(() =>
                    Visibility(
                        visible: controller.titleNew.value == enumMapNewsLabel[News.event],
                        child: Column(
                            children: controller.listEvent.mapIndexed((index, element) =>
                                itemBuilderAll(content: element)).toList()))),
                Obx(() =>
                    Visibility(
                        visible: controller.titleNew.value == enumMapNewsLabel[News.document],
                        child: Column(
                            children: controller.listDocument.mapIndexed((index, element) =>
                                itemBuilderAll(content: element)).toList()))),
                Obx(() =>
                    Visibility(
                        visible: controller.titleNew.value == enumMapNewsLabel[News.promotion],
                        child: Column(
                            children: controller.listPromotion.mapIndexed((index, element) =>
                                itemBuilderAll(content: element)).toList()))),
                // Obx(() => Column(children: )),
              ],
            ),
          )),
    ));
  }

  Widget buttonSelected(
      {String? title, Color? color, int? type, required Rx<bool>? isSelected}) {
    return Container(
      height: size60,
      width: size150,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(size5),
      ),
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(title ?? '',
            style: Themes.sfText16_700.copyWith(color: Colors.white)),
      ),
    );
  }

  Widget itemBuilderAll({Content? content}) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routers.news_detail, arguments: content);
      },
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: size16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ImagesRender.circleNetWorkImage(
                    content?.imageStores != null &&
                            content!.imageStores!.isNotEmpty
                        ? '${content.imageStores?.first.fileId}'
                        : '',
                    width: size64,
                    height: size64),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: size14),
                    child: Text(
                      '${content?.title.toString().capitalizeFirst}',
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Themes.sfText15_500,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Divider(height: 1, color: colorBorderTextField)
        ],
      ),
    );
  }
}
