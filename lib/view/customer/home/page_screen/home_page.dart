import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:myhoangtuan/view/customer/home/page_screen/home_screen.dart';
import 'package:myhoangtuan/view/customer/home/page_screen/message.dart';
import 'package:myhoangtuan/view/customer/home/page_screen/new_feeds.dart';
import 'package:myhoangtuan/view/customer/home/page_screen/profile_screen.dart';
import 'package:myhoangtuan/view/customer/home/services_screen/services_screen.dart';
import 'package:stylish_bottom_bar/model/bar_items.dart';
import 'package:stylish_bottom_bar/stylish_bottom_bar.dart';
import '../../../../core/routes/routers.dart';
import '../../../../utils/app/share_pref.dart';
import '../../../../utils/path/color_path.dart';
import '../../../../utils/path/const_key.dart';
import '../../../../utils/path/image_paths.dart';
import '../../../../utils/widget/dialog_common.dart';
import '../../../../view_model/home_vm/home_controller.dart';

class HomePage extends GetWidget<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  //trang chủ
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
       Offset offset = Offset(constrains.maxWidth, constrains.maxHeight);
      return Scaffold(
            resizeToAvoidBottomInset: false,
            body: Stack(
              children: [
                PageView(
                  controller: controller.pageController,
                  physics: const NeverScrollableScrollPhysics(),
                  children: const [
                    SafeArea(child: HomeScreen()),
                    Center(child: Text("Chức năng đang phát triển"),),
                    SafeArea(child: ServiceScreen()),
                    SafeArea(child: NewFeeds()),
                    Center(child: MessageChatPage()),
                    ProfileScreen(),
                  ],
                ),
              Obx(() =>  Visibility(
                visible: controller.indexPage.value == 0 || controller.indexPage.value ==1 || controller.indexPage.value ==2 ,
                child: Positioned(
                      right: controller.right.value,
                      bottom:  controller.bottom.value,
                      child: GestureDetector(
                        onTap: ()async{
                          if (PreferenceUtils.getString(keyAccessToken) == null) {
                            showDialog(
                                context: Get.context!,
                                builder: (_) {
                                  return AlertDialog(
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(24.0),
                                        ),
                                      ),
                                      content: Builder(builder: (context) {
                                        return RequiredLoginDialog(
                                          loginButton: () async {
                                            Get.toNamed(Routers.intro_page);
                                          },
                                        );
                                      }));
                                });
                          }
                          else {
                            final result = await Get.toNamed(Routers.book);
                            if (result != null) {
                              successNotificationNoAppBar(context: Get.context!,
                                  message: 'Đã đặt lịch thành công');
                            }
                          }
                        },
                      onPanUpdate: (details) {
                       controller.bottom.value = max(0, controller.bottom.value - details.delta.dy);
                       if(controller.bottom.value < 10){
                         controller.bottom.value = 10;
                       }
                       double maxDy = offset.dy - AppBar().preferredSize.height -Get.statusBarHeight - size48;
                       if(controller.bottom.value > maxDy){
                         controller.bottom.value = maxDy;
                       }
                       controller.right.value = max(0, controller.right.value - details.delta.dx)  < 10 ? 10 : max(0, controller.right.value - details.delta.dx);
                       double maxDx = offset.dx - size60;
                       if(controller.right.value > maxDx){
                         controller.right.value = maxDx;
                       }
                      },
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: colorIcon,
                              borderRadius: BorderRadius.circular(size100),
                            boxShadow: const [
                              BoxShadow(
                                color: Color(0x33000000),
                                blurRadius: 56,
                                offset: Offset(10, 14),
                                spreadRadius: 4,
                              )
                            ],
                          ),
                          padding: EdgeInsets.all(size16),
                          child: SizedBox(
                              child: ImagesRender.svgPicture(src: ImagePath.book_calendar,height: ic_20,width: ic_20))),
                        SizedBox(height: size5),
                        Text("Đặt lịch",style: Themes.sfText12_500.copyWith(color: colorIcon),),
                      ],
                    ))),
              ))
              ],
            ),
            bottomNavigationBar: Obx(() => StylishBottomBar(
                  items: [
                    BottomBarItem(
                        icon: ImagesRender.svgPicture(
                            src: ImagePath.unselectedHome,
                            width: ic_20,
                            height: ic_20),
                        selectedIcon: ImagesRender.svgPicture(
                            src: ImagePath.home, width: ic_20, height: ic_20),
                        backgroundColor: colorIcon,
                        selectedColor: colorIcon,
                        title: Text(
                          'Trang chủ',
                          style: Themes.sfText9_500,
                        )),
                    BottomBarItem(
                        icon: ImagesRender.svgPicture(
                            src: ImagePath.pre_book,
                            width: ic_20,
                            height: ic_20),
                        selectedIcon: ImagesRender.svgPicture(
                            src: ImagePath.pre_book_active, width: ic_20, height: ic_20),
                        backgroundColor: colorUnset,
                        selectedColor: colorIcon,
                        title: Text(
                          'Lịch hẹn',
                          style: Themes.sfText9_500,
                        )),
                    BottomBarItem(
                        icon: ImagesRender.svgPicture(
                            src: ImagePath.unselectedShop,
                            width: ic_20,
                            height: ic_20),
                        selectedIcon: ImagesRender.svgPicture(
                            src: ImagePath.shop, width: ic_20, height: ic_20),
                        backgroundColor: colorUnset,
                        selectedColor: colorIcon,
                        unSelectedColor: colorUnset,
                        title: Text(
                          'Dịch vụ',
                          style: Themes.sfText9_500,
                        )),
                    BottomBarItem(
                        icon: ImagesRender.svgPicture(
                            src: ImagePath.feeds,
                            width: ic_20,
                            height: ic_20),
                        selectedIcon: ImagesRender.svgPicture(
                            src: ImagePath.feeds_active, width: ic_20, height: ic_20),
                        backgroundColor: colorUnset,
                        selectedColor: colorIcon,
                        title: Text(
                          'Bảng tin',
                          style: Themes.sfText9_500,
                        )),
                    BottomBarItem(
                        icon: ImagesRender.svgPicture(
                            src: ImagePath.chat,
                            width: ic_20,
                            height: ic_20),
                        selectedIcon: ImagesRender.svgPicture(
                            src: ImagePath.chat_active, width: ic_20, height: ic_20),
                        backgroundColor: colorUnset,
                        selectedColor: colorIcon,
                        title: Text(
                          'Tin nhắn',
                          style: Themes.sfText9_500,
                        )),
                    BottomBarItem(
                        icon: ImagesRender.svgPicture(
                            src: ImagePath.unselectedUser,
                            width: ic_20,
                            height: ic_20),
                        selectedIcon: ImagesRender.svgPicture(
                            src: ImagePath.user, width: ic_20, height: ic_20),
                        backgroundColor: colorUnset,
                        selectedColor: colorIcon,
                        title: Text(
                          'Tài khoản',
                          style: Themes.sfText9_500,
                        )),
                  ],
                  currentIndex: controller.indexPage.value,
                  onTap: (index) {
                   if(PreferenceUtils.getString(keyAccessToken) == null){
                     if(index == 1 || index == 3  || index == 4){
                        showDialog(
                              context: context,
                              builder: (_) {
                                return AlertDialog(
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(24.0),
                                      ),
                                    ),
                                    content: Builder(builder: (context) {
                                      return RequiredLoginDialog(
                                        loginButton: () {
                                          Get.toNamed(Routers.intro_page);
                                        },
                                      );
                                    }));
                              });
                     }
                     else{
                       controller.pageController.jumpToPage(index);
                       controller.indexPage.value = index;
                     }
                   }
                    else{
                    controller.pageController.jumpToPage(index);
                    controller.indexPage.value = index;
                   }
                  },
                  option: AnimatedBarOptions(iconStyle: IconStyle.Default),
                )));});
  }
}
