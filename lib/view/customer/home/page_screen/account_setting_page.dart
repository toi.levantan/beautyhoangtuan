import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/view_model/home_vm/home_controller.dart';

class AccountSettingPage extends GetWidget<HomeController> {
  const AccountSettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButton(title: 'Tài khoản'),
    );
  }
}
