import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/model/chat/recently_chat_dto.dart';
import 'package:myhoangtuan/model/chat/user_chat_dto.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import '../../../../core/routes/routers.dart';
import '../../../../core/themes/themes.dart';
import '../../../../view_model/home_vm/home_controller.dart';

class MessageChatPage extends GetWidget<HomeController> {
  const MessageChatPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: appBarBook(
            childTab: Padding(
              padding: EdgeInsets.symmetric(horizontal: size16),
              child: Row(
                children: [
                  ImagesRender.svgPicture(
                      src: ImagePath.chat_search, width: ic_20, height: ic_20),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: size16,),
                      child: TextFormField(
                        style: Themes.sfText16_500.copyWith(color: Colors.white),
                        decoration: InputDecoration(
                            hintStyle: Themes.sfText16_500
                                .copyWith(color: Colors.white),
                            hintText: "Tìm kiếm",
                            border: InputBorder.none),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: size120,
                  child: Padding(
                    padding: EdgeInsets.only(left: size8, right: size8, top: size8),
                    child: Obx(() => ListView.builder(
                        shrinkWrap: true,
                        itemCount: controller.listUser.length,
                        itemBuilder: (context, index) {
                      return itemUser(controller.listUser[index]);
                    },scrollDirection: Axis.horizontal)))),

                ///TODO danh sách người chat
                Expanded(
                  child: Padding(
                      padding: EdgeInsets.only(left: size8, right: size8, top: size8),
                      child: Obx(() => ListView.builder(
                          padding: EdgeInsets.zero,
                          itemCount: controller.listRecentlyChat.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            // final Message chat = chats[index];
                            return itemChat(controller.listRecentlyChat[index]);
                          })))),
              ],
            )));
  }
  Widget itemUser(DataUserChat? dataUserChat){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: size4),
      child: InkWell(
        onTap: (){
          Get.toNamed(Routers.chat_detail);
        },
        child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(
                  children: [
                    ImagesRender.circleNetWorkImage(dataUserChat?.customJson?.avatarUrl ??'',width: ic_46,height: ic_46,circular: size100),
                    Positioned(bottom: 0,right: 0,child: dataUserChat!.isOnline! ?  Icon(Icons.circle,color: Colors.green,size: size10):
                    Icon(Icons.circle,color: Colors.grey,size: size10)),
                  ],
                ),
                SizedBox(height: size4),
                SizedBox(
                    width: size86,
                    child: Text('${dataUserChat.firstName.toString().capitalize ??''} ${dataUserChat.lastName.toString().capitalize??''}',style: Themes.sfText9_500,maxLines: 2,overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,)),
              ],
        ),
      ),
    );
  }
  Widget itemChat(DataRecentlyChat? dataRecentlyChat) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routers.chat_detail,arguments: dataRecentlyChat);
      },
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: size10, horizontal: size8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(size33),
                    child: ImagesRender.netWorkImage("",
                        width: ic_46, height: ic_46)),
                SizedBox(
                  width: size16,
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      dataRecentlyChat!.isDirectChat! ? Row(
                        children: dataRecentlyChat.people!.map((e) => Visibility(
                            visible: e.person?.username != PreferenceUtils.getString(myUserName),
                            child: Text('${e.person?.firstName.toString().capitalize} ${e.person?.lastName.toString().capitalize}',style: Themes.sfText14_700,overflow: TextOverflow.ellipsis,))).toList()
                      ) :
                      Row(
                          children: dataRecentlyChat.people!.map((e) =>Text('${e.person?.firstName.toString().capitalize} ${e.person?.lastName.toString().capitalize},',
                          style: Themes.sfText14_700,overflow: TextOverflow.ellipsis,)).toList()
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        dataRecentlyChat.lastMessage?.text ??"",
                        overflow: TextOverflow.ellipsis,
                        style: Themes.sfText12_400
                            .copyWith(color: Colors.blueGrey),
                      ),
                    ],
                  ),
                ),
                // Container(
                //   decoration: BoxDecoration(
                //       color: colorIcon,
                //       borderRadius: BorderRadius.circular(size16)),
                //   child: Padding(
                //     padding: EdgeInsets.all(size6),
                //     child: Text(
                //       "99",
                //       style: Themes.sfText10_500.copyWith(color: Colors.white),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
          const Divider()
        ],
      ),
    );
  }
}
