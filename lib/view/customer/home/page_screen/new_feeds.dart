import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/layout_grid_network.dart';
import '../../../../core/routes/routers.dart';
import '../../../../core/themes/themes.dart';
import '../../../../utils/app/application.dart';
import '../../../../utils/app/share_pref.dart';
import '../../../../utils/app/size_app.dart';
import '../../../../utils/path/color_path.dart';
import '../../../../utils/path/const_key.dart';
import '../../../../utils/widget/button_common.dart';
import '../../../../utils/widget/images_render.dart';
import '../../../../utils/widget/infinity.dart';
import '../../../../utils/widget/snack_bar.dart';
import '../../../../utils/widget/utils_common.dart';
import '../../../../view_model/home_vm/home_controller.dart';
import 'package:collection/collection.dart';

class NewFeeds extends GetWidget<HomeController> {
  const NewFeeds({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBgNewFeed,
      body: bodyWidget(
        isLoading: controller.isLoadingNewFeed,
        padding: EdgeInsets.zero,
        child: InfinityRefresh(
            refreshController: controller.newFeedRefreshController,
            onLoading: controller.onLoadMoreNewFeed,
            onRefresh: controller.onRefreshNewFeed,
            padding: EdgeInsets.zero,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(size16),
                    width: Get.width,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                            padding: EdgeInsets.only(right: size8),
                            child: Obx(() => ImagesRender.circleNetWorkImage(
                                "${controller.avatarValue}",
                                circular: 100,
                                width: ic_50,
                                height: ic_50,
                                boxFit: BoxFit.cover))),
                        Expanded(
                            child: InkWell(
                          onTap: () async {
                            final result =
                                await Get.toNamed(Routers.create_new_feeds);
                            if (result != null) {
                              controller.onRefreshNewFeed();
                              successNotificationNoAppBar(
                                  context: Get.context!,
                                  message: 'Tạo bài viết thành công');
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.all(size14),
                            decoration: BoxDecoration(
                              color: colorBackGroundSearch,
                              borderRadius: BorderRadius.circular(size16),
                            ),
                            child: Text(
                              'Hôm nay bạn thế nào',
                              style: Themes.sfText14_400,
                            ),
                          ),
                        )),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: size6,
                  ),
                  Obx(() => Column(
                        mainAxisSize: MainAxisSize.min,
                        children: controller.newFeeds
                            .mapIndexed((index, element) =>
                                itemNewFeeds(context, index))
                            .toList(),
                      )),
                  //    ListView.builder(
                  //     itemCount: controller.newFeeds.length,
                  //     shrinkWrap: true,
                  //     physics: const NeverScrollableScrollPhysics(),
                  //     itemBuilder: (context, index) {
                  //       return itemNewFeeds(context, controller.newFeeds[index].obs,index);
                  //     },
                  // ),
                ],
              ),
            )),
      ),
    );
  }

  Widget itemNewFeeds(BuildContext? context, int index) {
    controller.newFeeds[index].interactionTam =
        controller.newFeeds[index].interactionCount;
    return Obx(() => Column(
          children: [
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(size16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      ImagesRender.circleNetWorkImage(
                          PreferenceUtils.getString(avatar) ?? "",
                          width: ic_46,
                          height: ic_46,
                          circular: size100w),
                      SizedBox(width: size24),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${controller.newFeeds[index].fullName}',
                                  style: Themes.sfText14_500,
                                ),
                                SizedBox(height: size2),
                                Text(
                                  '${Application.showNearDay(time: controller.newFeeds[index].createdDate)}',
                                  style: Themes.sfTextHint12_400,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                          onTap: () {
                            Get.bottomSheet(Container(
                                width: Get.width,
                                height: Get.width * (3 / 5),
                                color: Colors.white,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    itemInfoShowTwoLineData(
                                        img: ImagePath.del_post,
                                        data1: 'Xoá bài viết',
                                        data2:
                                            "Bài viết này sẽ ẩn khỏi bảng tin",
                                        onTap: () {
                                          // Get.back();
                                          showDialog(
                                              context: Get.context!,
                                              builder: (_) {
                                                return AlertDialog(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              size24)),
                                                  title: Row(
                                                    children: [
                                                      Expanded(
                                                          child: Text(
                                                              "Xóa bài viết",
                                                              style: Themes
                                                                  .sfText15_700)),
                                                      InkWell(
                                                          onTap: () {
                                                            Get.back();
                                                          },
                                                          child: Icon(
                                                            CupertinoIcons
                                                                .clear_circled_solid,
                                                            color: colorIcon,
                                                            size: ic_18,
                                                          )),
                                                    ],
                                                  ),
                                                  content: SizedBox(
                                                    height: size76,
                                                    width: Get.width,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          'Bạn có chắc chắn muốn xóa bài viết?',
                                                          style: Themes
                                                              .sfText12_400,
                                                        ),
                                                        SizedBox(
                                                          height: size16,
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          mainAxisSize:
                                                              MainAxisSize.max,
                                                          children: [
                                                            buttonLogout(
                                                                title: "Có",
                                                                colorText:
                                                                    Colors
                                                                        .black,
                                                                function: () {
                                                                  Get.back();
                                                                  Get.back();
                                                                  controller.delNewFeed(
                                                                      id: controller
                                                                          .newFeeds[
                                                                              index]
                                                                          .id);
                                                                }),
                                                            buttonLogout(
                                                                colorButton:
                                                                    colorIcon,
                                                                title: 'Không',
                                                                function: () {
                                                                  Get.back();
                                                                }),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              });
                                        }),
                                    itemInfoShowTwoLineData(
                                        img: ImagePath.edit_post,
                                        data1: 'Chỉnh sửa bài viết',
                                        data2:
                                            "Bài viết của bạn sẽ được chỉnh sửa"),
                                    itemInfoShowTwoLineData(
                                        img: ImagePath.share_post,
                                        data1: 'Chia sẻ bài viết',
                                        data2:
                                            "Bài viết của bạn sẽ được chia sẻ",
                                        showLine: false),
                                  ],
                                )));
                          },
                          child: Visibility(
                            visible: controller.newFeeds[index].createdBy ==
                                PreferenceUtils.getString(myUserName),
                            child: Padding(
                              padding: EdgeInsets.all(size6),
                              child: Icon(
                                Icons.more_horiz_rounded,
                                size: ic_18,
                              ),
                            ),
                          ))
                    ],
                  ),
                  Visibility(
                    visible: controller
                        .newFeeds[index].beautyAttachments!.isNotEmpty,
                    child: SizedBox(
                      height: size16,
                    ),
                  ),
                  InkWell(
                      onTap: () {
                        Get.toNamed(Routers.commit_feed,
                            arguments: controller.newFeeds[index]);
                      },
                      child: linkify(
                          text: controller.newFeeds[index].content
                              .toString()
                              .trim())),

                  ///TODO check file từng index xem có ảnh không
                  Visibility(
                    visible: controller
                        .newFeeds[index].beautyAttachments!.isNotEmpty,
                    child: SizedBox(
                      height: size16,
                    ),
                  ),
                  layoutGridFile(newFeeds: controller.newFeeds, index: index),
                  SizedBox(height: size16),
                  Row(children: [
                    Expanded(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                          InkWell(
                              onTap: () {
                                controller.newFeeds[index].liked = !controller.newFeeds[index].liked!;
                                controller.isRefresher.value = false;
                                if (controller.newFeeds[index].liked!) {
                                  controller.newFeeds[index].interactionTam = controller.newFeeds[index].interactionCount! + 1;
                                } else {
                                  controller.newFeeds[index].interactionTam = controller.newFeeds[index].interactionCount! - 1;
                                }
                                controller.newFeeds.refresh();
                                controller.postLikeNewFeed(
                                  newsFeedId: controller.newFeeds[index].id,
                                  status: controller.newFeeds[index].liked!
                                      ? mapStatusLike[LikeStatus.values.first]
                                      : mapStatusLike[LikeStatus.values.last],
                                );
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    controller.newFeeds[index].liked!
                                        ? Icon(CupertinoIcons.heart_solid,
                                            size: ic_24,
                                            color: Colors.redAccent)
                                        : Icon(
                                            CupertinoIcons.heart,
                                            size: ic_24,
                                            color: colorIcon,
                                          ),
                                    SizedBox(width: size4),
                                    Obx(() => controller.newFeeds[index].liked!
                                        ? Text(
                                            "${Application.numberOfLike(number: "${controller.newFeeds[index].interactionTam}")}")
                                        : Text(
                                            "${Application.numberOfLike(number: "${controller.newFeeds[index].interactionTam}")}"))
                                  ])),
                          InkWell(
                            onTap: () {
                              Get.toNamed(Routers.commit_feed,
                                  arguments: controller.newFeeds[index]);
                            },
                            child: Row(children: [
                              ImagesRender.svgPicture(
                                  src: ImagePath.message_new_feeds,
                                  width: ic_24,
                                  height: ic_24),
                              SizedBox(width: size4),
                              Text(
                                  '${Application.numberOfLike(number: controller.newFeeds[index].commentCount.toString())}',
                                  style: Themes.sfText12_400)
                            ]),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              ImagesRender.svgPicture(
                                  src: ImagePath.share_new_feeds,
                                  height: ic_24,
                                  width: ic_24),
                              SizedBox(
                                width: size4,
                              ),
                              Text('Chia sẻ',
                                  textAlign: TextAlign.end,
                                  style: Themes.sfText12_400),
                            ],
                          )
                        ])),
                  ])
                ],
              ),
            ),
            SizedBox(height: size10),
          ],
        ));
  }
}
