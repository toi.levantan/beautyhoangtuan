import 'package:badges/badges.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/dialog_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/infinity.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import 'package:myhoangtuan/utils/widget/textfieldcommon.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/home_vm/home_controller.dart';
import 'package:collection/collection.dart';
import '../../../../core/routes/routers.dart';
import '../../../../core/themes/themes.dart';
import '../../../../model/service/res_services_dto.dart';
import '../../../../utils/app/share_pref.dart';
import '../../../../utils/path/const_key.dart';
import '../../../../utils/session/date_formatter.dart';
import '../../history_book/item_history_book.dart';
import 'package:badges/badges.dart' as badges;

class HomeScreen extends GetWidget<HomeController> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: bodyWidget(
        isLoading: controller.isLoading,
        child: Column(
          children: [
             Padding(
                    padding: EdgeInsets.only(right: size12w, top: size10w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Obx(() => controller.isLogin.value ?Text(
                              "Xin chào ${PreferenceUtils.getString(userKey).obs.toUpperCase()}",
                              style: Themes.sfText16_500,
                            ) : Text("Xin chào Khách hàng", style: Themes.sfText16_500,)),
                            const SizedBox(
                              height: 6,
                            ),
                            RichText(
                                text: TextSpan(
                                    text: 'Hôm nay',
                                    style: Themes.sfHintText11_500,
                                    children: [
                                      const TextSpan(text: " "),
                                      TextSpan(
                                          text: getValueDOE(dateToString(
                                              dateTime: DateTime.now(),
                                              dateFormatTime:
                                              dOEMap[DateFormatCustom.EEEE]))),
                                      const TextSpan(text: " "),
                                      const TextSpan(text: 'ngày'),
                                      const TextSpan(text: " "),
                                      TextSpan(
                                          text: dateToString(
                                              dateTime: DateTime.now(),
                                              dateFormatTime: dOEMap[
                                              DateFormatCustom.DD_MM_YYYY])),
                                    ])),
                          ],
                        ),
                        Container(
                          padding:  EdgeInsets.all(size10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(size20w),
                            border:
                                Border.all(color: colorRoundBorder, width: 1),
                          ),
                          child: Row(
                            children: [
                              ImagesRender.svgPicture(
                                  src: ImagePath.point,
                                  width: ic_8,
                                  height: ic_14),
                              const SizedBox(
                                width: 8,
                              ),
                              const Text('100'),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () async {
                              if (PreferenceUtils.getString(keyAccessToken) == null) {
                                showDialog(
                                    context: context,
                                    builder: (_) {
                                      return AlertDialog(
                                          shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(24.0),
                                            ),
                                          ),
                                          content: Builder(builder: (context) {
                                            return RequiredLoginDialog(
                                              loginButton: () {
                                              Get.toNamed(Routers.intro_page);
                                              },
                                            );
                                          }));
                                    });
                              }
                            else {
                              var result = await Get.toNamed(Routers.notification,arguments: fromHomeToNotify);
                              if (result == Routers.post) {
                                //TODO Tin tuc
                                // controller.indexPage.value = 2;
                              }
                            }
                          },
                          child: Obx(() => badges.Badge(
                            showBadge: !controller.isLogin.value ? false.obs.value : true.obs.value,
                            position: BadgePosition.topEnd(end: -6),
                            ignorePointer: true,
                            badgeContent: Obx(() => Text(
                                  '${countNotifyHome.value >= 10 ? "9+" : countNotifyHome.value}',
                              style: Themes.sfText8_500.copyWith(color: Colors.white),
                                )),
                            badgeAnimation: const BadgeAnimation.fade(),
                            child: iconCircleCommon(
                                ImagesRender.svgPicture(
                                  src: ImagePath.notification_home,
                                  width: ic_28,
                                  height: ic_28,
                                ),
                                padding: sizeTxt_6),
                          ),
                       )),
                      ],
                    )),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: InfinityRefresh(
                  padding: EdgeInsets.zero,
                  refreshController: controller.refreshControllerHome,
                  onRefresh: controller.onRefresherHome,
                  onLoading: controller.onLoadHome,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        //Show auto next view
                        Obx(() => InkWell(
                              onTap: () {
                                Get.toNamed(Routers.news_detail, arguments: controller.hotNew[controller.indexDot.value]);
                              },
                              child: CarouselSlider(
                                items: controller.imgList,
                                options: CarouselOptions(
                                    height: Get.width * 0.63,
                                    viewportFraction: 1,
                                    initialPage: 0,
                                    enlargeCenterPage: true,
                                    enableInfiniteScroll: true,
                                    autoPlay: true,
                                    autoPlayInterval:
                                        const Duration(seconds: 3),
                                    autoPlayAnimationDuration:
                                        const Duration(milliseconds: 800),
                                    autoPlayCurve: Curves.easeIn,
                                    enlargeFactor: 0.4,
                                    onPageChanged: (value, reason) {
                                      controller.indexDot.value = value;
                                    },
                                    scrollDirection: Axis.horizontal),
                              ),
                            )),
                        SizedBox(height: size8,),
                        Obx(() => Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: controller.imgList.mapIndexed(
                              (index, image) {
                                //these two lines//are changed
                                return Container(
                                    width: size8,
                                    height: size8,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: sizeTxt_2),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: controller.indexDot.value == index ? colorIcon : colorRoundBorder));
                              },
                            ).toList())),
                        SizedBox(
                          height: size8,
                        ),
                        serviceWidget,
                        SizedBox(
                          height: size24,
                        ),

                        ///TODO có lịch sẽ hiển thị và đã được đăng nhập sẽ hiển thị
                        Obx(() => Visibility(
                            visible: controller.isShowBook.value &&
                                controller.isLogin.value,
                            child: newBooking)),
                        SizedBox(
                          height: size24,
                        ),
                        toYou,
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get newBooking => SizedBox(
      width: Get.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Lich hẹn",
                style: Themes.sfText16_600,
              ),
              InkWell(
                  onTap: () {
                    controller.onRefresherHome();
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      Get.toNamed(Routers.historyBook);
                    });
                  },
                  child: Text("Xem tất cả",
                      style: Themes.sfText12_500.copyWith(color: colorPolicy))),
            ],
          ),
          SizedBox(height: size10),
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: controller.historyListHome.length,
              itemBuilder: (BuildContext context, int index) {
                return ItemHistoryBook(
                    context: context,
                    contentHistoryBook: controller.historyListHome[index],
                    onTapReport: () async {
                      var result = await Get.toNamed(Routers.createReport,
                          arguments: controller.historyListHome[index]);
                      if (result != null) {
                        successNotificationNoAppBar(
                            context: Get.context!,
                            message: 'Gửi phản ánh thành công');
                      }
                    },
                    onTap: (idBooking) async {
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (_) {
                            return AlertDialog(
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(24.0),
                                ),
                              ),
                              content: Builder(
                                builder: (context) {
                                  return ConfirmDialogBooking(
                                    content: "Xác nhận hủy lịch?",
                                    onTapClose: () {
                                      controller.reasonControllerEdt.clear();
                                      Get.back();
                                    },
                                    title: Column(
                                      children: [
                                        RichText(
                                            maxLines: 3,
                                            text: TextSpan(
                                                text:
                                                    'Bạn có đồng ý hủy lịch hẹn ',
                                                style: Themes.sfText14_400
                                                    .copyWith(
                                                        color: colorTextApp),
                                                children: [
                                                  TextSpan(
                                                      text: controller
                                                                  .historyListHome[index].packageType == 1 ? "có gói dịch vụ " : "có gói combo "),
                                                  TextSpan(
                                                      text: controller.historyListHome[index].packageType == 1
                                                          ? controller
                                                                  .historyListHome[
                                                                      index]
                                                                  .beautyBookingDetails
                                                                  ?.first
                                                                  .beautyServicePackage
                                                                  ?.name ??
                                                              ''
                                                          : controller
                                                                  .historyListHome[
                                                                      index]
                                                                  .beautyBookingDetails
                                                                  ?.first
                                                                  .beautyPackageCombo
                                                                  ?.name ??
                                                              "",
                                                      style: Themes.sfText14_700
                                                          .copyWith(
                                                              color:
                                                                  colorCancel)),
                                                  const TextSpan(
                                                      text: " không?"),
                                                ])),
                                        SizedBox(height: size6),
                                        FormFieldLabel(
                                            title: 'Lý do hủy',
                                            editingController:
                                                controller.reasonControllerEdt,
                                            maxLine: 1,
                                            errorText: RxnString()),
                                        SizedBox(height: size6),
                                      ],
                                    ),
                                    confirmText: "Hủy lịch",
                                    onTap: () {
                                      controller.reasonControllerEdt.clear();
                                      controller.cancelBooking(controller
                                              .historyListHome[index].id ??
                                          -1);
                                      Get.back();
                                    },
                                  );
                                },
                              ),
                            );
                          });
                    });
              }),
        ],
      ));

  Widget get serviceWidget => SizedBox(
        width: Get.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Dịch vụ",
              style: Themes.sfText16_600,
            ),
            SizedBox(height: size10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //TODO icon đặt lịch
                itemService(
                    icon: ImagesRender.svgPicture(
                        src: ImagePath.calendar, width: ic_28, height: ic_28),
                    label: 'Lịch hẹn',
                    function: () {
                      if (PreferenceUtils.getString(keyAccessToken) == null) {
                        showDialog(
                            context: Get.context!,
                            builder: (_) {
                              return AlertDialog(
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(24.0),
                                    ),
                                  ),
                                  content: Builder(builder: (context) {
                                    return RequiredLoginDialog(
                                      loginButton: () async {
                                         Get.toNamed(Routers.intro_page);
                                      },
                                    );
                                  }));
                            });
                      } else {
                        controller.onRefresherHome();
                        Get.toNamed(Routers.historyBook);
                      }
                    }),
                //TODO liệu trình
                itemService(
                    function: () {
                      Get.toNamed(Routers.develop_page,
                          arguments: Content(title: "Liệu trình"));
                    },
                    icon: ImagesRender.svgPicture(
                        src: ImagePath.ruler,
                      width: ic_28,
                      height: ic_28,),
                    label: "Liệu trình"),
                //TODO hỗ trợ
                itemService(
                  function: () {
                    Application.makePhoneCall("19006030");
                  },
                  icon: ImagesRender.svgPicture(
                      src: ImagePath.callReceived,   width: ic_28, height: ic_28,),
                  label: 'Hỗ trợ',
                ),
                //TODO icon phản ánh
                itemService(
                    icon: ImagesRender.svgPicture(
                        src: ImagePath.devMess, width: ic_28, height: ic_28,),
                    label: 'Phản ánh',
                    function: () => {
                          if (PreferenceUtils.getString(keyAccessToken) == null)
                            {
                              showDialog(
                                  context: Get.context!,
                                  builder: (_) {
                                    return AlertDialog(
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(24.0),
                                          ),
                                        ),
                                        content: Builder(builder: (context) {
                                          return RequiredLoginDialog(
                                              loginButton: () async {
                                           Get.toNamed(Routers.intro_page);
                                          });
                                        }));
                                  })
                            }
                          else
                            {Get.toNamed(Routers.report)}
                        }),
              ],
            ),
          ],
        ),
      );

  Widget itemService({
    String? label,
    Widget? icon,
    Function()? function,
  }) {
    return Column(
      children: [
        InkWell(onTap: function, child: iconCircleCommon(icon, padding: size16)),
        SizedBox(
          height: size8,
        ),
        Text(label ?? "", style: Themes.sfText12_500)
      ],
    );
  }

// TODO widget danh cho ban
  Widget get toYou => SizedBox(
        width: Get.width,
        child: Obx(() => Visibility(
              visible: controller.isShowHotBeauty.value,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Dịch vụ phổ biến',
                    style: Themes.sfText16_600,
                  ),
                  SizedBox(
                    height: sizeTxt_8,
                  ),
                  GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: controller.listHotBeauty.length,
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: size20,
                        mainAxisSpacing: 4,
                        childAspectRatio: getDeviceType() == DeviceType.Tablet ? 0.82 : 0.76,
                      ),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Get.toNamed(Routers.detailService,
                                arguments: controller.listHotBeauty[index]);
                          },
                          child: itemYour(
                              url: controller.listHotBeauty[index].imageStores != null &&
                                      controller.listHotBeauty[index].imageStores!.isNotEmpty
                                  ? "${controller.listHotBeauty[index].imageStores?.first.fileId ?? ''}"
                                  : "",
                              label:
                                  controller.listHotBeauty[index].name ?? ''),
                        );
                      })
                ],
              ),
            )),
      );

  Widget itemYour({String? url, required String? label}) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      ImagesRender.circleNetWorkImage(url ?? "",
          height: getDeviceType() == DeviceType.Tablet ? Get.width /3.7 : Get.width/3.8,
          width:Get.width),
      SizedBox(
        height: sizeTxt_8,
      ),
      Expanded(
          child: SizedBox(
        width: getDeviceType() == DeviceType.Tablet ? Get.width /3.7 : Get.width,
        child: Text(
          label.toString().capitalizeFirst ??'',
          style: Themes.sfText12_500,
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
        ),
      ))
    ]);
  }
}
