import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/home_vm/home_controller.dart';
import '../../../../core/repository/url.dart';
import '../../../../core/routes/routers.dart';
import '../../../../utils/session/enum_session.dart';
import '../../../../utils/widget/infinity.dart';
import '../../../../utils/widget/textfieldcommon.dart';
import 'item_package.dart';


class ServiceScreen extends GetWidget<HomeController> {
  ///TODO Dịch vụ
  const ServiceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Obx(
          () => bodyWidget(
            isLoading: controller.isSerVicesLoading,
            listDataCheck: controller.isSelectedServices.value == 0
                ? controller.listContent
                : controller.listContentCombo,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: sizeTxt_33,
                  width: size220,
                  decoration: BoxDecoration(
                    color: colorBackGroundSearch,
                    borderRadius: BorderRadius.circular(size8),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(
                          child: InkWell(
                              onTap: () {
                                controller.isSelectedServices.value = 0;
                                controller.searchServiceEdt.clear();
                              },
                              child: Obx(() => radioButtonCustom(
                                  isSelect:
                                      controller.isSelectedServices == 0.obs
                                          ? true
                                          : false,
                                  title: "Dịch vụ")))),
                      Expanded(
                          child: InkWell(
                              onTap: () {
                                controller.isSelectedServices.value = 1;
                                controller.searchServiceEdt.clear();
                              },
                              child: Obx(() => radioButtonCustom(
                                  isSelect:
                                      controller.isSelectedServices == 1.obs
                                          ? true
                                          : false,
                                  title: "Combo")))),
                    ],
                  ),
                ),
                SizedBox(
                  height: size12,
                ),

                /// từ textfiledcommon
                /// search textfield
                SearchFormField(
                  editingController: controller.searchServiceEdt,
                  onChangedFocus: (focus){
                      if(!focus){

                        controller.keySearch = controller.searchServiceEdt.text;
                        if (controller.isSelectedServices.value == 0) {
                          controller.callServicesApi();
                        }
                        if (controller.isSelectedServices.value == 1) {
                          controller.callComboApi();
                        }
                      }
                  },
                  onFieldSubmit: (_) {
                    controller.keySearch = controller.searchServiceEdt.text;
                    if (controller.isSelectedServices.value == 0) {
                      controller.callServicesApi();
                    }
                    if (controller.isSelectedServices.value == 1) {
                      controller.callComboApi();
                    }
                  },
                ),
                SizedBox(
                  height: size16,
                ),
                // GridviewBuilder
                Expanded(
                  child: InfinityRefresh(
                    refreshController: controller.refreshController,
                    onLoading: controller.onLoadingService,
                    onRefresh: controller.onRefreshService,
                    padding: EdgeInsets.zero,
                    child: Obx(
                      () => controller.isSelectedServices.value == 0
                          ? GridView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: getDeviceType() == DeviceType.Tablet ? 3: 2,
                                childAspectRatio:  getDeviceType() == DeviceType.Tablet ? 0.93 : 0.9,
                                crossAxisSpacing: size16,
                              ),
                              itemCount: controller.listContent.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return ItemPackage(
                                  onTap: () {
                                    Get.toNamed(Routers.packageService,
                                        arguments: controller.listContent[index]);
                                  },
                                  content:
                                      controller.listContent[index].name ?? '',
                                  link: controller.listContent[index]
                                          .imageStores!.isNotEmpty
                                      ? "${controller.listContent[index].imageStores?.first.fileId ?? ''}"
                                      : null,
                                );
                              })
                          : GridView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: getDeviceType() == DeviceType.Tablet ? 3: 2,
                                    childAspectRatio:  getDeviceType() == DeviceType.Tablet ? 0.93 : 0.9,
                                crossAxisSpacing: size16,
                              ),
                              itemCount: controller.listContentCombo.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return ItemPackageDetail(
                                  onTap: () {
                                    Get.toNamed(Routers.packageCombo,
                                        arguments:
                                            controller.listContentCombo[index]);
                                  },
                                  link: controller.listContentCombo[index]
                                          .imageStores!.isNotEmpty
                                      ? "${controller.listContentCombo[index].imageStores?.first.fileId ?? ''}"
                                      : null,
                                  content:
                                      controller.listContentCombo[index].name ??
                                          '',
                                  fee: controller.listContentCombo[index].price
                                          .toString(),
                                );
                              }),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      context: context,
    );
  }

  Widget radioButtonCustom({String? title, required bool isSelect}) {
    return isSelect
        ? Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(size8)),
            elevation: 2,
            margin: EdgeInsets.all(size2),
            child: Padding(
              padding: EdgeInsets.all(size2),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(size8),
                ),
                child: Text(
                  title ?? '',
                  style: Themes.sfText12_700,
                ),
              ),
            ),
          )
        : Center(
            child: Text(title ?? '',
                style: Themes.sfText12_700.copyWith(color: colorBtnBack)));
  }
}
