import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/application.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';

import '../../../../utils/path/color_path.dart';
import '../../../../utils/session/enum_session.dart';
import '../../../../utils/widget/images_render.dart';


class ItemPackage extends GetView {
  final String? link;
  final String? content;
  final String? fee;
  final Function? onTap;

  const ItemPackage({Key? key, this.link, this.content, this.fee,this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        if(onTap != null){
          onTap!();
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
        ImagesRender.circleNetWorkImage(link??"",height: getDeviceType() == DeviceType.Tablet ? Get.width / 3.9 : Get.width / 2.7,
            width: getDeviceType() == DeviceType.Tablet ? Get.width : Get.width,circular: size14),
        SizedBox(height: size6),
        Text(content.toString().capitalizeFirst!,style: Themes.sfText12_500),
        Visibility(
          visible: fee != null,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: size2),
                Text(fee??""),
              ],
            )),
      ]),
    );
  }
}
class ItemPackageDetail extends GetView {
  final String? link;
  final String? content;
  final String? fee;
  final Function? onTap;

  const ItemPackageDetail({Key? key, this.link, this.content, this.fee,this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        if(onTap != null){
          onTap!();
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
        ImagesRender.circleNetWorkImage(link??"",height: getDeviceType() == DeviceType.Tablet ? Get.width / 4 : Get.width / 2.7,
            width: getDeviceType() == DeviceType.Tablet ? Get.width : Get.width,circular: size14),
        SizedBox(height: size6),
        Text(content.toString().capitalizeFirst!,style: Themes.sfText12_500,
        maxLines: 1,
        overflow: TextOverflow.ellipsis),
        Visibility(
          visible: fee != null,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: size2),
                Text("${Application.currencyFormat.format(num.parse(fee.toString()))} đ",style: Themes.sfText12_500.copyWith(color: colorCurrency)),
              ],
            )),
      ]),
    );
  }
}
