import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/widget/dialog_common.dart';
import 'package:myhoangtuan/view_model/service_vm/detail_services_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/snack_bar.dart';
import '../../../../core/repository/url.dart';
import '../../../../core/routes/routers.dart';
import '../../../../core/themes/themes.dart';
import '../../../../utils/app/application.dart';
import '../../../../utils/path/color_path.dart';
import '../../../../utils/session/enum_session.dart';


class DetailPackageServiceScreen extends GetWidget<DetailServicesController> {
  const DetailPackageServiceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBackGroundSearch,
      body: Padding(
        padding: EdgeInsets.only(bottom: size24),
        child: Column(
          children: [
            SizedBox(
              height: getDeviceType() == DeviceType.Tablet ? Get.height/2.5 : size280,
              child: Stack(
                children: [
                  ImagesRender.netWorkImage(
                      controller.valueDetail!.imageStores!.isNotEmpty
                          ? "${controller.valueDetail?.imageStores?.first.fileId ?? ''}"
                          : null,
                      boxFit: BoxFit.cover,
                      width: Get.width,
                      height: Get.height),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: size16, vertical: size44),
                    child: InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Icon(
                        CupertinoIcons.back,
                        size: ic_20,
                        color: colorBtnBack,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(size16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      ImagesRender.svgPicture(
                          src: ImagePath.point_service, height: ic_10),
                      SizedBox(
                        width: size8,
                      ),
                      Text(
                        "${Application.currencyFormat.format(num.parse(controller.valueDetail?.unitPrice.toString() ?? "0"))} đ",
                        style:
                            Themes.sfText14_500.copyWith(color: colorCurrency),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: size10,
                  ),
                  Text(controller.valueDetail?.name?.toString().capitalizeFirst ?? "",
                      style: Themes.sfText18_500,
                      overflow: TextOverflow.ellipsis),
                ],
              ),
            ),
            Expanded(
                child: Padding(
              padding: EdgeInsets.symmetric(vertical: size8),
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(size16),
                width: Get.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Thông tin gói dịch vụ',
                      style: Themes.sfText14_500,
                    ),
                    SizedBox(
                      height: size16,
                    ),
                    Expanded(
                        child: SingleChildScrollView(
                            child: Text(
                                controller.valueDetail?.description ?? "",
                                style: Themes.sfText12_400))),
                  ],
                ),
              ),
            )),
            Container(
                color: Colors.white,
                padding: EdgeInsets.all(size16),
                child: buttonCommon(
                    title: 'Đặt lịch',
                    isOK: true.obs,
                    function: () async {
                      if(PreferenceUtils.getString(keyAccessToken) == null){
                        showDialog(
                            context: context,
                            builder: (_) {
                              return AlertDialog(
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(24.0),
                                    ),
                                  ),
                                  content: Builder(builder: (context) {
                                    return RequiredLoginDialog(
                                      loginButton: () async {
                                        Get.toNamed(Routers.intro_page);
                                      },
                                    );
                                  }));
                            });
                      }
                      else{
                        FocusScope.of(context).unfocus();
                        var result = await Get.toNamed(Routers.book, arguments: [
                          bookFromService,
                          controller.valueDetail?.id ?? -1
                        ]);
                        if(result == 'booked'){
                          successNotification(context: Get.context!,message: "Đăng ký lịch thành công");
                        }
                      }
                    })),
          ],
        ),
      ),
    );
  }
}
