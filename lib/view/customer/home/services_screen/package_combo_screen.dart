import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import '../../../../core/repository/url.dart';
import '../../../../core/routes/routers.dart';
import '../../../../utils/app/share_pref.dart';
import '../../../../utils/app/size_app.dart';
import '../../../../utils/session/enum_session.dart';
import '../../../../utils/widget/button_common.dart';
import '../../../../utils/widget/dialog_common.dart';
import '../../../../utils/widget/infinity.dart';
import '../../../../utils/widget/utils_common.dart';
import '../../../../view_model/service_vm/package_combo_controller.dart';
import 'item_package.dart';

class PackageComboScreen extends GetWidget<PackageComboController> {
  const PackageComboScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButton(title: controller.argument?.name?.toString().capitalizeFirst ?? ''),
      body: bodyWidget(
          isLoading: controller.isLoading,
          listDataCheck: controller.listContentCombo,
          child: Column(
            children: [
              // SearchFormField(
              //   onFieldSubmit: (value) {
              //     controller.keySearch = value;
              //     controller.getDetailCombo();
              //   },
              // ),
              SizedBox(
                height: size16,
              ),
              Expanded(
                child:  InfinityRefresh(
                  refreshController: controller.refreshController,
                  onLoading: controller.onLoadingService,
                  onRefresh: controller.onRefreshService,
                  padding: EdgeInsets.zero,
                  child: Obx(()=> GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: getDeviceType() == DeviceType.Tablet ? 1.1 : 0.96,
                        crossAxisSpacing: size16,
                      ),
                      itemCount: controller.listContentCombo.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return ItemPackage(
                          onTap: () {
                           // Get.toNamed(Routers.packageService, arguments: controller.listContentCombo[index]);
                          },
                          content: controller.listContentCombo[index].beautyServicePackage?.name ?? '',
                          link:  controller.listContentCombo[index].beautyServicePackage!.imageStores!.isNotEmpty ? "${controller.listContentCombo[index].beautyServicePackage?.imageStores?.first.fileId ??''}" : null,
                        );
                      }),
                  ),
                ),
              ),
            Obx(() =>  Container(
                  color: Colors.white,
                  child: buttonCommon(
                      title: 'Đặt lịch',
                      isOK: controller.isLoading.value == true ? false.obs: true.obs,
                      function: ()  {
                        if(PreferenceUtils.getString(keyAccessToken) == null){
                          showDialog(
                              context: context,
                              builder: (_) {
                                return AlertDialog(
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(24.0),
                                      ),
                                    ),
                                    content: Builder(builder: (context) {
                                      return RequiredLoginDialog(
                                        loginButton: () async {
                                        Get.toNamed(Routers.intro_page);
                                        },
                                      );
                                    }));
                              });
                        }
                        else{
                          Get.toNamed(Routers.book, arguments: [
                            bookFromCombo,
                            controller.argument?.id,
                          ]);
                        }
                      }))),
            ],
          )),
    );
  }
}
