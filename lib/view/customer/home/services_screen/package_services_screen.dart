import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/infinity.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/service_vm/package_service_controller.dart';

import '../../../../core/repository/url.dart';
import '../../../../core/routes/routers.dart';
import '../../../../utils/app/size_app.dart';
import '../../../../utils/session/enum_session.dart';
import '../../../../utils/widget/button_common.dart';
import '../../../../utils/widget/textfieldcommon.dart';
import 'item_package.dart';


class PackageServiceScreen extends GetWidget<PackageServiceController> {
  const PackageServiceScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
      context: context,
      child: Scaffold(
        appBar: appBarButton(title: controller.argument.name?.toString().capitalizeFirst ?? ''),
        body: bodyWidget(
          isLoading: controller.isLoading,
            listDataCheck: controller.listContent,
            child: Column(
          children: [
            SearchFormField(
              editingController: controller.textEditingController,
              onChangedFocus: (focus){
                if(!focus){
                  controller.isLoad = false;
                  controller.keySearch = controller.textEditingController.text.trim();
                  controller.getDetailPackService();
                }
              },
              onFieldSubmit: (value) {
                controller.isLoad = false;
                controller.keySearch = controller.textEditingController.text.trim();
                controller.getDetailPackService();
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
                  FocusManager.instance.primaryFocus!.unfocus();
                }
              },
            ),
          SizedBox(
            height: size16,
          ),
            Expanded(
              child:  InfinityRefresh(
                refreshController: controller.refreshController,
                onLoading: controller.onLoadingService,
                onRefresh: controller.onRefreshService,
                padding: EdgeInsets.zero,
                child: Obx(()=> GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: getDeviceType() == DeviceType.Tablet ? 1 : 0.8,
                      crossAxisSpacing: size16,
                    ),
                    itemCount: controller.listContent.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return ItemPackageDetail(
                        onTap: () {
                          controller.valueDetail = controller.listContent[index];
                          Get.toNamed(Routers.detailService, arguments: controller.valueDetail);
                        },
                        content: controller.listContent[index].name ?? '',
                        fee: controller.listContent[index].unitPrice?.toString()??"0",
                        link:  controller.listContent[index].imageStores!.isNotEmpty ? "${controller.listContent[index].imageStores?.first.fileId ??''}" : null,
                      );
                    }),
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
