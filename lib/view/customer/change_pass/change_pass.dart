import 'package:myhoangtuan/view_model/change_pass_vm/chane_pass_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import '../../../utils/path/color_path.dart';
import '../../../utils/widget/textfieldcommon.dart';

class ChangePassPage extends GetWidget<ChangePassController> {
  const ChangePassPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return gestureFocus(
        context: context,
        child: Scaffold(
          appBar: appBarButton(title: 'Đổi mật khẩu', isBack: true),
          body: bodyWidget(
            isLoading: controller.isLoading,
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: size12,
                      ),
                      SizedBox(
                        height: size12,
                      ),
                      FormFieldLabel(
                          editingController: controller.listEdtController.first,
                          title: 'Mật khẩu hiện tại',
                          obscureText: controller.hideOldPass,
                          isRequired: true,
                          suffixIcon: InkWell(
                            onTap: () {
                              controller.hideOldPass.value =
                                  !controller.hideOldPass.value;
                            },
                            child: Obx(() => controller.hideOldPass.value
                                ? Icon(
                                    CupertinoIcons.eye_slash_fill,
                                    size: ic_16,
                                    color: colorTextHint,
                                  )
                                : Icon(
                                    CupertinoIcons.eye_solid,
                                    size: ic_16,
                                    color: colorTextHint,
                                  )),
                          ),
                          onFocusChanged: (onFocus) {
                            if (!onFocus) {
                              controller.validateCheckOldPassword();
                              controller.validateButton();
                            }
                          },
                          errorText: controller.validateOldPass),
                      SizedBox(
                        height: size6,
                      ),
                      FormFieldLabel(
                          editingController: controller.listEdtController[1],
                          title: 'Mật khẩu',
                          obscureText: controller.hidePass,
                          isRequired: true,
                          suffixIcon: InkWell(
                            onTap: () {
                              controller.hidePass.value =
                                  !controller.hidePass.value;
                            },
                            child: Obx(() => controller.hidePass.value
                                ? Icon(
                                    CupertinoIcons.eye_slash_fill,
                                    size: ic_16,
                                    color: colorTextHint,
                                  )
                                : Icon(
                                    CupertinoIcons.eye_solid,
                                    size: ic_16,
                                    color: colorTextHint,
                                  )),
                          ),
                          onFocusChanged: (onFocus) {
                            if (!onFocus) {
                              controller.validateCheckPassword();
                              controller.checkConfirmPass();
                              controller.validateButton();
                            }
                          },
                          errorText: controller.validatePassword),
                      SizedBox(
                        height: size6,
                      ),
                      FormFieldLabel(
                          editingController: controller.listEdtController[2],
                          title: 'Xác nhận mật khẩu mới',
                          obscureText: controller.hidePassConfirm,
                          isRequired: true,
                          suffixIcon: InkWell(
                            onTap: () {
                              controller.hidePassConfirm.value =
                                  !controller.hidePassConfirm.value;
                            },
                            child: Obx(() => controller.hidePassConfirm.value
                                ? Icon(
                                    CupertinoIcons.eye_slash_fill,
                                    size: ic_16,
                                    color: colorTextHint,
                                  )
                                : Icon(
                                    CupertinoIcons.eye_solid,
                                    size: ic_16,
                                    color: colorTextHint,
                                  )),
                          ),
                          onFocusChanged: (onFocus) {
                            if (!onFocus) {
                              controller.checkConfirmPass();
                              controller.validateButton();
                            }
                          },
                          onChange: (value){
                            controller.checkConfirmPass();
                            controller.validateButton();
                          },
                          errorText: controller.validateConfirmPassword),
                    ],
                  ),
                ),
                buttonCommon(
                    isOK: controller.isOk,
                    title: 'Lưu thông tin',
                    function: () async {
                      FocusScope.of(context).unfocus();
                      controller.changePassword();
                    }),
              ],
            ),
          ),
        ));
  }
}
