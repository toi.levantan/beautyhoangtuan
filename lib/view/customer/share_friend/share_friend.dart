import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import 'package:myhoangtuan/view_model/share_friend_vm/share_friend_controller.dart';
import 'package:qr_flutter/qr_flutter.dart';
import '../../../core/themes/themes.dart';
import '../../../utils/path/const_key.dart';
import '../../../utils/path/image_paths.dart';

class ShareFriend extends GetWidget<ShareFriendController> {
  const ShareFriend({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarButton(title: 'Giới thiệu bạn bè'),
      body: bodyWidget(
          child: SizedBox(
        height: Get.height / 2,
        width: Get.width,
        child: Card(
          color: colorBottomBar,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: size16, vertical: size24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Đưa bạn bè quét mã này để tải và dùng ứng dụng MyHoangTuan',
                  textAlign: TextAlign.center,
                  style: Themes.sfText14_400,
                ),
                Expanded(
                    child: Center(
                        child: QrImage(
                            data: Platform.isIOS
                                ? PreferenceUtils.getString(urlShareIOS) ?? ""
                                : PreferenceUtils.getString(urlShareAndroid) ??
                                    "",
                            size: size200))),
                Row(
                  children: [
                    Expanded(child: buttonIcon(text: 'Lưu QR',images: ImagePath.download)),
                    SizedBox(width: size8,),
                    Expanded(child: buttonIcon(text: 'Chia sẻ',images: ImagePath.share_qr)),
                  ],
                ),
              ],
            ),
          ),
        ),
      )),
    );
  }
}
