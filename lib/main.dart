import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:myhoangtuan/firebase_options.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_phone_auth_handler/firebase_phone_auth_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:myhoangtuan/core/routes/pages.dart';
import 'package:myhoangtuan/core/routes/routers.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/core/translate/location_services.dart';
import 'package:myhoangtuan/utils/app/my_http_override.dart';
import 'package:myhoangtuan/utils/app/initial_binding.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';
import 'package:myhoangtuan/utils/app/util_logger.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/const_key.dart';
import 'model/fcm_body.dart';

// tạo channel cho android
const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'com.intlabs.hoantuanloyalty.urgent', 'High Importance Notifications',
    description: 'This channel is used for important notifications',
    importance: Importance.high,
    playSound: true,
    showBadge: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

RxString appBadgeSupportedCreate = 'Unknown'.obs;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  UtilLogger.log('logger', message.data);
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  RemoteNotification? notification = message.notification;
  FcmBody? fcmBody = FcmBody.fromJson(message.data);
  Map<String, dynamic> json = await jsonDecode(fcmBody.body ?? '');
  Body body = Body.fromJson(json);
  FlutterAppBadger.updateBadgeCount(body.totalNotSeen ?? 0);
  countNotifyHome.value = body.totalNotSeen ?? 0;
  countNotifyHomeEmployee.value = body.totalNotSeen ?? 0;
  flutterLocalNotificationsPlugin.show(
      notification.hashCode,
      notification?.title ?? '',
      body.content,
      payload: body.screenUrl,
      NotificationDetails(
          iOS: DarwinNotificationDetails(badgeNumber: body.totalNotSeen),
          android: AndroidNotificationDetails(
            channel.id,
            number: body.totalNotSeen,
            channel.name,
            channelDescription: channel.description,
            playSound: true,
            channelShowBadge: true,
            icon: '@mipmap/ic_launcher',
          )));
  // UtilLogger.log('message', body.totalNotSeen);
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
}

void initPlatformState() async {
  String appBadgeSupported;
  try {
    bool res = await FlutterAppBadger.isAppBadgeSupported();
    if (res) {
      appBadgeSupported = 'Supported';
    } else {
      appBadgeSupported = 'Not supported';
    }
  } on PlatformException {
    appBadgeSupported = 'Failed to get badge support.';
  }
  appBadgeSupportedCreate.value = appBadgeSupported;
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  //initMessage();
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true, sound: true, badge: true);
  await PreferenceUtils.init();
  FlutterAppBadger.removeBadge();
  initPlatformState();
  HttpOverrides.global = MyHttpOverrides();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((value) => runApp(const MyApp()));
}

class MyApp extends StatelessWidget implements WidgetsBindingObserver {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addObserver(this);
    return FirebasePhoneAuthProvider(
      child: ScreenUtilInit(
        designSize: const Size(430, 932),
        builder: ((context, child) {
          return GetMaterialApp(
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: const [Locale('vi', 'VN')],
            title: 'MyHoangTuan',
            transitionDuration: const Duration(milliseconds: 620),
            initialRoute: Routers.initialRoute,
            getPages: Pages.pages,
            debugShowCheckedModeBanner: false,
            initialBinding: InitialBinding(),
            // cung cap locale ứng dụng
            locale: LocalizationService.locale,
            // cung cấp fallbacklocale
            fallbackLocale: LocalizationService.fallbackLocale,
            // cung cấp bản dịch cho ứng dụng
            translations: LocalizationService(),
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              appBarTheme: const AppBarTheme(
                  systemOverlayStyle: SystemUiOverlayStyle.dark,
                  titleTextStyle: TextStyle(
                    color: colorTextApp,
                  )),
              primaryTextTheme: Theme.of(context).textTheme.apply(
                    bodyColor: colorTextApp,
                    fontFamily: Themes.sfProText,
                    displayColor: colorTextApp,
                  ),
              // This is the theme of your application.
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
            ),
          );
        }),
      ),
    );
  }

  @override
  void didChangeAccessibilityFeatures() {
    // TODO: implement didChangeAccessibilityFeatures
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState

    print('didChangeState $state');
  }

  @override
  void didChangeLocales(List<Locale>? locales) {
    // TODO: implement didChangeLocales
  }

  @override
  void didChangeMetrics() {
    // TODO: implement didChangeMetrics
  }

  @override
  void didChangePlatformBrightness() {
    // TODO: implement didChangePlatformBrightness
  }

  @override
  void didChangeTextScaleFactor() {
    // TODO: implement didChangeTextScaleFactor
  }

  @override
  void didHaveMemoryPressure() {
    // TODO: implement didHaveMemoryPressure
  }

  @override
  Future<bool> didPopRoute() {
    // TODO: implement didPopRoute
    throw UnimplementedError();
  }

  @override
  Future<bool> didPushRoute(String route) {
    // TODO: implement didPushRoute
    throw UnimplementedError();
  }

  @override
  Future<bool> didPushRouteInformation(RouteInformation routeInformation) {
    // TODO: implement didPushRouteInformation
    throw UnimplementedError();
  }
}
// void _removeBadge() {
//   FlutterAppBadger.removeBadge();
// }
// Future<void> initMessage() async {
//   // initialise the plugin. app_icon needs to be a added as a drawable resource t o the Android head project
//   const AndroidInitializationSettings initializationSettingsAndroid =
//   AndroidInitializationSettings('launch_background');
//   const DarwinInitializationSettings initializationSettingsDarwin =
//   DarwinInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
//   const InitializationSettings initializationSettings = InitializationSettings(
//       android: initializationSettingsAndroid,
//       iOS: initializationSettingsDarwin,);
//
//   flutterLocalNotificationsPlugin.initialize(initializationSettings,
//       onDidReceiveNotificationResponse: onDidReceiveNotificationResponse,
//       onDidReceiveBackgroundNotificationResponse:onDidReceiveNotificationResponse,
//   );
//   await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.createNotificationChannel(channel);
//   await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<IOSFlutterLocalNotificationsPlugin>()?.requestPermissions(
//     alert: true,
//     badge: true,
//     sound: true,
//   );
// }
// xử lý với IOS
// void onDidReceiveLocalNotification(int? id, String? title, String? body, String? payload) async {
//   final String? payload1 = '';
//   // display a dialog with the notification details, tap ok to go to another page
//   if (payload1 == Routers.historyBook) {
//     Get.toNamed(Routers.historyBook, arguments: 1);
//   }
//   else {
//     WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
//       Get.toNamed(payload1 != null && payload1!.isNotEmpty? payload1 : Routers.home);
//     });
//   }
// }

/*@pragma('vm:entry-point')
void notificationTapBackground(NotificationResponse notificationResponse) {
  // handle action
  print('vao xu ly background');
  if (notificationResponse.payload == Routers.historyBook) {
    Get.toNamed(
        notificationResponse.payload ?? Routers.notification, arguments: 1);
  }
  else {
    Get.toNamed(notificationResponse.payload != null && notificationResponse.payload!.isNotEmpty ? notificationResponse.payload! : Routers.home);
  }
} */
// xử lý chung
void onDidReceiveNotificationResponse(
    NotificationResponse notificationResponse) async {
     print('vao xu ly roi');

  if (notificationResponse.payload == Routers.historyBook) {
    Get.toNamed(Routers.historyBook, arguments: 1);
  }
  if (notificationResponse.payload == Routers.home) {}
  if (notificationResponse.payload == Routers.post) {
    Get.offNamedUntil(Routers.home, arguments: Routers.post, (route) => false);
  } else if (notificationResponse.payload != Routers.post &&
      notificationResponse.payload != Routers.home &&
      notificationResponse.payload == Routers.historyBook) {
    Get.toNamed(notificationResponse.payload!);
  }
}
