class ImagePath{
  static const path = 'assets/images/';
  static const renderErr = "${path}no_image_available.png";
  static const doctor_icon = "${path}doctor_icon.png";
  static const home = "${path}home.svg";
  static const unselectedHome = "${path}un_home.svg";
  static const qrCode = "${path}qr_code.svg";
  static const box = "${path}box.svg";
  static const unselectedBox = "${path}un_box.svg";
  static const unselectedShop = "${path}un_shop_add.svg";
  static const shop = "${path}shop_add.svg";
  static const unselectedUser = "${path}un_user.svg";
  static const user = "${path}user.svg";
  static const point = "${path}point.svg";
  static const notification = "${path}notification.svg";
  static const calendar = "${path}calendar.svg";
  static const callReceived = "${path}call-received.svg";
  static const devMess = "${path}device-message.svg";
  static const ruler = "${path}ruler.svg";
  static const starOutline = "${path}star_outline.svg";
  static const starFill = "${path}star_fill.svg";
  static const calendar_bold = "${path}calendar_bold.svg";
  static const imagePickDef = "${path}image_select.png";
  static const gallery = "${path}gallery.svg";
  static const camera_gallery = "${path}camera.svg";
  static const folder_gallery = "${path}folder_gallery.svg";
  static const err_icon = "${path}err_icon.svg";
  static const book_calendar = "${path}book_calendar.svg";
  static const hours_clock = "${path}hours_clock.svg";
  static const point_service = "${path}point_service.svg";
  static const clock = "${path}clock.svg";
  static const more_vertical = "${path}more_vertical.svg";
  static const dr_default = "${path}dr_default.svg";
  static const dot = "${path}dot.svg";
  static const music = "${path}music.svg";
  static const video = "${path}video.svg";
  static const image = "${path}images.svg";
  static const telephone_call = "${path}telephone_call.png";
  static const info = "${path}info.svg";
  static const close = "${path}close.svg";
  static const success = "${path}check.png";
  static const warning = "${path}warning.png";
  static const imageBlogNotFound = "${path}imageBlogNotFound.png";
  static const splash = "${path}splash.png";
  static const edit = "${path}edit.svg";
  static const calling = "${path}calling.svg";
  static const dieu_khoan = "${path}dieu_khoan.svg";
  static const phien_ban = "${path}phien_ban.svg";
  static const dia_chi = "${path}dia_chi.svg";
  static const share = "${path}share.png";
  static const loadingImage = "${path}loading.gif";
  static const account_bg = "${path}account_bg.jpg";
  static const logo_spa = "${path}logo_spa.png";
  static const lock = "${path}lock.svg";
  static const person = "${path}person.svg";
  static const checkbox = "${path}checkbox.svg";
  static const login_by_phone_text = "${path}login_by_phone_text.svg";
  static const secury = "${path}secury.png";
  static const change_password = "${path}password.png";
  static const global = "${path}global.png";
  static const alert = "${path}alert.png";
  static const book = "${path}book.png";
  static const policy = "${path}policy.png";
  static const settings = "${path}settings.png";
  static const gear = "${path}gear.png";
  static const regulation = "${path}regulation.png";
  static const guest_avatar = "${path}guest_avatar.png";
  static const back = "${path}back.png";
  static const toast = "${path}toast.svg";
  static const dialog_login = "${path}dialog_login.png";
  static const account_background = "${path}account_background.png";
  static const sercurity_lock = "${path}lock.png";
  static const home_background = "${path}home_background.png";
  static const notification_home = '${path}notification_home.svg';
  static const folder = '${path}folder.png';
  static const candidate = '${path}candidate.png';
  static const employee = '${path}employee.png';
  static const address = '${path}address.svg';
  static const birthday = '${path}birthday.svg';
  static const department = '${path}department.svg';
  static const email = '${path}email.svg';
  static const phone = '${path}phone.svg';
  static const phone_green = '${path}phone_green.svg';
  static const position_account = '${path}postion.svg';
  static const user_account = '${path}user_account.svg';
  static const share_qr = '${path}share_qr.svg';
  static const download = '${path}download.svg';
  static const uploading = '${path}uploading.gif';
  static const donbao = '${path}donbao.svg';
  static const phonebook = '${path}phonebook.svg';
  static const pre_book = '${path}pre_book.svg';
  static const pre_book_active = '${path}pre_book_active.svg';
  static const chat = '${path}chat.svg';
  static const chat_active = '${path}chat_active.svg';
  static const feeds = '${path}feeds.svg';
  static const feeds_active = '${path}feeds_active.svg';
  static const send = '${path}send.svg';
  static const send_active = '${path}send_active.svg';
  static const del_post = '${path}del_post.svg';
  static const edit_post = '${path}edit_post.svg';
  static const share_post = '${path}share_post.svg';
  static const share_new_feeds = '${path}share_new_feeds.svg';
  static const message_new_feeds = '${path}message_new_feeds.svg';
  static const chat_search = '${path}chat_search.svg';
}