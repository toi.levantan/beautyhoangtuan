import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:get/get.dart';

const String keyAccessToken = 'keyAccessToken';
//keyAccessFcm
const String keyAccessFcm = 'keyAccessFcm';
const String tokenFcm = 'tokenFcm';
//TODO sdt dung dang nhap
const String phoneAccess = 'phoneAccess';
const String userName = 'userName';
//TODO lay so dien thoai da dang nhap de hien thi
const String phoneData = 'phoneData';
//TODO mã khách hàng
const String customerCode = 'customerCode';
const int successful = 200;
const int create = 201;
const int expiredToken = 401;
const String organizationId = 'organizationId';
const String userKey = 'userKey';
const String userAccount = 'userAccount';
const String bookFromService = 'bookFromService';
const String bookFromCombo = 'bookFromCombo';
const String booked = 'booked';
const String avatar = 'avatar';
const String address = 'address';
const String urlShareIOS = 'urlShareIOS';
const String urlShareAndroid = 'urlShareAndroid';
const String urlRemoteIOS = 'urlRemoteIOS';
const String urlRemoteAndroid = 'urlRemoteAndroid';
const String pwdKey = 'pwdKey';
const String pwdKeyEmp = 'pwdKeyEmp';
const String email = 'email';
const String birthday = 'birthday';
/// check userName từ phía backend để check có là mình không
const String myUserName = 'myUserName';

/// key đã lưu tài khoản
const String rememberKey = 'rememberKey';
const String rememberKeyEmp = 'rememberKeyEmp';
final RxInt countNotifyHome = 0.obs;
final RxInt countNotifyHomeEmployee = 0.obs;
const String secretKey = "nL8yBfElsu1fj2PU+3IsNZjLYIbLBQ1T";
const String accessKey = "NuzMRgjMmyq4ZZgnR6vvQzUvqbKrTYdK";
const String idAccount = "idAccount";
const String department = "department";
const String position = "position";
//Data
const String userDataKey = "userDataKey";
const String fromHomeToNotify = "fromHomeToNotify";
const String fromHomeStaffToNotify = "fromHomeStaffToNotify";
//// key chat message

const publicKeyChat = 'a7e5ca03-7f33-4fa4-9153-53405340d443';
const defaultConnectChat = 'Inviv@123456q';


