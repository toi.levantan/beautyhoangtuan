import 'package:flutter/cupertino.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class InfinityRefresh extends StatefulWidget {
  const InfinityRefresh(
      {Key? key,
      required this.refreshController,
      required this.onRefresh,
        required this.onLoading,
      required this.child,
        this.padding})
      : super(key: key);
  final RefreshController refreshController;
  final Function()? onLoading;
  final Function()? onRefresh;
  final Widget child;
  final EdgeInsetsGeometry? padding;

  @override
  State<InfinityRefresh> createState() => _InfinityRefreshState();
}

class _InfinityRefreshState extends State<InfinityRefresh> {
  final GlobalKey _refresherKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
        key: _refresherKey,
        enablePullDown: true,
        enablePullUp: true,
        footer: CustomFooter(
          builder: ( context, status)
    {
      Widget body = Container();
        if (status == LoadStatus.loading) {
          body = const CupertinoActivityIndicator();
         }
            return SizedBox(
              height: 15.0,
              child: Center(child: body),
            );
          },
        ),
        controller: widget.refreshController,
        onLoading:  widget.onLoading,
        onRefresh:  widget.onRefresh,
        child: Padding(
          padding: widget.padding ??
              const EdgeInsets.symmetric(
                horizontal: 20,
              ),
          child: widget.child));
  }
}
Widget get buildLoading => const Center(
  child: CupertinoActivityIndicator(),
);
