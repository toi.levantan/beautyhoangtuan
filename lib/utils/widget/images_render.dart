import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../core/themes/themes.dart';
import '../app/size_app.dart';
import '../path/color_path.dart';
import '../path/image_paths.dart';

class ImagesRender {
  ImagesRender._();

  static Widget imagesAssets(
      {required String src,
         double? width,
         double? height,
        BoxFit? boxFit}) {
    return Image.asset(
      src,
      errorBuilder: (context, err, stackTrace) {
        return Image.asset(
          ImagePath.renderErr,
          alignment: Alignment.center,
          width: width,
          height: height,
          fit: boxFit ?? BoxFit.fill,
        );
      },
      width: width ?? 42,
      height: height ?? 42,
      fit: boxFit ?? BoxFit.fill,
    );
  }
  static Widget imagesAssetsCircle(
      {required String src,
        double? width,
        double? height,
        double? circular,
        BoxFit? boxFit}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(circular ?? 8),
      child: Image.asset(
        src,
        errorBuilder: (context, err, stackTrace) {
          return Image.asset(
            ImagePath.renderErr,
            alignment: Alignment.center,
            width: width,
            height: height,
            fit: boxFit ??  BoxFit.fill,
          );
        },
        width: width ?? 42,
        height: height ?? 42,
        fit: boxFit ?? BoxFit.fill,
      ),
    );
  }

  static Widget svgPicture(
      {required String src,
        double? width,
        double? height,
        BoxFit? boxFit}) {
    return SvgPicture.asset(
      src,
      fit: boxFit ?? BoxFit.cover,
      width: width,
      height: height,
      placeholderBuilder: (err){
       return ImagesRender.imagesAssets(src: ImagePath.renderErr, width: width ?? 20, height: height ??20,
            boxFit: BoxFit.fill);
      },
    );
  }

  static Widget netWorkImage(String? src,
      {double? width, double? height, BoxFit? boxFit}) {
    return CachedNetworkImage(
      width: width ?? 300,
      height: height ?? 100,
      imageUrl: src??'',
      fit: boxFit ?? BoxFit.cover,

      placeholder: (context,err){
        return ImagesRender.imagesAssetsCircle(src: ImagePath.loadingImage, width: width ?? 20, height: height ??20, boxFit: BoxFit.fill);
      },
      errorWidget: (context, url, error) => ImagesRender.imagesAssets(src: ImagePath.renderErr, width: width ?? 20, height: height ??20,
      boxFit: BoxFit.fill),
    );
  }
  static Widget circleNetWorkImage(String src,
      {double? width, double? height, BoxFit? boxFit, double? circular}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(circular ??sizeTxt_13),
      child: CachedNetworkImage(
        width: width ?? 300,
        height: height ?? 100,
        imageUrl: src,
        fit: boxFit ?? BoxFit.fill,
        placeholder: (context,err){
          return ImagesRender.imagesAssetsCircle(src: ImagePath.loadingImage, width: width ?? 20, height: height ??20, boxFit: boxFit ?? BoxFit.fill,circular: circular);
        },
        errorWidget: (context, url, error) => ImagesRender.imagesAssets(src: ImagePath.renderErr, width: width ?? 20, height: height ??20,boxFit:boxFit ?? BoxFit.cover),
      ),
    );
  }
}

//TODO  viền ảnh tròn
Widget iconCircleCommon(Widget? child, {double? padding}) {
  return Container(
      padding: EdgeInsets.all(padding ?? size16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        border: Border.all(color: colorBackGroundAppBar, width: 0.5),
      ),
      child: child);
}
Widget ImageNotFoundBlogWidget(){
  return Center(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ImagesRender.imagesAssets(src: ImagePath.imageBlogNotFound,height: ic_166
        ,width: ic_250),
        SizedBox(height: size8,),
        Column(children: [
          Text("Không tìm thấy bài viết nào",style: Themes.sfTextHint14_500,),
          Text("Hãy thử tìm một từ khóa đơn giản hơn",style :Themes.sfTextHint14_500,),
        ],),
      ],
    ),
  );
}