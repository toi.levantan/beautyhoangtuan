import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import '../path/color_path.dart';



errNotification({required BuildContext context, String? description, String? message}) {
  return ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(SnackBar(
       elevation: 0,
       margin: EdgeInsets.only(bottom:  MediaQuery.of(context).size.height - 200),
       behavior: SnackBarBehavior.floating,
      duration: const Duration(seconds: 2),
       backgroundColor: Colors.transparent,
      content: Container(
        decoration:  BoxDecoration(color: colorBackGrErr,borderRadius: BorderRadius.circular(size16)),
        height: size90,
        width: Get.width,
        child: Row(
          children: [
            SizedBox(
                width: size56,
                child: Center(child: ImagesRender.svgPicture(src: ImagePath.err_icon,height: ic_24,width: ic_24))),
            Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(description??'Thông báo',style: Themes.sfText12_700.copyWith(color: colorTitleNotify),),
                const SizedBox(height: 4,),
                Text(message??'Có lỗi xảy ra',style: Themes.sfText12_400.copyWith(color: colorMessNotify),)
              ],
            )),
            SizedBox(
                width: size44,
                child: Center(child: InkWell(
                    onTap: (){
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                    child: ImagesRender.svgPicture(src: ImagePath.close,height: ic_12,width: ic_12)))),
          ],
        ),
      )));
}
errNotificationNoAppBar({required BuildContext context, String? description, String? message}) {
  return ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(SnackBar(
       elevation: 0,
       margin: EdgeInsets.only(bottom:  MediaQuery.of(context).size.height - 300.h),
       behavior: SnackBarBehavior.floating,
      duration: const Duration(seconds: 2),
       backgroundColor: Colors.transparent,
      content: Container(
        decoration:  BoxDecoration(color: colorBackGrErr,borderRadius: BorderRadius.circular(size16)),
        height: size90,
        width: size280,
        child: Row(
          children: [
            SizedBox(
                width: size56,
                child: Center(child: ImagesRender.svgPicture(src: ImagePath.err_icon,height: ic_24,width: ic_24))),
            Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(description??'Thông báo',style: Themes.sfText12_700.copyWith(color: colorTitleNotify),),
                const SizedBox(height: 4,),
                Text(message??'Có lỗi xảy ra',style: Themes.sfText12_400.copyWith(color: colorMessNotify),)
              ],
            )),
            SizedBox(
                width: size44,
                child: Center(child: InkWell(
                    onTap: (){
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                    child: ImagesRender.svgPicture(src: ImagePath.close,height: ic_12,width: ic_12)))),
          ],
        ),
      )));
}
successNotification({required BuildContext context, String? description, String? message}) {
  return ScaffoldMessenger.of(context)..hideCurrentMaterialBanner()..showSnackBar(SnackBar(
        elevation: 0,
        margin: EdgeInsets.only(bottom:  MediaQuery.of(context).size.height - 200),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.transparent,
        duration: const Duration(seconds: 2),
        content: Container(
          decoration:  BoxDecoration(color: colorBackGrSuccess,borderRadius: BorderRadius.circular(size16)),
          height: size90,
          width: size280,
          child: Row(
            children: [
              SizedBox(
                  width: size56,
                  child: Center(child: ImagesRender.imagesAssetsCircle(src: ImagePath.success,height: ic_24,width: ic_24,circular: 100))),
              Expanded(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(description??'Thông báo',style: Themes.sfText12_700.copyWith(color: colorTitleNotify),),
                  const SizedBox(height: 4,),
                  Text(message??'Thành công',style: Themes.sfText12_400.copyWith(color: colorMessNotify),)
                ],
              )),
              SizedBox(
                  width: size44,
                  child: Center(child: InkWell(
                      onTap: (){
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                      child: ImagesRender.svgPicture(src: ImagePath.close,height: ic_12,width: ic_12)))),
            ],
          ),
        ))
  );
}
successNotificationNoAppBar({required BuildContext context, String? description, String? message}) {
  return ScaffoldMessenger.of(context)..hideCurrentMaterialBanner()..showSnackBar(
    SnackBar(
        elevation: 0,
        margin: EdgeInsets.only(bottom:  MediaQuery.of(context).size.height - 300.h),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.transparent,
        duration: const Duration(seconds: 2),
        content: Container(
          decoration:  BoxDecoration(color: colorBackGrSuccess,borderRadius: BorderRadius.circular(size16)),
          height: size90,
          width: size280,
          child: Row(
            children: [
              SizedBox(
                  width: size56,
                  child: Center(child: ImagesRender.imagesAssetsCircle(src: ImagePath.success,height: ic_24,width: ic_24,circular: 100))),
              Expanded(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(description??'Thông báo',style: Themes.sfText12_700.copyWith(color: colorTitleNotify),),
                  const SizedBox(height: 4,),
                  Text(message??'Thành công',style: Themes.sfText12_400.copyWith(color: colorMessNotify),)
                ],
              )),
              SizedBox(
                  width: size44,
                  child: Center(child: InkWell(
                      onTap: (){
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      },
                      child: ImagesRender.svgPicture(src: ImagePath.close,height: ic_12,width: ic_12)))),
            ],
          ),
        ))
  );
}
warningNotification({required BuildContext context, String? description, String? message}) {
  return ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(SnackBar(
      elevation: 0,
      margin: EdgeInsets.only(bottom:  MediaQuery.of(context).size.height - 200),
      duration: const Duration(seconds: 2),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Colors.transparent,
        content: Container(
          decoration:  BoxDecoration(color: colorBackGrWarning,borderRadius: BorderRadius.circular(size16)),
          height: size90,
          width: size280,
          child: Row(
            children: [
              SizedBox(
                width: size56,
                  child: Center(child: ImagesRender.imagesAssetsCircle(src: ImagePath.warning,height: ic_24,width: ic_24))),
              Expanded(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(description??'Thông báo',style: Themes.sfText12_700.copyWith(color: colorTitleNotify),),
                  const SizedBox(height: 4,),
                  Text(message??'Cảnh báo',style: Themes.sfText12_400.copyWith(color: colorMessNotify),)
                ],
              )),
              SizedBox(
                  width: size44,
                  child: Center(child: InkWell(
                    onTap: (){
                     ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    },
                      child: ImagesRender.svgPicture(src: ImagePath.close,height: ic_12,width: ic_12)))),
            ],
          ),
        )));
}
