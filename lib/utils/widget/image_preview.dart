import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';

import '../app/size_app.dart';

class ImagePreview extends GetWidget {
  final String? imgSrc;
  const ImagePreview({this.imgSrc, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: [
            Positioned(right: ic_8,top: ic_8, child: InkWell(
                onTap: (){
                  Get.back();
                },
                child: Icon(CupertinoIcons.clear_thick_circled,size: ic_24,color: Colors.red,))),
               Center(child: ImagesRender.netWorkImage(imgSrc ??'',boxFit: BoxFit.fitWidth,height: Get.width,width: Get.height)),
          ],
        ),
      ),
    );
  }
}
