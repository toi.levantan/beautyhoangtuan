import 'package:get/get.dart';

import '../../model/new_feeds/new_feed_dto.dart';
import '../../view/customer/preview_media/preview_media.dart';
import '../path/color_path.dart';

showBottomSheetImage({ Rx<int>? index,List<BeautyAttachments>? beautyAttachments}) {
  return Get.bottomSheet(PreViewMedia(beautyAttachments: beautyAttachments, index: index,),
      backgroundColor: colorRoundBorder,
      isScrollControlled: true);
}