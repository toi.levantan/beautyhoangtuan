import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/widget/show_bottom_img_network.dart';

import '../../core/routes/routers.dart';
import '../../core/themes/themes.dart';
import '../../model/new_feeds/new_feed_dto.dart';
import '../app/application.dart';
import '../app/size_app.dart';
import '../path/color_path.dart';
import 'images_render.dart';

///TODO showLayoutGridFile
Widget layoutGridFile({RxList<ContentFeed>? newFeeds,required int index}) {
  return Obx(() => newFeeds?[index].beautyAttachments?.length == 1
      ? SizedBox(
      child: Stack(alignment: Alignment.topRight, children: [
        Stack(
          alignment: Alignment.center,
          children: [
            InkWell(
              onTap: (){
                showBottomSheetImage(beautyAttachments : newFeeds?[index].beautyAttachments,index: 0.obs);
              },
              child: ImagesRender.netWorkImage(
                Application.imageNetworkPath(
                    filedId: newFeeds?[index].beautyAttachments?.first.thumbnail),
                boxFit: BoxFit.cover,
                width: Get.width,
                height: Get.height / 2,
              ),
            ),
            Visibility(visible: Application.isTypeVideoFromUrl(
                type: newFeeds?[index].beautyAttachments
                    ?.first.fileType).value,
                child: Icon(
                  CupertinoIcons.play_circle,
                  color: Colors.white,
                  size: ic_30,
                ))
          ],
        )
      ]))
      : newFeeds?[index].beautyAttachments?.length == 2
      ? SizedBox(
      width: Get.width,
      height: Get.width,
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        itemCount:
        newFeeds?[index].beautyAttachments?.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, indexAttachments) {
          return InkWell(
            onTap: () {},
            child: SizedBox(
              child: Stack(
                alignment: Alignment.topRight,
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: (){
                          showBottomSheetImage(beautyAttachments : newFeeds?[index].beautyAttachments,index: indexAttachments.obs);
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            ImagesRender.netWorkImage(
                              Application.imageNetworkPath(
                                  filedId: newFeeds?[index].beautyAttachments?[indexAttachments].thumbnail),
                              width: index == 0
                                  ? Get.width / 2 - size14
                                  : Get.width / 2 - size18,
                              height: Get.width - size33,
                              boxFit: BoxFit.cover,
                            ),
                            Visibility(visible: Application.isTypeVideoFromUrl(
                                type: newFeeds?[index].beautyAttachments?[indexAttachments].fileType).value,
                                child: Icon(
                                  CupertinoIcons.play_circle,
                                  color: Colors.white,
                                  size: ic_30,
                                ))
                          ],
                        ),
                      ),
                      Visibility(
                          visible: indexAttachments == 0,
                          child: SizedBox(width: size4)),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ))
     ///TODO = 3
      : newFeeds?[index].beautyAttachments?.length == 3
      ? SizedBox(
      width: Get.width,
      height: Get.width,
      child: Row(
          children: [
            Expanded(
                child: InkWell(
                  onTap: (){
                    showBottomSheetImage(beautyAttachments : newFeeds?[index].beautyAttachments,index: 0.obs);
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      ImagesRender.netWorkImage(
                        Application.imageNetworkPath(
                            filedId: newFeeds?[index].beautyAttachments?.first.thumbnail),
                        height: Get.width,
                        width: Get.width,
                        boxFit: BoxFit.cover,
                      ),
                      Visibility(
                          visible: Application.isTypeVideoFromUrl(type: 1)
                              .value,
                          child: Icon(
                            CupertinoIcons.play_circle,
                            color: Colors.white,
                            size: ic_30,
                          ))
                    ],
                  ),
                )),
            SizedBox(width: size4),
            Expanded(
                child: ListView.builder(
                    itemCount: newFeeds?[index].beautyAttachments?.length,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, indexAttachment) {
                      return Visibility(
                          visible: indexAttachment != 0,
                          child: InkWell(
                              onTap: () {
                                showBottomSheetImage(beautyAttachments : newFeeds?[index].beautyAttachments,index: indexAttachment.obs);
                              },
                              child: Column(
                                  children: [
                                    Stack(alignment: Alignment.center, children: [
                                      ImagesRender.netWorkImage(
                                        Application.imageNetworkPath(filedId: newFeeds?[index].beautyAttachments?[indexAttachment].thumbnail ?? ''),
                                        width: Get.width,
                                        height: Get.width /2,
                                        boxFit: BoxFit.cover,
                                      ),
                                      Visibility(
                                          visible: Application.isTypeVideoFromUrl(type: newFeeds?[index].beautyAttachments?[indexAttachment].fileType).value,
                                          child: Icon(
                                            CupertinoIcons.play_circle,
                                            color: Colors.white,
                                            size: ic_30,
                                          ))]),
                                    SizedBox(height: size4)
                                  ])));}))])) :  newFeeds?[index].beautyAttachments?.length == 4? SizedBox(
      width: Get.width,
      height: Get.width,
      child: Row(children: [
        Expanded(
            flex : 3,
            child: InkWell(
              onTap: (){
                showBottomSheetImage(beautyAttachments : newFeeds?[index].beautyAttachments,index: 0.obs);
              },
              child: Stack(
                  alignment: Alignment.center,
                  children: [
                    ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: newFeeds?[index].beautyAttachments?.first.thumbnail),
                        height: Get.width,
                        boxFit: BoxFit.cover),
                    Visibility(
                        visible: Application.isTypeVideoFromUrl(type: newFeeds?[index].beautyAttachments?.first.fileType).value,
                        child: Icon(
                          CupertinoIcons.play_circle,
                          color: Colors.white,
                          size: ic_30))]),
            )),
        SizedBox(width: size4),
        Expanded(
          flex : 2,
          child: ListView.builder(
            itemCount: newFeeds?[index].beautyAttachments?.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, indexAttachment) {
              return Visibility(
                visible: indexAttachment!= 0,
                child: InkWell(
                  onTap: () async{
                    showBottomSheetImage(beautyAttachments: newFeeds[index].beautyAttachments,index: indexAttachment.obs);
                  },
                  child: Column(
                    children: [
                      Stack(
                          alignment: Alignment.center, children: [
                        ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: newFeeds?[index].beautyAttachments?[indexAttachment].thumbnail ?? ''),
                            width: Get.width,
                            height: Get.width / 3,
                            boxFit: BoxFit.cover),
                        Visibility(
                            visible: Application.isTypeVideoFromUrl(type: newFeeds?[index].beautyAttachments?[indexAttachment].fileType).value,
                            child: Icon(
                              CupertinoIcons.play_circle,
                              color: Colors.white,
                              size: ic_30,
                            )),
                      ]),
                      Visibility(
                          visible: indexAttachment != newFeeds![index].beautyAttachments!.length -1,
                          child: SizedBox(height: size4))
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ])) : newFeeds![index].beautyAttachments!.length > 4 ? SizedBox(
      width: Get.width,
      height: Get.width,
      child: Row(children: [
        Expanded(
            flex : 3,
            child: InkWell(
              onTap: (){
                showBottomSheetImage(beautyAttachments: newFeeds[index].beautyAttachments,index: 0.obs);
              },
              child: Stack(
                  alignment: Alignment.center,
                  children: [
                    ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: newFeeds[index].beautyAttachments?.first.thumbnail),
                        height: Get.width,
                        boxFit: BoxFit.cover),
                    Visibility(
                        visible: Application.isTypeVideoFromUrl(type: newFeeds[index].beautyAttachments?.first.fileType).value,
                        child: Icon(
                          CupertinoIcons.play_circle,
                          color: Colors.white,
                          size: ic_30,
                        )),
                  ]),
            )),
        SizedBox(width: size4),
        Expanded(
          flex : 2,
          child: ListView.builder(
            itemCount: newFeeds[index].beautyAttachments?.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, indexAttachment) {
              return Visibility(
                visible: indexAttachment!= 0,
                child: InkWell(
                  onTap: () async{
                    if(indexAttachment == newFeeds[index].beautyAttachments!.length -1){
                      Get.toNamed(Routers.commit_feed);
                      return;
                    }
                    showBottomSheetImage(beautyAttachments: newFeeds[index].beautyAttachments,index: indexAttachment.obs);
                  },
                  child: Column(
                    children: [
                      Stack(
                          alignment: Alignment.center, children: [
                        ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: newFeeds[index].beautyAttachments?[indexAttachment].thumbnail ?? ''),
                            width: Get.width,
                            height: Get.width / 4,
                            boxFit: BoxFit.cover),
                        Visibility(
                            visible: Application.isTypeVideoFromUrl(type: newFeeds[index].beautyAttachments?[indexAttachment].fileType).value,
                            child: Icon(
                              CupertinoIcons.play_circle,
                              color: Colors.white,
                              size: ic_30,
                            )),
                        Visibility(
                          visible: indexAttachment == newFeeds[index].beautyAttachments!.length -1,
                          child: Container(
                            color: colorBackGrErr.withOpacity(0.2),
                            child: Text('+${newFeeds[index].beautyAttachments!.length - 4}',style: Themes.sfText20_500.copyWith(color: Colors.white),),
                          ),
                        ),
                      ]),
                      Visibility(
                          visible: indexAttachment != newFeeds[index].beautyAttachments!.length -1,
                          child: SizedBox(height: size4))
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ])): Container());
}
Widget layoutGridFileDetail({required RxList<BeautyAttachments>? beautyAttachments}) {
  return Obx(() => beautyAttachments?.length == 1
      ? SizedBox(
      child: Stack(alignment: Alignment.topRight, children: [
        InkWell(
          onTap: (){
            showBottomSheetImage(beautyAttachments : beautyAttachments,index: 0.obs);
          },
          child: Stack(
            alignment: Alignment.center,
            children: [
              ImagesRender.netWorkImage(
                Application.imageNetworkPath(
                    filedId: beautyAttachments?.first.thumbnail),
                boxFit: BoxFit.cover,
                width: Get.width,
                height: Get.height / 2,
              ),
              Visibility(visible: Application.isTypeVideoFromUrl(
                  type: beautyAttachments?.first.fileType).value,
                  child: Icon(
                    CupertinoIcons.play_circle,
                    color: Colors.white,
                    size: ic_30,
                  ))
            ],
          ),
        )
      ]))
      : beautyAttachments?.length == 2
      ? SizedBox(
      width: Get.width,
      height: Get.width,
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        itemCount: beautyAttachments?.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, indexAttachments) {
          return InkWell(
            onTap: () {},
            child: SizedBox(
              child: Stack(
                alignment: Alignment.topRight,
                children: [
                  Row(
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          InkWell(
                            onTap: (){
                              showBottomSheetImage(beautyAttachments : beautyAttachments,index: indexAttachments.obs);
                            },
                            child: ImagesRender.netWorkImage(
                              Application.imageNetworkPath(
                                  filedId: beautyAttachments?[indexAttachments].thumbnail),
                              width: indexAttachments == 0
                                  ? Get.width / 2 - size14
                                  : Get.width / 2 - size18,
                              height: Get.width - size33,
                              boxFit: BoxFit.cover,
                            ),
                          ),
                          Visibility(visible: Application.isTypeVideoFromUrl(
                              type: beautyAttachments?[indexAttachments].fileType).value,
                              child: Icon(
                                CupertinoIcons.play_circle,
                                color: Colors.white,
                                size: ic_30,
                              ))
                        ],
                      ),
                      Visibility(
                          visible: indexAttachments == 0,
                          child: SizedBox(width: size4)),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ))
     ///TODO = 3
      : beautyAttachments?.length == 3
      ? SizedBox(
      width: Get.width,
      height: Get.width,
      child: Row(
          children: [
            Expanded(
                child: InkWell(
                  onTap: (){
                    showBottomSheetImage(beautyAttachments : beautyAttachments,index: 0.obs);
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      ImagesRender.netWorkImage(
                        Application.imageNetworkPath(
                            filedId: beautyAttachments?.first.thumbnail),
                        height: Get.width,
                        width: Get.width,
                        boxFit: BoxFit.cover,
                      ),
                      Visibility(
                          visible: Application.isTypeVideoFromUrl(type: 1)
                              .value,
                          child: Icon(
                            CupertinoIcons.play_circle,
                            color: Colors.white,
                            size: ic_30,
                          ))
                    ],
                  ),
                )),
            SizedBox(width: size4),
            Expanded(
                child: ListView.builder(
                    itemCount: beautyAttachments?.length,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, indexAttachment) {
                      return Visibility(
                          visible: indexAttachment != 0,
                          child: InkWell(
                              onTap: () {
                                showBottomSheetImage(beautyAttachments : beautyAttachments,index: indexAttachment.obs);
                              },
                              child: Column(
                                  children: [
                                    Stack(alignment: Alignment.center, children: [
                                      ImagesRender.netWorkImage(
                                        Application.imageNetworkPath(filedId: beautyAttachments?[indexAttachment].thumbnail ?? ''),
                                        width: Get.width,
                                        height: Get.width /2,
                                        boxFit: BoxFit.cover,
                                      ),
                                      Visibility(
                                          visible: Application.isTypeVideoFromUrl(type: beautyAttachments?[indexAttachment].fileType).value,
                                          child: Icon(
                                            CupertinoIcons.play_circle,
                                            color: Colors.white,
                                            size: ic_30,
                                          ))]),
                                    SizedBox(height: size4)
                                  ])));}))])) :  beautyAttachments?.length == 4? SizedBox(
      width: Get.width,
      height: Get.width,
      child: Row(children: [
        Expanded(
            flex : 3,
            child: InkWell(
              onTap: (){
                showBottomSheetImage(beautyAttachments : beautyAttachments,index: 0.obs);
              },
              child: Stack(
                  alignment: Alignment.center,
                  children: [
                    ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: beautyAttachments?.first.thumbnail),
                        height: Get.width,
                        boxFit: BoxFit.cover),
                    Visibility(
                        visible: Application.isTypeVideoFromUrl(type: beautyAttachments?.first.fileType).value,
                        child: Icon(
                          CupertinoIcons.play_circle,
                          color: Colors.white,
                          size: ic_30))]),
            )),
        SizedBox(width: size4),
        Expanded(
          flex : 2,
          child: ListView.builder(
            itemCount: beautyAttachments?.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, indexAttachment) {
              return Visibility(
                visible: indexAttachment!= 0,
                child: InkWell(
                  onTap: () async{
                    showBottomSheetImage(beautyAttachments:beautyAttachments,index: indexAttachment.obs);
                  },
                  child: Column(
                    children: [
                      Stack(
                          alignment: Alignment.center, children: [
                        ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: beautyAttachments?[indexAttachment].thumbnail ?? ''),
                            width: Get.width,
                            height: Get.width / 3,
                            boxFit: BoxFit.cover),
                        Visibility(
                            visible: Application.isTypeVideoFromUrl(type: beautyAttachments?[indexAttachment].fileType).value,
                            child: Icon(
                              CupertinoIcons.play_circle,
                              color: Colors.white,
                              size: ic_30,
                            )),
                      ]),
                      Visibility(
                          visible: indexAttachment != beautyAttachments!.length -1,
                          child: SizedBox(height: size4))
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ])) : beautyAttachments!.length > 4 ? SizedBox(
      width: Get.width,
      height: Get.width,
      child: Row(children: [
        GridView.builder(
            itemCount: beautyAttachments.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, indexAttachment) {
              return Visibility(
                visible: indexAttachment!= 0,
                child: InkWell(
                  onTap: () async{
                    if(indexAttachment == beautyAttachments.length -1){
                      Get.toNamed(Routers.commit_feed);
                      return;
                    }
                    showBottomSheetImage(beautyAttachments: beautyAttachments,index: indexAttachment.obs);
                  },
                  child: Column(
                    children: [
                      Stack(
                          alignment: Alignment.center, children: [
                        ImagesRender.netWorkImage(Application.imageNetworkPath(filedId: beautyAttachments[indexAttachment].thumbnail ?? ''),
                            width: Get.width,
                            height: Get.width / 4,
                            boxFit: BoxFit.cover),
                        Visibility(
                            visible: Application.isTypeVideoFromUrl(type: beautyAttachments[indexAttachment].fileType).value,
                            child: Icon(
                              CupertinoIcons.play_circle,
                              color: Colors.white,
                              size: ic_30,
                            )),
                        Visibility(
                          visible: indexAttachment == beautyAttachments.length -1,
                          child: Container(
                            color: colorBackGrErr.withOpacity(0.2),
                            child: Text('+${beautyAttachments.length - 4}',style: Themes.sfText20_500.copyWith(color: Colors.white),),
                          ),
                        ),
                      ]),
                      Visibility(
                          visible: indexAttachment != beautyAttachments.length -1,
                          child: SizedBox(height: size4))
                    ],
                  ),
                ),
              );
            }, gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          ),
      ])): Container());
}