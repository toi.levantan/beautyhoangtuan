import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/utils_common.dart';
import '../../core/themes/themes.dart';
import '../path/color_path.dart';

Widget buttonCommon(
    {required String? title,
    Function()? function,
    Color? color,
    Rx<bool?>? isOK,
    Color? titleColor}) {
  isOK ?? true;
  return Obx(() => MaterialButton(
      height: sizeTxt_44,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      textColor: titleColor ?? Colors.white,

      ///T?0DO is Active : OK ,
      /// isValue null thì set là false
      /// color truyền vào không có mặc địnH LÀ COLORPOLICY
      /// !isOk is colorButton isBorderTextFiled
      color: isOK?.value ?? RxnBool(false) == RxnBool(true)
          ? color ?? colorIcon
          : colorBorderTextField,
      onPressed: () {
        FocusScope.of(Get.context!).unfocus();
        if (isOK != null && isOK.value == false) {
          return;
        }
        function?.call();
      },
      child: Center(
        child: Text(title ?? "",
            style: Themes.sfText14_500
                .copyWith(color: titleColor ?? Colors.white)),
      )));
}

Widget buttonLogout(
    {Function? function,
    Color? colorButton,
    String? title,
    Color? colorText,
    double? height}) {
  return Expanded(
    child: InkWell(
      onTap: () {
        if (function != null) {
          function.call();
        }
      },
      child: Container(
        height: height ?? size40,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(size8), color: colorButton),
        child: Center(
            child: Text(
          title ?? '',
          style: Themes.sfText12_500.copyWith(color: colorText ?? Colors.white),
        )),
      ),
    ),
  );
}

Widget buttonIcon({String? images, String? text,Function? function}) {
  return InkWell(
    onTap: (){
      if(function != null){
        function.call();
      }
    },
    child: Container(
      padding: EdgeInsets.all(size12),
      alignment: Alignment.center,
      decoration:  BoxDecoration(
        color: colorIcon,
        borderRadius: BorderRadius.circular(size6),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        ImagesRender.svgPicture(src: images ??''),
        SizedBox(width: size12),
        Text(text??'',style: Themes.sfText14_400.copyWith(color: Colors.white))
      ],),
    ),
  );
}

AppBar appBarButton(
    {required String title,
    List<Widget>? actions,
    Color? colorBackgroundAppbar,
    bool? showBottomBorder,
    bool? isBack,
    Function? leading,
    TabBar? tabBar,
    bool? isCenterTitle}) {
  return AppBar(
    centerTitle: isCenterTitle ?? true,
    title: Text(
      title,
      style: Themes.sfText16_600,
    ),
    bottom: tabBar ??
        PreferredSize(
          preferredSize: const Size.fromHeight(4.0),
          child: Container(
            color: colorRoundBorder,
            height: 0.5,
          ),
        ),
    backgroundColor: colorBackgroundAppbar ?? Colors.transparent,
    bottomOpacity: showBottomBorder ?? true ? 1 : 0,
    elevation: 0,
    actions: actions ?? [],
    leading: InkWell(
      onTap: () {
        leading != null ? leading.call() : Get.back();
      },
      child: Visibility(
        visible: isBack ?? true,
        child: Icon(
          CupertinoIcons.back,
          size: ic_20,
          color: colorBtnBack,
        ),
      ),
    ),
  );
}

AppBar appBarButtonEmp(
    {required String title,
    List<Widget>? actions,
    Color? colorBackgroundAppbar,
    bool? showBottomBorder,
    bool? isBack,
    Function? leading,
    TabBar? tabBar,
    bool? isCenterTitle}) {
  return AppBar(
    centerTitle: isCenterTitle ?? true,
    title: Text(
      title,
      style: Themes.sfText16_600.copyWith(color: Colors.white),
    ),
    bottom: tabBar ??
        PreferredSize(
          preferredSize: const Size.fromHeight(4.0),
          child: Container(
            color: colorRoundBorder,
            height: 0.5,
          ),
        ),
    backgroundColor: colorIcon,
    bottomOpacity: showBottomBorder ?? true ? 1 : 0,
    elevation: 0,
    actions: actions ?? [],
    leading: InkWell(
      onTap: () {
        leading != null ? leading.call() : Get.back();
      },
      child: Visibility(
        visible: isBack ?? true,
        child: Icon(
          CupertinoIcons.back,
          size: ic_20,
          color: Colors.white,
        ),
      ),
    ),
  );
}

AppBar appBarButtonTabBar(
    {required String title,
    List<Widget>? actions,
    Color? colorBackgroundAppbar,
    bool? showBottomBorder,
    bool? isBack,
    Function? leading,
    TabBar? tabBar,
    bool? isCenterTitle}) {
  return AppBar(
    centerTitle: isCenterTitle ?? true,
    title: Text(
      title,
      style: Themes.sfText16_600.copyWith(color: Colors.white),
    ),
    bottom: tabBar ??
        PreferredSize(
          preferredSize: const Size.fromHeight(4.0),
          child: Container(
            color: colorRoundBorder,
            height: 0.5,
          ),
        ),
    backgroundColor: colorBackgroundAppbar ?? colorIcon,
    bottomOpacity: showBottomBorder ?? true ? 1 : 0,
    elevation: 0,
    actions: actions ?? [],
    leading: InkWell(
      onTap: () {
        leading != null ? leading.call() : Get.back();
      },
      child: Visibility(
        visible: isBack ?? true,
        child: Icon(
          CupertinoIcons.back,
          size: ic_20,
          color: Colors.white,
        ),
      ),
    ),
  );
}

Widget buttonPickRangeTime(
    {Function(List<DateTime?>?)? onPickTime,
    Function(bool? value)? isShowFunction}) {
  Rx<bool> isShowAll = true.obs;
  RxList<DateTime?> dateTimes = RxList<DateTime>();
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 8.0),
    child: Row(
      children: [
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: colorIcon),
              borderRadius: BorderRadius.circular(size8)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: size8, vertical: size5),
            child: InkWell(
              onTap: () {
                Get.dialog(calendarRangePickerDialog(
                    dateTimeChanges: (List<DateTime?>? dateTime) {
                      dateTimes.clear();
                      dateTime?.forEach((element) {
                        dateTimes.add(element);
                      });
                      dateTimes.last = DateTime(dateTime!.last!.year,
                          dateTime.last!.month, dateTime.last!.day, 23, 59);
                      isShowAll.value = false;
                      isShowFunction?.call(isShowAll.value);
                      if (onPickTime != null) {
                        onPickTime.call(dateTimes);
                      }
                    },
                    context: Get.context!));
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(CupertinoIcons.calendar_today,
                      color: colorIcon, size: size24),
                  SizedBox(width: size8),
                  Obx(() => Text(
                      isShowAll.value
                          ? "Tất cả"
                          : "${dateToString(dateTime: dateTimes.first, dateFormatTime: dOEMap[DateFormatCustom.DD_MM_YYYY])} - ${dateToString(dateTime: dateTimes.last, dateFormatTime: dOEMap[DateFormatCustom.DD_MM_YYYY])}",
                      style: Themes.sfText14_400.copyWith(color: colorIcon))),
                  SizedBox(width: size8),
                  Icon(
                    CupertinoIcons.chevron_down,
                    size: size18,
                    color: colorIcon,
                  ),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          width: size8,
        ),
        Obx(() => Visibility(
            //TODO khi chon thoi gian hien thi xem tat ca
            visible: !isShowAll.value,
            child: buttonLogout(
                title: 'Tất cả',
                height: size36,
                colorButton: colorIcon,
                function: () {
                  isShowAll.value = true;
                  dateTimes.clear();
                  dateTimes.add(DateTime(DateTime.now().year,
                          DateTime.now().month, DateTime.now().day, 0, 0, 0)
                      .subtract(const Duration(days: 31)));
                  dateTimes.add(DateTime.now());
                  onPickTime?.call(dateTimes);
                  if (isShowFunction != null) {
                    isShowFunction.call(isShowAll.value);
                  }
                })))
      ],
    ),
  );
}
Widget appBarBook({Widget? childTab, Widget? child}){
  return Stack(
    children: [
      Container(color: colorIcon, height: size145w,child: childTab,),
      Padding(padding: EdgeInsets.only(top: size120w),
      child: Container(decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(size12)),
      child: child?? Container()),)
    ],
  );
}
