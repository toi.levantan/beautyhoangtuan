import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/widget/button_common.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';

import '../../core/routes/routers.dart';
import '../path/color_path.dart';

class ConfirmDialogBooking extends GetView {
  final String? message;
  final String? confirmText;
  final String? cancelText;
  final Widget? title;
  final String? content;
  final Function()? onTap;
  final Function()? onTapClose;

  const ConfirmDialogBooking(
      {this.message,
      this.confirmText,
      this.cancelText,
      this.title,
      this.content,
        this.onTap,
        this.onTapClose,
      super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width,
      height: size220,
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          InkWell(
            onTap: () {
             if(onTapClose != null){
               onTapClose?.call();
             }
             else{
               Get.back();
             }
            },
            child: Icon(
              Icons.close,
              color: colorIconSearch,
              size: ic_20,
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(content ?? 'Xác nhận', style: Themes.sfText16_700),
              title ?? const Text('Đồng ý hành động này được thực hiện'),
              buttonCommon(title: confirmText ?? 'Đồng ý',color: Colors.green,isOK: true.obs,function: (){
                FocusScope.of(context).unfocus();
                 if(onTap != null){
                   onTap?.call();
                 }
              }),
            ],
          ),
        ],
      ),
    );
  }
}
class RequiredLoginDialog extends GetView{
  const RequiredLoginDialog({this.loginButton,this.signinButton, super.key});
  final Function? loginButton;
  final Function? signinButton;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return SizedBox(
     width: Get.width,
     height: Get.height /2,
     child: Padding(
       padding:  EdgeInsets.symmetric(vertical:size10),
       child: Stack(
         children: [
            Column(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(child: ImagesRender.imagesAssets(src: ImagePath.dialog_login,boxFit: BoxFit.contain,width: Get.width)),
                      Padding(
                        padding:  EdgeInsets.only(top: size8),
                        child: Text("Đăng nhập để sử dụng tính năng này",style: Themes.sfText17_700),
                      ),
                      Padding(
                        padding:  EdgeInsets.only(top: size8,bottom: size24),
                        child: Text("Bạn cần đăng nhập hoặc đăng ký tài khoản để sử dụng tính năng này",style: Themes.sfTextHint12_400),
                      ),
                    ],
                  ),
                ),
                buttonCommon(title: "Đăng nhập",isOK: true.obs,
                    function: (){
                      Get.toNamed(Routers.intro_page);
                    }),
              ],
            ),
           Positioned(right: 0,top: 0,child: InkWell(
               onTap: (){
                 Get.back();
               },
               child: Padding(
                 padding:  EdgeInsets.all(size8),
                 child: Icon(CupertinoIcons.clear_circled_solid,color: colorIcon,size: ic_18,),
               ))),
         ],
       ),
     ),
   );
  }
}
class InternetDialog extends GetView {
  final String? message;
  final String? confirmText;
  final String? cancelText;
  final Widget? title;
  final String? content;
  final Function()? onTap;

  const InternetDialog(
      {this.message,
      this.confirmText,
      this.cancelText,
      this.title,
      this.content,
        this.onTap,
      super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width,
      height: Get.width/3,
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(content ?? 'Thông báo', style: Themes.sfText16_700),
              title ??  Text('Không có kết nối internet',style: Themes.sfText16_400,),
              buttonCommon(title: confirmText ?? 'Thử lại',isOK: true.obs,function: (){
                FocusScope.of(context).unfocus();
                 if(onTap != null){
                   onTap?.call();
                 }
              }),
            ],
          ),
        ],
      ),
    );
  }
}
class InternetDialogHome extends GetView {
  final String? message;
  final String? confirmText;
  final String? cancelText;
  final Widget? title;
  final String? content;
  final Function()? onTap;

  const InternetDialogHome(
      {this.message,
      this.confirmText,
      this.cancelText,
      this.title,
      this.content,
        this.onTap,
      super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width,
      height: Get.width/3,
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(content ?? 'Thông báo', style: Themes.sfText16_700),
              title ??  Text('Không có kết nối internet',style: Themes.sfText16_400,),
              buttonCommon(title: confirmText ?? 'Đóng',isOK: true.obs,function: (){
                FocusScope.of(context).unfocus();
                 if(onTap != null){
                   onTap?.call();
                 }
              }),
            ],
          ),
        ],
      ),
    );
  }
}
