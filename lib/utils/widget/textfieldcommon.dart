import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';

class FormFieldLabel extends GetWidget {
  final TextEditingController? editingController;
  final String? title;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool? readOnly;
  final bool? isRequired;
  final Function(bool focus)? onFocusChanged;
  final Function()? onTapSuffixIcon;
  final Function()? onTap;
  final Function()? onTapPrefixIcon;
  final Rx<String?>? errorText;
  final bool? enable;
  final Function(String value)? onChange;
  final int? minLine;
  final int? maxLine;
  final Rx<bool>? obscureText;

  const FormFieldLabel(
      {this.prefixIcon,
      this.suffixIcon,
      this.readOnly,
      this.isRequired,
      this.onFocusChanged,
      this.onTapSuffixIcon,
      this.onTapPrefixIcon,
      this.errorText,
      this.onChange,
      this.enable,
      this.obscureText,
      this.onTap,
      required this.title,
      required this.editingController,
      this.minLine,
      this.maxLine,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
              text: title ?? '',
              style: Themes.sfText12_700,
              children: [
                TextSpan(
                    text: isRequired != null && isRequired != false ? '*' : '',
                    style:
                        Themes.sfText14_500.copyWith(color: Colors.redAccent)),
              ]),
        ),
        SizedBox(
          height: size6,
        ),
        FocusScope(
            child: Focus(
                onFocusChange: (focus) {
                  if (onFocusChanged != null) {
                    onFocusChanged?.call(focus);
                  }
                },
                child: Obx(() => TextFormField(
                      controller: editingController,
                      obscureText: obscureText?.value ?? false.obs.value,
                      onChanged: (value) {
                        if (onChange != null) {
                          onChange?.call(value);
                        }
                      },
                      onTap: () {
                        if (onTap != null) {
                          onTap?.call();
                        }
                      },
                      enabled: enable ?? true,
                      style: Themes.sfText14_400,
                      minLines: minLine,
                      maxLines: maxLine ?? 1,
                      decoration: InputDecoration(
                          suffixIcon: suffixIcon,
                          prefixIcon: prefixIcon,
                          errorText: errorText?.value,
                          contentPadding: EdgeInsets.only(
                              top: 0,
                              bottom: 0,
                              left: prefixIcon == null ? size14 : zeroSize,
                              right: suffixIcon == null ? size14 : zeroSize),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: const BorderSide(
                                  color: colorBorderTextField)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(12.0),
                              borderSide: const BorderSide(
                                  color: colorBorderTextField))),
                      readOnly: readOnly ?? false,
                    )))),
      ],
    );
  }
}
class FormFieldLabelMessage extends GetWidget {
  final TextEditingController? editingController;
  final String? title;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool? readOnly;
  final bool? isRequired;
  final Function(bool focus)? onFocusChanged;
  final Function()? onTapSuffixIcon;
  final Function()? onTap;
  final Function()? onTapPrefixIcon;
  final Rx<String?>? errorText;
  final bool? enable;
  final Function(String value)? onChange;
  final int? minLine;
  final int? maxLine;
  final Rx<bool>? obscureText;

  const FormFieldLabelMessage(
      {this.prefixIcon,
        this.suffixIcon,
        this.readOnly,
        this.isRequired,
        this.onFocusChanged,
        this.onTapSuffixIcon,
        this.onTapPrefixIcon,
        this.errorText,
        this.onChange,
        this.enable,
        this.obscureText,
        this.onTap,
        required this.title,
        required this.editingController,
        this.minLine,
        this.maxLine,
        Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
              text: title ?? '',
              style: Themes.sfText12_700,
              children: [
                TextSpan(
                    text: isRequired != null && isRequired != false ? '*' : '',
                    style:
                    Themes.sfText14_500.copyWith(color: Colors.redAccent)),
              ]),
        ),
        SizedBox(
          height: size6,
        ),
        FocusScope(
            child: Focus(
                onFocusChange: (focus) {
                  if (onFocusChanged != null) {
                    onFocusChanged?.call(focus);
                  }
                },
                child: Obx(() => TextFormField(
                  controller: editingController,
                  obscureText: obscureText?.value ?? false.obs.value,
                  onChanged: (value) {
                    if (onChange != null) {
                      onChange?.call(value);
                    }
                  },
                  onTap: () {
                    if (onTap != null) {
                      onTap?.call();
                    }
                  },
                  enabled: enable ?? true,
                  style: Themes.sfText14_400,
                  minLines: minLine,
                  maxLines: maxLine ?? 1,
                  decoration: InputDecoration(
                      suffixIcon: suffixIcon,
                      prefixIcon: prefixIcon,
                      errorText: errorText?.value,
                      contentPadding: EdgeInsets.only(
                          top: size8,
                          bottom: size8,
                          left: prefixIcon == null ? size16 : zeroSize,
                          right: suffixIcon == null ? size16 : zeroSize)),
                  readOnly: readOnly ?? false,)))),],
    );
  }
}
class FormFieldLabelNoBorder extends GetWidget {
  final TextEditingController? editingController;
  final String? title;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool? readOnly;
  final bool? isRequired;
  final Function(bool focus)? onFocusChanged;
  final Function()? onTapSuffixIcon;
  final Function()? onTap;
  final Function()? onTapPrefixIcon;
  final Rx<String?>? errorText;
  final bool? enable;
  final Function(String value)? onChange;
  final int? minLine;
  final int? maxLine;
  final Rx<bool?>? obscureText;
  final String? hintText;
  final TextInputType? keyboardType;
  final TextStyle? textStyle;

  const FormFieldLabelNoBorder(
      {this.prefixIcon,
      this.suffixIcon,
      this.readOnly,
      this.isRequired,
      this.onFocusChanged,
      this.onTapSuffixIcon,
      this.onTapPrefixIcon,
      this.errorText,
      this.onChange,
      this.hintText,
      this.enable,
      this.obscureText,
      this.onTap,
      required this.title,
      required this.editingController,
      this.minLine,
      this.maxLine,
      this.textStyle,
        this.keyboardType,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
              text: title ?? '',
              style: Themes.sfText12_700,
              children: [
                TextSpan(
                    text: isRequired != null && isRequired != false ? '*' : '',
                    style:  Themes.sfText14_500.copyWith(color: Colors.redAccent)),
              ]),
        ),
        SizedBox(
          height: size6,
        ),
        FocusScope(
            child: Focus(
                onFocusChange: (focus) {
                  if (onFocusChanged != null) {
                    onFocusChanged?.call(focus);
                  }
                },
                child: Obx(() => TextFormField(
                      controller: editingController,
                      obscureText: obscureText?.value ?? false,
                      onChanged: (value) {
                        if (onChange != null) {onChange?.call(value);}
                      },
                      onTap: () {
                        if (onTap != null) {
                          onTap?.call();
                        }
                      },
                     cursorHeight: size21,
                      keyboardType: keyboardType ??TextInputType.text,
                      obscuringCharacter: "*",
                      enabled: enable ?? true,
                      style:textStyle ?? Themes.sfText14_400,
                      minLines: minLine,
                      maxLines: maxLine ?? 1,
                      decoration: InputDecoration(
                          suffixIcon: suffixIcon,
                          hintText: hintText,
                          prefixIcon: prefixIcon,
                          errorText: errorText?.value,
                          isDense: true,
                          contentPadding:  EdgeInsets.all(zeroSize),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none),
                      readOnly: readOnly ?? false,
                    )))),
      ],
    );
  }
}

class SearchFormField extends GetWidget {
  final Function(String? value)? onChange;
  final Function(String? value)? onFieldSubmit;
  final Function()? onEditingComplete;
  final TextEditingController? editingController;
  final Function(bool value)? onChangedFocus;
  final bool? readOnly;

  const SearchFormField(
      {this.editingController,
      this.onChange,
      this.onFieldSubmit,
      this.onChangedFocus,
      this.onEditingComplete,
        this.readOnly,

      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FocusScope(
      child: Focus(
        onFocusChange: (onChanged) {
          if (onChangedFocus != null) {
            onChangedFocus?.call(onChanged);
          }
        },
        child: TextFormField(
          controller: editingController ?? TextEditingController(),
          style: Themes.sfText14_400.copyWith(decorationThickness: 0),
          readOnly: readOnly ?? false,
          decoration: InputDecoration(
              hintStyle: Themes.sfText14_400,
              hintText: 'Tìm kiếm',
              prefixIcon:
                  Icon(Icons.search, size: ic_16, color: colorIconSearch),
              fillColor: colorBackGroundSearch,
              contentPadding: EdgeInsets.symmetric(vertical: zeroSize, horizontal: size4),
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(size12),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(size12),
              ),
              filled: true),
           onChanged: (value) {
            if (onChange != null) {
              onChange?.call(value);
            }
          },
          onEditingComplete: () {
            if (onEditingComplete != null) {
              onEditingComplete?.call();
            }
          },
          onFieldSubmitted: (value) {
            if (onFieldSubmit != null) {
              onFieldSubmit?.call(value);
            }
          },
        ),
      ),
    );
  }
}

class textFieldDropDown extends GetWidget {
  final Function? onTap;
  final String? hintText;
  final TextStyle? hintTextStyle;
  final TextStyle? labelStyle;
  final String? label;
  final Rx<String?>? errorText;
  final Widget? icon;
  final Function(bool value)? focusChanged;
  final TextEditingController? editingController;
  final bool? enabled;
  final int? maxLines;
  final int? minLines;

  const textFieldDropDown(
      {super.key,
        this.onTap,
      this.hintText,
      this.hintTextStyle,
      this.labelStyle,
      this.label,
      this.errorText,
      this.icon,
      this.focusChanged,
        this.enabled,
        this.maxLines,
        this.minLines,
      this.editingController});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: size8, left: size8, right: size4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size10),
          border: Border.all(color: colorBackGroundAppBar)),
      child: Row(
        children: [
          Expanded(
              child: InkWell(
            onTap: () {
              if (onTap != null && enabled == null) {
                onTap?.call();
              }
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  label ?? "",
                  style: Themes.sfTextHint12_500,
                ),
                FocusScope(
                  child: Focus(
                    onFocusChange: (onFocus) {
                      if (focusChanged != null) {
                        focusChanged?.call(onFocus);
                      }
                    },
                    child: Obx(
                      () => TextFormField(
                        controller: editingController,
                        readOnly: true,
                        enabled: false,
                        style: Themes.sfText12_500
                            .copyWith(color: colorTextDropDown),
                        maxLines: maxLines ?? 1,
                        minLines: minLines ?? 1,
                        decoration: InputDecoration(
                          hintText: hintText,
                          hintStyle: Themes.sfTextHint11_400,
                          isDense: true,
                          errorText: errorText?.value,
                          errorStyle: const TextStyle(color: Colors.red),
                          iconColor: colorIconDropDown,
                          enabledBorder: InputBorder.none,
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )),
          icon ??
              Icon(
                Icons.keyboard_arrow_down,
                size: ic_20,
                color: colorIconDropDown,
              ),
        ],
      ),
    );
  }
}

class textFieldEdtDropDown extends GetWidget {
  final Function? onTap;
  final String? hintText;
  final TextStyle? hintTextStyle;
  final TextStyle? labelStyle;
  final String? label;
  final Rx<String?>? errorText;
  final Widget? icon;
  final Function(bool value)? focusChanged;
  final TextEditingController? editingController;
  final int? maxLine;
  final int? minLine;

  const textFieldEdtDropDown(
      {super.key,
        this.onTap,
      this.hintText,
      this.hintTextStyle,
      this.labelStyle,
      this.label,
      this.errorText,
      this.icon,
      this.focusChanged,
      this.maxLine,
      this.minLine,
      this.editingController});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: size8, left: size8, right: size4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size10),
          border: Border.all(color: colorBackGroundAppBar)),
      child: InkWell(
        onTap: () {
          if (onTap != null) {
            onTap?.call();
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              label ?? "",
              style: Themes.sfTextHint12_500,
            ),
            FocusScope(
              child: Focus(
                onFocusChange: (onFocus) {
                  if (focusChanged != null) {
                    focusChanged?.call(onFocus);
                  }
                },
                child: Obx(
                  () => TextFormField(
                    controller: editingController,
                    style:
                        Themes.sfText14_500.copyWith(color: colorTextDropDown),
                    maxLines: maxLine ?? 6,
                    minLines: minLine ?? 1,
                    decoration: InputDecoration(
                      hintText: hintText,
                      hintStyle: Themes.sfTextHint14_500,
                      isDense: true,
                      errorText: errorText?.value,
                      errorStyle: const TextStyle(color: Colors.red),
                      enabledBorder: InputBorder.none,
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FieldReadOnlyLabel extends GetWidget {
  final String? labelText;
  final TextEditingController? textEditingController;
  final Widget? suffixIcon;
  final Function()? function;
  final String? hintText;

  const FieldReadOnlyLabel(
      {super.key,
      this.function,
      this.suffixIcon,
      this.labelText,
      this.hintText,
      this.textEditingController});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onTap: () {
        if (function != null) {
          function!();
        }
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: colorBackGroundBorder),
            borderRadius: BorderRadius.circular(size8)),
        padding: EdgeInsets.only(left: size10, top: 2, bottom: 2),
        child: TextFormField(
            controller: textEditingController ?? TextEditingController(),
            enabled: false,
            decoration: InputDecoration(
              labelStyle: Themes.sfTextHint14_400,
              enabledBorder: InputBorder.none,
              border: InputBorder.none,
              labelText: labelText ?? '',
              isDense: true,
              suffixIcon:
                  Container(padding: EdgeInsets.all(size14), child: suffixIcon),
            )),
      ),
    );
  }
}
