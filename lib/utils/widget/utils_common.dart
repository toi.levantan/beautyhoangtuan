import 'dart:io';
import 'package:calendar_date_picker2/calendar_date_picker2.dart';
import 'package:drop_down_list/drop_down_list.dart';
import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/themes/themes.dart';
import 'package:myhoangtuan/utils/app/size_app.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:myhoangtuan/utils/path/image_paths.dart';
import 'package:myhoangtuan/utils/session/file_picker.dart';
import 'package:myhoangtuan/utils/widget/images_render.dart';
import 'package:myhoangtuan/utils/widget/textfieldcommon.dart';
import 'package:url_launcher/url_launcher.dart';

import '../session/date_formatter.dart';
import '../session/enum_session.dart';

Widget timeHoursNullOrDefault({String? time}) => Container(
      padding:
          EdgeInsets.symmetric(horizontal: sizeTxt_12, vertical: sizeTxt_2),
      decoration: BoxDecoration(
          color: colorBackGroundBorder,
          borderRadius: BorderRadius.circular(sizeTxt_16)),
      child: Text(time ?? '--:--',
          style: Themes.sfText11_500.copyWith(color: colorTimerText)),
    );

Widget appRatingBar(
        {Function(int? value)? updateStar, double? showStar, bool? readOnly}) =>
    RatingBar(
      maxRating: 5,
      minRating: 1,
      itemCount: 5,
      initialRating: showStar ?? 0,
      itemSize: ic_20,
      allowHalfRating: false,
      ignoreGestures: readOnly ?? false,
      direction: Axis.horizontal,
      itemPadding: const EdgeInsets.only(right: 4),
      onRatingUpdate: (value) {
        if (updateStar != null) {
          updateStar.call(value.toInt());
        }
      },
      ratingWidget: RatingWidget(
        full: ImagesRender.svgPicture(src: ImagePath.starFill),
        empty: ImagesRender.svgPicture(src: ImagePath.starOutline),
        half: ImagesRender.svgPicture(src: ImagePath.starFill),
      ),
    );


/// initial value để set value mặc định
/// function để thay đổi value
Widget cardSwitchCupertino(
    {required RxList<dynamic> items,
    Widget? content,
    Widget? icons,
    required Function(RxBool value, int index) function,
    bool? isEnable,
    required Function(int index) initialValue}) {
  return Card(
      child: ListView.builder(
    itemCount: items.length,
    shrinkWrap: true,
    physics: const NeverScrollableScrollPhysics(),
    itemBuilder: (BuildContext context, int index) {
      return Obx(() => Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Visibility(
                  visible: icons == null ? true : false,
                  child: SizedBox(width: 42, child: icons ?? Container())),
              Expanded(child: content ?? Container()),
              Switch(
                  value: initialValue(index),
                  onChanged: (val) {
                    if (isEnable ?? true) {
                      function.call(val.obs, index);
                    }
                  })
            ],
          ));
    },
  ));
}

Widget gestureFocus({required BuildContext context, Widget? child}) {
  return GestureDetector(
    onTap: () {
      FocusScopeNode currentFocus = FocusScope.of(context);
      if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
        FocusManager.instance.primaryFocus!.unfocus();
      }
    },
    child: child ?? Container(),
  );
}

Widget bodyWidget(
    {required Widget? child,
    Rx<bool?>? isLoading,
    EdgeInsets? padding,
    Widget? viewNoData,
    RxList<dynamic>? listDataCheck}) {
  RxBool isStatusSuccess = false.obs;
  RxBool isNotEmpty = false.obs;
  RxBool isEmpty = true.obs;
  return Stack(
    children: [
      Container(
        padding: padding ??
            const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 20),
        child: child ?? const SizedBox(),
      ),
      Obx(() => Visibility(
          visible: isLoading?.value ?? isStatusSuccess.value,
          child: Container(
            color: Colors.transparent,
            constraints:
                BoxConstraints(maxHeight: Get.height, maxWidth: Get.width),
            width: Get.width,
            height: Get.height,
            child: const Center(
              child: CupertinoActivityIndicator(),
            ),
          ))),
      Obx(() => Visibility(
          visible: listDataCheck != null &&
                  listDataCheck.isEmpty &&
                  isLoading?.value == false
              ? isEmpty.value
              : isNotEmpty.value,
          child: Center(
            child: viewNoData ??
                Text('Không có dữ liệu', style: Themes.sfText14_500),
          ))),
    ],
  );
}

Widget calendarPickerDialog({
  required Function(DateTime? dateTime)? dateTimeChange,
  required BuildContext context,
  DateTime? currentDate,
  DateTime? firstDate,
  DateTime? lastDate,
}) {
  return AlertDialog(
      contentPadding: EdgeInsets.only(top: size16, left: size4, right: size4),
      alignment: Alignment.center,
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(size12)),
      content: Theme(
        data: Theme.of(context).copyWith(
            textTheme: TextTheme(
                titleSmall: Themes.sfText14_800, bodySmall: Themes.sfText12_500),
            colorScheme: Theme.of(context).colorScheme.copyWith()),
        child: SizedBox(
          height: Get.height / 3,
          width: Get.width,
          child: CalendarDatePicker(
            initialDate: DateTime.now(),
            firstDate: firstDate ?? DateTime(DateTime.now().year - 100),
            lastDate: lastDate ?? DateTime.now(),
            initialCalendarMode: DatePickerMode.day,
            currentDate: currentDate,
            onDateChanged: (datetime) {
              dateTimeChange!(datetime);
              Get.back();
            },
          ),
        ),
      ));
}

Widget calendarRangePickerDialog({
  required Function(List<DateTime?>? dateTimes)? dateTimeChanges,
  required BuildContext context,
  DateTime? currentDate,
  DateTime? firstDate,
  DateTime? lastDate,
}) {
  return AlertDialog(
      contentPadding: EdgeInsets.only(top: size16, left: size4, right: size4),
      alignment: Alignment.center,
      shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(size12)),
      content: Theme(
        data: Theme.of(context).copyWith(
            textTheme: TextTheme(
                titleSmall: Themes.sfText14_800, bodySmall: Themes.sfText12_500),
            colorScheme: Theme.of(context).colorScheme.copyWith()),
        child: SizedBox(
          height: Get.height / 3,
          width: Get.width,
          child: CalendarDatePicker2(
            onDisplayedMonthChanged: (datetime) {},
            value: [DateTime.now()],
            config: CalendarDatePicker2Config(
                calendarType: CalendarDatePicker2Type.range,
                lastDate: DateTime.now()),
            onValueChanged: (dates) => {
              dateTimeChanges?.call(dates),
            },
          ),
        ),
      ));
}

Widget itemInfo({String? img, String? data}) {
  return Row(
    mainAxisSize: MainAxisSize.max,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Flexible(
          child: Text(data ?? '',
              style: Themes.sfText12_400.copyWith(color: Colors.white),
              overflow: TextOverflow.ellipsis)),
    ],
  );
}

///TODO widget pick image and video

Widget pickFromImageAndVideo(
    {Function? pickImage, Function? pickVideo, Color? colorBackground}) {
  return Container(
    height: size82,
    width: Get.width,
    decoration:
        BoxDecoration(color: colorBackground ?? Colors.white, boxShadow: const [
      BoxShadow(
          color: colorOtpBox,
          offset: Offset(-1, -1),
          blurRadius: 4,
          spreadRadius: 1)
    ]),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
            child: Center(
                child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              onTap: () {
                if (pickImage != null) {
                  pickImage.call();
                }
              },
              child: ImagesRender.svgPicture(
                  src: ImagePath.gallery, width: ic_24, height: ic_24),
            ),
            SizedBox(
              height: size6,
            ),
            Text(
              'Ảnh',
              style: Themes.sfText10_400.copyWith(color: colorTextApp),
            ),
          ],
        ))),
        Expanded(
            child: Center(
                child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              onTap: () {
                if (pickVideo != null) {
                  pickVideo.call();
                }
              },
              child: ImagesRender.svgPicture(
                  src: ImagePath.video, width: ic_24, height: ic_24),
            ),
            SizedBox(height: size6),
            Text(
              'Video',
              style: Themes.sfText10_400.copyWith(color: colorTextApp),
            ),
          ],
        ))),
      ],
    ),
  );
}

Widget itemInfoShow({String? img, String? data, Function? onTap}) {
  return InkWell(
    onTap: () {
      if (onTap != null) {
        onTap.call();
      }
    },
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  children: [
                    ImagesRender.imagesAssets(
                        src: img ?? '',
                        width: ic_18,
                        height: ic_18,
                        boxFit: BoxFit.contain),
                    SizedBox(
                      width: size18,
                    ),
                    Text(
                      data ?? '',
                      style: Themes.sfText14_400,
                    ),
                  ],
                ),
              ),
              const Icon(Icons.keyboard_arrow_right_outlined),
            ],
          ),
        ),
        //  const Divider(height: 1,),
      ],
    ),
  );
}

Widget itemInfoShowAsset({String? img, String? data, Function? onTap}) {
  return InkWell(
    onTap: () {
      if (onTap != null) {
        onTap.call();
      }
    },
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  children: [
                    ImagesRender.imagesAssets(
                        src: img ?? '',
                        width: ic_18,
                        height: ic_18,
                        boxFit: BoxFit.contain),
                    SizedBox(
                      width: size18,
                    ),
                    Text(
                      data ?? '',
                      style: Themes.sfText14_400,
                    ),
                  ],
                ),
              ),
              const Icon(Icons.keyboard_arrow_right_outlined),
            ],
          ),
        ),
        //  const Divider(height: 1,),
      ],
    ),
  );
}
Widget itemInfoShowTwoLineData({String? img, String? data1, String? data2, Function? onTap, bool? showLine}) {
  return InkWell(
    onTap: () {
      if (onTap != null) {
        onTap.call();
      }
    },
    child: Column(
      children: [
        Padding(
          padding:  EdgeInsets.all(size16),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  children: [
                    ImagesRender.svgPicture(
                        src: img ?? '',
                        width: ic_24,
                        height: ic_24,
                        boxFit: BoxFit.contain),
                    SizedBox(
                      width: size18,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data1 ?? '',
                            style: Themes.sfText16_400,
                          ),
                          SizedBox(height: size5),
                          Text(
                            data2 ?? '',
                            style: Themes.sfText12_400.copyWith(color: colorUnset),
                          ),
                          SizedBox(height: size10),
                          Visibility(
                              visible: showLine ?? true,
                              child: const Divider(height: 0.5,color: colorBackGroundBorder)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
/// co tu them cuoi
Widget itemInfoShowEndTitle(
    {String? img, String? data, Function? onTap, RxnString? endTitle}) {
  return InkWell(
    onTap: () {
      if (onTap != null) {
        onTap.call();
      }
    },
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Row(
                  children: [
                    ImagesRender.imagesAssets(
                        src: img ?? '',
                        width: ic_18,
                        height: ic_18,
                        boxFit: BoxFit.contain),
                    SizedBox(
                      width: size18,
                    ),
                    Text(
                      data ?? '',
                      style: Themes.sfText14_400,
                    ),
                  ],
                ),
              ),
              Obx(() => Text(
                    endTitle?.value ?? ''.obs.value,
                    style: Themes.sfTextHint14_400,
                  )),
              const Icon(Icons.keyboard_arrow_right_outlined),
            ],
          ),
        ),
        //  const Divider(height: 1,),
      ],
    ),
  );
}

Widget itemInfoShowEndTitleNoIcon(
    {String? img, String? data, Function? onTap, String? endTitle,TextStyle? textStyle,TextStyle? textStyleEnd}) {
  return InkWell(
    onTap: () {
      if (onTap != null) {
        onTap.call();
      }
    },
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                children: [
                  ImagesRender.svgPicture(
                      src: img ?? '',
                      width: ic_18,
                      height: ic_18,
                      boxFit: BoxFit.contain),
                  SizedBox(
                    width: size6,
                  ),
                  Text(
                    data ?? '',
                    style: textStyle ?? Themes.sfText14_500,
                  ),
                  SizedBox(width: size5)
                ],
              ),
              Expanded(
                child: Text(endTitle ?? '',
                    textAlign: TextAlign.end, style:textStyleEnd?? Themes.sfText14_400),
              )
            ],
          ),
        ),
        //  const Divider(height: 1,),
      ],
    ),
  );
}
Widget itemInfoOnlyTitle({String? data, Function? onTap, String? endTitle}) {
  return InkWell(
    onTap: () {
      if (onTap != null) {
        onTap.call();
      }
    },
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                children: [
                  Text(
                    data ?? '',
                    style: Themes.sfTextHint14_400,
                  ),
                  SizedBox(width: size5)
                ],
              ),
              Expanded(
                child: Text(endTitle ?? '',
                    textAlign: TextAlign.end, style: Themes.sfText14_400.copyWith(color: colorIcon)),
              )
            ],
          ),
        ),
        //  const Divider(height: 1,),
      ],
    ),
  );
}

Widget closeStack({Function? function,double? size}) {
  return InkWell(
    onTap: () {
      if (function != null) {
        function.call();
      }
    },
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: size8, vertical: size4),
      child: ImagesRender.svgPicture(
          src: ImagePath.close, width: size ?? ic_20, height: size ?? ic_20),
    ),
  );
}
Widget editStack({Function? function}) {
  return InkWell(
    onTap: () {
      if (function != null) {
        function.call();
      }
    },
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: size8, vertical: size4),
      child: Icon(Icons.edit_outlined,size: ic_18,color: Colors.white,),
    ),
  );
}

Widget linkify({String? text}) {
  return Linkify(
    onOpen: (link) async {
      if (!await launchUrl(Uri.parse(link.url))) {
        throw Exception('Could not launch ${link.url}');
      }
    },
    text: "$text",
    style: Themes.sfText14_400,
    maxLines: 4,
    overflow: TextOverflow.ellipsis,
  );
}

Widget linkifyUnlimited({String? text, int? maxLines}) {
  return Linkify(
    onOpen: (link) async {
      if (!await launchUrl(Uri.parse(link.url))) {
        throw Exception('Could not launch ${link.url}');
      }
    },
    text: "$text",
    style: Themes.sfText14_400,
  );
}

dropDownSearch(
    {required List<SelectedListItem>? sourceList,
    String? bottomSheetTitle,
    Function(String? name, String value)? onSelectedValue,
    required BuildContext? context}) {
  sourceList!.isEmpty ? Get.bottomSheet(BottomSheet(
          onClosing: () {},
          builder: (context) {
            return Center(
              child: Text(
                'Không có dữ liệu',
                style: Themes.sfText14_500.copyWith(color: colorTextApp),
              ),
            );
          }))
      : DropDownState(
          DropDown(
            bottomSheetTitle: Text(
              bottomSheetTitle ?? '',
              style: Themes.sfText14_500),
            submitButtonChild: const Text(
              'Xong',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            data: sourceList,
            selectedItems: (selectedList) {
              if (onSelectedValue != null) {
                onSelectedValue.call(
                    selectedList.first.name, selectedList.first.value ?? "-1");
              }
            },
            enableMultipleSelection: false)).showModal(context);
}

Widget searchDropDownBottomString({
  required List<dynamic> items,
  String? hintText,
  String? textLabel,
  Function(dynamic value)? function,
  FocusNode? focusNode,
  Function(dynamic value)? itemObj,
  TextEditingController? editingController,
}) {
  return Column(
    children: [
      Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(size8),
            border: Border.all(color: colorBackGroundBorder)),
        padding: EdgeInsets.only(left: size10, top: size2, bottom: 2),
        child: DropdownSearch(
            items: items,
            itemAsString: (value) => itemObj?.call(value),
            onChanged: (value) {
              if (function != null) {
                function.call(value);
              }
            },
            dropdownButtonProps: const DropdownButtonProps(
                icon: Icon(
              Icons.keyboard_arrow_down,
            )),
            dropdownDecoratorProps: DropDownDecoratorProps(
                dropdownSearchDecoration: InputDecoration(
                    isDense: true,
                    labelText: textLabel ?? '',
                    hintText: hintText ?? '--Chọn giá trị--',
                    labelStyle: Themes.sfTextHint14_400,
                    disabledBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    errorBorder: InputBorder.none)),
            popupProps: PopupProps.modalBottomSheet(
                emptyBuilder: (context, searchEntry) {
                  return const Center(child: Text('Không có dữ liệu'));
                },
                errorBuilder: (context, searchEntry, exception) {
                  return const Center(
                    child: Text('Đã xảy ra lỗi.Thử lai sau!'),
                  );
                },
                modalBottomSheetProps: ModalBottomSheetProps(
                    backgroundColor: colorBackGroundBorder,
                    barrierDismissible: true,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(size8))),
                //TODO dữ liệu vào rỗng thì không hiển thị search
                fit: FlexFit.tight,
                showSearchBox: items.isEmpty ? false : true,
                textStyle: Themes.sfText14_500,
                searchFieldProps: TextFieldProps(
                    style: Themes.sfText14_500,
                    decoration: InputDecoration(
                        fillColor: colorBackGroundSearch,
                        hintText: 'Nhập từ khóa tìm kiếm',
                        suffixIcon: Icon(
                          CupertinoIcons.search,
                          color: colorIcon,
                          size: ic_18,
                        ))))),
      ),
    ],
  );
}

/// hient thi thong tin thu ngay thang nam
Widget infoDay({String? time}) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: size16),
    child: Row(
      children: [
        ///TODO stack để ẩn đường giao dien duoi icon
        Stack(
          children: [
            Padding(
                padding: EdgeInsets.symmetric(vertical: size3),
                child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(size16),
                        color: colorFontStaff),
                    width: Get.height / 3,
                    height: size33,
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: size33),
                        child: RichText(
                            text: TextSpan(
                                text: getValueDOE(dateToString(
                                    dateTime:
                                        strToDate(valueTimeString: time ?? ''),
                                    dateFormatTime:
                                        dOEMap[DateFormatCustom.EEEE])),
                                children: [
                                  const TextSpan(text: ','),
                                  const TextSpan(text: ' ngày '),
                                  TextSpan(
                                      text: dateToString(
                                          dateTime: strToDate(
                                              valueTimeString: time ?? ""),
                                          dateFormatTime: dOEMap[
                                              DateFormatCustom.DD_MM_YYYY])),
                                ],
                                style: Themes.sfText15_700
                                    .copyWith(color: colorFontStaff2)))))),

            ///TODO card lich
            Card(
              margin: EdgeInsets.zero,
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(size100),
              ),
              child: Padding(
                padding: EdgeInsets.all(size3),
                child: Container(
                  decoration: BoxDecoration(
                      color: colorIcon,
                      borderRadius: BorderRadius.circular(size100)),
                  child: Padding(
                    padding: EdgeInsets.all(size5),
                    child: const Icon(CupertinoIcons.calendar_today,
                        color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    ),
  );
}

///TODO addComment
Widget addComment({Enum? typeComment,Function({String? comment, String? image})? function, required TextEditingController? textEditingController}) {
  RxString? filePath = RxString('');
  return Column(
    children: [
      Obx(() =>
          Visibility(
            visible: filePath.value.isNotEmpty,
            child: Container(
              width: Get.width,
              alignment: Alignment.center,
              color: colorBackGroundHome.withOpacity(0.3),
              child: Padding(
                padding: EdgeInsets.all(size12),
                child: Stack(
                  alignment: Alignment.topRight,
                  children: [
                    Obx(() =>
                        ClipRRect(
                            borderRadius: BorderRadius.circular(size8),
                            child: Image.file(
                              File(filePath.value), width: ic_86,
                              height: ic_86,
                              fit: BoxFit.cover,))),
                    closeStack(function: () {
                      filePath.value = '';
                    }, size: ic_14)
                  ],
                ),
              ),
            ),
          )),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: size16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
                child: FormFieldLabelNoBorder(
                  title: '',
                  editingController: textEditingController,
                  errorText: Rxn(),
                  hintText: 'Nhập bình luận',
                  maxLine: 4,
                  minLine: 1,
                )),
            SizedBox(
              width: ic_12,
            ),
            InkWell(
                onTap: () async {
                  final result = await PickerFile.singleImageFilePicker();
                  if (result != null && result != File('')) {
                    filePath.value = result.path;
                  }
                },
                child: ImagesRender.svgPicture(src: ImagePath.gallery, width: ic_20, height: ic_20)),
            SizedBox(
              width: ic_16,
            ),
                InkWell(
                  onTap: () async {
                    if (function != null ) {
                      function.call(comment :textEditingController?.text.trim(),image : filePath.value);
                    }
                  },
                  child: Icon(CupertinoIcons.paperplane_fill,
                      size:  ic_20,
                      color: colorIcon.withOpacity(0.6)),
                )
          ],
        ),
      ),
      SizedBox(height: size36),
    ],
  );
}
  Widget markTitle({bool? isShowRequired,String? title, String? value}){
  return Column(
    children: [
      Row(
        children: [
           Icon(Icons.radio_button_checked,color: colorIcon,size: ic_10,),
           SizedBox(width: size8,),
           Text(title ??'',style: Themes.sfText12_500.copyWith(color: colorIcon),),
           SizedBox(width: size4,),
           Text(value ??'',style: Themes.sfText12_500),
        ],
      ),

    ],
  );

}
