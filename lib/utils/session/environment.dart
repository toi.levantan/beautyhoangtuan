import 'package:myhoangtuan/utils/session/enum_session.dart';

class Environment {
  static const product = <Enum,String>{
    EnvironmentEnum.authUrl :"https://api.inviv.vn",
    EnvironmentEnum.baseEndPoint :"https://api.inviv.vn",
    EnvironmentEnum.linkImage :"https://cdnimage.inviv.vn/",
    EnvironmentEnum.linkImageProcessor :"https://api.inviv.vn/processor/api/v1/",
  };
  static const test = <Enum,String>{
    EnvironmentEnum.authUrl :"https://api.staging.inviv.vn",
    EnvironmentEnum.baseEndPoint :"https://api.staging.inviv.vn",
    EnvironmentEnum.linkImage :"https://file.staging.inviv.vn/",
  };
  static const testAHuy = <Enum,String>{
    EnvironmentEnum.authUrl :"http://192.168.1.35:8582",
    EnvironmentEnum.baseEndPoint :"http://192.168.1.35:8582",
    EnvironmentEnum.linkImage :"https://file.staging.inviv.vn/",
  };
  static const testHieu = <Enum,String>{
    EnvironmentEnum.authUrl :"http://192.168.1.18:8582",
    EnvironmentEnum.baseEndPoint :"http://192.168.1.18:8582",
    EnvironmentEnum.linkImage :"https://file.staging.inviv.vn/",
  };
}