import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class PickerFile {
  static Future<List<File?>> multiFilePicker() async {
    FilePickerResult? result;
    try {
      result = await FilePicker.platform.pickFiles(
          allowMultiple: true, dialogTitle: 'Dung lượng các file < 300 Mb');
      List<File> files = <File>[];
      if (result != null) {
        files = result.paths.map((e) => File(e!)).toList();
        return files;
      } else {
        return [];
      }
    } catch (e, s) {}
    return [];
  }

  static Future<List<File?>> audio() async {
    FilePickerResult? result;
    try {
      result = await FilePicker.platform.pickFiles(
        dialogTitle: 'File có kich thước < 100Mb',
        type: FileType.custom,
        allowedExtensions: [
          'mp3',
          'wav',
          'm4a',
        ],
        allowMultiple: true,
      );
      List<File> files = <File>[];
      if (result != null) {
        files = result.paths.map((e) => File(e!)).toList();
        return files;
      } else {
        return [];
      }
    } catch (e, s) {
      if (kDebugMode) {
        print(s);
      }
    }
    return [];
  }

  static Future<File?> singleImageFilePicker() async {
    FilePickerResult? result;
    try {
      result = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.image,
      );
      if (result != null) {
        File file = File(result.files.single.path ?? '');
        return file;
      } else {
        return File('');
      }
    } catch (e, s) {
      if (kDebugMode) {
        print(s);
      }
    }
    return File('');
  }

  static Future<List<File?>> multiFilePickerImage() async {
    FilePickerResult? result;
    try {
      result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.media,
      );
      List<File> files = <File>[];
      if (result != null) {
        files = result.paths.map((e) => File(e!)).toList();
        return files;
      } else {
        return [];
      }
    } catch (e, s) {}
    return [];
  }

  static Future<XFile?> videoFromCameraPicker() async {
    final ImagePicker pickerCamera = ImagePicker();
    XFile? fileCamera;
    try {
      fileCamera = await pickerCamera.pickVideo(
          source: ImageSource.camera, maxDuration: const Duration(minutes: 5));
      if (fileCamera != null) {
        return fileCamera;
      }
    } catch (ex) {}
    return null;
  }

  static Future<File?> getImageFromVideo({String? srcPath}) async {
    try {
      final thumb = await VideoThumbnail.thumbnailFile(
        video: srcPath ?? "",
        imageFormat: ImageFormat.PNG,
        quality: 100,
      );
      File file = File(thumb ?? '');
      return file;
    } catch (ex) {
      return File('');
    }
  }
}
