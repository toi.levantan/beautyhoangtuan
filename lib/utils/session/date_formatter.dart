import 'package:intl/intl.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';

DateFormat dateFormat = DateFormat();
// convert date type time to String type
String dateToString({ DateTime? dateTime, String? dateFormatTime,}) {
  dateFormat = DateFormat(dateFormatTime);
  return dateFormat.format(dateTime ?? DateTime.now());
}
// convert str to DatetimeStr
String? isoDateToString({required String? isoDate, String? dateTimeFormat }){
  final date  = DateTime.parse(isoDate ??'');
  final timeStamp = date.millisecondsSinceEpoch;
  dateFormat = DateFormat(dateTimeFormat);
  final dateTimeStamp = DateTime.fromMillisecondsSinceEpoch(timeStamp);
  return dateFormat.format(dateTimeStamp);
}
//
String? timeStampToString({required int? timeStamp, String? dateTimeFormat }){
  dateFormat = DateFormat(dateTimeFormat);
  final dateTimeStamp = DateTime.fromMillisecondsSinceEpoch(timeStamp ?? 0);
  return dateFormat.format(dateTimeStamp);
}
// convert str to Datetime
DateTime strToDate({required String valueTimeString, String? dateTimeFormat }){
  final dateFormat = DateFormat(dateTimeFormat ?? dOEMap[DateFormatCustom.DD_MM_YYYY]);
  return dateFormat.parse(valueTimeString);
}