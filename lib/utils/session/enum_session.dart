import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum DateFormatCustom { MM, EEEE, DD_MM_YY,DD_MM_YYYY,HH_mm_a,HH_mm,UTC,HH_UTC,DD_MM,HH_DD_YYYY}

enum DOE { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday }
enum AppState{loading,complete}
enum News{all,news,event,document,promotion}
enum EnvironmentEnum{authUrl,baseEndPoint,baseEndPointProcessor,linkImage,linkImageProcessor}
enum TypeReport{thaido,chatluong,giaca,chinhsach,khac,tatca}
enum CheckUser{create,used,forgotPwd}
enum DeviceType { Phone, Tablet }
enum StatusBook {WAITING,CONFIRMED,COMPLETED,CANCELLED}
enum ImageTypeEnum  {networkImage,fileImage}
enum FileTypeUri  {images,videos}
enum LikeStatus  {like,unlike}
//map cac su kien

Map<Enum,int?> enumMapNews = {
  News.all : null,
  News.news : 1,
  News.event : 2,
  News.document : 3,
  News.promotion : 4,
};
Map<Enum,String> imageTypeMap = {
  ImageTypeEnum.networkImage : 'networkImage' ,
  ImageTypeEnum.fileImage : 'fileImage' ,
};

Map<Enum,int> mapEnumStatusBook = {
  StatusBook.WAITING : 1,
  StatusBook.CONFIRMED : 2,
  StatusBook.COMPLETED : 3,
  StatusBook.CANCELLED : 4,
};

DeviceType getDeviceType() {
  final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
  return data.size.shortestSide < 550 ? DeviceType.Phone : DeviceType.Tablet;
}

Map<Enum,int> mapStatusLike ={
  LikeStatus.values.first : 1,
  LikeStatus.values.last : 0,
} ;
Map<Enum,int> fileTypeUriMap ={
  FileTypeUri.images : 1,
  FileTypeUri.videos : 2,
} ;
Map<Enum,int?> mapEnumTypeReport = {
  TypeReport.khac : 5,
  TypeReport.thaido : 1,
  TypeReport.chatluong : 2,
  TypeReport.giaca : 3,
  TypeReport.chinhsach : 4,
  TypeReport.tatca : null,
};
Map<Enum,String?> mapEnumTypeReportString = {
  TypeReport.khac : 'Khác',
  TypeReport.thaido : "Thái độ",
  TypeReport.chatluong : "Chất lượng",
  TypeReport.giaca : "Giá cả",
  TypeReport.chinhsach : "Chính sách",
  TypeReport.tatca : "Tất cả",
};
Map<int,String?> mapTypeFeedBack = {
  5 : 'Khác',
  1 : "Thái độ",
  2 : "Chất lượng",
  3 : "Giá cả",
  4 : "Chính sách",

};
Map<Enum,String> enumMapNewsLabel = {
  News.all : "Tất cả",
  News.news : "Tin tức",
  News.event : "Sự kiện",
  News.document : "Tài liệu thẩm mỹ",
  News.promotion : "Khuyến mãi",
};
// goi tuownfg minh cac gia tri tu Enum
Map<Enum, String> dOEMap = {
  DateFormatCustom.MM: "MM",
  DateFormatCustom.EEEE: "EEEE",
  DateFormatCustom.DD_MM_YY: "dd/MM/yy",
  DateFormatCustom.DD_MM_YYYY: "dd/MM/yyyy",
  DateFormatCustom.HH_DD_YYYY: "HH:mm:ss dd/MM/yyy",
  DateFormatCustom.HH_mm_a: "hh:mm a",
  DateFormatCustom.HH_mm: "HH:mm",
  DateFormatCustom.UTC: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z",
  DateFormatCustom.HH_UTC: "yyyy-MM-dd",
  DateFormatCustom.DD_MM: "dd/MM",
};
Map<Enum, Rx<bool?>> mapAppState = {
  AppState.loading : true.obs,
  AppState.complete : false.obs,
};
// return value to String EN or VN
String? getValueDOE(String? value) {
  switch (value) {
    case 'Monday':
      return "Thứ 2";
    case 'Tuesday':
      return 'Thứ 3';
    case 'Wednesday':
      return 'Thứ 4';
    case 'Thursday':
      return "Thứ 5";
    case 'Friday':
      return "Thứ 6";
    case 'Saturday':
      return "Thứ 7";
    case 'Sunday':
      return "CN";
  }
  return "";
}

// return value to String EN or VN
String? getValueDOEShort(String? value) {
  switch (value) {
    case 'Monday':
      return "T2";
    case 'Tuesday':
      return 'T3';
    case 'Wednesday':
      return 'T4';
    case 'Thursday':
      return "T5";
    case 'Friday':
      return "T6";
    case 'Saturday':
      return "T7";
    case 'Sunday':
      return "CN";
  }
  return "";
}

enum SizeBox {
  size4,
  size6,
  size8,
  size10,
  size12,
  size18,
  size20,
  size24,
}

Map<SizeBox, double?> sizeMap = {
  SizeBox.size4: 4,
  SizeBox.size6: 6,
  SizeBox.size8: 8,
  SizeBox.size10: 10,
};
String? departmentType(String? department){
  switch(department){
    case "0": return "Nhân viên";
    case "1": return "Trưởng phòng";
  }
 return "";
}
String? positionType(String? position){
  switch(position){
    case "1": return "Trường phòng";
    case "2": return "Phó phòng";
    case "3": return "Bác sĩ";
    case "4": return "Phụ tá";
    case "5": return "Lễ tân";
    case "6": return "Bảo vệ";
    case "7": return "Chăm sóc khách hàng";
    default: return position;
  }
}
