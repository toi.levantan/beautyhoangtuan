import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/api_manager.dart';
import 'package:myhoangtuan/utils/app/share_pref.dart';

import '../path/const_key.dart';

Rx<bool> checkHasLogin(){
  if(PreferenceUtils.getString(keyAccessToken) != null){
    return true.obs;
  }
  return false.obs;
}