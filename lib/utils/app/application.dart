import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:myhoangtuan/core/repository/url.dart';
import 'package:myhoangtuan/utils/session/date_formatter.dart';
import 'package:myhoangtuan/utils/session/enum_session.dart';
import 'package:url_launcher/url_launcher.dart';
import '../path/color_path.dart';

class Application {
  static bool debug = true;
  static String version = '0.0.1';

  ///Singleton factory
  static final Application _instance = Application._internal();

  factory Application() {
    return _instance;
  }

  Application._internal();

  static String? checkEmptyErrAndShort({String? param}) {
    if (param != null && param.isNotEmpty) {
      if (param.length < 4) {
        return 'Thông tin chứa ít nhất 4 ký tự';
      }
      return null;
    }
    return 'Vui lòng điền thông tin vào trường này';
  }

  static String? checkEmptyLess8Symbol({String? param}) {
    if (param != null && param.isNotEmpty) {
      if (param.length < 8) {
        return 'Thông tin chứa ít nhất 8 ký tự';
      }
      return null;
    }
    return 'Vui lòng điền thông tin vào trường này';
  }

  static String? checkEmptyErr({String? param}) {
    if (param != null && param.isNotEmpty) {
      return null;
    }
    return 'Vui lòng điền thông tin vào trường này';
  }

  ///   r'^
  //   (?=.*[A-Z])       // should contain at least one upper case
  //   (?=.*[a-z])       // should contain at least one lower case
  //   (?=.*?[0-9])      // should contain at least one digit
  //   (?=.*?[!@#\$&*~]) // should contain at least one Special character
  //   .{8,}             // Must be at least 8 characters in length
  // $
  static String? checkValidatePassword({required String pwd}) {
    RegExp regex = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
    if (pwd.isEmpty) {
      return 'Vui lòng điền thông tin vào trường này';
    }
    if (pwd.length < 8) {
      return 'Thông tin chứa ít nhất 8 ký tự';
    }
    if (!regex.hasMatch(pwd)) {
      return 'Mật khẩu bao gồm chữ hoa,chữ thường và số ';
    }
    return null;
  }

  static String? checkPhoneNumberText(String? params) {
    if (params != null && params.isNotEmpty) {
      String pattern = r'(^0(3|5|7|8|9)\d{8}$)';
      RegExp regExp = RegExp(pattern);
      if (!regExp.hasMatch(params)) {
        return "Vui lòng nhập thông tin là số điện thoại";
      }
      return null;
    }
    return 'Vui lòng điền thông tin vào trường này';
  }

  static Future<void> launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.inAppWebView,
    )) {
      throw Exception('Could not launch $url');
    }
  }

  static Future<void> makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }
  static Future<void> makeMessage(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'sms',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }

  static final currencyFormat = NumberFormat.currency(
      locale: 'en_US',
      customPattern: '#,##0',
      decimalDigits: 0);

  static RxString? numberOfLike({String? number}) {
    if (number != null && number.isNotEmpty) {
      String thousand;
      String hundred;
      if (double.parse(number) >= 1000 && number != "0") {
        thousand = (int.parse(number) ~/ 1000).toString();
        String split = number.substring(number.length -3,number.length);
        hundred = (int.parse(split) ~/100).toString();
        return '$thousand.${hundred}K'.obs;
      }
    }
      return (number ?? "0").obs;
    }
    static Rx<bool> isTypeVideo({String? srcPath}){
      if(srcPath != null && srcPath.endsWith('.mp4') || srcPath != null && srcPath.endsWith('.hevc') || srcPath != null && srcPath.endsWith('.mov')){
        return true.obs;
      }
      return false.obs;
    }
    static Rx<bool> isTypeVideoFromUrl({int? type}){

    /// type ==1 là ảnh 2 là video
     if(type == fileTypeUriMap[FileTypeUri.videos]){
        return true.obs;
     }
     return false.obs;
    }
    static showSnackCopy({required BuildContext context}){
      var snackBar =  SnackBar(content: const Text('Đã sao chép dữ liệu'),backgroundColor: colorTextApp.withOpacity(0.6),);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
    static String? showNearDay({String? time}){
     DateTime? dateTime = DateTime.parse(time??'');

     DateTime now = DateTime.now();
     final today = DateTime(now.year, now.month, now.day);
     final yesterday = DateTime(now.year, now.month, now.day - 1);
     final theDayAfterTomorrow = DateTime(now.year, now.month, now.day - 2);
     String? hourString = isoDateToString(isoDate: time,dateTimeFormat: dOEMap[DateFormatCustom.HH_mm]);
     if(dateTime.day == today.day){
       return "Hôm nay lúc $hourString";
     }
     else if(dateTime.day == yesterday.day){
       return "Hôm qua lúc $hourString";
     }
     else if(dateTime.day == theDayAfterTomorrow.day){
       return "Hôm kia lúc $hourString";
     }
       return "${isoDateToString(isoDate: time ??'',dateTimeFormat: dOEMap[DateFormatCustom.DD_MM_YY])} lúc $hourString";
    }
    static String imageNetworkPath({String? filedId}){
      return "$linkImage$filedId";
    }
  }
