import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:myhoangtuan/core/repository/http_manager.dart';

import '../../core/repository/api_manager.dart';

class InitialBinding implements Bindings {
  @override
  void dependencies() {
    // khoi tao http truyên di
    Get.put<HttpManager>(HttpManager(Dio(),Dio(), Dio(),Dio(),Dio()), permanent: true);
    // khoi tao quan ly restful api call tới http manager
    Get.put<ApiManager>(ApiManager(Get.find<HttpManager>()), permanent: true);
  }
}
