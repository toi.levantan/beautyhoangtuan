import 'package:shared_preferences/shared_preferences.dart';

class PreferenceUtils {

  static SharedPreferences? _prefsInstance;

  static init() async {
    _prefsInstance = await SharedPreferences.getInstance();
    return _prefsInstance;
  }

  static String? getString(String key) {
    return _prefsInstance?.getString(key);
  }

  static Future setString(String key, String? value) async {
    return _prefsInstance?.setString(key, value??'');
  }
  static Future setBool(String key, bool value) async {
    return _prefsInstance?.setBool(key, value);
  }
  static bool? getBool(String key) {
    return _prefsInstance?.getBool(key);
  }
  static clearAllSharePre() async {
    return _prefsInstance?.clear();
  }
  static remove(String key)async{
    return _prefsInstance?.remove(key);
  }

}
