import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:myhoangtuan/utils/path/color_path.dart';
import 'package:quickalert/models/quickalert_type.dart';
import 'package:quickalert/widgets/quickalert_dialog.dart';

class DialogQuickAlert {
  static confirmDialog(BuildContext context, {String? title, String? message, Function? confirm, String? cancelText,String? confirmText}) {
    return QuickAlert.show(context: context, type: QuickAlertType.confirm,
    cancelBtnText: cancelText ?? "Hủy",
    confirmBtnText: confirmText ??'Đồng ý',
    text: message,

    title: title,
    confirmBtnColor: colorIcon,
    onCancelBtnTap: (){
      Get.back();
    },
    onConfirmBtnTap: (){
      if(confirm != null){
        confirm.call();
      }
    });
  }

}
