import 'package:myhoangtuan/model/booking/performer.dart';
import 'package:myhoangtuan/model/service/res_services_dto.dart';

class HistoryBookingDto {
  HistoryBookingDto({
      this.content, 
      this.totalPages, 
      this.pageSize, 
      this.currentPage, 
      this.totalElements, 
      this.sort, 
      this.pageable, 
      this.first, 
      this.last, 
      this.empty,});

  HistoryBookingDto.fromJson(dynamic json) {
    if (json['content'] != null) {
      content = [];
      json['content'].forEach((v) {
        content?.add(ContentHis.fromJson(v));
      });
    }
    totalPages = json['totalPages'];
    pageSize = json['pageSize'];
    currentPage = json['currentPage'];
    totalElements = json['totalElements'];
    sort = json['sort'];
    pageable = json['pageable'];
    first = json['first'];
    last = json['last'];
    empty = json['empty'];
  }
  List<ContentHis>? content;
  int? totalPages;
  int? pageSize;
  int? currentPage;
  int? totalElements;
  dynamic sort;
  dynamic pageable;
  bool? first;
  bool? last;
  bool? empty;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (content != null) {
      map['content'] = content?.map((v) => v.toJson()).toList();
    }
    map['totalPages'] = totalPages;
    map['pageSize'] = pageSize;
    map['currentPage'] = currentPage;
    map['totalElements'] = totalElements;
    map['sort'] = sort;
    map['pageable'] = pageable;
    map['first'] = first;
    map['last'] = last;
    map['empty'] = empty;
    return map;
  }

}

class ContentHis {
  ContentHis({
      this.id, 
      this.status,
      this.packageType,
      this.organizationId,
      this.organizationBranchId, 
      this.performerName,
      this.loyaltyCustomer,
      this.organizationBranchName,
      this.departmentName,
      this.beautyBookingDetails,});

  ContentHis.fromJson(dynamic json) {
    id = json['id'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    performerName = json['performerName'];
    departmentName = json['departmentName'];
    organizationBranchName = json['organizationBranchName'];
    status = json['status'];
    packageType = json['packageType'];
    loyaltyCustomer = json['loyaltyCustomer'] != null ? LoyaltyCustomer.fromJson(json['loyaltyCustomer']) : null;
    if (json['beautyBookingDetails'] != null) {
      beautyBookingDetails = [];
      json['beautyBookingDetails'].forEach((v) {
        beautyBookingDetails?.add(BeautyBookingDetails.fromJson(v));
      });
    }
  }
  int? id;
  int? organizationId;
  int? organizationBranchId;
  int? status;
  int? packageType;
  String? performerName;
  String? departmentName;
  String? organizationBranchName;
  LoyaltyCustomer? loyaltyCustomer;
  List<BeautyBookingDetails>? beautyBookingDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    if (loyaltyCustomer != null) {
      map['loyaltyCustomer'] = loyaltyCustomer?.toJson();
    }
    if (beautyBookingDetails != null) {
      map['beautyBookingDetails'] = beautyBookingDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class BeautyBookingDetails {
  BeautyBookingDetails({
      this.id, 
      this.beautyServicePackage, 
      this.beautyPackageCombo, 
      this.beautyService, 
      this.performerId, 
      this.bookingDate, 
      this.startTime, 
      this.endTime, 
      this.status, 
      this.performer, 
      this.performerName, 
      this.departmentId,
      this.departmentName,
      this.bookingDateStr,});

  BeautyBookingDetails.fromJson(dynamic json) {
    id = json['id'];
    beautyServicePackage = json['beautyServicePackage'] != null ? BeautyServicePackage.fromJson(json['beautyServicePackage']) : null;
    beautyPackageCombo = json['beautyPackageCombo'] != null ? BeautyPackageCombo.fromJson(json['beautyPackageCombo']) : null;
    beautyService = json['beautyService'];
    performerId = json['performerId'];
    bookingDate = json['bookingDate'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    status = json['status'];
    performer =  json['performer'] != null ? Performer.fromJson(json['performer']): null;
    performerName = json['performerName'];
    departmentName = json['departmentName'];
    bookingDateStr = json['bookingDateStr'];
    departmentId = json['departmentId'];
  }
  int? id;
  BeautyServicePackage? beautyServicePackage;
  BeautyPackageCombo? beautyPackageCombo;
  dynamic beautyService;
  dynamic performerId;
  dynamic departmentId;
  String? bookingDate;
  String? departmentName;
  String? startTime;
  dynamic endTime;
  dynamic status;
  dynamic performer;
  dynamic performerName;
  String? bookingDateStr;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    if (beautyServicePackage != null) {
      map['beautyServicePackage'] = beautyServicePackage?.toJson();
    }
    if (beautyPackageCombo != null) {
      map['beautyPackageCombo'] = beautyPackageCombo?.toJson();
    }
    map['beautyService'] = beautyService;
    map['performerId'] = performerId;
    map['bookingDate'] = bookingDate;
    map['startTime'] = startTime;
    map['endTime'] = endTime;
    map['status'] = status;
    map['performer'] = performer;
    map['performerName'] = performerName;
    map['bookingDateStr'] = bookingDateStr;
    return map;
  }

}

class BeautyPackageCombo {
  BeautyPackageCombo({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.name, 
      this.code, 
      this.description, 
      this.organizationId, 
      this.organizationBranchId, 
      this.point, 
      this.price, 
      this.beautyPackageComboDetails,});

  BeautyPackageCombo.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    name = json['name'];
    code = json['code'];
    description = json['description'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    point = json['point'];
    price = json['price'];
    if (json['beautyPackageComboDetails'] != null) {
      beautyPackageComboDetails = [];
      json['beautyPackageComboDetails'].forEach((v) {
        beautyPackageComboDetails?.add(BeautyPackageComboDetails.fromJson(v));
      });
    }
  }
  int? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  String? name;
  String? code;
  dynamic description;
  int? organizationId;
  int? organizationBranchId;
  dynamic point;
  int? price;
  List<BeautyPackageComboDetails>? beautyPackageComboDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['name'] = name;
    map['code'] = code;
    map['description'] = description;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['point'] = point;
    map['price'] = price;
    if (beautyPackageComboDetails != null) {
      map['beautyPackageComboDetails'] = beautyPackageComboDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class BeautyPackageComboDetails {
  BeautyPackageComboDetails({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.beautyServicePackage, 
      this.times,});

  BeautyPackageComboDetails.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    beautyServicePackage = json['beautyServicePackage'] != null ? BeautyServicePackage.fromJson(json['beautyServicePackage']) : null;
    times = json['times'];
  }
  int? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  BeautyServicePackage? beautyServicePackage;
  int? times;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    if (beautyServicePackage != null) {
      map['beautyServicePackage'] = beautyServicePackage?.toJson();
    }
    map['times'] = times;
    return map;
  }

}

class BeautyServicePackage {
  BeautyServicePackage({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.organizationId, 
      this.name, 
      this.code, 
      this.unit, 
      this.unitPrice, 
      this.description, 
      this.time, 
      this.point, 
      this.times, 
      this.status, 
      this.imageStores,});

  BeautyServicePackage.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    organizationId = json['organizationId'];
    name = json['name'];
    code = json['code'];
    unit = json['unit'];
    unitPrice = json['unitPrice'];
    description = json['description'];
    time = json['time'];
    point = json['point'];
    times = json['times'];
    status = json['status'];
    if (json['imageStores'] != null) {
      imageStores = [];
      json['imageStores'].forEach((v) {
        imageStores?.add(ImageStores.fromJson(v));
      });
    }
  }
  int? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  int? organizationId;
  String? name;
  String? code;
  String? unit;
  int? unitPrice;
  String? description;
  int? time;
  dynamic point;
  dynamic times;
  int? status;
  List<ImageStores>? imageStores;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['organizationId'] = organizationId;
    map['name'] = name;
    map['code'] = code;
    map['unit'] = unit;
    map['unitPrice'] = unitPrice;
    map['description'] = description;
    map['time'] = time;
    map['point'] = point;
    map['times'] = times;
    map['status'] = status;
    if (imageStores != null) {
      map['imageStores'] = imageStores?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}


class LoyaltyCustomer {
  LoyaltyCustomer({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.ethnic, 
      this.country, 
      this.city, 
      this.district, 
      this.street, 
      this.houseNumber, 
      this.fullAddress, 
      this.phoneNumber, 
      this.job, 
      this.email, 
      this.code, 
      this.firstName, 
      this.lastName, 
      this.fullName, 
      this.birthday, 
      this.weight, 
      this.gender, 
      this.passport, 
      this.organizationId, 
      this.organizationBranchId, 
      this.username, 
      this.card, 
      this.enabled,});

  LoyaltyCustomer.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    ethnic = json['ethnic'];
    country = json['country'];
    city = json['city'];
    district = json['district'];
    street = json['street'];
    houseNumber = json['houseNumber'];
    fullAddress = json['fullAddress'];
    phoneNumber = json['phoneNumber'];
    job = json['job'];
    email = json['email'];
    code = json['code'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    fullName = json['fullName'];
    birthday = json['birthday'];
    weight = json['weight'];
    gender = json['gender'];
    passport = json['passport'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    username = json['username'];
    card = json['card'] != null ? CardHis.fromJson(json['card']) : null;
    enabled = json['enabled'];
  }
  int? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  dynamic ethnic;
  dynamic country;
  dynamic city;
  dynamic district;
  dynamic street;
  dynamic houseNumber;
  dynamic fullAddress;
  String? phoneNumber;
  dynamic job;
  String? email;
  String? code;
  dynamic firstName;
  dynamic lastName;
  String? fullName;
  String? birthday;
  dynamic weight;
  String? gender;
  dynamic passport;
  int? organizationId;
  int? organizationBranchId;
  dynamic username;
  CardHis? card;
  bool? enabled;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['ethnic'] = ethnic;
    map['country'] = country;
    map['city'] = city;
    map['district'] = district;
    map['street'] = street;
    map['houseNumber'] = houseNumber;
    map['fullAddress'] = fullAddress;
    map['phoneNumber'] = phoneNumber;
    map['job'] = job;
    map['email'] = email;
    map['code'] = code;
    map['firstName'] = firstName;
    map['lastName'] = lastName;
    map['fullName'] = fullName;
    map['birthday'] = birthday;
    map['weight'] = weight;
    map['gender'] = gender;
    map['passport'] = passport;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['username'] = username;
    if (card != null) {
      map['card'] = card?.toJson();
    }
    map['enabled'] = enabled;
    return map;
  }

}

class CardHis {
  CardHis({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.number, 
      this.points,});

  CardHis.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    number = json['number'];
    points = json['points'];
  }
  int? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  int? number;
  int? points;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['number'] = number;
    map['points'] = points;
    return map;
  }

}