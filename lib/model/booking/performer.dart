

class Performer {
  Performer({
      this.id, 
      this.firstName, 
      this.lastName, 
      this.fullName, 
      this.email, 
      this.phoneNumber, 
      this.organizationId, 
      this.gender, 
      this.birthday, 
      this.city, 
      this.district, 
      this.street, 
      this.houseNumber, 
      this.fullAddress, 
      this.salary, 
      this.organizationBranchId, 
      this.employeeId, 
      this.employeeCode, 
      this.position, 
      this.type, 
      this.faceId, 
      this.privileges, 
      this.positions, 
      this.departmentDTO, 
      this.departmentType, 
      this.imageStore, 
      this.imageStores,
      this.avatar,
      this.username, 
      this.enabled, 
      this.activeStatus, 
      this.cardId, 
      this.startWork, 
      this.employeeWorkShiftByDays, 
      this.onLeaveDTOS,});

  Performer.fromJson(dynamic json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    fullName = json['fullName'];
    email = json['email'];
    phoneNumber = json['phoneNumber'];
    organizationId = json['organizationId'];
    gender = json['gender'];
    birthday = json['birthday'];
    city = json['city'];
    district = json['district'];
    street = json['street'];
    houseNumber = json['houseNumber'];
    fullAddress = json['fullAddress'];
    salary = json['salary'];
    organizationBranchId = json['organizationBranchId'];
    employeeId = json['employeeId'];
    employeeCode = json['employeeCode'];
    position = json['position'];
    type = json['type'];
    faceId = json['faceId'];
    privileges = json['privileges'];
    positions = json['positions'];
    departmentDTO = json['departmentDTO'] != null ? DepartmentDto.fromJson(json['departmentDTO']) : null;
    departmentType = json['departmentType'];
    if (json['imageStore'] != null) {
      imageStore = [];
      json['imageStore'].forEach((v) {
        imageStore?.add(ImageStore.fromJson(v));
      });
    }
    if (json['imageStores'] != null) {
      imageStores = [];
      json['imageStores'].forEach((v) {
        imageStores?.add(ImageStore.fromJson(v));
      });
    }
    avatar = json['avatar'];
    username = json['username'];
    enabled = json['enabled'];
    activeStatus = json['activeStatus'];
    cardId = json['cardId'];
    startWork = json['startWork'];
    employeeWorkShiftByDays = json['employeeWorkShiftByDays'];
    onLeaveDTOS = json['onLeaveDTOS'];
  }
  int? id;
  String? firstName;
  String? lastName;
  String? fullName;
  String? email;
  String? phoneNumber;
  int? organizationId;
  int? gender;
  int? birthday;
  String? city;
  String? district;
  String? street;
  String? houseNumber;
  String? fullAddress;
  int? salary;
  int? organizationBranchId;
  int? employeeId;
  String? employeeCode;
  String? position;
  int? type;
  dynamic faceId;
  dynamic privileges;
  dynamic positions;
  DepartmentDto? departmentDTO;
  int? departmentType;
  List<ImageStore>? imageStore;
  List<ImageStore>? imageStores;
  dynamic avatar;
  String? username;
  bool? enabled;
  dynamic activeStatus;
  dynamic cardId;
  String? startWork;
  dynamic employeeWorkShiftByDays;
  dynamic onLeaveDTOS;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['firstName'] = firstName;
    map['lastName'] = lastName;
    map['fullName'] = fullName;
    map['email'] = email;
    map['phoneNumber'] = phoneNumber;
    map['organizationId'] = organizationId;
    map['gender'] = gender;
    map['birthday'] = birthday;
    map['city'] = city;
    map['district'] = district;
    map['street'] = street;
    map['houseNumber'] = houseNumber;
    map['fullAddress'] = fullAddress;
    map['salary'] = salary;
    map['organizationBranchId'] = organizationBranchId;
    map['employeeId'] = employeeId;
    map['employeeCode'] = employeeCode;
    map['position'] = position;
    map['type'] = type;
    map['faceId'] = faceId;
    map['privileges'] = privileges;
    map['positions'] = positions;
    if (departmentDTO != null) {
      map['departmentDTO'] = departmentDTO?.toJson();
    }
    map['departmentType'] = departmentType;
    if (imageStore != null) {
      map['imageStore'] = imageStore?.map((v) => v.toJson()).toList();
    }
    if (imageStores != null) {
      map['imageStores'] = imageStores?.map((v) => v.toJson()).toList();
    }
    map['avatar'] = avatar;
    map['username'] = username;
    map['enabled'] = enabled;
    map['activeStatus'] = activeStatus;
    map['cardId'] = cardId;
    map['startWork'] = startWork;
    map['employeeWorkShiftByDays'] = employeeWorkShiftByDays;
    map['onLeaveDTOS'] = onLeaveDTOS;
    return map;
  }

}

class ImageStore {
  ImageStore({
      this.id, 
      this.fileId, 
      this.fileName, 
      this.size, 
      this.url, 
      this.determinedType, 
      this.etag,});

  ImageStore.fromJson(dynamic json) {
    id = json['id'];
    fileId = json['fileId'];
    fileName = json['fileName'];
    size = json['size'];
    url = json['url'];
    determinedType = json['determinedType'];
    etag = json['etag'];
  }
  int? id;
  String? fileId;
  String? fileName;
  int? size;
  String? url;
  dynamic? determinedType;
  String? etag;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['fileId'] = fileId;
    map['fileName'] = fileName;
    map['size'] = size;
    map['url'] = url;
    map['determinedType'] = determinedType;
    map['etag'] = etag;
    return map;
  }

}

class DepartmentDto {
  DepartmentDto({
      this.id, 
      this.organizationId, 
      this.organizationBranchId, 
      this.departmentName,});

  DepartmentDto.fromJson(dynamic json) {
    id = json['id'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    departmentName = json['departmentName'];
  }
  int? id;
  int? organizationId;
  int? organizationBranchId;
  String? departmentName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['departmentName'] = departmentName;
    return map;
  }

}