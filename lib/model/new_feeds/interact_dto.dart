/// commentHistoryId : 0
/// interactionType : 0
/// newsFeedId : 0

class InteractDto {
  InteractDto({
      this.commentHistoryId, 
      this.interactionType, 
      this.newsFeedId,});

  InteractDto.fromJson(dynamic json) {
    commentHistoryId = json['commentHistoryId'];
    interactionType = json['interactionType'];
    newsFeedId = json['newsFeedId'];
  }
  num? commentHistoryId;
  num? interactionType;
  num? newsFeedId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['commentHistoryId'] = commentHistoryId;
    map['interactionType'] = interactionType;
    map['newsFeedId'] = newsFeedId;
    return map;
  }

}