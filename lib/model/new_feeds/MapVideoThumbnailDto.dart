class MapVideoThumbnailDto {
  MapVideoThumbnailDto({
      this.thumbnailName, 
      this.videoName,});

  MapVideoThumbnailDto.fromJson(dynamic json) {
    thumbnailName = json['thumbnailName'];
    videoName = json['videoName'];
  }
  String? thumbnailName;
  String? videoName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['thumbnailName'] = thumbnailName;
    map['videoName'] = videoName;
    return map;
  }

}