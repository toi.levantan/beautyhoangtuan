import 'new_feed_dto.dart';

/// content : [{"id":12,"createdDate":"2023-08-07T07:52:49.284+0000","createdBy":"intlabs","parentId":null,"beautyNewsFeedId":null,"interactionCount":null,"content":" 24/7/2023 #muavui247 bajskdbasjkdb #muavui248 andasjndsajk #muavui247,a","childList":[{"id":13,"createdDate":"2023-08-07T07:53:08.131+0000","createdBy":"0964908363@gmail.com","parentId":12,"beautyNewsFeedId":null,"interactionCount":null,"content":"Tạo bình luận mới","childList":null,"avatar":null,"fullName":"Huy Hiệu","beautyAttachments":[{"id":504,"createdDate":"2023-08-07T07:53:08.078+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:53:08.078+0000","modifiedBy":"0964908363@gmail.com","fileId":"1,08a9bfa52a2f","fileName":"7ef96e5506ebb56285fee61948f7a87f.jpeg","size":367483,"url":"https://cdnimage.inviv.vn/1,08a9bfa52a2f","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":7,"objectType":null,"status":null,"parentId":null,"thumbnail":"1,08a9bfa52a2f","etag":"\"4de52a63\""},{"id":505,"createdDate":"2023-08-07T07:53:08.122+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:53:08.122+0000","modifiedBy":"0964908363@gmail.com","fileId":"7,08aaa7587721","fileName":"7ef96e5506ebb56285fee61948f7a87f.jpeg","size":367483,"url":"https://cdnimage.inviv.vn/7,08aaa7587721","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"parentId":null,"thumbnail":"7,08aaa7587721","etag":"\"4de52a63\""}],"liked":false}],"avatar":null,"fullName":"Admin organization","beautyAttachments":[{"id":502,"createdDate":"2023-08-07T07:52:47.958+0000","createdBy":"intlabs","modifiedDate":"2023-08-07T07:52:47.958+0000","modifiedBy":"intlabs","fileId":"7,08a7cd8a3af3","fileName":"istockphoto-493739042-612x612.jpg","size":26438,"url":"https://cdnimage.inviv.vn/7,08a7cd8a3af3","aliasName":null,"organizationId":1,"organizationBranchId":null,"fileType":7,"objectType":null,"status":null,"parentId":null,"thumbnail":"7,08a7cd8a3af3","etag":"\"5b1cc9eb\""},{"id":503,"createdDate":"2023-08-07T07:52:48.492+0000","createdBy":"intlabs","modifiedDate":"2023-08-07T07:52:48.492+0000","modifiedBy":"intlabs","fileId":"2,08a8416c9dec","fileName":"istockphoto-493739042-612x612.jpg","size":26438,"url":"https://cdnimage.inviv.vn/2,08a8416c9dec","aliasName":null,"organizationId":1,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"parentId":null,"thumbnail":"2,08a8416c9dec","etag":"\"5b1cc9eb\""}],"liked":false},{"id":11,"createdDate":"2023-08-07T07:52:43.189+0000","createdBy":"0964908363@gmail.com","parentId":null,"beautyNewsFeedId":null,"interactionCount":null,"content":"tao binh luan moi","childList":[],"avatar":null,"fullName":"Huy Hiệu","beautyAttachments":[{"id":501,"createdDate":"2023-08-07T07:52:43.176+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:52:43.176+0000","modifiedBy":"0964908363@gmail.com","fileId":"3,08a61b2852a3","fileName":"Screen Shot 2023-08-02 at 14.24.29.png","size":689682,"url":"https://cdnimage.inviv.vn/3,08a61b2852a3","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"parentId":null,"thumbnail":"3,08a61b2852a3","etag":"\"4b25ba62\""},{"id":500,"createdDate":"2023-08-07T07:52:43.118+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:52:43.118+0000","modifiedBy":"0964908363@gmail.com","fileId":"7,08a513a4cd29","fileName":"Screen Shot 2023-08-02 at 14.24.29.png","size":689682,"url":"https://cdnimage.inviv.vn/7,08a513a4cd29","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":7,"objectType":null,"status":null,"parentId":null,"thumbnail":"7,08a513a4cd29","etag":"\"4b25ba62\""}],"liked":false}]
/// totalPages : 1
/// pageSize : 0
/// currentPage : 0
/// totalElements : 3
/// sort : null
/// pageable : null
/// first : false
/// last : false
/// empty : false

class CommentFeedbackDto {
  CommentFeedbackDto({
      this.content, 
      this.totalPages, 
      this.pageSize, 
      this.currentPage, 
      this.totalElements, 
      this.sort, 
      this.pageable, 
      this.first, 
      this.last, 
      this.empty,});

  CommentFeedbackDto.fromJson(dynamic json) {
    if (json['content'] != null) {
      content = [];
      json['content'].forEach((v) {
        content?.add(ContentFeedBack.fromJson(v));
      });
    }
    totalPages = json['totalPages'];
    pageSize = json['pageSize'];
    currentPage = json['currentPage'];
    totalElements = json['totalElements'];
    sort = json['sort'];
    pageable = json['pageable'];
    first = json['first'];
    last = json['last'];
    empty = json['empty'];
  }
  List<ContentFeedBack>? content;
  int? totalPages;
  int? pageSize;
  int? currentPage;
  int? totalElements;
  dynamic sort;
  dynamic pageable;
  bool? first;
  bool? last;
  bool? empty;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (content != null) {
      map['content'] = content?.map((v) => v.toJson()).toList();
    }
    map['totalPages'] = totalPages;
    map['pageSize'] = pageSize;
    map['currentPage'] = currentPage;
    map['totalElements'] = totalElements;
    map['sort'] = sort;
    map['pageable'] = pageable;
    map['first'] = first;
    map['last'] = last;
    map['empty'] = empty;
    return map;
  }

}

/// id : 12
/// createdDate : "2023-08-07T07:52:49.284+0000"
/// createdBy : "intlabs"
/// parentId : null
/// beautyNewsFeedId : null
/// interactionCount : null
/// content : " 24/7/2023 #muavui247 bajskdbasjkdb #muavui248 andasjndsajk #muavui247,a"
/// childList : [{"id":13,"createdDate":"2023-08-07T07:53:08.131+0000","createdBy":"0964908363@gmail.com","parentId":12,"beautyNewsFeedId":null,"interactionCount":null,"content":"Tạo bình luận mới","childList":null,"avatar":null,"fullName":"Huy Hiệu","beautyAttachments":[{"id":504,"createdDate":"2023-08-07T07:53:08.078+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:53:08.078+0000","modifiedBy":"0964908363@gmail.com","fileId":"1,08a9bfa52a2f","fileName":"7ef96e5506ebb56285fee61948f7a87f.jpeg","size":367483,"url":"https://cdnimage.inviv.vn/1,08a9bfa52a2f","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":7,"objectType":null,"status":null,"parentId":null,"thumbnail":"1,08a9bfa52a2f","etag":"\"4de52a63\""},{"id":505,"createdDate":"2023-08-07T07:53:08.122+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:53:08.122+0000","modifiedBy":"0964908363@gmail.com","fileId":"7,08aaa7587721","fileName":"7ef96e5506ebb56285fee61948f7a87f.jpeg","size":367483,"url":"https://cdnimage.inviv.vn/7,08aaa7587721","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"parentId":null,"thumbnail":"7,08aaa7587721","etag":"\"4de52a63\""}],"liked":false}]
/// avatar : null
/// fullName : "Admin organization"
/// beautyAttachments : [{"id":502,"createdDate":"2023-08-07T07:52:47.958+0000","createdBy":"intlabs","modifiedDate":"2023-08-07T07:52:47.958+0000","modifiedBy":"intlabs","fileId":"7,08a7cd8a3af3","fileName":"istockphoto-493739042-612x612.jpg","size":26438,"url":"https://cdnimage.inviv.vn/7,08a7cd8a3af3","aliasName":null,"organizationId":1,"organizationBranchId":null,"fileType":7,"objectType":null,"status":null,"parentId":null,"thumbnail":"7,08a7cd8a3af3","etag":"\"5b1cc9eb\""},{"id":503,"createdDate":"2023-08-07T07:52:48.492+0000","createdBy":"intlabs","modifiedDate":"2023-08-07T07:52:48.492+0000","modifiedBy":"intlabs","fileId":"2,08a8416c9dec","fileName":"istockphoto-493739042-612x612.jpg","size":26438,"url":"https://cdnimage.inviv.vn/2,08a8416c9dec","aliasName":null,"organizationId":1,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"parentId":null,"thumbnail":"2,08a8416c9dec","etag":"\"5b1cc9eb\""}]
/// liked : false

class ContentFeedBack {
  ContentFeedBack({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.parentId, 
      this.beautyNewsFeedId, 
      this.interactionCount, 
      this.content, 
      this.childList, 
      this.avatar, 
      this.fullName, 
      this.beautyAttachments, 
      this.liked,});

  ContentFeedBack.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    parentId = json['parentId'];
    beautyNewsFeedId = json['beautyNewsFeedId'];
    interactionCount = json['interactionCount'];
    content = json['content'];
    if (json['childList'] != null) {
      childList = [];
      json['childList'].forEach((v) {
        childList?.add(ChildList.fromJson(v));
      });
    }
    avatar = json['avatar'];
    fullName = json['fullName'];
    if (json['beautyAttachments'] != null) {
      beautyAttachments = [];
      json['beautyAttachments'].forEach((v) {
        beautyAttachments?.add(BeautyAttachments.fromJson(v));
      });
    }
    liked = json['liked'];
  }
  int? id;
  String? createdDate;
  String? createdBy;
  dynamic parentId;
  dynamic beautyNewsFeedId;
  dynamic interactionCount;
  String? content;
  List<ChildList>? childList;
  dynamic avatar;
  String? fullName;
  List<BeautyAttachments>? beautyAttachments;
  bool? liked;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['parentId'] = parentId;
    map['beautyNewsFeedId'] = beautyNewsFeedId;
    map['interactionCount'] = interactionCount;
    map['content'] = content;
    if (childList != null) {
      map['childList'] = childList?.map((v) => v.toJson()).toList();
    }
    map['avatar'] = avatar;
    map['fullName'] = fullName;
    if (beautyAttachments != null) {
      map['beautyAttachments'] = beautyAttachments?.map((v) => v.toJson()).toList();
    }
    map['liked'] = liked;
    return map;
  }

}

/// id : 13
/// createdDate : "2023-08-07T07:53:08.131+0000"
/// createdBy : "0964908363@gmail.com"
/// parentId : 12
/// beautyNewsFeedId : null
/// interactionCount : null
/// content : "Tạo bình luận mới"
/// childList : null
/// avatar : null
/// fullName : "Huy Hiệu"
/// beautyAttachments : [{"id":504,"createdDate":"2023-08-07T07:53:08.078+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:53:08.078+0000","modifiedBy":"0964908363@gmail.com","fileId":"1,08a9bfa52a2f","fileName":"7ef96e5506ebb56285fee61948f7a87f.jpeg","size":367483,"url":"https://cdnimage.inviv.vn/1,08a9bfa52a2f","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":7,"objectType":null,"status":null,"parentId":null,"thumbnail":"1,08a9bfa52a2f","etag":"\"4de52a63\""},{"id":505,"createdDate":"2023-08-07T07:53:08.122+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-08-07T07:53:08.122+0000","modifiedBy":"0964908363@gmail.com","fileId":"7,08aaa7587721","fileName":"7ef96e5506ebb56285fee61948f7a87f.jpeg","size":367483,"url":"https://cdnimage.inviv.vn/7,08aaa7587721","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"parentId":null,"thumbnail":"7,08aaa7587721","etag":"\"4de52a63\""}]
/// liked : false

class ChildList {
  ChildList({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.parentId, 
      this.beautyNewsFeedId, 
      this.interactionCount, 
      this.content, 
      this.childList, 
      this.avatar, 
      this.fullName, 
      this.beautyAttachments, 
      this.liked,});

  ChildList.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    parentId = json['parentId'];
    beautyNewsFeedId = json['beautyNewsFeedId'];
    interactionCount = json['interactionCount'];
    content = json['content'];
    childList = json['childList'];
    avatar = json['avatar'];
    fullName = json['fullName'];
    if (json['beautyAttachments'] != null) {
      beautyAttachments = [];
      json['beautyAttachments'].forEach((v) {
        beautyAttachments?.add(BeautyAttachments.fromJson(v));
      });
    }
    liked = json['liked'];
  }
  int? id;
  String? createdDate;
  String? createdBy;
  int? parentId;
  dynamic beautyNewsFeedId;
  dynamic interactionCount;
  String? content;
  dynamic childList;
  dynamic avatar;
  String? fullName;
  List<BeautyAttachments>? beautyAttachments;
  bool? liked;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['parentId'] = parentId;
    map['beautyNewsFeedId'] = beautyNewsFeedId;
    map['interactionCount'] = interactionCount;
    map['content'] = content;
    map['childList'] = childList;
    map['avatar'] = avatar;
    map['fullName'] = fullName;
    if (beautyAttachments != null) {
      map['beautyAttachments'] = beautyAttachments?.map((v) => v.toJson()).toList();
    }
    map['liked'] = liked;
    return map;
  }

}

/// id : 504
/// createdDate : "2023-08-07T07:53:08.078+0000"
/// createdBy : "0964908363@gmail.com"
/// modifiedDate : "2023-08-07T07:53:08.078+0000"
/// modifiedBy : "0964908363@gmail.com"
/// fileId : "1,08a9bfa52a2f"
/// fileName : "7ef96e5506ebb56285fee61948f7a87f.jpeg"
/// size : 367483
/// url : "https://cdnimage.inviv.vn/1,08a9bfa52a2f"
/// aliasName : null
/// organizationId : 459
/// organizationBranchId : null
/// fileType : 7
/// objectType : null
/// status : null
/// parentId : null
/// thumbnail : "1,08a9bfa52a2f"
/// etag : "\"4de52a63\""
