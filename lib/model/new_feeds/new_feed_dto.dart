/// content : [{"id":9,"createdDate":"2023-07-31T11:50:37.359+0000","createdBy":"0964908363@gmail.com","content":"Đang cảm thấy mệt mỏi 😤😫😫😫","status":1,"rejectReason":null,"interactionCount":0,"beautyInteractionHistories":[],"beautyCommentHistories":[],"beautyAttachments":[{"id":404,"createdDate":"2023-07-31T11:50:37.358+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-07-31T11:50:37.358+0000","modifiedBy":"0964908363@gmail.com","fileId":"1,0825f78af664","fileName":"IMG_0209.png","size":773342,"url":"https://cdnimage.inviv.vn/1,0825f78af664","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"etag":"\"0d323310\""}]},{"id":8,"createdDate":"2023-07-31T11:49:39.742+0000","createdBy":"0964908363@gmail.com","content":"Đang cảm thấy đói 😠😠😠😠😠😠😫😤😤","status":1,"rejectReason":null,"interactionCount":0,"beautyInteractionHistories":[],"beautyCommentHistories":[],"beautyAttachments":[{"id":403,"createdDate":"2023-07-31T11:49:39.739+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-07-31T11:49:39.739+0000","modifiedBy":"0964908363@gmail.com","fileId":"6,08244cf5e190","fileName":"IMG_0209.png","size":773342,"url":"https://cdnimage.inviv.vn/6,08244cf5e190","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"etag":"\"0d323310\""}]},{"id":7,"createdDate":"2023-07-31T11:47:37.041+0000","createdBy":"0964908363@gmail.com","content":"","status":1,"rejectReason":null,"interactionCount":0,"beautyInteractionHistories":[],"beautyCommentHistories":[],"beautyAttachments":[]},{"id":6,"createdDate":"2023-07-31T11:43:30.129+0000","createdBy":"0964908363@gmail.com","content":"","status":1,"rejectReason":null,"interactionCount":0,"beautyInteractionHistories":[],"beautyCommentHistories":[],"beautyAttachments":[]},{"id":5,"createdDate":"2023-07-31T11:42:59.425+0000","createdBy":"0964908363@gmail.com","content":"","status":1,"rejectReason":null,"interactionCount":0,"beautyInteractionHistories":[],"beautyCommentHistories":[],"beautyAttachments":[]},{"id":2,"createdDate":"2023-07-28T07:25:25.761+0000","createdBy":"hoangtuan_test","content":"muavui","status":2,"rejectReason":"abc","interactionCount":0,"beautyInteractionHistories":[],"beautyCommentHistories":[],"beautyAttachments":[{"id":396,"createdDate":"2023-07-28T07:25:25.752+0000","createdBy":"hoangtuan_test","modifiedDate":"2023-07-28T07:25:25.752+0000","modifiedBy":"hoangtuan_test","fileId":"2,081d4ef7c9da","fileName":"istockphoto-493739042-612x612.jpg","size":26438,"url":"https://cdnimage.inviv.vn/2,081d4ef7c9da","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"etag":"\"5b1cc9eb\""}]},{"id":3,"createdDate":"2023-07-28T08:15:49.296+0000","createdBy":"hoangtuan_test","content":"đm 24/7/2023 #muavui247 bajskdbasjkdb #muavui248 andasjndsajk #muavui247","status":2,"rejectReason":null,"interactionCount":0,"beautyInteractionHistories":[],"beautyCommentHistories":[{"id":3,"createdDate":"2023-07-28T09:23:30.258+0000","createdBy":"hoangtuan_test","modifiedDate":"2023-07-28T09:23:30.258+0000","modifiedBy":"hoangtuan_test","parentId":null,"content":"vui vcl 24/7/2023 #muavui247 bajskdbasjkdb #muavui248 andasjndsajk #muavui247","interactionCount":null}],"beautyAttachments":[{"id":397,"createdDate":"2023-07-28T08:15:49.277+0000","createdBy":"hoangtuan_test","modifiedDate":"2023-07-28T08:15:49.277+0000","modifiedBy":"hoangtuan_test","fileId":"4,081e3d64065b","fileName":"istockphoto-493739042-612x612.jpg","size":26438,"url":"https://cdnimage.inviv.vn/4,081e3d64065b","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"etag":"\"5b1cc9eb\""}]}]
/// totalPages : 1
/// pageSize : 0
/// currentPage : 0
/// totalElements : 7
/// sort : null
/// pageable : null
/// first : false
/// last : false
/// empty : false

class NewFeedDto {
  NewFeedDto({
    this.content,
    this.totalPages,
    this.pageSize,
    this.currentPage,
    this.totalElements,
    this.sort,
    this.pageable,
    this.first,
    this.last,
    this.empty,
  });

  NewFeedDto.fromJson(dynamic json) {
    if (json['content'] != null) {
      content = [];
      json['content'].forEach((v) {
        content?.add(ContentFeed.fromJson(v));
      });
    }
    totalPages = json['totalPages'];
    pageSize = json['pageSize'];
    currentPage = json['currentPage'];
    totalElements = json['totalElements'];
    sort = json['sort'];
    pageable = json['pageable'];
    first = json['first'];
    last = json['last'];
    empty = json['empty'];
  }

  List<ContentFeed>? content;
  int? totalPages;
  int? pageSize;
  int? currentPage;
  int? totalElements;
  dynamic sort;
  dynamic pageable;
  bool? first;
  bool? last;
  bool? empty;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (content != null) {
      map['content'] = content?.map((v) => v.toJson()).toList();
    }
    map['totalPages'] = totalPages;
    map['pageSize'] = pageSize;
    map['currentPage'] = currentPage;
    map['totalElements'] = totalElements;
    map['sort'] = sort;
    map['pageable'] = pageable;
    map['first'] = first;
    map['last'] = last;
    map['empty'] = empty;
    return map;
  }
}

/// id : 9
/// createdDate : "2023-07-31T11:50:37.359+0000"
/// createdBy : "0964908363@gmail.com"
/// content : "Đang cảm thấy mệt mỏi 😤😫😫😫"
/// status : 1
/// rejectReason : null
/// interactionCount : 0
/// beautyInteractionHistories : []
/// beautyCommentHistories : []
/// beautyAttachments : [{"id":404,"createdDate":"2023-07-31T11:50:37.358+0000","createdBy":"0964908363@gmail.com","modifiedDate":"2023-07-31T11:50:37.358+0000","modifiedBy":"0964908363@gmail.com","fileId":"1,0825f78af664","fileName":"IMG_0209.png","size":773342,"url":"https://cdnimage.inviv.vn/1,0825f78af664","aliasName":null,"organizationId":459,"organizationBranchId":null,"fileType":1,"objectType":null,"status":null,"etag":"\"0d323310\""}]

class ContentFeed {
  ContentFeed(
      {this.id,
      this.createdDate,
      this.createdBy,
      this.content,
      this.status,
      this.rejectReason,
      this.interactionCount,
      this.interactionTam,
      this.beautyInteractionHistories,
      this.beautyCommentHistories,
      this.beautyAttachments,
      this.avatar,
      this.liked,
      this.commentCount,
        this.actionStatus,
      this.fullName});

  ContentFeed.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    content = json['content'];
    status = json['status'];
    rejectReason = json['rejectReason'];
    interactionCount = json['interactionCount'];
    if (json['beautyInteractionHistories'] != null) {
      beautyInteractionHistories = [];
      json['beautyInteractionHistories'].forEach((v) {
        // beautyInteractionHistories?.add(Dynamic.fromJson(v));
      });
    }
    if (json['beautyCommentHistories'] != null) {
      beautyCommentHistories = [];
      json['beautyCommentHistories'].forEach((v) {
        //beautyCommentHistories?.add(Dynamic.fromJson(v));
      });
    }
    if (json['beautyAttachments'] != null) {
      beautyAttachments = [];
      json['beautyAttachments'].forEach((v) {
        beautyAttachments?.add(BeautyAttachments.fromJson(v));
      });
    }
    avatar = json['avatar'];
    fullName = json['fullName'];
    commentCount = json['commentCount'];
    liked = json['liked'];
  }

  int? id;
  String? createdDate;
  String? createdBy;
  String? content;
  num? status;
  dynamic rejectReason;
  int? interactionCount;
  int? interactionTam;
  List<dynamic>? beautyInteractionHistories;
  List<dynamic>? beautyCommentHistories;
  List<BeautyAttachments>? beautyAttachments;
  String? avatar;
  String? fullName;
  int? commentCount;
  bool? liked;
  bool? actionStatus;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['content'] = content;
    map['status'] = status;
    map['rejectReason'] = rejectReason;
    map['interactionCount'] = interactionCount;
    if (beautyInteractionHistories != null) {
      map['beautyInteractionHistories'] =
          beautyInteractionHistories?.map((v) => v.toJson()).toList();
    }
    if (beautyCommentHistories != null) {
      map['beautyCommentHistories'] =
          beautyCommentHistories?.map((v) => v.toJson()).toList();
    }
    if (beautyAttachments != null) {
      map['beautyAttachments'] =
          beautyAttachments?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 404
/// createdDate : "2023-07-31T11:50:37.358+0000"
/// createdBy : "0964908363@gmail.com"
/// modifiedDate : "2023-07-31T11:50:37.358+0000"
/// modifiedBy : "0964908363@gmail.com"
/// fileId : "1,0825f78af664"
/// fileName : "IMG_0209.png"
/// size : 773342
/// url : "https://cdnimage.inviv.vn/1,0825f78af664"
/// aliasName : null
/// organizationId : 459
/// organizationBranchId : null
/// fileType : 1
/// objectType : null
/// status : null
/// etag : "\"0d323310\""

class BeautyAttachments {
  BeautyAttachments({
    this.id,
    this.createdDate,
    this.createdBy,
    this.modifiedDate,
    this.modifiedBy,
    this.fileId,
    this.fileName,
    this.size,
    this.url,
    this.aliasName,
    this.organizationId,
    this.organizationBranchId,
    this.fileType,
    this.objectType,
    this.status,
    this.etag,
    this.thumbnail,
  });

  BeautyAttachments.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    fileId = json['fileId'];
    thumbnail = json['thumbnail'];
    fileName = json['fileName'];
    size = json['size'];
    url = json['url'];
    aliasName = json['aliasName'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    fileType = json['fileType'];
    objectType = json['objectType'];
    status = json['status'];
    etag = json['etag'];
  }

  int? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  String? fileId;
  String? thumbnail;
  String? fileName;
  int? size;
  String? url;
  dynamic aliasName;
  num? organizationId;
  dynamic organizationBranchId;
  int? fileType;
  dynamic objectType;
  dynamic status;
  String? etag;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['fileId'] = fileId;
    map['fileName'] = fileName;
    map['size'] = size;
    map['url'] = url;
    map['aliasName'] = aliasName;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['fileType'] = fileType;
    map['objectType'] = objectType;
    map['status'] = status;
    map['etag'] = etag;
    return map;
  }
}
