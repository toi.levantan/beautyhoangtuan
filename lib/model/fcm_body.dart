class FcmBody {
  FcmBody({
    this.body,
  });

  FcmBody.fromJson(dynamic json) {
    body = json['body'];
  }

  String? body;
}

class Body {
  Body({
    this.id,
    this.content,
    this.screenId,
    this.screenUrl,
  });

  Body.fromJson(dynamic json) {
    id = json['id'];
    content = json['content'];
    screenId = json['screenId'];
    screenUrl = json['screenUrl'];
    totalNotSeen = json['totalNotSeen'];
    params = Params.fromJson(json['params']);
  }

  String? id;
  String? content;
  int? screenId;
  int? totalNotSeen;
  String? screenUrl;
  Params? params;
}

class Params {
  int? organizationId;
  int? organizationBranchId;
  int? id;
  int? employeeId;

  Params({
    this.id,
    this.organizationId,
    this.organizationBranchId,
    this.employeeId,
  });

  Params.fromJson(dynamic json) {
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    id = json['id'];
    employeeId = json['employeeId'];
  }
}
