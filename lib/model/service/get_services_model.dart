/// keySearch : "string"
/// organizationBranchId : 0
/// pageNumber : 0
/// pageSize : 0

class GetServicesModel {
  GetServicesModel({
    this.beautyServicePackageId,
    this.keySearch,
    this.pageNumber,
    this.pageSize,
    this.organizationBranchId,
    this.departmentId,
    this.performerId,
    this.bookingDate,
    this.beautyServiceId,
    this.type,
    this.beautyBookingId,
  });

  GetServicesModel.fromJson(dynamic json) {
    beautyServicePackageId = json['beautyServicePackageId'];
    keySearch = json['keySearch'];
    pageNumber = json['pageNumber'];
    pageSize = json['pageSize'];
    performerId = json['performerId'];
  }

  String? keySearch;
  num? pageNumber;
  num? pageSize;
  int? beautyServicePackageId;
  int? organizationBranchId;
  int? beautyServiceId;
  int? departmentId;
  int? performerId;
  String? bookingDate;
  String? startTime;
  int? beautyPackageComboId;
  int? type;
  int? beautyBookingId;
  int? rate;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['keySearch'] = keySearch;
    map['pageNumber'] = pageNumber;
    map['pageSize'] = pageSize;
    map['beautyServicePackageId'] = beautyServicePackageId;
    map['organizationBranchId'] = organizationBranchId;
    map['beautyServiceId'] = beautyServiceId;
    map['departmentId'] = departmentId;
    map['performerId'] = performerId;
    map['bookingDate'] = bookingDate;
    map['startTime'] = startTime;
    map['beautyPackageComboId'] = beautyPackageComboId;
    map['type'] = type;
    map['rate'] = rate ?? 5;
    map['beautyBookingId'] = beautyBookingId;
    return map;
  }
}
