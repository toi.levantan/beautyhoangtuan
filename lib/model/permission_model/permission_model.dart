class PermissionModel{
  String? id;
  String? title;
  String? image;
  String? subTitle;
  String? color;

  PermissionModel({this.id, this.title, this.image, this.subTitle, this.color});
}