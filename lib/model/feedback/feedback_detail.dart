class FeedbackDetail {
  FeedbackDetail({
      this.id, 
      this.beautyBooking, 
      this.content, 
      this.rate, 
      this.type, 
      this.reply, 
      this.status, 
      this.departmentId, 
      this.organizationId, 
      this.organizationBranchId, 
      this.performerId, 
      this.peopleInvolved, 
      this.result, 
      this.customerId, 
      this.images, 
      this.videos, 
      this.records, 
      this.files, 
      this.createdDate, 
      this.modifiedDate, 
      this.departmentName, 
      this.createdDateStr, 
      this.organizationBranchName, 
      this.performerName,});

  FeedbackDetail.fromJson(dynamic json) {
    id = json['id'];
    beautyBooking = json['beautyBooking'];
    content = json['content'];
    rate = json['rate'];
    type = json['type'];
    reply = json['reply'];
    status = json['status'];
    departmentId = json['departmentId'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    performerId = json['performerId'];
    peopleInvolved = json['peopleInvolved'];
    result = json['result'];
    customerId = json['customerId'];
    if (json['images'] != null) {
      images = [];
      json['images'].forEach((v) {
        images?.add(Images.fromJson(v));
      });
    }
    if (json['videos'] != null) {
      videos = [];
      json['videos'].forEach((v) {
        videos?.add(Videos.fromJson(v));
      });
    }
    if (json['records'] != null) {
      records = [];
      json['records'].forEach((v) {
        records?.add(Records.fromJson(v));
      });
    }
    if (json['files'] != null) {
      files = [];
      json['files'].forEach((v) {
        files?.add(Files.fromJson(v));
      });
    }
    createdDate = json['createdDate'];
    modifiedDate = json['modifiedDate'];
    departmentName = json['departmentName'];
    createdDateStr = json['createdDateStr'];
    organizationBranchName = json['organizationBranchName'];
    performerName = json['performerName'];
  }
  num? id;
  dynamic beautyBooking;
  String? content;
  num? rate;
  num? type;
  dynamic reply;
  num? status;
  num? departmentId;
  num? organizationId;
  num? organizationBranchId;
  num? performerId;
  String? peopleInvolved;
  dynamic result;
  num? customerId;
  List<Images>? images;
  List<Videos>? videos;
  List<Records>? records;
  List<Files>? files;
  String? createdDate;
  String? modifiedDate;
  String? departmentName;
  dynamic createdDateStr;
  String? organizationBranchName;
  String? performerName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['beautyBooking'] = beautyBooking;
    map['content'] = content;
    map['rate'] = rate;
    map['type'] = type;
    map['reply'] = reply;
    map['status'] = status;
    map['departmentId'] = departmentId;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['performerId'] = performerId;
    map['peopleInvolved'] = peopleInvolved;
    map['result'] = result;
    map['customerId'] = customerId;
    if (images != null) {
      map['images'] = images?.map((v) => v.toJson()).toList();
    }
    if (videos != null) {
      map['videos'] = videos?.map((v) => v.toJson()).toList();
    }
    if (records != null) {
      map['records'] = records?.map((v) => v.toJson()).toList();
    }
    if (files != null) {
      map['files'] = files?.map((v) => v.toJson()).toList();
    }
    map['createdDate'] = createdDate;
    map['modifiedDate'] = modifiedDate;
    map['departmentName'] = departmentName;
    map['createdDateStr'] = createdDateStr;
    map['organizationBranchName'] = organizationBranchName;
    map['performerName'] = performerName;
    return map;
  }

}

class Records {
  Records({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.fileId, 
      this.fileName, 
      this.size, 
      this.url, 
      this.aliasName, 
      this.organizationId, 
      this.organizationBranchId, 
      this.fileType, 
      this.objectType, 
      this.status, 
      this.etag,});

  Records.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    fileId = json['fileId'];
    fileName = json['fileName'];
    size = json['size'];
    url = json['url'];
    aliasName = json['aliasName'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    fileType = json['fileType'];
    objectType = json['objectType'];
    status = json['status'];
    etag = json['etag'];
  }
  num? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  String? fileId;
  String? fileName;
  num? size;
  String? url;
  dynamic aliasName;
  num? organizationId;
  dynamic organizationBranchId;
  num? fileType;
  num? objectType;
  dynamic status;
  String? etag;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['fileId'] = fileId;
    map['fileName'] = fileName;
    map['size'] = size;
    map['url'] = url;
    map['aliasName'] = aliasName;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['fileType'] = fileType;
    map['objectType'] = objectType;
    map['status'] = status;
    map['etag'] = etag;
    return map;
  }

}

class Videos {
  Videos({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.fileId, 
      this.fileName, 
      this.size, 
      this.url, 
      this.aliasName, 
      this.organizationId, 
      this.organizationBranchId, 
      this.fileType, 
      this.objectType, 
      this.status, 
      this.etag,});

  Videos.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    fileId = json['fileId'];
    fileName = json['fileName'];
    size = json['size'];
    url = json['url'];
    aliasName = json['aliasName'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    fileType = json['fileType'];
    objectType = json['objectType'];
    status = json['status'];
    etag = json['etag'];
  }
  num? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  String? fileId;
  String? fileName;
  num? size;
  String? url;
  dynamic aliasName;
  num? organizationId;
  dynamic organizationBranchId;
  num? fileType;
  num? objectType;
  dynamic status;
  String? etag;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['fileId'] = fileId;
    map['fileName'] = fileName;
    map['size'] = size;
    map['url'] = url;
    map['aliasName'] = aliasName;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['fileType'] = fileType;
    map['objectType'] = objectType;
    map['status'] = status;
    map['etag'] = etag;
    return map;
  }

}

class Images {
  Images({
      this.id, 
      this.createdDate, 
      this.createdBy, 
      this.modifiedDate, 
      this.modifiedBy, 
      this.fileId, 
      this.fileName, 
      this.size, 
      this.url, 
      this.aliasName, 
      this.organizationId, 
      this.organizationBranchId, 
      this.fileType, 
      this.objectType, 
      this.status, 
      this.etag,});

  Images.fromJson(dynamic json) {
    id = json['id'];
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    modifiedDate = json['modifiedDate'];
    modifiedBy = json['modifiedBy'];
    fileId = json['fileId'];
    fileName = json['fileName'];
    size = json['size'];
    url = json['url'];
    aliasName = json['aliasName'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    fileType = json['fileType'];
    objectType = json['objectType'];
    status = json['status'];
    etag = json['etag'];
  }
  num? id;
  String? createdDate;
  String? createdBy;
  String? modifiedDate;
  String? modifiedBy;
  String? fileId;
  String? fileName;
  num? size;
  String? url;
  dynamic aliasName;
  num? organizationId;
  dynamic organizationBranchId;
  num? fileType;
  num? objectType;
  dynamic status;
  String? etag;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['createdDate'] = createdDate;
    map['createdBy'] = createdBy;
    map['modifiedDate'] = modifiedDate;
    map['modifiedBy'] = modifiedBy;
    map['fileId'] = fileId;
    map['fileName'] = fileName;
    map['size'] = size;
    map['url'] = url;
    map['aliasName'] = aliasName;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['fileType'] = fileType;
    map['objectType'] = objectType;
    map['status'] = status;
    map['etag'] = etag;
    return map;
  }

}
class Files {
  Files({
    this.fileId,
    this.fileName,
    this.size,
    this.url,
    this.aliasName,
    this.organizationId,
    this.organizationBranchId,
    this.fileType,
    this.objectType,
    this.status,
    this.etag,});

  Files.fromJson(dynamic json) {
    fileId = json['fileId'];
    fileName = json['fileName'];
    size = json['size'];
    url = json['url'];
    aliasName = json['aliasName'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    fileType = json['fileType'];
    objectType = json['objectType'];
    status = json['status'];
    etag = json['etag'];
  }
  String? fileId;
  String? fileName;
  int? size;
  String? url;
  dynamic aliasName;
  int? organizationId;
  dynamic organizationBranchId;
  int? fileType;
  int? objectType;
  dynamic status;
  String? etag;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fileId'] = fileId;
    map['fileName'] = fileName;
    map['size'] = size;
    map['url'] = url;
    map['aliasName'] = aliasName;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['fileType'] = fileType;
    map['objectType'] = objectType;
    map['status'] = status;
    map['etag'] = etag;
    return map;
  }

}