import '../booking/performer.dart';

class UserData {
  UserData({
    this.id,
    this.firstName,
    this.lastName,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.organizationId,
    this.gender,
    this.city,
    this.district,
    this.street,
    this.houseNumber,
    this.fullAddress,
    this.salary,
    this.organizationBranchId,
    this.employeeId,
    this.employeeCode,
    this.position,
    this.type,
    this.faceId,
    this.positionShow,
    this.privileges,
    this.positions,
    this.departmentDTO,
    this.departmentType,
    this.imageStore,
    this.avatar,
    this.username,
    this.enabled,
    this.activeStatus,
    this.cardId,
    this.startWork,
    this.employeeWorkShiftByDays,
    this.onLeaveDTOS,
    this.password,
    this.birthday,
    this.userType,
  });

  UserData.fromJson(dynamic json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    fullName = json['fullName'];
    email = json['email'];
    phoneNumber = json['phoneNumber'];
    organizationId = json['organizationId'];
    gender = json['gender'];
    city = json['city'];
    district = json['district'];
    street = json['street'];
    houseNumber = json['houseNumber'];
    fullAddress = json['fullAddress'];
    salary = json['salary'];
    birthday = json['birthday'];
    organizationBranchId = json['organizationBranchId'];
    employeeId = json['employeeId'];
    employeeCode = json['employeeCode'];
    position = json['position'];
    type = json['type'];
    faceId = json['faceId'];
    privileges = json['privileges'];
    departmentDTO = json['departmentDTO'] != null ? DepartmentDto.fromJson(json['departmentDTO']) : null;
    departmentType = json['departmentType'];
    if (json['imageStore'] != null) {
      imageStore = [];
      json['imageStore'].forEach((v) {
        imageStore?.add(ImageStore.fromJson(v));
      });
    }
    avatar = json['avatar'];
    username = json['username'];
    enabled = json['enabled'];
    activeStatus = json['activeStatus'];
    cardId = json['cardId'];
    startWork = json['startWork'];
    employeeWorkShiftByDays = json['employeeWorkShiftByDays'];
    onLeaveDTOS = json['onLeaveDTOS'];
    password = json['password'];
    userType = json['userType'];
  }

  dynamic id;
  dynamic birthday;
  String? firstName;
  String? lastName;
  String? fullName;
  String? email;
  String? phoneNumber;
  num? organizationId;
  num? gender;
  String? city;
  String? district;
  String? street;
  String? houseNumber;
  String? fullAddress;
  num? salary;
  num? organizationBranchId;
  num? employeeId;
  String? employeeCode;
  String? position;
  String? positionShow;
  dynamic type;
  dynamic faceId;
  dynamic privileges;
  List<dynamic>? positions;
  DepartmentDto? departmentDTO;
  dynamic departmentType;
  List<ImageStore>? imageStore;
  dynamic avatar;
  String? username;
  bool? enabled;
  num? activeStatus;
  dynamic cardId;
  dynamic startWork;
  dynamic employeeWorkShiftByDays;
  dynamic onLeaveDTOS;
  dynamic password;
  num? userType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['firstName'] = firstName;
    map['lastName'] = lastName;
    map['fullName'] = fullName;
    map['email'] = email;
    map['phoneNumber'] = phoneNumber;
    map['organizationId'] = organizationId;
    map['gender'] = gender;
    map['city'] = city;
    map['district'] = district;
    map['street'] = street;
    map['houseNumber'] = houseNumber;
    map['fullAddress'] = fullAddress;
    map['salary'] = salary;
    map['organizationBranchId'] = organizationBranchId;
    map['employeeId'] = employeeId;
    map['employeeCode'] = employeeCode;
    map['position'] = position;
    map['type'] = type;
    map['faceId'] = faceId;
    map['privileges'] = privileges;
    if (positions != null) {
      map['positions'] = positions?.map((v) => v.toJson()).toList();
    }
    map['departmentDTO'] = departmentDTO;
    map['departmentType'] = departmentType;
    if (imageStore != null) {
      map['imageStore'] = imageStore?.map((v) => v.toJson()).toList();
    }
    map['avatar'] = avatar;
    map['username'] = username;
    map['enabled'] = enabled;
    map['activeStatus'] = activeStatus;
    map['cardId'] = cardId;
    map['startWork'] = startWork;
    map['employeeWorkShiftByDays'] = employeeWorkShiftByDays;
    map['onLeaveDTOS'] = onLeaveDTOS;
    map['password'] = password;
    map['userType'] = userType;
    return map;
  }

}
/// id : 7201
/// fileId : "3,0320990be685"
/// fileName : "image_cropper_30DD8C69-51A7-4901-A507-ECDAA2F6F43C-10805-000008CACA2985C9.jpg"
/// size : 2551554
/// url : "https://cdnimage.inviv.vn/3,0320990be685"
/// determinedType : "NOT_DETERMINED"
/// etag : "\"9a30abf5\""

class ImageStore {
  ImageStore({
    this.id,
    this.fileId,
    this.fileName,
    this.size,
    this.url,
    this.determinedType,
    this.etag,
  });

  ImageStore.fromJson(dynamic json) {
    id = json['id'];
    fileId = json['fileId'];
    fileName = json['fileName'];
    size = json['size'];
    url = json['url'];
    determinedType = json['determinedType'];
    etag = json['etag'];
  }

  num? id;
  String? fileId;
  String? fileName;
  num? size;
  String? url;
  String? determinedType;
  String? etag;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['fileId'] = fileId;
    map['fileName'] = fileName;
    map['size'] = size;
    map['url'] = url;
    map['determinedType'] = determinedType;
    map['etag'] = etag;
    return map;
  }
}
