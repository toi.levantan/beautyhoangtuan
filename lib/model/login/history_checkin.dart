class HistoryCheckin {
  HistoryCheckin({
      this.id, 
      this.organizationId, 
      this.organizationBranchId, 
      this.cameraId, 
      this.type, 
      this.employeeId, 
      this.imageUrl, 
      this.createdAt, 
      this.thirdPartyType, 
      this.actionType, 
      this.deviceType, 
      this.faceType, 
      this.fullName, 
      this.entechCustomerGroupId,});

  HistoryCheckin.fromJson(dynamic json) {
    id = json['id'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    cameraId = json['cameraId'];
    type = json['type'];
    employeeId = json['employeeId'];
    imageUrl = json['imageUrl'];
    createdAt = json['createdAt'];
    thirdPartyType = json['thirdPartyType'];
    actionType = json['actionType'];
    deviceType = json['deviceType'];
    faceType = json['faceType'];
    fullName = json['fullName'];
    entechCustomerGroupId = json['entechCustomerGroupId'];
  }
  String? id;
  int? organizationId;
  int? organizationBranchId;
  int? cameraId;
  dynamic? type;
  int? employeeId;
  String? imageUrl;
  String? createdAt;
  dynamic? thirdPartyType;
  dynamic? actionType;
  String? deviceType;
  String? faceType;
  String? fullName;
  dynamic? entechCustomerGroupId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['cameraId'] = cameraId;
    map['type'] = type;
    map['employeeId'] = employeeId;
    map['imageUrl'] = imageUrl;
    map['createdAt'] = createdAt;
    map['thirdPartyType'] = thirdPartyType;
    map['actionType'] = actionType;
    map['deviceType'] = deviceType;
    map['faceType'] = faceType;
    map['fullName'] = fullName;
    map['entechCustomerGroupId'] = entechCustomerGroupId;
    return map;
  }

}