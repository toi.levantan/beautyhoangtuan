class CheckinInfoDto {
  CheckinInfoDto({
      this.id, 
      this.employeeId, 
      this.employeeCode, 
      this.fullName, 
      this.groupName, 
      this.workShiftName, 
      this.startHour, 
      this.endHour, 
      this.date, 
      this.endDate, 
      this.checkinTime, 
      this.checkoutTime, 
      this.checkInImageUrl, 
      this.checkOutImageUrl, 
      this.bufferTime, 
      this.checkinToTime, 
      this.checkoutToTime, 
      this.actionType, 
      this.checkinTimeDate, 
      this.workStartAtStr, 
      this.workStartAt, 
      this.bufferTimeDate, 
      this.organizationId, 
      this.workShiftId, 
      this.departmentName, 
      this.urlImage, 
      this.organizationBranchName, 
      this.classIds, 
      this.teachingScheduleId, 
      this.studentId, 
      this.fileId, 
      this.workEndAt,
      this.organizationBranchId,});

  CheckinInfoDto.fromJson(dynamic json) {
    id = json['id'];
    employeeId = json['employeeId'];
    employeeCode = json['employeeCode'];
    fullName = json['fullName'];
    groupName = json['groupName'];
    workShiftName = json['workShiftName'];
    startHour = json['startHour'];
    endHour = json['endHour'];
    date = json['date'];
    endDate = json['endDate'];
    checkinTime = json['checkinTime'];
    checkoutTime = json['checkoutTime'];
    checkInImageUrl = json['checkInImageUrl'];
    checkOutImageUrl = json['checkOutImageUrl'];
    bufferTime = json['bufferTime'];
    checkinToTime = json['checkinToTime'];
    checkoutToTime = json['checkoutToTime'];
    actionType = json['actionType'];
    checkinTimeDate = json['checkinTimeDate'];
    workStartAtStr = json['workStartAtStr'];
    workStartAt = json['workStartAt'];
    workEndAt = json['workEndAt'];
    bufferTimeDate = json['bufferTimeDate'];
    organizationId = json['organizationId'];
    workShiftId = json['workShiftId'];
    departmentName = json['departmentName'];
    urlImage = json['urlImage'];
    organizationBranchName = json['organizationBranchName'];
    if (json['classIds'] != null) {
      classIds = [];
      json['classIds'].forEach((v) {
        //classIds?.add(Dynamic.fromJson(v));
      });
    }
    teachingScheduleId = json['teachingScheduleId'];
    studentId = json['studentId'];
    fileId = json['fileId'];
    organizationBranchId = json['organizationBranchId'];
  }
  num? id;
  num? employeeId;
  String? employeeCode;
  String? fullName;
  String? groupName;
  String? workShiftName;
  num? startHour;
  num? endHour;
  String? date;
  String? endDate;
  String? checkinTime;
  String? checkoutTime;
  String? checkInImageUrl;
  dynamic checkOutImageUrl;
  num? bufferTime;
  num? checkinToTime;
  dynamic checkoutToTime;
  num? actionType;
  dynamic checkinTimeDate;
  dynamic workStartAtStr;
  String? workStartAt;
  String? workEndAt;
  dynamic bufferTimeDate;
  dynamic organizationId;
  num? workShiftId;
  dynamic departmentName;
  dynamic urlImage;
  dynamic organizationBranchName;
  List<dynamic>? classIds;
  dynamic teachingScheduleId;
  dynamic studentId;
  dynamic fileId;
  dynamic organizationBranchId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['employeeId'] = employeeId;
    map['employeeCode'] = employeeCode;
    map['fullName'] = fullName;
    map['groupName'] = groupName;
    map['workShiftName'] = workShiftName;
    map['startHour'] = startHour;
    map['endHour'] = endHour;
    map['date'] = date;
    map['endDate'] = endDate;
    map['checkinTime'] = checkinTime;
    map['checkoutTime'] = checkoutTime;
    map['checkInImageUrl'] = checkInImageUrl;
    map['checkOutImageUrl'] = checkOutImageUrl;
    map['bufferTime'] = bufferTime;
    map['checkinToTime'] = checkinToTime;
    map['checkoutToTime'] = checkoutToTime;
    map['actionType'] = actionType;
    map['checkinTimeDate'] = checkinTimeDate;
    map['workStartAtStr'] = workStartAtStr;
    map['workStartAt'] = workStartAt;
    map['bufferTimeDate'] = bufferTimeDate;
    map['organizationId'] = organizationId;
    map['workShiftId'] = workShiftId;
    map['departmentName'] = departmentName;
    map['urlImage'] = urlImage;
    map['organizationBranchName'] = organizationBranchName;
    if (classIds != null) {
      map['classIds'] = classIds?.map((v) => v.toJson()).toList();
    }
    map['teachingScheduleId'] = teachingScheduleId;
    map['studentId'] = studentId;
    map['fileId'] = fileId;
    map['organizationBranchId'] = organizationBranchId;
    return map;
  }

}