class RecentlyChatDto {
  RecentlyChatDto({
      this.data,});

  RecentlyChatDto.fromJson(dynamic json) {
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(DataRecentlyChat.fromJson(v));
      });
    }
  }
  List<DataRecentlyChat>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class DataRecentlyChat {
  DataRecentlyChat({
      this.id, 
      this.title, 
      this.isDirectChat, 
      this.accessKey, 
      this.isAuthenticated, 
      this.created, 
      this.customJson, 
      this.admin, 
      this.people, 
      this.attachments, 
      this.lastMessage,});

  DataRecentlyChat.fromJson(dynamic json) {
    id = json['id'];
    title = json['title'];
    isDirectChat = json['is_direct_chat'];
    accessKey = json['access_key'];
    isAuthenticated = json['is_authenticated'];
    created = json['created'];
    customJson = json['custom_json'];
    admin = json['admin'] != null ? Admin.fromJson(json['admin']) : null;
    if (json['people'] != null) {
      people = [];
      json['people'].forEach((v) {
        people?.add(People.fromJson(v));
      });
    }
    if (json['attachments'] != null) {
      attachments = [];
      json['attachments'].forEach((v) {
        attachments?.add(Attachments.fromJson(v));
      });
    }
    lastMessage = json['last_message'] != null ? LastMessage.fromJson(json['last_message']) : null;
  }
  int? id;
  String? title;
  bool? isDirectChat;
  String? accessKey;
  bool? isAuthenticated;
  String? created;
  String? customJson;
  Admin? admin;
  List<People>? people;
  List<Attachments>? attachments;
  LastMessage? lastMessage;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['title'] = title;
    map['is_direct_chat'] = isDirectChat;
    map['access_key'] = accessKey;
    map['is_authenticated'] = isAuthenticated;
    map['created'] = created;
    map['custom_json'] = customJson;
    if (admin != null) {
      map['admin'] = admin?.toJson();
    }
    if (people != null) {
      map['people'] = people?.map((v) => v.toJson()).toList();
    }
    if (attachments != null) {
      map['attachments'] = attachments?.map((v) => v.toJson()).toList();
    }
    if (lastMessage != null) {
      map['last_message'] = lastMessage?.toJson();
    }
    return map;
  }

}

class LastMessage {
  LastMessage({
      this.id, 
      this.text, 
      this.created, 
      this.attachments, 
      this.senderUsername, 
      this.customJson, 
      this.sender,});

  LastMessage.fromJson(dynamic json) {
    id = json['id'];
    text = json['text'];
    created = json['created'];
    if (json['attachments'] != null) {
      attachments = [];
      json['attachments'].forEach((v) {
       // attachments?.add(Dynamic.fromJson(v));
      });
    }
    senderUsername = json['sender_username'];
    customJson = json['custom_json'];
    sender = json['sender'] != null ? Sender.fromJson(json['sender']) : null;
  }
  int? id;
  String? text;
  String? created;
  List<dynamic>? attachments;
  String? senderUsername;
  String? customJson;
  Sender? sender;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['text'] = text;
    map['created'] = created;
    if (attachments != null) {
      map['attachments'] = attachments?.map((v) => v.toJson()).toList();
    }
    map['sender_username'] = senderUsername;
    map['custom_json'] = customJson;
    if (sender != null) {
      map['sender'] = sender?.toJson();
    }
    return map;
  }

}

class Sender {
  Sender({
      this.id, 
      this.isAuthenticated, 
      this.lastMessage, 
      this.username, 
      this.secret, 
      this.email, 
      this.firstName, 
      this.lastName, 
      this.avatar, 
      this.isOnline, 
      this.created, 
      this.customJson,});

  Sender.fromJson(dynamic json) {
    id = json['id'];
    isAuthenticated = json['is_authenticated'];
    lastMessage = json['last_message'];
    username = json['username'];
    secret = json['secret'];
    email = json['email'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    isOnline = json['is_online'];
    created = json['created'];
    customJson = json['custom_json'];
  }
  dynamic id;
  dynamic isAuthenticated;
  dynamic lastMessage;
  String? username;
  dynamic secret;
  dynamic email;
  String? firstName;
  String? lastName;
  dynamic avatar;
  bool? isOnline;
  dynamic created;
  String? customJson;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['is_authenticated'] = isAuthenticated;
    map['last_message'] = lastMessage;
    map['username'] = username;
    map['secret'] = secret;
    map['email'] = email;
    map['first_name'] = firstName;
    map['last_name'] = lastName;
    map['avatar'] = avatar;
    map['is_online'] = isOnline;
    map['created'] = created;
    map['custom_json'] = customJson;
    return map;
  }

}

class Attachments {
  Attachments({
      this.id, 
      this.file, 
      this.created,});

  Attachments.fromJson(dynamic json) {
    id = json['id'];
    file = json['file'];
    created = json['created'];
  }
  int? id;
  String? file;
  String? created;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['file'] = file;
    map['created'] = created;
    return map;
  }

}

class People {
  People({
      this.lastRead, 
      this.chatUpdated, 
      this.person,});

  People.fromJson(dynamic json) {
    lastRead = json['last_read'];
    chatUpdated = json['chat_updated'];
    person = json['person'] != null ? Person.fromJson(json['person']) : null;
  }
  dynamic lastRead;
  String? chatUpdated;
  Person? person;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['last_read'] = lastRead;
    map['chat_updated'] = chatUpdated;
    if (person != null) {
      map['person'] = person?.toJson();
    }
    return map;
  }

}

class Person {
  Person({
      this.id, 
      this.isAuthenticated, 
      this.lastMessage, 
      this.username, 
      this.secret, 
      this.email, 
      this.firstName, 
      this.lastName, 
      this.avatar, 
      this.isOnline, 
      this.created, 
      this.customJson,});

  Person.fromJson(dynamic json) {
    id = json['id'];
    isAuthenticated = json['is_authenticated'];
    lastMessage = json['last_message'];
    username = json['username'];
    secret = json['secret'];
    email = json['email'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    isOnline = json['is_online'];
    created = json['created'];
    customJson = json['custom_json'];
  }
  dynamic id;
  dynamic isAuthenticated;
  dynamic lastMessage;
  String? username;
  dynamic secret;
  dynamic email;
  String? firstName;
  String? lastName;
  dynamic avatar;
  bool? isOnline;
  dynamic created;
  String? customJson;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['is_authenticated'] = isAuthenticated;
    map['last_message'] = lastMessage;
    map['username'] = username;
    map['secret'] = secret;
    map['email'] = email;
    map['first_name'] = firstName;
    map['last_name'] = lastName;
    map['avatar'] = avatar;
    map['is_online'] = isOnline;
    map['created'] = created;
    map['custom_json'] = customJson;
    return map;
  }

}

class Admin {
  Admin({
      this.id, 
      this.isAuthenticated, 
      this.lastMessage, 
      this.username, 
      this.secret, 
      this.email, 
      this.firstName, 
      this.lastName, 
      this.avatar, 
      this.isOnline, 
      this.created, 
      this.customJson,});

  Admin.fromJson(dynamic json) {
    id = json['id'];
    isAuthenticated = json['is_authenticated'];
    lastMessage = json['last_message'];
    username = json['username'];
    secret = json['secret'];
    email = json['email'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    isOnline = json['is_online'];
    created = json['created'];
    customJson = json['custom_json'];
  }
  dynamic id;
  dynamic isAuthenticated;
  dynamic lastMessage;
  String? username;
  dynamic secret;
  dynamic email;
  String? firstName;
  String? lastName;
  dynamic avatar;
  bool? isOnline;
  dynamic created;
  String? customJson;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['is_authenticated'] = isAuthenticated;
    map['last_message'] = lastMessage;
    map['username'] = username;
    map['secret'] = secret;
    map['email'] = email;
    map['first_name'] = firstName;
    map['last_name'] = lastName;
    map['avatar'] = avatar;
    map['is_online'] = isOnline;
    map['created'] = created;
    map['custom_json'] = customJson;
    return map;
  }

}