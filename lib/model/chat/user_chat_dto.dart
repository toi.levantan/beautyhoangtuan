class UserChatDto {
  UserChatDto({
      this.data,});

  UserChatDto.fromJson(dynamic json) {
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(DataUserChat.fromJson(v));
      });
    }
  }
  List<DataUserChat>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class DataUserChat {
  DataUserChat({
      this.id, 
      this.isAuthenticated, 
      this.lastMessage, 
      this.username, 
      this.secret, 
      this.email, 
      this.firstName, 
      this.lastName, 
      this.avatar, 
      this.isOnline, 
      this.created, 
      this.customJson,});

  DataUserChat.fromJson(dynamic json) {
    id = json['id'];
    isAuthenticated = json['is_authenticated'];
    lastMessage = json['last_message'] != null ? LastMessage.fromJson(json['last_message']) : null;
    username = json['username'];
    secret = json['secret'];
    email = json['email'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    avatar = json['avatar'];
    isOnline = json['is_online'];
    created = json['created'];
    customJson = json['custom_json'] != null ? CustomJson.fromJson(json['custom_json']) : null;
  }
  int? id;
  bool? isAuthenticated;
  LastMessage? lastMessage;
  dynamic username;
  dynamic secret;
  String? email;
  String? firstName;
  String? lastName;
  dynamic avatar;
  bool? isOnline;
  String? created;
  CustomJson? customJson;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['is_authenticated'] = isAuthenticated;
    if (lastMessage != null) {
      map['last_message'] = lastMessage?.toJson();
    }
    map['username'] = username;
    map['secret'] = secret;
    map['email'] = email;
    map['first_name'] = firstName;
    map['last_name'] = lastName;
    map['avatar'] = avatar;
    map['is_online'] = isOnline;
    map['created'] = created;
    if (customJson != null) {
      map['custom_json'] = customJson?.toJson();
    }
    return map;
  }

}

class CustomJson {
  CustomJson({
      this.organizationId, 
      this.organizationBranchId, 
      this.employeeId, 
      this.phoneNumber, 
      this.userType, 
      this.avatarUrl,});

  CustomJson.fromJson(dynamic json) {
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    employeeId = json['employeeId'];
    phoneNumber = json['phoneNumber'];
    userType = json['userType'];
    avatarUrl = json['avatarUrl'];
  }
  int? organizationId;
  int? organizationBranchId;
  int? employeeId;
  String? phoneNumber;
  int? userType;
  String? avatarUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['employeeId'] = employeeId;
    map['phoneNumber'] = phoneNumber;
    map['userType'] = userType;
    map['avatarUrl'] = avatarUrl;
    return map;
  }

}

class LastMessage {
  LastMessage({
      this.id, 
      this.text, 
      this.created, 
      this.attachments, 
      this.senderUsername, 
      this.customJson, 
      this.sender,});

  LastMessage.fromJson(dynamic json) {
    id = json['id'];
    text = json['text'];
    created = json['created'];
    if (json['attachments'] != null) {
      attachments = [];
      json['attachments'].forEach((v) {
        //attachments?.add(Dynamic.fromJson(v));
      });
    }
    senderUsername = json['sender_username'];
    customJson = json['custom_json'];
    sender = json['sender'];
  }
  dynamic id;
  String? text;
  dynamic created;
  List<dynamic>? attachments;
  dynamic senderUsername;
  dynamic customJson;
  dynamic sender;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['text'] = text;
    map['created'] = created;
    if (attachments != null) {
      map['attachments'] = attachments?.map((v) => v.toJson()).toList();
    }
    map['sender_username'] = senderUsername;
    map['custom_json'] = customJson;
    map['sender'] = sender;
    return map;
  }

}