

import 'package:myhoangtuan/model/notify/body_notification.dart';

import '../fcm_body.dart';

class NotifyEmp {
  NotifyEmp({
      this.id, 
      this.organizationId, 
      this.organizationBranchId, 
      this.createdDate, 
      this.seenStatus, 
      this.title, 
      this.body, 
      this.url, 
      this.totalNotSeen,});

  NotifyEmp.fromJson(dynamic json) {
    id = json['id'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    createdDate = json['createdDate'];
    seenStatus = json['seenStatus'];
    title = json['title'];
    body = json['body'];
    url = json['url'];
    totalNotSeen = json['totalNotSeen'];
  }
  int? id;
  int? organizationId;
  int? organizationBranchId;
  String? createdDate;
  int? seenStatus;
  String? title;
  String? body;
  String? url;
  int? totalNotSeen;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['createdDate'] = createdDate;
    map['seenStatus'] = seenStatus;
    map['title'] = title;
    map['body'] = body;
    map['url'] = url;
    map['totalNotSeen'] = totalNotSeen;
    return map;
  }

}
class NotifyEmpWithBody {
  NotifyEmpWithBody({
    this.id,
    this.organizationId,
    this.organizationBranchId,
    this.createdDate,
    this.seenStatus,
    this.title,
    this.body,
    this.url,
    this.createdDateStr,
    this.totalNotSeen,});

  NotifyEmpWithBody.fromJson(dynamic json) {
    id = json['id'];
    organizationId = json['organizationId'];
    organizationBranchId = json['organizationBranchId'];
    createdDate = json['createdDate'];
    seenStatus = json['seenStatus'];
    title = json['title'];
    body = json['body'];
    url = json['url'];
    totalNotSeen = json['totalNotSeen'];
    createdDateStr = json['createdDateStr'];
  }

  int? id;
  int? organizationId;
  int? organizationBranchId;
  String? createdDate;
  int? seenStatus;
  String? title;
  BodyNotify? body;
  String? url;
  String? createdDateStr;
  int? totalNotSeen;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['organizationId'] = organizationId;
    map['organizationBranchId'] = organizationBranchId;
    map['createdDate'] = createdDate;
    map['seenStatus'] = seenStatus;
    map['title'] = title;
    map['body'] = body;
    map['url'] = url;
    map['totalNotSeen'] = totalNotSeen;
    return map;
  }
}
  class BodyNotify {
    BodyNotify({
  this.content,
  this.screenId,
  this.screenUrl,
  });

    BodyNotify.fromJson(dynamic json) {
  content = json['content'];
  screenId = json['screenId'];
  screenUrl = json['screenUrl'];

  }
  String? id;
  String? content;
  int? screenId;
  String? screenUrl;

  }